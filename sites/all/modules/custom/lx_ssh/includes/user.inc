<?php

function lx_ssh_redirect_to_update_mfs() {
    global $user;
    
    if (user_is_logged_in()) {
        drupal_goto('user/' .$user->uid .'/update-mfs');
    }
    
    drupal_set_message('Please, login to your account.');
    drupal_goto('<front>', ['query' => ['destination' => 'user/update-mfs']]);    
}

function lx_ssh_redirect_to_install_mfs() {
    global $user;
    
    if (user_is_logged_in()) {
        drupal_goto('user/' .$user->uid .'/install-mfs');
    }
    
    drupal_set_message('Please, login to your account.');
    drupal_goto('<front>', ['query' => ['destination' => 'user/install-mfs']]);    
}

function lx_ssh_redirect_to_reinstall_mfs() {
    global $user;
    
    if (user_is_logged_in()) {
        drupal_goto('user/' .$user->uid .'/reinstall-mfs');
    }
    
    drupal_set_message('Please, login to your account.');
    drupal_goto('<front>', ['query' => ['destination' => 'user/reinstall-mfs']]);    
}

function lx_ssh_redirect_to_mfs() {
    global $user;
    
    if (user_is_logged_in()) {
        drupal_goto('user/' .$user->uid .'/mfs');
    }
    
    drupal_set_message('Please, login to your account.');
    drupal_goto('<front>', ['query' => ['destination' => 'user/mfs']]);    
}

function _lx_ssh_submit_btn_txt($op) {
    $text = 'Install MFS';
    
    if (($op == 'update-mfs') || ($op == 'reinstall-mfs')) {
        $text = 'Update MFS';
    }
    else if ($op == 'mfs') {
        $text = 'Install / Update MFS';
    }
    
    return $text;
}

function lx_ssh_form($form, &$form_state, $acc, $opname = 'install-mfs') {
    
    if ($opname != 'update-mfs') {
        
         $form['txt1'] = ['#type' => 'markup',
            '#markup' => '<h1>Server Сonnection</h1>'
            ];
        
    }
    
    
    $form['opname'] = ['#type' => 'value',
        '#value' => $opname];
    
    $form['uid'] = ['#type' => 'value',
        '#value' => $acc->uid];
    
    $form['ip'] = ['#type' => 'textfield',
        '#title' => 'Server IP',
        '#required' => true,
        '#description' => 'Example: 213.12.78.17'
        ];
    
    $form['port'] = ['#type' => 'textfield',
        '#title' => 'SSH Port',
        '#required' => true,
        '#default_value' => 22,
        '#description' => 'Leave default if you don\'t know your ssh port'
        ];
    
    $form['root_pass'] = ['#type' => 'password',
        '#title' => 'Root Password',
        '#description' => 'We do not store your password',
        '#required' => true
        ];
    
    if ($opname != 'update-mfs') {
        $form['txt2'] = ['#type' => 'markup',
            '#markup' => '<br /><h1>Filter Checking</h1><p>If you\'ll define parameters below we\'ll check if filter is installed correctly</p>'
            ];
        
        $form['path_to_index'] = ['#type' => 'textfield',
//        '#title' => 'Path to folder where you have uploaded our Magic File (index.php)',
        '#title' => 'Path to domain folder',
        '#description' => 'Example: /var/www/example.com' //'<br />For Binom users it is usually /var/www/binom'
        ];
        
        $form['url_to_index'] = ['#type' => 'textfield',
//            '#title' => 'URL of our Magic File (index.php)',
            '#title' => 'URL of domain',
            '#description' => 'Example: http://example.com'
        ];
    }
            
    
    $form['submit'] = ['#type' => 'submit',
        '#value' => _lx_ssh_submit_btn_txt($opname)
        ];
    
    return $form;
    
}

function lx_ssh_form_validate($form, &$form_state) {
    if (!preg_match('/^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$/', $form_state['values']['ip'])) {
        form_set_error('ip', 'Check your IP');
    }
    else if (!is_numeric($form_state['values']['port'])) {
        form_set_error('port', 'Port must be numeric. Set 22 if you don\'t know your ssh port.');
    }
    
    if (isset($form_state['values']['url_to_index']) && !valid_url($form_state['values']['url_to_index'], true)) {
        form_set_error('url_to_index', 'URL to our index.php must be valid URL.');
    }
}

function lx_ssh_form_submit($form, &$form_state) {
    $function = 'lx_ssh_run_' .str_replace('-','_',$form_state['values']['opname']);
    
    if (function_exists($function)) {
        
        $extra_data = [];
        if ($form_state['values']['opname'] != 'update-mfs') {
            if (isset($form_state['values']['path_to_index']) && isset($form_state['values']['url_to_index'])) {
                $extra_data['path_to_index'] = trim($form_state['values']['path_to_index']);
                $extra_data['url_to_index'] = trim($form_state['values']['url_to_index']);
            }
        }
        
        $result = $function($form_state['values']['uid'], trim($form_state['values']['ip']), trim($form_state['values']['port']), trim($form_state['values']['root_pass']), $extra_data);
    }
    else {
        $__op = ($form_state['values']['opname'] == 'install-mfs') ? 'install' : 'update';
        
        lx_magic_log2('User ' .$form_state['values']['uid'] .' can\'t ' .$__op .' proxy: function not found - ' .$form_state['values']['opname'] .'.', 'User ' .$form_state['values']['uid'] .' can\'t ' .$__op .' proxy: function error - ' .$form_state['values']['opname'] .'.', '', 3, true, '88.99.56.17', $form_state['values']['uid']);
        drupal_set_message('Something went wrong. Write us and we\'ll help you to solve your problem', 'error');
    }
}

function lx_ssh_update_rollback($conn) {
    lx_ssh_exec($conn, 'mv -f /usr/share/mcproxy/sproxy-prev /usr/share/mcproxy/sproxy');
    lx_ssh_exec($conn, 'cd /usr/share/mcproxy && ./start.sh');      
}

// обновляем старый прокси
function lx_ssh_run_update_mfs($uid, $ip, $port, $pass, $extra_data = []) {
    $success = false;
    $errors = [];
    
    $conn = ssh2_connect($ip, $port);
    
    if ($conn) {
        $auth = ssh2_auth_password($conn, 'root', $pass);
        if ($auth) {
            
            $data = lx_ssh_exec($conn, 'mv -f /usr/share/mcproxy/stop.sh /usr/share/mcproxy/stop-prev.sh');
            $data = lx_ssh_exec($conn, 'mv -f /usr/share/mcproxy/start.sh /usr/share/mcproxy/start-prev.sh');
            
            $data = lx_ssh_exec($conn, 'wget --no-check-certificate -O /usr/share/mcproxy/stop.sh https://clients.magicchecker.com/files/proxy/new/stop-new');
            $data = lx_ssh_exec($conn, 'wget --no-check-certificate -O /usr/share/mcproxy/start.sh https://clients.magicchecker.com/files/proxy/new/start-new');
            
            $data = lx_ssh_exec($conn, 'chmod 755 /usr/share/mcproxy/stop.sh');
            $data = lx_ssh_exec($conn, 'chmod 755 /usr/share/mcproxy/start.sh');

    //        $data = lx_ssh_exec($conn, 'du -b /usr/share/mcproxy/sproxy');
            $data = lx_ssh_exec($conn, 'cd /usr/share/mcproxy && ./stop.sh');

            if (empty($data['error'])) {
//                $data = lx_ssh_exec($conn, 'rm /usr/share/mcproxy/sproxy');
                $data = lx_ssh_exec($conn, 'mv -f /usr/share/mcproxy/sproxy /usr/share/mcproxy/sproxy-prev');
                
                if (empty($data['error'])) {
//                    $data = lx_ssh_exec($conn, 'curl -s -o /usr/share/mcproxy/sproxy --insecure https://clients.magicchecker.com/files/proxy/new/sproxy');
                    $data = lx_ssh_exec($conn, 'wget --no-check-certificate -O /usr/share/mcproxy/sproxy https://clients.magicchecker.com/files/proxy/new/sproxy');
                    
                    if (!empty($data['error']) && (stripos($data['error'], "saved") !== false)) {
                        $data['error'] = '';
                    }
                    
                    if (empty($data['error'])) {
                        $data = lx_ssh_exec($conn, 'chmod 755 /usr/share/mcproxy/sproxy');
                        
                        if (empty($data['error'])) {
                            $data = lx_ssh_exec($conn, 'cd /usr/share/mcproxy && ./start.sh');
                            
                            if (empty($data['error'])) {
                                $success = true;
                            }
                            else {
                                $errors[] = './start.sh ==> ' .$data['error'];
                                lx_ssh_update_rollback($conn);
                            }
                        }
                        else {
                            $errors[] = 'chmod ==> ' .$data['error'];
                            lx_ssh_update_rollback($conn);
                        }
                    }    
                    else {
                        $errors[] = 'wget ==> ' .$data['error'];
                        lx_ssh_update_rollback($conn);                       
                    }
                }
                else {
                    $errors[] = 'mv ==> ' .$data['error'];
                }
            }
            else {
                $errors[] = './stop.sh ==> ' .$data['error'];
            }

            lx_ssh_exec($conn, 'exit');
            
            $inst_data = '';
            if (!$success) {
                $inst_data = implode("\n", $errors);
                drupal_set_message('Something went wrong.<br />Contact us, please.', 'error');
            }
            else {
                drupal_set_message('Filter is updated successfully!');
                lx_ssh_exec($conn, 'rm /usr/share/mcproxy/sproxy-prev');
            }

            db_insert('proxy_iu')
                ->fields([ 'uid' => $uid, 
                             'ip' => $ip, 
                             'status' => 'update', 
                             'result' => (int)$success, 
                             'autoinstall' => 0,
                             'data' => $inst_data]
                        )->execute();            
            
            lx_magic_log2('User ' .$uid .' updated proxy: ' .$ip.':'.$port .' - ' .($success ? 'successfully' : 'not updated!!!'), 'User ' .$uid .' updated proxy: ' .$ip.':'.$port .' - ' .($success ? 'successfully' : 'not updated!!!'), $inst_data, 3, true, '88.99.56.17', $uid);
            
        }
        else {
            drupal_set_message('Wrong password for user "root".', 'error');
        }
    }
    else {
        drupal_set_message('Can\'t connect to your server. Check IP and port.', 'error');
    }
}


// установка mfs
function lx_ssh_run_install_mfs($uid, $ip, $port, $pass, $extra_data = []) {
    $success = false;
    $errors = [];
    $users_error = [];
    
    $extra_messages = [];
    
    $test_file_name = md5(uniqid()) .'.php';
    
    $conn = ssh2_connect($ip, $port);
    
    if ($conn) {
        $auth = ssh2_auth_password($conn, 'root', $pass);
        if ($auth) {

            $data = lx_ssh_exec($conn, 'cd /root');

            if (empty($data['error'])) {                
                
//                    $data = lx_ssh_exec($conn, 'curl -s -o /usr/share/mcproxy/sproxy --insecure https://clients.magicchecker.com/files/proxy/new/sproxy');
                $data = lx_ssh_exec($conn, 'wget --no-check-certificate -O /root/mcproxy.sh https://clients.magicchecker.com/files/proxy/mcproxy-install');
//                lx_ssh_exec($conn, 'exit');
//                print_r($data); die();

                if (!empty($data['error']) && (stripos($data['error'], "saved") !== false)) {
                    $data['error'] = '';
                }

                if (empty($data['error'])) {
                    $data = lx_ssh_exec($conn, 'chmod 755 /root/mcproxy.sh');

                    if (empty($data['error'])) {
                        $data = lx_ssh_exec($conn, 'cd /root && ./mcproxy.sh --install');
                        $no_crontab_for_root_found = (!empty($data['error']) && (strpos($data['error'], 'no crontab for root') !== false)) ? true : false;

                        if (empty($data['error']) || $no_crontab_for_root_found) {
                            if ((stripos($data['data'], 'successfully installed for all domains.') !== false) || $no_crontab_for_root_found ){
                                $data = lx_ssh_exec($conn, 'cd /root && ./mcproxy.sh --status');
                                
                                if (empty($data['error'])) {
                                    if (stripos($data['data'], 'mcproxy is started') !== false) {
                                        $success = true;
//                                        print_r($extra_data);
                                        
                                        if (!empty($extra_data)) {
                                            $extra_data = lx_ssh_normalize_paths($extra_data);
                                            
//                                            print 'cp /usr/share/mcproxy/test.php ' .$extra_data['path_to_index'] .'/' .$test_file_name;
                                                
                                            $data = lx_ssh_exec($conn, 'cp /usr/share/mcproxy/test.php ' .$extra_data['path_to_index'] .'/' .$test_file_name);
                                            
//                                            print_r($data);

                                            if (empty($data['error'])) {
                                                $data = lx_ssh_exec($conn, 'wget' .((stripos($extra_data['url_to_index'], 'https') !== false) ? ' --no-check-certificate' : '') .' -qO- ' .$extra_data['url_to_index'] .'/' .$test_file_name);

                                                if ($data['data'] == 'ok') {
//                                                    drupal_set_message('Filter is installed correctly.');
                                                    
                                                    $extra_messages[] = ['Filter is installed correctly.', 'status'];
                                                }
                                                else {
//                                                    drupal_set_message('Problem with filter testing. Use MFS guide to check it yourself.', 'warning');
                                                    
                                                    $extra_messages[] = ['Problem with filter testing. Use MFS guide to check that filter is working correctly yourself.', 'warning'];
                                                    
                                                    $errors[] = $extra_data['url_to_index'] .'/' .$test_file_name  .' ==> responce: ' .$data['data'];
                                                }
                                                
                                                lx_ssh_exec($conn, 'rm ' .$extra_data['path_to_index'] .'/' .$test_file_name);

                                            }
                                            else {
//                                                drupal_set_message('Problem with filter testing. Use MFS guide to check it yourself.', 'warning');
                                                
                                                $extra_messages[] = ['Problem with filter testing. Use MFS guide to check that filter is working correctly yourself.', 'warning'];
                                                
                                                
                                                $errors[] = 'copy test.php ==> ' .$data['data'];
                                            }
                                            
                                        }
                                        
//                                        die();
                                        
                                    }
                                    else {
                                        $errors[] = 'status responce ==> ' .$data['data'];
                                    }
                                }
                                else {
                                    $errors[] = './mcproxy.sh --status ==> ' .$data['error'];
                                }
                            }
                            else {
                                $errors[] = 'install responce ==> ' .$data['data'];
                            }
                        }
                        else {
                            $errors[] = './mcproxy.sh --install ==> ' .$data['error'];
                        }
                    }
                    else {
                        $errors[] = 'chmod ==> ' .$data['error'];
                    }
                }    
                else {
                    $errors[] = 'wget ==> ' .$data['error'];                    
                }

            }
            else {
                $errors[] = 'cd /root ==> ' .$data['error'];
            }

            lx_ssh_exec($conn, 'exit');
            
            $inst_data = '';
            if (!$success) {
                $inst_data = implode("\n", $errors);
                drupal_set_message('Something went wrong.<br />Contact us, please.', 'error');
            }
            else {
                drupal_set_message('Filter is installed successfully!');
                
                if (sizeof($extra_messages)) {
                    foreach ($extra_messages as $mess) {
                        drupal_set_message($mess[0], $mess[1]);
                    }
                }
            }

            db_insert('proxy_iu')
                ->fields([ 'uid' => $uid, 
                             'ip' => $ip, 
                             'status' => 'install', 
                             'result' => (int)$success, 
                             'autoinstall' => 1,
                             'data' => $inst_data]
                        )->execute();            
            
            lx_magic_log2('User ' .$uid .' installed proxy: ' .$ip.':'.$port .' - ' .($success ? 'successfully' : 'not installed!!!'), 'User ' .$uid .' installed proxy: ' .$ip.':'.$port .' - ' .($success ? 'successfully' : 'not installed!!!'), $inst_data, 3, true, '88.99.56.17', $uid);
            
        }
        else {
            drupal_set_message('Wrong password for user "root".', 'error');
        }
    }
    else {
        drupal_set_message('Can\'t connect to your server. Check IP and port.', 'error');
    }    
}



// рестарт mfs
function lx_ssh_run_reinstall_mfs($uid, $ip, $port, $pass, $extra_data = []) {
    $success = false;
    $errors = [];
    $users_error = [];
    
    $extra_messages = [];
    
    $test_file_name = md5(uniqid()) .'.php';
    
    $conn = ssh2_connect($ip, $port);
    
    if ($conn) {
        $auth = ssh2_auth_password($conn, 'root', $pass);
        if ($auth) {

            $data = lx_ssh_exec($conn, 'cd /root');
            
            $data = lx_ssh_exec($conn, 'cd /root && ./mcproxy.sh --status');
            
            if ((stripos($data['data'], 'No such file or directory') !== false) || (stripos($data['error'], 'No such file or directory') !== false) ) {
                
                $data = lx_ssh_exec($conn, 'wget --no-check-certificate -O /root/mcproxy.sh https://clients.magicchecker.com/files/proxy/mcproxy-install');

                if (!empty($data['error']) && (stripos($data['error'], "saved") !== false)) {
                    $data['error'] = '';
                }

                if (empty($data['error'])) {
                    $data = lx_ssh_exec($conn, 'chmod 755 /root/mcproxy.sh');
                }    
            }



            // удаляем, если нет ошибок... и ставим по новой
            $data = lx_ssh_exec($conn, 'cd /root && ./mcproxy.sh --delete');


            if (empty($data['error'])) {

                if (empty($data['error'])) {
                    $data = lx_ssh_exec($conn, 'cd /root && ./mcproxy.sh --install');

                    if (empty($data['error'])) {
                        if (stripos($data['data'], 'successfully installed for all domains.') !== false) {
                            $data = lx_ssh_exec($conn, 'cd /root && ./mcproxy.sh --status');

                            if (empty($data['error'])) {
                                if (stripos($data['data'], 'mcproxy is started') !== false) {
                                    $success = true;

                                    if (!empty($extra_data)) {
                                        $extra_data = lx_ssh_normalize_paths($extra_data);

                                        $data = lx_ssh_exec($conn, 'cp /usr/share/mcproxy/test.php ' .$extra_data['path_to_index'] .'/' .$test_file_name);


                                        if (empty($data['error'])) {
                                            $data = lx_ssh_exec($conn, 'wget' .((stripos($extra_data['url_to_index'], 'https') !== false) ? ' --no-check-certificate' : '') .' -qO- ' .$extra_data['url_to_index'] .'/' .$test_file_name);

                                            if ($data['data'] == 'ok') {                                                    
                                                $extra_messages[] = ['Filter is installed correctly.', 'status'];
                                            }
                                            else {                                                    
                                                $extra_messages[] = ['Problem with filter testing. Use MFS guide to check that filter is working correctly yourself.', 'warning'];

                                                $errors[] = $extra_data['url_to_index'] .'/' .$test_file_name  .' ==> responce: ' .$data['data'];
                                            }

                                            lx_ssh_exec($conn, 'rm ' .$extra_data['path_to_index'] .'/' .$test_file_name);

                                        }
                                        else {                                                
                                            $extra_messages[] = ['Problem with filter testing. Use MFS guide to check that filter is working correctly yourself.', 'warning'];
                                            $errors[] = 'copy test.php ==> ' .$data['data'];
                                        }

                                    }
                                }
                                else {
                                    $errors[] = 'status responce ==> ' .$data['data'];
                                }
                            }
                            else {
                                $errors[] = './mcproxy.sh --status ==> ' .$data['error'];
                            }
                        }
                        else {
                            $errors[] = 'install responce ==> ' .$data['data'];
                        }
                    }
                    else {
                        $errors[] = './mcproxy.sh --install ==> ' .$data['error'];
                    }
                }
                else {
                    $errors[] = 'chmod ==> ' .$data['error'];
                }
            }    
            else {
                $errors[] = 'wget ==> ' .$data['error'];                    
            }



            lx_ssh_exec($conn, 'exit');
            
            $inst_data = '';
            if (!$success) {
                $inst_data = implode("\n", $errors);
                drupal_set_message('Something went wrong.<br />Contact us, please.', 'error');
            }
            else {
                drupal_set_message('Filter is updated successfully!');
                
                if (sizeof($extra_messages)) {
                    foreach ($extra_messages as $mess) {
                        drupal_set_message($mess[0], $mess[1]);
                    }
                }
            }

            db_insert('proxy_iu')
                ->fields([ 'uid' => $uid, 
                             'ip' => $ip, 
                             'status' => 'updated', 
                             'result' => (int)$success, 
                             'autoinstall' => 1,
                             'data' => $inst_data]
                        )->execute();            
            
            lx_magic_log2('User ' .$uid .' installed proxy: ' .$ip.':'.$port .' - ' .($success ? 'successfully' : 'not installed!!!'), 'User ' .$uid .' updated proxy: ' .$ip.':'.$port .' - ' .($success ? 'successfully' : 'not updated!!!'), $inst_data, 3, true, '88.99.56.17', $uid);
            
        }
        else {
            drupal_set_message('Wrong password for user "root".', 'error');
        }
    }
    else {
        drupal_set_message('Can\'t connect to your server. Check IP and port.', 'error');
    }    
}



// установка или рестарт mfs
function lx_ssh_run_mfs($uid, $ip, $port, $pass, $extra_data = []) {
    $success = false;
    $errors = [];
    $users_error = [];
    
    $is_install = true;
    
    $extra_messages = [];
    
    $test_file_name = md5(uniqid()) .'.php';
    
    $conn = ssh2_connect($ip, $port);
    
    if ($conn) {
        $auth = ssh2_auth_password($conn, 'root', $pass);
        if ($auth) {

            $data = lx_ssh_exec($conn, 'cd /root');
            
            $data = lx_ssh_exec($conn, 'cd /root && ./mcproxy.sh --status');
            
            if ((stripos($data['data'], 'No such file or directory') !== false) || (stripos($data['error'], 'No such file or directory') !== false) ) {
                   
            }
            else {
                $is_install = false;
                
            }
            
//            $data = 
            lx_ssh_exec($conn, 'wget --no-check-certificate -O /root/mcproxy.sh https://clients.magicchecker.com/files/proxy/mcproxy-install');
            
//            if (!empty($data['error']) && (stripos($data['error'], "saved") !== false)) {
//                $data['error'] = '';
//            }            
            
//            if (empty($data['error'])) {
//                $data = 
            lx_ssh_exec($conn, 'chmod 755 /root/mcproxy.sh');
//            }                 
            
            // удаляем, если установлен... и ставим по новой
            lx_ssh_exec($conn, 'cd /root && ./mcproxy.sh --delete');             


            $data = lx_ssh_exec($conn, 'cd /root && ./mcproxy.sh --install');

            if (empty($data['error'])) {
                if (stripos($data['data'], 'successfully installed for all domains.') !== false) {
                    $data = lx_ssh_exec($conn, 'cd /root && ./mcproxy.sh --status');

                    if (empty($data['error'])) {
                        if (stripos($data['data'], 'mcproxy is started') !== false) {
                            $success = true;

                            if (!empty($extra_data)) {
                                $extra_data = lx_ssh_normalize_paths($extra_data);

                                $data = lx_ssh_exec($conn, 'cp /usr/share/mcproxy/test.php ' .$extra_data['path_to_index'] .'/' .$test_file_name);


                                if (empty($data['error'])) {
                                    $data = lx_ssh_exec($conn, 'wget' .((stripos($extra_data['url_to_index'], 'https') !== false) ? ' --no-check-certificate' : '') .' -qO- ' .$extra_data['url_to_index'] .'/' .$test_file_name);

                                    if ($data['data'] == 'ok') {                                                    
                                        $extra_messages[] = ['Filter is working correctly.', 'status'];
                                    }
                                    else {                                                    
                                        $extra_messages[] = ['Problem with filter testing. Use MFS guide to check that filter is working correctly yourself.', 'warning'];

                                        $errors[] = $extra_data['url_to_index'] .'/' .$test_file_name  .' ==> responce: ' .$data['data'];
                                    }

                                    lx_ssh_exec($conn, 'rm ' .$extra_data['path_to_index'] .'/' .$test_file_name);

                                }
                                else {                                                
                                    $extra_messages[] = ['Problem with filter testing. Use MFS guide to check that filter is working correctly yourself.', 'warning'];
                                    $errors[] = 'copy test.php ==> ' .$data['data'];
                                }

                            }
                        }
                        else {
                            $errors[] = 'status responce ==> ' .$data['data'];
                        }
                    }
                    else {
                        $errors[] = './mcproxy.sh --status ==> ' .$data['error'];
                    }
                }
                else {
                    $errors[] = 'install responce ==> ' .$data['data'];
                }
            }
            else {
                $errors[] = './mcproxy.sh --install ==> ' .$data['error'];
            }



            lx_ssh_exec($conn, 'exit');
            
            $inst_data = '';
            if (!$success) {
                $inst_data = implode("\n", $errors);
                drupal_set_message('Something went wrong.<br />Contact us, please.', 'error');
            }
            else {
                drupal_set_message('Filter is ' .($is_install ? 'installed' : 'updated') .' successfully!');
                
                if (sizeof($extra_messages)) {
                    foreach ($extra_messages as $mess) {
                        drupal_set_message($mess[0], $mess[1]);
                    }
                }
            }

            db_insert('proxy_iu')
                ->fields([ 'uid' => $uid, 
                             'ip' => $ip, 
                             'status' => 'updated', 
                             'result' => (int)$success, 
                             'autoinstall' => 1,
                             'data' => $inst_data]
                        )->execute();            
            
            lx_magic_log2('User ' .$uid .' ' .($is_install ? 'installed' : 'updated') .' proxy: ' .$ip.':'.$port .' - ' .($success ? 'successfully' : 'not ' .($is_install ? 'installed' : 'updated') .'!!!'), 'User ' .$uid .' ' .($is_install ? 'installed' : 'updated') .' proxy: ' .$ip.':'.$port .' - ' .($success ? 'successfully' : 'not ' .($is_install ? 'installed' : 'updated') .'!!!'), $inst_data, 3, true, '88.99.56.17', $uid);
            
        }
        else {
            drupal_set_message('Wrong password for user "root".', 'error');
        }
    }
    else {
        drupal_set_message('Can\'t connect to your server. Check IP and port.', 'error');
    }    
}




function lx_ssh_exec($conn, $cmd) { 
        
    $stream = ssh2_exec($conn, $cmd);

    if (!$stream) {
        drupal_set_message('No stream', 'error');
    }

    $errorStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);

    // Enable blocking for both streams
    stream_set_blocking($errorStream, true);
    stream_set_blocking($stream, true);

    // Whichever of the two below commands is listed first will receive its appropriate output.  The second command receives nothing
    $data =  stream_get_contents($stream);
    $error = stream_get_contents($errorStream);

    // Close the streams        
    fclose($errorStream);
    fclose($stream);        

    return ['data' => $data, 'error' => $error];
} 


// приводим данные к нужному виду
// если кто-то указал пути с файлами, то отрезаем их
function lx_ssh_normalize_paths($extra_data) {
    $path_to_index = explode('/', $extra_data['path_to_index']);
    if ( (strpos($path_to_index[sizeof($path_to_index)-1], '.php') !== false) || (strpos($path_to_index[sizeof($path_to_index)-1], '.htm') !== false) ) {
        unset($path_to_index[sizeof($path_to_index)-1]);
    }
    
    $extra_data['path_to_index'] = rtrim(trim(implode('/', $path_to_index)), '/');
    
    $url_to_index = explode('/', $extra_data['url_to_index']);
    if ( (strpos($url_to_index[sizeof($url_to_index)-1], '.php') !== false) || (strpos($url_to_index[sizeof($url_to_index)-1], '.htm') !== false) ) {
        unset($url_to_index[sizeof($url_to_index)-1]);
    }
    
    $extra_data['url_to_index'] = rtrim(trim(implode('/', $url_to_index)), '/');
    
    return $extra_data;
}