<?php

function __get_file_rows_num($file) {
    $rows = 0;
    
    if (file_exists($file)) {
        exec('wc ' .$file, $out);
        $data = explode(' ',$out[0]);
        foreach ($data as $d) {
            if (trim($d)) {
                $rows = $d;
                break;
            }
        }  
    }
    
    return $rows;
}

function lx_ipchecker_control_page() {
    
    if (isset($_GET['reset'])) {
        variable_set('lx_ipchecker_state', 0);
        
        drupal_set_message('Process is reset');
        drupal_goto('admin/content/ip-checker');
    }
    
    $cur_state = variable_get('lx_ipchecker_state', 0);
    
    if (isset($_GET['state'])) {
        $cur_state = $_GET['state'];
        variable_set('lx_ipchecker_state', $_GET['state']);
    }
    
    
    $output = '<div class="cur-options" style="background: #eee; padding: 10px">'
                . ' <p>Cur version: ' .variable_get('lx_ipchecker_splitter_cur_version', 'v6') .'<br />'
                . '    Cur threads: ' .variable_get('lx_ipchecker_splitter_threads', 20) .'</p>'
                . '</div>';
    
    if ($cur_state == 0) {
        $form = drupal_get_form('lx_ipchecker_options');
        $output .= drupal_render($form);
    }
    
    if ($cur_state == 0) {
        
        $output .= '<p>Nothing happens... You can do the next:</p><p><ul><li><a href="/admin/content/ip-checker?state=1">Split file</a></li><li><a href="/admin/content/ip-checker?state=5">Split (ones) file</a></li></ul></p>';
    }
    else if ($cur_state == 1) {
        $output .= '<p>Splitting file. Wait...</p>';

        $threads = variable_get('lx_ipchecker_splitter_threads', 20);
        $version = variable_get('lx_ipchecker_splitter_cur_version', 'v6');

        $rows = __get_file_rows_num(dirname(__FILE__) .'/csvfrom/' .$version .'.csv');

        $rows_in_file = ceil($rows/$threads);
        
        exec('split -d -l ' .$rows_in_file .' -a 3 ' .dirname(__FILE__) .'/csvfrom/' .$version .'.csv');

        $src_folder = $_SERVER['DOCUMENT_ROOT'];
        $dst_folder = $_SERVER['DOCUMENT_ROOT'] .'/' .drupal_get_path('module', 'lx_ipchecker') .'/threads';

        for ($i = 0; $i < $threads; $i ++) {
            $src_file = $i < 10 ? '0' .$i : $i;
            copy($src_folder .'/x0' .$src_file, $dst_folder .'/files_' .$version .'/file_' .$src_file .'.csv');
            unlink($src_folder .'/x0' .$src_file);
        }

        drupal_set_message('Total rows in source file: ' .$rows);
        drupal_set_message('Splitted to ' .$threads .' files with ' .$rows_in_file .' rows in each (except last).');
        
        variable_set('lx_ipchecker_state', 2);
        drupal_goto('admin/content/ip-checker');
    }    
    else if ($cur_state == 2) {
        $output .= '<p>File defined in options is splitted...<br />You can do the next:</p>'
                . '<p><ul>'
                . '     <li><a href="/admin/content/ip-checker?state=3">Start ip checking</a></li>'
                . '     <li><a href="/admin/content/ip-checker?reset=1">Reset process</a></li>'
                . '</ul></p>';
    }
    else if ($cur_state == 3) {
        $output .= '<p>Ip check starting. Wait...</p>';        
        
        $threads = variable_get('lx_ipchecker_splitter_threads', 20);
        $version = variable_get('lx_ipchecker_splitter_cur_version', 'v6');
        
        for ($i = 0; $i < $threads; $i ++) {
            $thread = $i < 10 ? '0' .$i : $i;
            exec('/usr/bin/php ' .dirname(__FILE__) .'/threads/ipchecker_' .$version .'.php ' .dirname(__FILE__) .'/threads/files_' .$version .'/file_' .$thread .'.csv ' .$thread .'  > /dev/null &');
        }
        
        variable_set('lx_ipchecker_state', 4);
        
        drupal_set_message('Ip checkers are started...');
        drupal_goto('admin/content/ip-checker');
    }
    else if ($cur_state == 4) {
        $threads = variable_get('lx_ipchecker_splitter_threads', 20);
        $version = variable_get('lx_ipchecker_splitter_cur_version', 'v6');        
        
        $output .= '<p>Ip checking in process...</p>'; 
        
        $output .= '<div class="cur-options" style="background: #efefef; padding: 10px">';
        
        $_total = 0;
        $_parsed_total = 0;
        
        for ($i = 0; $i < $threads; $i ++) {
            $thread_id = $i < 10 ? '0' .$i : $i;
            $total_num = __get_file_rows_num(dirname(__FILE__) .'/threads/files_' .$version .'/file_' .$thread_id .'.csv');
            
            $_total += $total_num;
            
            $file_parsed = dirname(__FILE__) .'/threads/files_' .$version .'/num_' .$thread_id .'.txt';
            $parsed = file_exists($file_parsed) ? file_get_contents($file_parsed) : 0;
            
            $_parsed_total += $parsed;
            
            $output .= '<p>Thread ' .$thread_id .': ' .$total_num .' / ' .$parsed .' (' .ceil($parsed/$total_num*100) .'%)</p>';  
        } 
        
        $output .= '<p><b>Total: ' .$_total .' / ' .$_parsed_total .'</b></p>';
        
        $output .= '</div>';
        
        $output .= '<p>You can do the next:</p>'
                . '<p><ul>'
                . '     <li><a href="/admin/content/ip-checker?state=5">Combine diapasons (under construction)</a></li>'
                . '     <li><a href="/admin/content/ip-checker?reset=1">Reset process</a></li>'
                . '</ul></p>';
    }
    else if ($cur_state == 5) {
        $output .= '<p>Splitting ones file. Wait...</p>';

        $threads = variable_get('lx_ipchecker_splitter_threads', 20);
        $version = variable_get('lx_ipchecker_splitter_cur_version', 'v6');

        $rows = __get_file_rows_num(dirname(__FILE__) .'/csvfrom-ones/' .$version .'.csv');

        $rows_in_file = ceil($rows/$threads);
        
        exec('split -d -l ' .$rows_in_file .' -a 3 ' .dirname(__FILE__) .'/csvfrom-ones/' .$version .'.csv');

        $src_folder = $_SERVER['DOCUMENT_ROOT'];
        $dst_folder = $_SERVER['DOCUMENT_ROOT'] .'/' .drupal_get_path('module', 'lx_ipchecker') .'/threads';

        for ($i = 0; $i < $threads; $i ++) {
            $src_file = $i < 10 ? '0' .$i : $i;
            copy($src_folder .'/x0' .$src_file, $dst_folder .'/files_' .$version .'/file_' .$src_file .'.csv');
            unlink($src_folder .'/x0' .$src_file);
        }

        drupal_set_message('Total rows in source file: ' .$rows);
        drupal_set_message('Splitted to ' .$threads .' files with ' .$rows_in_file .' rows in each (except last).');
        
        variable_set('lx_ipchecker_state', 6);
        drupal_goto('admin/content/ip-checker');
    }        
    else if ($cur_state == 6) {
        $output .= '<p>File defined in options is splitted...<br />You can do the next:</p>'
                . '<p><ul>'
                . '     <li><a href="/admin/content/ip-checker?state=7">Start ip checking (ones)</a></li>'
                . '     <li><a href="/admin/content/ip-checker?reset=1">Reset process</a></li>'
                . '</ul></p>';
    }    
    else if ($cur_state == 7) {
        $output .= '<p>Ip check starting. Wait...</p>';        
        
        $threads = variable_get('lx_ipchecker_splitter_threads', 20);
        $version = variable_get('lx_ipchecker_splitter_cur_version', 'v6');
        
        for ($i = 0; $i < $threads; $i ++) {
            $thread = $i < 10 ? '0' .$i : $i;
            exec('/usr/bin/php ' .dirname(__FILE__) .'/threads/ipchecker_ones_' .$version .'_periods.php ' .dirname(__FILE__) .'/threads/files_' .$version .'/file_' .$thread .'.csv ' .$thread .'  > /dev/null &');
        }
        
        variable_set('lx_ipchecker_state', 8);
        
        drupal_set_message('Ip checkers are started...');
        drupal_goto('admin/content/ip-checker');
    }    
    else if ($cur_state == 8) {
        $threads = variable_get('lx_ipchecker_splitter_threads', 20);
        $version = variable_get('lx_ipchecker_splitter_cur_version', 'v6');        
        
        $output .= '<p>Ip checking in process...</p>'; 
        
        $output .= '<div class="cur-options" style="background: #efefef; padding: 10px">';
        
        $_total = 0;
        $_parsed_total = 0;
        
        for ($i = 0; $i < $threads; $i ++) {
            $thread_id = $i < 10 ? '0' .$i : $i;
            $total_num = __get_file_rows_num(dirname(__FILE__) .'/threads/files_' .$version .'/file_' .$thread_id .'.csv');
            
            $_total += $total_num;
            
            $file_parsed = dirname(__FILE__) .'/threads/files_' .$version .'/num_' .$thread_id .'.txt';
            $parsed = file_exists($file_parsed) ? file_get_contents($file_parsed) : 0;
            
            $_parsed_total += $parsed;
            
            $output .= '<p>Thread ' .$thread_id .': ' .$total_num .' / ' .$parsed .' (' .ceil($parsed/$total_num*100) .'%)</p>';  
        } 
        
        $output .= '<p><b>Total: ' .$_total .' / ' .$_parsed_total .'</b></p>';
        
        $output .= '</div>';
        
        $output .= '<p>You can do the next:</p>'
                . '<p><ul>'
                . '     <li><a href="/admin/content/ip-checker?state=5">Combine diapasons (under construction)</a></li>'
                . '     <li><a href="/admin/content/ip-checker?reset=1">Reset process</a></li>'
                . '</ul></p>';
    }    
    
    return $output;
}

function lx_ipchecker_options() {
    
    $form['fs'] = array('#type' => 'fieldset',
        '#title' => 'Change options',
        '#collapsible' => true,
        '#collapsed' => true,
        );
    
    $form['fs']['lx_ipchecker_splitter_threads'] = array('#type' => 'textfield',
        '#title' => 'Threads',
        '#default_value' => variable_get('lx_ipchecker_splitter_threads', 20)
        );
    $form['fs']['lx_ipchecker_splitter_cur_version'] = array('#type' => 'textfield',
        '#title' => 'Threads',
        '#default_value' => variable_get('lx_ipchecker_splitter_cur_version', 'v6')
        );
    
    $form['fs']['submit'] = array('#type' => 'submit',
        '#value' => 'Save'
        );
    
    return $form;
}

function lx_ipchecker_options_submit($form, &$form_state) {
    variable_set('lx_ipchecker_splitter_threads', $form_state['values']['lx_ipchecker_splitter_threads']);
    variable_set('lx_ipchecker_splitter_cur_version', $form_state['values']['lx_ipchecker_splitter_cur_version']);
    
    drupal_set_message('Options are changed');
    drupal_goto('admin/content/ip-checker');
}
