<?php

ini_set('max_execution_time', 864000);


function _save_to_file($name, $row) {
    $f = fopen($name, 'a');
    fputcsv($f, $row);
    fclose($f);
}

function _check_ip($ip, $thread_id, $save_location) {
    $xml = file_get_contents('http://com-main.link/blockscript/detector.php?blockscript=api&api_key=5ranqaoimremdeywnsp4jsg4pvif9jzr4rkrnqxbf8&action=test_ipv4&ip='.$ip .'&agent=bingbot-Googlebot-msnbot-Baiduspider-YandexBot-YandexMobileBot-Yahoo!%20Slurp');
    
    if ($xml) {
        preg_match('/<reason>([^<]+)<\/reason>/', $xml, $m);
        $reason1 = @$m[1];            

        $_row = array($ip);

        $type = '';

        if (strpos($reason1, 'Bot IP') !== false) {
            $type = 'bots_';
        }
        else if (strpos($reason1, 'Hosting') !== false) {
            $type = 'hosts_';
        }
        else if (strpos($reason1, 'Passed') !== false) {
            $type = 'search_';
        }

        if ($type) {
            _save_to_file($save_location .'/found_1/' .$type .$thread_id .'.csv', $_row);
        }             
    }
}

function _get_next_ip($ip) {
    $_ip_cur_arr = explode('.', $ip);
    
    if ($_ip_cur_arr[3] < 255) {
        $_ip_cur_arr[3] ++;
    }
    else {
        $_ip_cur_arr[3] = 0;
        $_ip_cur_arr[2] ++;
    }
    
    return implode('.', $_ip_cur_arr);
}

$filepath = $argv[1];
$thread_id = $argv[2];

$save_location = dirname(__FILE__) .'/files_v4/';

if (($handle = fopen($filepath, "r")) !== FALSE) {
    
    $i = 0;
    
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $ip_from_arr = explode('.', trim($data[3]));
        $ip_to_arr = explode('.', trim($data[4]));
        
        if ($ip_from_arr[1] == $ip_to_arr[1]) {
            $ip_to = trim($data[4]);
            $ip_cur = $data[3];

            while($ip_cur != $ip_to) {
                _check_ip($ip_cur, $thread_id, $save_location);
                $ip_cur = _get_next_ip($ip_cur);
            }    
            
            _check_ip($ip_to, $thread_id, $save_location);

            $i ++;
            $fi = fopen($save_location .'/num_' .$thread_id .'.txt', 'w');
            fputs($fi, $i);
            fclose($fi);
        }    
    }
    fclose($handle);
}
