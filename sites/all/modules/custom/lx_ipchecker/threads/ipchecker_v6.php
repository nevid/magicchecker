<?php
ini_set('max_execution_time', 864000);


function ip2long_v6($ip) {
    $ip_n = inet_pton($ip);
    $bin = '';
    for ($bit = strlen($ip_n) - 1; $bit >= 0; $bit--) {
        $bin = sprintf('%08b', ord($ip_n[$bit])) . $bin;
    }

    if (function_exists('gmp_init')) {
        return gmp_strval(gmp_init($bin, 2), 10);
    } elseif (function_exists('bcadd')) {
        $dec = '0';
        for ($i = 0; $i < strlen($bin); $i++) {
            $dec = bcmul($dec, '2', 0);
            $dec = bcadd($dec, $bin[$i], 0);
        }
        return $dec;
    } else {
        trigger_error('GMP or BCMATH extension not installed!', E_USER_ERROR);
    }
}

function cidrToRange_v6($cidr) {
    $range = array();
  
    // Split in address and prefix length
    list($firstaddrstr, $prefixlen) = explode('/', $cidr);

    // Parse the address into a binary string
    $firstaddrbin = inet_pton($firstaddrstr);

    // Convert the binary string to a string with hexadecimal characters
    # unpack() can be replaced with bin2hex()
    # unpack() is used for symmetry with pack() below
    $firstaddrhex = reset(unpack('H*', $firstaddrbin));

    // Overwriting first address string to make sure notation is optimal
    $firstaddrstr = inet_ntop($firstaddrbin);

    // Calculate the number of 'flexible' bits
    $flexbits = 128 - $prefixlen;

    // Build the hexadecimal string of the last address
    $lastaddrhex = $firstaddrhex;

    // We start at the end of the string (which is always 32 characters long)
    $pos = 31;
    while ($flexbits > 0) {
      // Get the character at this position
      $orig = substr($lastaddrhex, $pos, 1);

      // Convert it to an integer
      $origval = hexdec($orig);

      // OR it with (2^flexbits)-1, with flexbits limited to 4 at a time
      $newval = $origval | (pow(2, min(4, $flexbits)) - 1);

      // Convert it back to a hexadecimal character
      $new = dechex($newval);

      // And put that character back in the string
      $lastaddrhex = substr_replace($lastaddrhex, $new, $pos, 1);

      // We processed one nibble, move to previous position
      $flexbits -= 4;
      $pos -= 1;
    }

    // Convert the hexadecimal string to a binary string
    # Using pack() here
    # Newer PHP version can use hex2bin()
    $lastaddrbin = pack('H*', $lastaddrhex);

    // And create an IPv6 address from the binary string
    $lastaddrstr = inet_ntop($lastaddrbin);

    
    $range = array($firstaddrstr, $lastaddrstr);
    
    return $range;
}

function _save_to_file($name, $row) {
    $f = fopen($name.'.csv', 'a');
    fputcsv($f, $row);
    fclose($f);
}

function _check_ip($ip, $row, $thread_id, $save_location) {
    $found = 0;
    $err1 = '';
    $err2 = '';
    $reason1 = '';
    $reason2 = '';
    $type1 = '';    
    $type2 = '';

    
    $xml = file_get_contents('http://com-main.link/blockscript/detector.php?blockscript=api&api_key=5ranqaoimremdeywnsp4jsg4pvif9jzr4rkrnqxbf8&action=test_ipv6&ip='.$ip[0] .'&agent=bingbot-Googlebot-msnbot-Baiduspider-YandexBot-YandexMobileBot-Yahoo!%20Slurp'); 
    if ($xml) {
        
        preg_match('/<reason>([^<]+)<\/reason>/', $xml, $m);
        $reason1 = @$m[1];            

        if (strpos($reason1, 'Bot IP') !== false) {
            $type1 = 'bots_';
            $found ++;
        }
        else if (strpos($reason1, 'Hosting') !== false) {
            $type1 = 'hosts_';
            $found ++;
        }
        else if (strpos($reason1, 'Passed') !== false) {
            $type1 = 'search_';
            $found ++;
        }             
    }
    else {
        $err1 = "xml not found for " .$ip[0] .'<br />';
    }
    
    //-----
    
    $xml = file_get_contents('http://com-main.link/blockscript/detector.php?blockscript=api&api_key=5ranqaoimremdeywnsp4jsg4pvif9jzr4rkrnqxbf8&action=test_ipv6&ip='.$ip[1] .'&agent=bingbot-Googlebot-msnbot-Baiduspider-YandexBot-YandexMobileBot-Yahoo!%20Slurp'); 
    if ($xml) {
        
        preg_match('/<reason>([^<]+)<\/reason>/', $xml, $m);
        $reason2 = @$m[1];            

        if (strpos($reason2, 'Bot IP') !== false) {
            $type2 = 'bots_';
            $found ++;
        }
        else if (strpos($reason2, 'Hosting') !== false) {
            $type2 = 'hosts_';
            $found ++;
        }
        else if (strpos($reason2, 'Passed') !== false) {
            $type2 = 'search_';
            $found ++;
        }             
    }
    else {
        $err2 = "xml not found for " .$ip[1] .'<br />';
    }   
    
    $_row = array();
    $_row[] = $row[0];
    $_row[] = ip2long_v6($ip[0]);
    $_row[] = ip2long_v6($ip[1]);
    $_row[] = $ip[0];
    $_row[] = $ip[1];
    $_row[] = $row[1];
    $_row[] = $row[2];
    $_row[] = $reason1;
    $_row[] = $reason2;
    $_row[] = $err1;
    $_row[] = $err2;

    _save_to_file($save_location .'/found_' .$found .'/' .$type1 .$type2  .$thread_id .'.csv', $_row);
    
}

$filepath = $argv[1];
$thread_id = $argv[2];

$save_location = dirname(__FILE__) .'/files_v6/';

if (($handle = fopen($filepath, "r")) !== FALSE) {
    
    $i = 0;
    
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if ($i == 0) {
            $i++; 
            continue;
        }
        
        $ips = cidrToRange_v6(trim($data[0]));
        
        _check_ip($ips, $data, $thread_id, $save_location);
        
        $i ++;
        $fi = fopen($save_location .'/num_' .$thread_id .'.txt', 'w');
        fputs($fi, $i);
        fclose($fi);
    }
    fclose($handle);
}