<?php

ini_set('max_execution_time', 864000);

function cidrToRange($cidr) {
  $range = array();
  $cidr = explode('/', $cidr);
  $range[0] = long2ip((ip2long($cidr[0])) & ((-1 << (32 - (int)$cidr[1]))));
  $range[1] = long2ip((ip2long($cidr[0])) + pow(2, (32 - (int)$cidr[1])) - 1);
  return $range;
}

function _save_to_file($name, $row) {
    $f = fopen($name, 'a');
    fputcsv($f, $row);
    fclose($f);
}

function _check_ip($ip, $row, $thread_id, $save_location) {
    $found = 0;
    $err1 = '';
    $err2 = '';
    $reason1 = '';
    $reason2 = '';     
    $type1 = '';    
    $type2 = '';

    
    $xml = file_get_contents('http://com-main.link/blockscript/detector.php?blockscript=api&api_key=5ranqaoimremdeywnsp4jsg4pvif9jzr4rkrnqxbf8&action=test_ipv4&ip='.$ip[0] .'&agent=bingbot-Googlebot-msnbot-Baiduspider-YandexBot-YandexMobileBot-Yahoo!%20Slurp'); 
    if ($xml) {
        
        preg_match('/<reason>([^<]+)<\/reason>/', $xml, $m);
        $reason1 = @$m[1];            

        if (strpos($reason1, 'Bot IP') !== false) {
            $type1 = 'bots_';
            $found ++;
        }
        else if (strpos($reason1, 'Hosting') !== false) {
            $type1 = 'hosts_';
            $found ++;
        }
        else if (strpos($reason1, 'Passed') !== false) {
            $type1 = 'search_';
            $found ++;
        }             
    }
    else {
        $err1 = "xml not found for " .$ip[0] .'<br />';
    }
    
    //-----
    
    $xml = file_get_contents('http://com-main.link/blockscript/detector.php?blockscript=api&api_key=5ranqaoimremdeywnsp4jsg4pvif9jzr4rkrnqxbf8&action=test_ipv4&ip='.$ip[1] .'&agent=bingbot-Googlebot-msnbot-Baiduspider-YandexBot-YandexMobileBot-Yahoo!%20Slurp'); 
    if ($xml) {
        
        preg_match('/<reason>([^<]+)<\/reason>/', $xml, $m);
        $reason2 = @$m[1];            

        if (strpos($reason2, 'Bot IP') !== false) {
            $type2 = 'bots_';
            $found ++;
        }
        else if (strpos($reason2, 'Hosting') !== false) {
            $type2 = 'hosts_';
            $found ++;
        }
        else if (strpos($reason2, 'Passed') !== false) {
            $type2 = 'search_';
            $found ++;
        }             
    }
    else {
        $err2 = "xml not found for " .$ip[1] .'<br />';
    }    
    
    
    $_row = array();
    $_row[] = $row[0];
    $_row[] = ip2long($ip[0]);
    $_row[] = ip2long($ip[1]);
    $_row[] = $ip[0];
    $_row[] = $ip[1];
    $_row[] = $row[1];
    $_row[] = $row[2];
    $_row[] = $reason1;
    $_row[] = $reason2;
    $_row[] = $err1;
    $_row[] = $err2;

    _save_to_file($save_location .'/found_' .$found .'/' .$type1 .$type2 .$thread_id .'.csv', $_row);
    
}

$filepath = $argv[1];
$thread_id = $argv[2];

$save_location = dirname(__FILE__) .'/files_v4/';

if (($handle = fopen($filepath, "r")) !== FALSE) {
    
    $i = 0;
    
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if ($i == 0) {
            $i++; 
            continue;
        }
        
        $ips = cidrToRange(trim($data[0]));
        
        _check_ip($ips, $data, $thread_id, $save_location);
        
        $i ++;
        $fi = fopen($save_location .'/num_' .$thread_id .'.txt', 'w');
        fputs($fi, $i);
        fclose($fi);
    }
    fclose($handle);
}
