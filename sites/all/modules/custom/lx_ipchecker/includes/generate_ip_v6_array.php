<?php

function ip2long_v6($ip) {
    $ip_n = inet_pton($ip);
    $bin = '';
    for ($bit = strlen($ip_n) - 1; $bit >= 0; $bit--) {
        $bin = sprintf('%08b', ord($ip_n[$bit])) . $bin;
    }

    $dec = '0';
    for ($i = 0; $i < strlen($bin); $i++) {
        $dec = bcmul($dec, '2', 0);
        $dec = bcadd($dec, $bin[$i], 0);
    }
    return $dec;
}

function _sort_ips($a, $b) {
    if ($a[0] > $b[0]) {
        return 1;
    }
    else if ($a[0] < $b[0]) {
        return -1;
    }
    else {
        if ($a[1] > $b[1]) {
            return 1;
        }
        else if ($a[1] < $b[1]) {
            return -1;
        }
        else return 0;
    }
}

$file = 'Blockscript_v6.txt';


//$ips = array();
//$handle = fopen($file, "r");
//while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
//    $ips[] = array(ip2long_v6(trim($data[3])), ip2long_v6(trim($data[4])), trim($data[3]), trim($data[4]));
//}
//fclose($handle);




$f = file($file);

$ips = array();

foreach ($f as $row) {
    $data = explode('-', $row);
    $ips[] = array(ip2long_v6(trim($data[0])), ip2long_v6(trim($data[1])));
}


usort($ips, '_sort_ips');

$_result = array();

$parts = 10;

// первый уровень
$items_in_part = ceil(sizeof($ips)/$parts);

$cur = 0;
$key = '';
for ($i = 0; $i < sizeof($ips); $i++) {
    if ($cur == 0) {
        $last_key = isset($ips[$i+$items_in_part][1]) ? $ips[$i+$items_in_part-1][1] : $ips[sizeof($ips)-1][1];
        $key = $ips[$i][0] .'-' .$last_key;
    }
    
    $_result[$key][] = $ips[$i];
    $cur ++;
    
    if ($cur == $items_in_part) $cur = 0;
}

// второй уровень
foreach ($_result as $lev_1_key => $ips) {
    $__result = array();
    
    $items_in_part = ceil(sizeof($ips)/$parts);

    $cur = 0;
    $key = '';
    for ($i = 0; $i < sizeof($ips); $i++) {
        if ($cur == 0) {
            $last_key = isset($ips[$i+$items_in_part][1]) ? $ips[$i+$items_in_part-1][1] : $ips[sizeof($ips)-1][1];
            $key = $ips[$i][0] .'-' .$last_key;
        }

        $__result[$key][] = $ips[$i];
        $cur ++;

        if ($cur == $items_in_part) $cur = 0;
    }   
    
    $_result[$lev_1_key] = $__result;
}

// третий уровень
foreach ($_result as $lev_1_key => $lev_1_ips) {
    foreach ($lev_1_ips as $lev_2_key => $ips) {
        $__result = array();
        $items_in_part = ceil(sizeof($ips)/$parts);

        $cur = 0;
        $key = '';
        for ($i = 0; $i < sizeof($ips); $i++) {
            if ($cur == 0) {
                $last_key = isset($ips[$i+$items_in_part][1]) ? $ips[$i+$items_in_part-1][1] : $ips[sizeof($ips)-1][1];
                $key = $ips[$i][0] .'-' .$last_key;
            }

            $__result[$key][] = $ips[$i];
            $cur ++;

            if ($cur == $items_in_part) $cur = 0;
        }   
        
        $_result[$lev_1_key][$lev_2_key] = $__result;
    }    
}

$output = '<?php' ."\n" .'$block_ips_array = array(' ."\n";



foreach ($_result as $k1 => $v1) {
    $output .= '"' .$k1 .'" => array(' ."\n";

    foreach ($v1 as $k2 => $v2) {
        $output .= "\t" .'"' .$k2 .'" => array(' ."\n";    
    
        foreach ($v2 as $k3 => $v3) {
            $output .= "\t\t" .'"' .$k3 .'" => array(' ."\n"; 
            
            foreach ($v3 as $k4 => $v4) {
                $output .= "\t\t\t" .$k4 .' => array(' .$v4[0] .', ' .$v4[1] .'),' ."\n";
            }   
            
            $output .= '),' ."\n";
        }
        
        $output .= '),' ."\n";
    }
    
    $output .= '),' ."\n";
}


$output .= ');';
$output .= "\n?>";


$f = fopen('ip_v6_array.txt', 'w');
fputs($f, $output);
fclose($f);

print 'File ip_v6_array.txt created. Rename it to ip_v6_array.php and include in php script.';
