<?php

define('INTERCOM_TOKEN', 'dG9rOmZhY2FjMzU2XzZlMzVfNDYyOV9hNGFiX2FmNWVkNWYwNTNmNzoxOjA=');

require "vendor/autoload.php";
use Intercom\IntercomClient;

function _decodeTimezone($string) {
    $digits = ['t' => 0, 'h' => 1, 'k' => 2, 'r' => 3, 'a' => 4, 'o' => 5, 'j' => 6, 'p' => 7, 'e' => 8, 'w' => 9];
//    $digits_cnt = [1 => 'x', 2 => 'f', 3 => 'u', 4 => 'd'];

    $tz = '';
    $sign = ($string[0] == 'f') ? -1 : 1;
    for ($i = 2; $i < strlen($string); $i ++) {
        $tz .= $digits[$string[$i]];
        
        print $string[$i] .' => ' .$tz .'<br />';
    }
    return $sign*((int)$tz);
}


function lx_intercom_test_func() {

    print _decodeTimezone('uxhet');
    die();

//    lx_intercom_init_table();
//    print 'done';
    
    $client = new IntercomClient(INTERCOM_TOKEN, null);
    $opts = [];
            
    try {
        $leads = $client->leads->getLeads($opts);
    }
    catch (Exception $e) {
        print_r($e);
    }
    
    print_r($leads);
    print('ok');
}


function lx_intercom_init_table() {
//    $res = db_query('SELECT uid FROM {intercom_data}')->fetchAll();
//    foreach ($res as $r) {
//        $country = db_select('magic_log', 'm')
//                        ->fields('m', array('country'))
//                        ->condition('uid', $r->uid)
//                        ->orderBy('id', 'DESC')
//                        ->range(0, 1)
//                        ->execute()->fetchField();
//        db_update('intercom_data')
//            ->fields(array('country' => $country))
//            ->condition('uid', $r->uid)
//            ->execute();
//                        
//    }

//    try {
//        $res = db_query('SELECT uid, created, mail FROM {users} WHERE uid IN (SELECT uid FROM {intercom_data})')->fetchAll();
//
//        foreach ($res as $acc) {
//            $client = new IntercomClient(INTERCOM_TOKEN, null);
//            $client->users->create(["email" => $acc->mail, "signed_up_at" => $acc->created, "user_id" => $acc->uid]);
//            $intercom_obj = $client->users->getUsers(["email" => $acc->mail]);
//            $client = null;
//
//            $intercom_id = $intercom_obj->id;    
//            db_update('intercom_data')
//                ->fields(array('intercom_id' => $intercom_id))
//                ->condition('uid', $acc->uid)
//                ->execute();
//        }    
//    }
//    catch (Exception $e) {
//        print_r($e); die();
//    }

//    $res = db_query('SELECT uid, first_payment_date FROM {intercom_data} WHERE first_payment_date IS NOT NULL');
//    foreach ($res as $r) {
//        $t = explode(' ', $r->first_payment_date);
//        $t = explode('-', $t[0]);
//        $t_time = mktime(0,0,0, ltrim($t[1], '0'), ltrim($t[2], '0'), $t[0]);
//        $days = ceil((time() - $t_time)/86400);
//        db_update('intercom_data')
//            ->fields(array('number_of_subscription_days' => $days))
//            ->condition('uid', $r->uid)
//            ->execute();
//    }
}

// обновление статистики по кампаниям для Intercom (+)
function lx_intercom_inc_refund_issued($uid, $invoice_id) {
    $local_stat = db_select('intercom_data', 'd')
                    ->fields('d')
                    ->condition('uid', $uid)
                    ->execute()->fetchObject();
    
    if (!empty($local_stat)) {    
        $nid = db_select('field_data_field_payment_system_invoice_id', 'i')
                        ->fields('i', array('entity_id'))
                        ->condition('field_payment_system_invoice_id_value', $invoice_id)
                        ->execute()->fetchField();
        
        if ($nid) {
            $node = node_load($nid);
            $refund_sum = @(int)$node->field_payment_total['und'][0]['value'];
            
            if ($refund_sum) {
                $fields = array('number_of_refunds' => $local_stat->number_of_refunds + 1,
                                'refunds_value' => $local_stat->refunds_value + $refund_sum);                
                
                lx_intercom_update_user_local($uid, $fields);
            }
        }
    }    
}

// обновление статистики для Intercom: MFL disabled
function lx_intercom_mfl_disabled($uid) {
    $local_stat = db_select('intercom_data', 'd')
                    ->fields('d')
                    ->condition('uid', $uid)
                    ->execute()->fetchObject();
    
    if (!empty($local_stat)) {    
        if (!$local_stat->first_mfl_disabled_date) {
           $fields = array('first_mfl_disabled_date' => date('Y-m-d H:i:s'));
           lx_intercom_update_user_local($uid, $fields);
        }
    }    
}

// обновление статистики по кампаниям для Intercom (+)
function lx_intercom_inc_cs_request($uid, $date) {
    $local_stat = db_select('intercom_data', 'd')
                    ->fields('d')
                    ->condition('uid', $uid)
                    ->execute()->fetchObject();
    
    if (!empty($local_stat)) {    
        $fields = array('number_of_support_reqs' => $local_stat->number_of_support_reqs + 1);
        
        if (!$local_stat->first_cs_request_date) {
           $fields['first_cs_request_date'] = $date;
        }
        $fields['last_cs_request_date'] = $date;        
        lx_intercom_update_user_local($uid, $fields);
    }    
}

// обновление статистики по кампаниям для Intercom (+)
function lx_intercom_inc_user_campaign($uid, $date) {
    $local_stat = db_select('intercom_data', 'd')
                    ->fields('d')
                    ->condition('uid', $uid)
                    ->execute()->fetchObject();
    
    if (!empty($local_stat)) {    
        $fields = array('number_of_campaigns' => $local_stat->number_of_campaigns + 1);
        
        if (!$local_stat->first_campaign_date) {
           $fields['first_campaign_date'] = $date;
        }
        $fields['last_campaign_date'] = $date;        
        lx_intercom_update_user_local($uid, $fields);
    }    
    
}

// обновление статистики по кампаниям для Intercom (-)
function lx_intercom_dec_user_campaign($uid) {
    $local_stat = db_select('intercom_data', 'd')
                    ->fields('d')
                    ->condition('uid', $uid)
                    ->execute()->fetchObject();
    
    if (!empty($local_stat)) {    
        $fields = array('number_of_campaigns' => $local_stat->number_of_campaigns - 1);   
        lx_intercom_update_user_local($uid, $fields);
    }        
}


// обновление статистики по RECCURENT_STOPPED юзеров для Intercom
function lx_intercom_inc_user_recc_stop($uid, $date) {
    $local_stat = db_select('intercom_data', 'd')
                    ->fields('d')
                    ->condition('uid', $uid)
                    ->execute()->fetchObject();
    
    if (!empty($local_stat)) {    
        $fields = array('number_of_subscription_cancel' => $local_stat->number_of_subscription_cancel + 1);
        
        if (!$local_stat->subscription_first_cancel_date) {
           $fields['subscription_first_cancel_date'] = $date;
        }
        $fields['subscription_last_cancel_date'] = $date;    
        
        $fields['plan'] = 'unsubscribed';
        
        lx_intercom_update_user_local($uid, $fields);
    }
}

// обновление статистики по оплатам юзеров для Intercom
function lx_intercom_inc_user_payment($node) {
    $local_stat = db_select('intercom_data', 'd')
                    ->fields('d')
                    ->condition('uid', $node->uid)
                    ->execute()->fetchObject();
    
    if (!empty($local_stat)) {
        $fields = array(
                        //'country' => '',
                        'plan' => '',
                        'number_of_payments' => $local_stat->number_of_payments + 1,
                        'total_sum' => $local_stat->total_sum + $node->field_payment_total['und'][0]['value']
            );
        $fields['average_order_value'] = number_format($fields['total_sum']/$fields['number_of_payments'], 2, '.', '');


//        $payment_data_rows = explode("\n", $node->body['und'][0]['value']);
//        foreach ($payment_data_rows as $row) {
////            if (strpos($row, 'bill_country') !== false) {
////                $country = explode('=>', $row);
////                $fields['country'] = @trim($country[1]);
////            }
////            else 
//                if (!in_array($node->uid, array(251)) && strpos($row, 'item_name_1') !== false) {
//                $tarif = explode('=>', $row);
//                $fields['plan'] = @trim($tarif[1]);
//            }
//        }
        
        try {
            if (isset($node->field_payment_tarif)) {
               $tarif_id = (int)$node->field_payment_tarif['und'][0]['nid'];
               if ($tarif_id) {
                    $plan_name = db_select('node', 'n')
                                     ->fields('n', array('title'))
                                     ->condition('nid', $tarif_id)
                                     ->condition('type', 'tarif')
                                     ->execute()->fetchField();
                   if ($plan_name) {
                       $fields['plan'] = $plan_name;
                   }
               }
           }
        }
        catch (Exception $e) {
            
        }
    
    
       if (!$local_stat->first_payment_date) {
           $fields['first_payment_date'] = date('Y-m-d H:i:s', $node->created);
           $fields['number_of_subscription_days'] = 1;
       }
       
       $fields['last_payment_date'] = date('Y-m-d H:i:s', $node->created);
       
       if ($local_stat->plan != $fields['plan']) {
           // trigger event
       }
       
       lx_intercom_update_user_local($node->uid, $fields);
    }
}

// Обновляет инкрементируемое значение в таблице для экспорта в Интерком
function lx_intercom_inc_field_user_local($uid, $field) {
    db_query('UPDATE {intercom_data} SET ' .$field .'=' .$field .'+1 WHERE uid=:uid', array(':uid' => $uid));
}

function lx_intercom_update_user_local($uid, $fields) {
    
    $country = db_select('magic_log', 'm')
                        ->fields('m', array('country'))
                        ->condition('uid', $uid)
                        ->orderBy('id', 'DESC')
                        ->range(0, 1)
                        ->execute()->fetchField(); 
    if ($country) {
        $fields['country'] = $country;
    }
    
    
    db_update('intercom_data')
        ->fields($fields)
        ->condition('uid', $uid)
        ->execute();
}

// обновляет юзеро в Интерком (по крон)
function lx_intercom_update_users() {
    $client = new IntercomClient(INTERCOM_TOKEN, null);
    
//    variable_set('lx_intercom_last_updated', '2010-01-01 00:00:00');
    
    $last_checked = variable_get('lx_intercom_last_updated', '2017-01-01 00:00:00');
    
    $res = db_select('intercom_data', 'd')
                ->fields('d')
                ->condition('updated', $last_checked, '>')
                ->execute();
    
    $last_checked = date('Y-m-d H:i:s');
    variable_set('lx_intercom_last_updated', $last_checked);
    
    foreach ($res as $r) {
        $r = (array)$r;
        
        $custom_attributes = array();
        foreach ($r as $k => $v) {
//            $data_to_update = array('email' => $r['mail'],
//                                    'custom_attributes' => array());
            $data_to_update = array('user_id' => $r['uid'],
                                    'custom_attributes' => array());

            
            if (in_array($k, ['uid', 'name','intercom_id', 'updated'])) {
                continue;
            }
            else if ($k == 'total_sum') {
                $custom_attributes['LTV'] = $r['total_sum'];
                continue;
            }
            else if (in_array($k, ['first_login_date', 'trial_start_date', 'trial_end_date', 'registration_date', 'first_payment_date', 'last_payment_date', 'subscription_first_cancel_date', 'subscription_last_cancel_date', 'first_login_date', 'last_login_date', 'first_cs_request_date', 'last_cs_request_date', 'first_mfl_disabled_date', 'first_campaign_date', 'last_campaign_date'])) {
                if ($v) {
                    $__dt = explode(' ', $v);
                    $custom_attributes[$k] = $__dt[0];
                }
                continue;
            }
            else if ($k == 'bleed_rate_last_7days') {
                $custom_attributes[$k] = round($r['bleed_rate_last_7days']/$r['traffic_amount_last_7ays']*100);
                continue;
            }
            else if ($k == 'mail') {
                $custom_attributes['email'] = $v;
                continue;
            }
            
            $custom_attributes[$k] = $v; 
        }
        
        $data_to_update['custom_attributes'] = $custom_attributes;
        
        $client->users->update($data_to_update);
    }
    
}    

// создает пользователя в Интерком (по крону)
function _lx_intercom_create_user($reg_data) {
    try {
        
        
        $acc = unserialize($reg_data->account);
        $extra = unserialize($reg_data->data);
        $with_trial_access = $reg_data->trial_access;
        
        
        // --- функции при регистрации
        
        // отправка почты на подтверждение мыла
        lx_domains_cmp_mailconfirm($acc);
        
        $users_iss_fields = array('uid' => $acc->uid);
        db_insert('users_iss')
            ->fields($users_iss_fields)
            ->execute();
        
        lx_magic_log2('New user registered' .$with_trial_access .': UID= ' .$acc->uid .' (' .$acc->mail .')', 'Registration', '', 7, true, '88.99.56.17', $acc->uid);        
        
        // ---- Дальше отправка юзера в Интеркоом
        
        return true; // временно, чтобы не отправить все в интеркм
        
        $client = new IntercomClient(INTERCOM_TOKEN, null);

        $user_created_at = $acc->created;

        // ищем в лидах
        $leads = $client->leads->getLeads([]);
        if (!empty($leads) && isset($leads->contacts) && sizeof($leads->contacts)) {
            foreach ($leads->contacts as $contact) {
                // если нашли в лидах, конвертим в пользователя
                if ($contact->email == $acc->mail) {
                    $client->leads->convertLead([
                        "contact" => [
                          "user_id" => $contact->user_id
                        ],
                        "user" => [
                          "email" => $contact->email
                        ]
                      ]);

                    if (isset($client->created_at) && is_numeric($client->created_at)) {
                        $user_created_at = $client->created_at;
                    }    
                    break;
                }
            }
        }

        $users_plan = isset($extra['referer']) ? 'trial-14' : 'trial-7';
        
        $client->users->create(["email" => $acc->mail, "signed_up_at" => $user_created_at, "user_id" => $acc->uid, 'plan' => $users_plan]);
        $intercom_obj = $client->users->getUsers(["email" => $acc->mail]);
        $client = null;

        $intercom_id = $intercom_obj->id;

        $fields = array('uid' => $acc->uid,
                        'name' => lx_user_name_from_mail($acc->mail),
                        'intercom_id' => $intercom_id,
                        'mail' => $acc->mail,
                        'registration_date' => date('Y-m-d H:i:s', $acc->created),
                        'number_of_logins' => 0, 
                        'first_login_date' => date('Y-m-d H:i:s', $acc->created) 
            );

        if (isset($extra['country'])) {
            $fields['country'] = $extra['country'];
        }
        
        if (isset($extra['referer'])) {
            $fields['referer'] = $extra['referer'];
            $fields['trial_start_date'] = date('Y-m-d H:i:s', $acc->created);
            $fields['trial_end_date'] = date('Y-m-d 00:00:00', $acc->created + 15*86400);
        }
        else {
            $fields['trial_start_date'] = date('Y-m-d H:i:s', $acc->created);
            $fields['trial_end_date'] = date('Y-m-d 00:00:00', $acc->created + 8*86400);
        }
        
        $fields['plan'] = $users_plan;

        db_insert('intercom_data')
            ->fields($fields)->execute();
    }
    catch (Exception $e) {
//        print_r($e); die();
    }
}

function lx_intercom_create_user($acc, $extra = array(), $with_trial_access = '') {
    db_insert('users_registration')
        ->fields([
                        'created' => time(),
                        'mail' => $acc->mail,
                        'trial_access' => $with_trial_access,
                        'account' => serialize($acc),
                        'data' => serialize($extra)
                    ]
                )->execute();
}

// удаляет пользователя из Интерком
function lx_intercom_delete_user($acc) {
    try {
        $intercom_id = db_select('intercom_data', 'd')
                            ->fields('d', array('uid'))
                            ->condition('uid', $acc->uid)
                            ->execute()->fetchField();

        db_delete('intercom_data')
            ->condition('uid', $acc->uid)
            ->execute();

        $client = new IntercomClient(INTERCOM_TOKEN, null);
//        $client->users->permanentlyDeleteUser($intercom_id);
        $client->users->deleteUser($intercom_id);
    }
    catch (Exception $e) {    
        print_r($e); die();
    }
}

// обновляет данные по трафу за последние 7 дней
function lx_intercom_update_traff() {
    $traff = array();
    
    // текущий день
    db_set_active('stata');
    $res = db_select('day_cmp_stat_' .date('Y_m_d'), 'm');
    $res->fields('m', array('uid'));
    $res->addExpression('SUM(total_clicks)', 'total_clicks');
    $res->addExpression('SUM(adplex_clicks+hosting_clicks+country_clicks+bot_clicks+isp_clicks+isp_clicks+referer_clicks+ip_clicks+device_os_clicks+url_clicks+ua_clicks+time_clicks+status_clicks+headers_clicks+light_clicks+super_clicks)', 'blocked');
    $res->groupBy('m.uid');
    $data = $res->execute();
    
    foreach ($data as $r) {
        if (!isset($traff[$r->uid])) {
            $traff[$r->uid] = array('traffic_amount_last_7ays' => 0, 'bleed_rate_last_7days' => 0);
        }
        $traff[$r->uid]['traffic_amount_last_7ays'] += $r->total_clicks;
        $traff[$r->uid]['bleed_rate_last_7days'] += $r->blocked;
        
    }

    db_set_active('default');   
    
//    // предыдущие 6 дней
    $last_date = db_query('SELECT MAX(created) FROM {domain_compaigns_users_stat}')->fetchField();
    
    $day_from = $last_date - 86400*5;
    
    $res = db_select('domain_compaigns_users_stat', 'm');
    $res->fields('m', array('uid'));
    $res->addExpression('SUM(total_clicks)', 'total_clicks');
    $res->addExpression('SUM(adplex_clicks+hosting_clicks+country_clicks+bot_clicks+isp_clicks+isp_clicks+referer_clicks+ip_clicks+device_os_clicks+url_clicks+ua_clicks+time_clicks+status_clicks+headers_clicks+light_clicks+super_clicks)', 'blocked');
    $res->condition('created', $day_from, '>=');
    $res->groupBy('m.uid');
    $data = $res->execute();
    
    foreach ($data as $r) {
        if (!isset($traff[$r->uid])) {
            $traff[$r->uid] = array('traffic_amount_last_7ays' => 0, 'bleed_rate_last_7days' => 0);
        }
        $traff[$r->uid]['traffic_amount_last_7ays'] += $r->total_clicks;
        $traff[$r->uid]['bleed_rate_last_7days'] += $r->blocked;
    }    
    
    db_update('intercom_data')
                ->fields(array('traffic_amount_last_7ays' => 0, 'bleed_rate_last_7days' => 0))
                ->execute();
    
    if (sizeof($traff)) {
        foreach ($traff as $uid => $row) {
            db_update('intercom_data')
                ->fields($row)
                ->condition('uid', $uid)
                ->execute();
        }
    }
    
    // отправляем данные на Intercom
//    lx_intercom_update_users();
}

function lx_intercom_update_subscription_days() {
//    print 'ok!';
    
    db_query('UPDATE {intercom_data} SET number_of_subscription_days=number_of_subscription_days+1 WHERE first_payment_date IS NOT NULL');
}


function lx_intercom_get_all_users_data() {
    $intercom = new IntercomClient(INTERCOM_TOKEN, null);
    
    $intercom->users->getUsers([]);
    $users= $intercom->users->getUsers([]);

    
    $i = 0;
    
    foreach ($users->users as $user) {
        print_r($user);
        print "\n\n";
        
        $i ++;
        
        if ($i == 10) die();
    }    
    
}