<?php

function lx_moder_page() {
    
    $form = [];
    
    if (isset($_SESSION['moder_found_cmps'])) {
        $cmps = [];
        foreach ($_SESSION['moder_found_cmps'] as $c) {
            $cmps[] = l($c->name, 'mcmoderate/campaign/' .$c->uid .'/' .$c->cid .'/view');
        }
        
        $form['cmps'] = ['#type' => 'markup',
        '#markup' => '<h2>Found campaigns:</h2><ul><li>' .implode('</li><li>', $cmps) .'</li></ul><br /><br /><hr><br /><br />'
        ];
        
        unset($_SESSION['moder_found_cmps']);
    }
    
    $form['cid'] = ['#type' => 'textfield',
        '#title' => 'Campaign ID',
        ];
    
    $form['mu'] = ['#type' => 'markup',
        '#markup' => '<div class="or-word">OR<br /><br /></div>'
        ];
    
    $form['uid'] = ['#type' => 'textfield',
        '#title' => 'User ID',
        ];    
    
    $form['cmpname'] = ['#type' => 'textfield',
        '#title' => 'Campaign Name',
        ];    
    
    $form['submit'] = ['#type' => 'submit',
        '#value' => 'Search'
        ];
    
    return $form;
}

function lx_moder_page_validate(&$form, &$form_state) {
    
    if (empty($form_state['values']['cid']) && empty($form_state['values']['cmpname'])) {
        form_set_error('cid', 'Campaign ID or Campaign Name is required.');
    }
    else {
    
        if (!empty($form_state['values']['cid'])) {
            if (!preg_match('/^[a-z0-9]{32}$/', trim($form_state['values']['cid']))) {
                form_set_error('cid', 'Wrong campaign ID');
            }    
        }
        else {
            if (empty($form_state['values']['uid'])) {
                form_set_error('uid', 'User ID is required.');                
            }
            else if (!is_numeric($form_state['values']['uid'])) {
                form_set_error('uid', 'User ID must be integer.');
            }
            else if (empty($form_state['values']['cmpname'])) {
                form_set_error('uid', 'Campaign Name is required');
            }
        }
    }    

}

function lx_moder_page_submit(&$form, &$form_state) {
    $cid = trim($form_state['values']['cid']);
    $cid_exists = null;
    
    if ($cid) {
        $cid_exists = db_select('domain_compaigns', 'c')
                            ->fields('c', ['uid','cid'])
                            ->condition('cid', $cid)
                            ->execute()->fetchObject();

        if (empty($cid_exists)) {
            drupal_set_message('Campaign not found', 'error');
            drupal_goto('mcmoderate');
        }
        else {
            drupal_goto('mcmoderate/campaign/' .$cid_exists->uid .'/' .$cid_exists->cid .'/view');
        }        
    }
    else {
        $uid = (int)$form_state['values']['uid'];
        
        $cid_exists = db_select('domain_compaigns', 'c')
                            ->fields('c', ['uid','cid', 'name'])
                            ->condition('uid', $uid)
                            ->condition('name', '%'.db_like($form_state['values']['cmpname']) .'%', 'LIKE')
                            ->execute()->fetchAll();          
        
        if (sizeof($cid_exists)) {
            if (sizeof($cid_exists) == 1) {
                drupal_goto('mcmoderate/campaign/' .$cid_exists[0]->uid .'/' .$cid_exists[0]->cid .'/view');
            }
            else {
                $found_cmps = [];
                foreach ($cid_exists as $c) {
                    $found_cmps[] = $c;
                }
                
                $_SESSION['moder_found_cmps'] = $found_cmps;
                drupal_goto('mcmoderate'); 
            }
        }
        else {
            drupal_set_message('Campaign not found', 'error');
            drupal_goto('mcmoderate');            
        }
    }
    

    
}

function __lx_moder_wrap_field($title, $value, $inline = true) {
    return '<div class="moder-page-item ' .($inline ? ' inline' : '') .'"><div class="ttl">' .$title .':</div><div class="val">' .$value .'</div></div>';
}

function __lx_moder_wrap_header($title) {
    return '<h3 class="moder-page-header">' .$title .'</h3>';
}

function __lx_moder_wrap_not_defined() {
    return '<div class="not-defined">Filter is not defined</div>';
}

function lx_moder_campaign_page($uid, $cid) {
    
    if (!is_numeric($uid)) {
        drupal_set_message('Wrong UID', 'error');
        drupal_goto('mcmoderate');        
    }
    
    if (empty($cid) || !preg_match('/^[a-z0-9]{32}$/', trim($cid))) {
        drupal_set_message('Wrong campaign ID', 'error');
        drupal_goto('mcmoderate');
    }
    
    drupal_add_css(drupal_get_path('module', 'lx_moder') .'/css/styles.css');
    
    date_default_timezone_set('UTC');
    
    
    // Short statistics
    $output = '';
    $total_today_clicks = 0;
    $campaigns_stat_output = '';
    
    $campaigns_stat = __get_campaigns_cnt_stat([$cid], true);

    $block_reasons = __get_reasons_translate();
    $block_reasons[103] = 'Show Money Page to All';

    if (sizeof($campaigns_stat)) {
        foreach ($campaigns_stat as $_cid => $r) {
            if (isset($r['today'][1])) {
                if (!isset($r['today'][4])) {
                    $r['today'][4] = 0;
                }
                $r['today'][4] += $r['today'][1];
                unset($r['today'][1]);
            }

            $short_stat = array();
            $total_today_clicks = $r['today']['all'];
            $total_today_blocked = 0;


            foreach ($r['today'] as $kk => $vv) {
                if (is_numeric($kk)) {
                    $total_today_blocked += $vv;

                    if ($vv > 0) {
                        $short_stat[] = array($block_reasons[$kk], mcfn($vv) .' <span class="percent">(' .mcfn($vv/$r['today']['all']*100, 2, false) .'%)</span>');
                    }
                }
            }

            $rows = [0 => []];
            $rows[0][0] = mcfn($total_today_clicks);
            $rows[0][1] = $total_today_clicks ? mcfn($total_today_blocked) .' <span class="percent">('.mcfn(($total_today_blocked/$total_today_clicks)*100, 2, false) .'%)</span>' : 0;

            if (sizeof($short_stat)) {
                $stat_output = '<div class="short_stat">';
                $stat_output .= '<br />';
                $stat_output .= theme('table', array('header' => array('Total clicks', 'Total blocked'), 'rows' => $rows));
                $stat_output .= '<br />';
                $stat_output .= theme('table', array('header' => array('Block reason', 'Safe page %'), 'rows' => $short_stat));
                $stat_output .= '</div>';
                $campaigns_stat_output= $stat_output;
            }
            else {
                $campaigns_stat_output = '';
            }
        }
    }    
    
    
    
    // /Short statistics
    
    
    // campaign warnings
    $warn_data = [];
    $cmp = db_select('domain_compaigns', 'c')
                            ->fields('c')
                            ->condition('cid', $cid)
                            ->execute()->fetchObject();
    $warn_data[] = $cmp;
    $warnings = lx_version_control_check($warn_data);
    
//    $warnings[$cid] = 'You need to update Magic Filter Super on your server. <a target="_blank" href="/faq/magic-filter-super">Follow this guide.</a>';
    
    if (sizeof($warnings) && isset($warnings[$cid])) {
        $output .= '<div class="cmp-warnning"><i class="fa fa-exclamation-triangle"></i> ' .$warnings[$cid] .'</div>';
    }
        
    
    
    
    
    $three_states = [0 => 'OFF', 1 => 'Allow Only', 100 => 'Block Only'];
    
    
    $cmp = db_select('domain_compaigns', 'c')
                        ->fields('c')
                        ->condition('cid', $cid)
                        ->execute()->fetchObject();
    
    $data = unserialize($cmp->data);
    
    $output .= '<div class="moder-links" style="text-align: right; font-weight: bold">'
                .'<a href="/mcmoderate">Search Campaign</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;'
                .'<a href="/user/' .$uid .'/campaigns/' .$cid .'/stastistics/log/' .date('Y-m-d') .'">Campaign\'s Log</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;'
                .'<a href="/user/' .$uid .'/campaigns/' .$cid .'/stastistics">Campaign\'s Statistics</a>'
                .'<br /><br /><hr></div>';
    
    
    $output .= '<div class="moder-short-stat" style="float:right">' .$campaigns_stat_output .'</div>';
    
    $output .= '<div class="moder-inline-div">';
    $output .= __lx_moder_wrap_header('General');    
    $output .= __lx_moder_wrap_field('Campaign ID', $cmp->cid);
    $output .= __lx_moder_wrap_field('Campaign Name', $cmp->name);
    $output .= __lx_moder_wrap_field('Created', date('Y-m-d H:i:s', $cmp->created));
    $output .= __lx_moder_wrap_field('Updated', $cmp->updated);

    $output .= __lx_moder_wrap_field('Safe page', $data['safe_page']);
    $output .= __lx_moder_wrap_field('Safe page type', ($data['safe_page_show_type'] == 1 ? 'Redirect' : 'Show content'));
    
    if ($data['safe_page_show_type'] == 1) {
        $output .= @__lx_moder_wrap_field('Append URL query', ($data['safe_page_send_params'] == 10 ? 'ON' : 'OFF'));
    }
    
    $output .= __lx_moder_wrap_field('Money page', $data['money_page']);
    $output .= __lx_moder_wrap_field('Money page type', ($data['money_page_show_type'] == 1 ? 'Redirect' : 'Show content'));
    
    if ($data['money_page_show_type'] == 1) {
        $output .= @__lx_moder_wrap_field('Append URL query', ($data['money_page_send_params'] == 10 ? 'ON' : 'OFF'));
    }    
    
    $output .= '</div>';
    
    $output .= '<div class="moder-inline-div">';
    $output .= __lx_moder_wrap_header('Status');    
    $statuses = __lx_campaign_statuses(true);
    
    $output .= __lx_moder_wrap_field('Status', $statuses[$cmp->status]);
    $output .= __lx_moder_wrap_field('Set to "Active" after', $cmp->set_active_after_x_imps .' impressions');
    $output .= __lx_moder_wrap_field('Set to "Under Review" after', ($cmp->set_under_review_after ? $cmp->set_under_review_after .' minutes' : 'Never'));

    $output .= '</div>';
    
    $output .= '<div class="moder-inline-div">';
    $output .= __lx_moder_wrap_header('Global filters');
    $output .= __lx_moder_wrap_field('Bots', (isset($data['bots']) && ($data['bots'] == 1) ? "ON" : "OFF"));
    $output .= __lx_moder_wrap_field('VPN or Proxy IP', (isset($data['hosting']) && ($data['hosting'] == 1) ? "ON" : "OFF"));
    $output .= __lx_moder_wrap_field('Magic Filter Light', (isset($data['magic_filter_light']) && ($data['magic_filter_light'] == 10) ? "ON" : "OFF"));
    $output .= __lx_moder_wrap_field('Magic Filter Normal', (isset($data['magic_filter_normal']) && ($data['magic_filter_normal'] == 10) ? "ON" : "OFF"));
    $output .= __lx_moder_wrap_field('Magic Filter Super', (isset($data['magic_filter_super']) && ($data['magic_filter_super'] == 10) ? "ON" : "OFF"));
    $output .= '</div>';
    
    $output .= __lx_moder_wrap_header('Visitor\'s Location');
    
    
    $output .= __lx_moder_wrap_field('Status', $three_states[$data['location_status']]);
    
    if (isset($data['countries']) && sizeof($data['countries'])) {
    
        require_once(drupal_get_path('module', 'lx_domains_cmp') . '/includes/countries.inc');
        $countries_list = ___get_countries_list();
        $selected_countries = [];
        foreach ($data['countries'] as $v) {
            $selected_countries[] = $countries_list[$v] .' (' .strtoupper($v) .')';
        }
        
        $output .= __lx_moder_wrap_field('Countries', implode(', ', $selected_countries));
    }
    
    $output .= __lx_moder_wrap_header('Devices/OSs');
    
    $output .= __lx_moder_wrap_field('Desktops', ((isset($data['check_device_desktops']) && sizeof($data['check_device_desktops'])) ? implode(', ', $data['check_device_desktops']) : 'Not selected'));
    $output .= __lx_moder_wrap_field('Smartphones', ((isset($data['check_device_smartphones']) && sizeof($data['check_device_smartphones'])) ? implode(', ', $data['check_device_smartphones']) : 'Not selected'));
    $output .= __lx_moder_wrap_field('Gameconsoles', ((isset($data['check_device_gameconsoles']) && sizeof($data['check_device_gameconsoles'])) ? implode(', ', $data['check_device_gameconsoles']) : 'Not selected'));
    $output .= __lx_moder_wrap_field('Smart TV', ((isset($data['check_device_smarttv']) && sizeof($data['check_device_smarttv'])) ? implode(', ', $data['check_device_smarttv']) : 'Not selected'));

    
    $output .= __lx_moder_wrap_header('Browsers/Languages');         
    
    $browsers = (isset($data['browsers']) && sizeof($data['browsers'])) ? implode(', ',array_keys(__get_browser_families_by_browsers($data['browsers']))) : 'Not selected';
    
    $output .= __lx_moder_wrap_field('Browsers', $browsers);
    $output .= __lx_moder_wrap_field('Languages status', $three_states[$data['lang_status']]);
    $output .= __lx_moder_wrap_field('Selected languages', ((isset($data['langs']) && sizeof($data['langs'])) ? implode(', ', $data['langs']) : 'Not selected'));
    
    $output .= __lx_moder_wrap_header('ISP/Organizations'); 
    $output .= __lx_moder_wrap_field('Status', $three_states[$data['isp_list_type']]);
    $output .= __lx_moder_wrap_field('Selected ISPs', ((isset($data['isp_list']) && sizeof($data['isp_list'])) ? str_replace('!!!', '<br />',htmlspecialchars(implode('!!!', $data['isp_list']), ENT_QUOTES)) : 'Not selected'), false);    
    
    $output .= __lx_moder_wrap_header('IP List'); 
    $output .= __lx_moder_wrap_field('Status', $three_states[$data['ip_list_type']]);
    $output .= __lx_moder_wrap_field('Selected ISPs', ((isset($data['ip_list']) && sizeof($data['ip_list'])) ? sizeof($data['ip_list']) .' ips defined' : 'Not selected'), false);  
    
    $output .= __lx_moder_wrap_header('Referer'); 
    $output .= __lx_moder_wrap_field('Status', $three_states[$data['ref_list_type']]);
    $output .= __lx_moder_wrap_field('Block blank referer', (isset($data['clk_blank']) ? 'Yes' : 'No'));
    $output .= __lx_moder_wrap_field('Selected ISPs', ((isset($data['ref_list']) && sizeof($data['ref_list'])) ? sizeof($data['ref_list']) .' rows defined' : 'Not selected'), false);  
    
    $output .= __lx_moder_wrap_header('Url substrings'); 
    $output .= __lx_moder_wrap_field('Status', (isset($data['urlsbs_list_type']) && ($data['urlsbs_list_type'] == 1) ? 'ON' : 'OFF'));
    $output .= __lx_moder_wrap_field('Required List', (isset($data['urlsbs_list_required']) ? str_replace('!!!', '<br />',htmlspecialchars(implode('!!!', $data['urlsbs_list_required']), ENT_QUOTES)) : 'NO'));
    $output .= __lx_moder_wrap_field('Disallowed List', (isset($data['urlsbs_list_disallowed']) ? str_replace('!!!', '<br />',htmlspecialchars(implode('!!!', $data['urlsbs_list_disallowed']), ENT_QUOTES)) : 'NO'));
    
    $output .= __lx_moder_wrap_header('User Agent'); 
    $output .= __lx_moder_wrap_field('Status', (isset($data['ua_list_type']) && ($data['ua_list_type'] == 1) ? 'ON' : 'OFF'));
    $output .= __lx_moder_wrap_field('Required List', (isset($data['ua_list_required']) ? str_replace('!!!', '<br />',htmlspecialchars(implode('!!!', $data['ua_list_required']), ENT_QUOTES)) : 'NO'));
    $output .= __lx_moder_wrap_field('Disallowed List', (isset($data['ua_list_disallowed']) ? str_replace('!!!', '<br />',htmlspecialchars(implode('!!!', $data['ua_list_disallowed']), ENT_QUOTES)) : 'NO'));

                    
    $output .= __lx_moder_wrap_header('Referer'); 
    $output .= __lx_moder_wrap_field('Status', (isset($data['time_type']) && ($data['time_type'] == 1) ? 'ON' : 'OFF'));
    $output .= __lx_moder_wrap_field('Selected Time', (isset($data['selected_time']) ? $data['selected_time'] : 'Not selected'));
    $output .= __lx_moder_wrap_field('Timezone', (isset($data['cmp_timezone']) ? $data['cmp_timezone'] : 'Not selected')); 
                 
                  
//
//                    'time_of_day' => [
//                        'status' => $cmp->data['time_type'],
//                        'selected_time'     => isset($cmp->data['selected_time']) ? $cmp->data['selected_time'] : '',
//                        'timezone'          => isset($cmp->data['cmp_timezone'])  ? $cmp->data['cmp_timezone'] : '',
//                    ],     
    

    

    
    return $output;
}