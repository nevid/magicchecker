<?php
// to view php errors (it can be useful if you got blank screen and there is no clicks in site statictics) uncomment next two strings:
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

define('ENC_KEY', 'abcdef1234567890abcdef1234567890');
define('CAMPAIGN_ID', $campaign_id);

function translateCurlError($code) {
    if ($code === 2) {
        return "Can't init curl. (code: 2)";
    } else if ($code === 6) {
        return "Can't resolve server's DNS of our domain. Please contact your hosting provider and tell them about this issue. (code: 6)";
    } else if ($code === 7) {
        return "Can't connect to the server. (code: 7)";
    } else if ($code === 28) {
        return "Operation timeout. Check you DNS setting. (code: 28)";
    } else {
        return "Error code: ".$code .'. Check if php cURL library installed and enabled on your server.';
    }
}
 
// Encrypt Function
function mc_encrypt($encrypt) {$key = ENC_KEY;$encrypt = serialize($encrypt);$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);$key = pack('H*', $key);$mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));$passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);$encoded = base64_encode($passcrypt).'|'.base64_encode($iv);return $encoded;}
// Decrypt Function
function mc_decrypt($decrypt) {$key = ENC_KEY;$decrypt = explode('|', $decrypt.'|');$decoded = base64_decode($decrypt[0]);$iv = base64_decode($decrypt[1]);if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }$key = pack('H*', $key);$decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));$mac = substr($decrypted, -64);$decrypted = substr($decrypted, 0, -64);$calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));if($calcmac!==$mac){ return false; }$decrypted = unserialize($decrypted);return $decrypted;}

function generate_click_id($result) {
    return mc_encrypt($result->click_id.'||'.(($result->urlType == 'redirect') ? _redirectPage($result->url, $result->send_params, true) : $result->url).'||'.(($result->spType == 'redirect') ? _redirectPage($result->sp, $result->send_params, true) : '').'||' .time() .'||' .(isset($result->tp) ? $result->tp : 'N') .'||' .(isset($result->mms) ? $result->mms : 'N') .'||' .(isset($result->lls) ? $result->lls : 'N'));
}

// отправляем запрос
function sendRequest($data, $path = 'index') {
    $headers = array();
    $headers[] = 'adapi: 2.0';
    
    $data_to_post = array('cmp' =>  CAMPAIGN_ID, 'headers' => $data);    
    
    $ch = curl_init($linktoserver .$path .'.php');
    curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, $dns_cache);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data_to_post));

    $output = curl_exec($ch);    
    if (strlen($output) == 0) {
        $curl_err_num = curl_errno($ch);
        print '$curl_err_num = ' .$curl_err_num;
        if (is_numeric($curl_err_num)) {
            print "Empty answer from server. <br />" .translateCurlError($curl_err_num);
        }
        curl_close($ch);
        die();
    }    
    else {
        $info = curl_getinfo($ch);
        if ($info['http_code'] != 200) {
            print 'HTTP ERROR ' .$info['http_code'];
            $output = '';
        }    
    }
    curl_close($ch); 
    return $output;
}

// обновляем данные по клику
function updateClick($click_id, $data) {
    if (function_exists('fastcgi_finish_request')) {
        fastcgi_finish_request();
    } 
    sendRequest($data, 'update');
}

// первый запрос на php проверки
function isBlocked($testmode = false) {
    $result = new stdClass();
    $result->hasResponce = false;
    $result->isBlocked = false;
    $result->errorMessage = '';
    
    if ($testmode) {
        if (!function_exists('curl_init')) {
            print "php-curl is disabled on your host. Please ask your host provider to enable php-curl. Error message: undefined function curl_init";
        }
        else {
            print 'Successful test. Curl found on your host';
        }
        die();
    }

    $data_headers = array();
    foreach ( $_SERVER as $name => $value ) {
        if (is_array($value)) {
            $value = implode(', ', $value);
        }
        
        if ((strlen($value) < 1024) || ($name == 'HTTP_REFERER') || ($name == 'QUERY_STRING') || ($name == 'REQUEST_URI') || ($name == 'HTTP_USER_AGENT')) {
            $data_headers[$name] = $value;
        } else {
            $data_headers[$name] = 'TRIMMED: ' .substr($value, 0, 1024);
        }
    }    
    
    $output = sendRequest($data_headers);
    
    if ($output) {
        $result->hasResponce = true;
        $answer = json_decode($output, TRUE);
        if (isset($answer['ban']) && ($answer['ban'] == 1)) die();
        
        if ($answer['success'] == 1) {
            foreach ($answer as $ak => $av) {
                $result->{$ak} = $av;
            }
        }
        else {
            $result->errorMessage = $answer['errorMessage'];
        }
    }
    
    return $result;
}

function _redirectPage($url, $send_params, $return_url = false) {
    if ($send_params) {
        if ($_SERVER['QUERY_STRING'] != '') {
            if (strpos($url, '?')) {
                    $url .= '&' . $_SERVER['QUERY_STRING'];
            } else {
                    $url .= '?' . $_SERVER['QUERY_STRING'];
            }
        }
    } 
    
    if ($return_url) return $url;
    else header("Location: $url", true, 302);
}

function _includeFileName($url) {
    if (strpos($url, '/') !== false) {
        $url = ltrim(strrchr($url, '/'), '/');
    }      
    
    return $url;
}

//////////////////////////////////////////////////////////////////////////////// 

// disable this page caching
header('Cache-Control: no-store, no-cache, private, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header('Expires: 0');

//$send_404_error = false;

if (!isset($_POST['click'])) {
    // call this function like this: 
    // $result = isBlocked(true);
    // to check if curl is enabled on your server
    $result = isBlocked();  // check page for ip block

    if (!isset($result->js)) {
        if ($result->hasResponce && !isset($result->error_message)) {
            if ($result->urlType == 'redirect') {
                _redirectPage($result->url, $result->send_params);       
            }
            else {
                include _includeFileName($result->url);
            }
        }
        else {
            die('Error: ' .$result->errorMessage);
        }
    }
    else {
        $insert_script = '<script>window.click_id="' .generate_click_id($result) .'";' .$result->js .'</script>';
        
        if ($result->spType == 'redirect') {
            print '<html><head><title></title><meta charset="UTF-8"><noscript><style>html,body{visibility:hidden;background-color:#ffffff!important;}</style><meta http-equiv="refresh" content="0; url=' .$result->sp .'"></noscript>' .$insert_script .'</head><body></body></html>';
        }
        else {
            $include_file = file_get_contents(dirname(__FILE__) .'/' ._includeFileName($result->sp));
            if (strpos($include_file, '<head>') !== false) {
                $include_file = str_ireplace('<head>', '<head>' .$insert_script, $include_file);
            }
            else {
                $include_file = str_ireplace('<body', '<head>' .$insert_script .'</head><body', $include_file);
            }
            
            if (strpos($include_file, '<?') !== false) {
                eval('?>'.$include_file .'<?php ');
            }
            else {
                print $include_file;
            }
        }
    }
}
else {
    $click_id = mc_decrypt($_POST['click']);
    if (strpos($click_id, '||')) {
        $cdata = explode('||', $click_id);
        if ($cdata[3] + 10 >= time()) {
            $update_data = array();
            
            //tp=xhet&plr=1&pn=MacIntel&or=undefined&click=LoF7Nf6SAd1gY6eUnxncbd4ivG0mRIW0osdbgFDknJFsey3rv7MEzsDqyVXJ2kngFEzeIAx8d7JUp3OJf4jlOJShysKM4YwX3+wnrQG6St5xE99gXyaCRcDVhuyZAk4+90kSfrrKxVU+1qTNlEGdl2pyzeaBpV42peZJmAVVplqBQ9dqlgoLocNfqWofrFXPsAdImVxPLCVRcwU7j6Qg+Q==|G3BpGLE97fnAHzVS5lu9uB83NdDfiYfpZu5GHtR+yUE=
            
            $tp = isset($_POST['tp']) ? $_POST['tp'] : null;  // получаем таймзону
            $plr = isset($_POST['plr']) ? (int)$_POST['plr'] : null; // получаем ответ на проверку по  js magic filter            
            $lls = isset($_POST['lls']) ? (int)$_POST['lls'] : null; // получаем ответ по люминати
            
            // если есть проверка на таймзону, проверяем ее соответствие
            if (($cdata[4] != 'N') && ($cdata[4] != $tp)) {
                $update_data['r'] = 'tp';
            }
            
            // проверка по js magic filter
            if (($cdata[5] != 'N') && ($plr == 0)) {
                $update_data['r'] = 'pn';
            }
            
            // проверка на люминати
            if (($cdata[6] != 'N') && ($lls == 1)) {
                $update_data['r'] = 'lls';
            }
            
            $show_page = 1; // показываем money_page
            if (isset($update_data['r'])) {
                $update_data['click_id'] = $cdata[0];
                if ($tp) $update_data['tp'] = $tp;
                if (isset($_POST['pn'])) { $update_data['pn'] = $_POST['pn']; }
                if (isset($_POST['or'])) { $update_data['or'] = $_POST['or']; }
                updateClick($click_id, $update_data);
                $show_page = 2; // показываем safe_page
            }
            
            if ($cdata[$show_page]) {
                print "<script>location.href=\"" .$cdata[$show_page] ."\";</script>";
            }
        }
        else {
            if ($cdata[2]) {
                print "<script>location.href=\"" .$cdata[2] ."\";</script>";
            }
        }
    }
    else {
//        $send_404_error = true;
    }
}

//if ($send_404_error) {
//    header('HTTP/1.1 404 Not Found');
//    header('Status: 404 Not Found');
//    exit;                
//}