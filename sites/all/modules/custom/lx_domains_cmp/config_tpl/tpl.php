<?php
// to view php errors (it can be useful if you got blank screen and there is no clicks in site statictics) uncomment next two strings:
//  error_reporting(E_ALL);
//  ini_set('display_errors', 1);

function translateCurlError($code) {
    if ($code === 2) {
        return "Can't init curl. (code: 2)";
    } else if ($code === 6) {
        return "Can't resolve server's DNS of our domain. Please contact your hosting provider and tell them about this issue. (code: 6)";
    } else if ($code === 7) {
        return "Can't connect to the server. (code: 7)";
    } else if ($code === 28) {
        return "Operation timeout. Check you DNS setting. (code: 28)";
    } else {
        return "Error code: ".$code .'. Check if php cURL library installed and enabled on your server.';
    }
}

//insert-counterdatafunc

function isBlocked($testmode = false) {
    
    $result = new stdClass();
    $result->hasResponce = false;
    $result->isBlocked = false;
    $result->errorMessage = '';
    
    if ($testmode) {
        if (!function_exists('curl_init')) {
            print "php-curl is disabled on your host. Please ask your host provider to enable php-curl. Error message: undefined function curl_init";
        }
        else {
            print 'Successful test. Curl found on your host';
        }
        die();
    }

    $data_headers = array();
    
    foreach ( $_SERVER as $name => $value ) {
        if (is_array($value)) {
            $value = implode(', ', $value);
        }
        if ((strlen($value) < 1024) || ($name == 'HTTP_REFERER') || ($name == 'QUERY_STRING') || ($name == 'REQUEST_URI') || ($name == 'HTTP_USER_AGENT')) {
            $data_headers[$name] = $value;
        } else {
            $data_headers[$name] = 'TRIMMED: ' .substr($value, 0, 1024);
        }
    }    
    
    $data_to_post = array("cmp"     =>  $campaign_id,
                          "headers" => $data_headers,
                          //insert-counterdataval
                         );
    
    $headers = array();
    $headers[] = 'adapi: 1.0';
    
    $ch = curl_init($linktoserver);
    curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, $dns_cache);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data_to_post));

    $output = curl_exec($ch);
    
    if (strlen($output) == 0) {
        $curl_err_num = curl_errno($ch);
        print '$curl_err_num = ' .$curl_err_num;
        if (is_numeric($curl_err_num)) {
            $result->errorMessage = "Empty answer from server. <br />" .translateCurlError($curl_err_num);
        }
    }    
    else {
        $info = curl_getinfo($ch);
        $result->hasResponce = true;
        
        if ($info['http_code'] == 200) {
            $answer = json_decode($output, TRUE);
            if (isset($answer['ban']) && ($answer['ban'] == 1)) {
                curl_close($ch);
                die();
            }
            if ($answer['success'] == 1) {
                $result->isBlocked = $answer['isBlocked'];
                $result->urlType = $answer['urlType'];
                $result->url = $answer['url'];
                $result->send_params = $answer['send_params'];
            }
            else {
                $result->errorMessage = $answer['errorMessage'];
            }
        }    
        else {
            $result->errorMessage = 'HTTP ERROR ' .$info['http_code'];
        }
    }
    
    curl_close($ch);
    return $result;
}

function _redirectPage($url, $send_params) {
    if ($send_params) {
        if ($_SERVER['QUERY_STRING'] != '') {
            if (strpos($url, '?')) {
                    $url .= '&' . $_SERVER['QUERY_STRING'];
            } else {
                    $url .= '?' . $_SERVER['QUERY_STRING'];
            }
        }
    }    
    header("Location: $url", true, 302);
}

//////////////////////////////////////////////////////////////////////////////// 

// disable this page caching
header('Cache-Control: no-store, no-cache, private, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header('Expires: 0');


// call this function like this: 
// $result = isBlocked(true);
// to check if curl is enabled on your server
$result = isBlocked();  // check page for ip block

if ($result->hasResponce && !isset($result->error_message)) {
    if ($result->urlType == 'redirect') {
        _redirectPage($result->url, $result->send_params);       
    }
    else {
        $url = $result->url;
        if (strpos($url, '/') !== false) {
            $url = ltrim(strrchr($url, '/'), '/');
        }   
        include $url;    
    }
}
else {
    die('Error: ' .$result->errorMessage);
}