$ = jQuery;
$(document).ready(function () {
    
    function waitNum() {
        $('.batch-ops-btn .selected-cmps .txt .seled').html('<i class="fa fa-refresh fa-spin" aria-hidden="true"></i>');
    }
    
    function saveCids(el) {
        waitNum();
        var len = $('#lx-batch-operations-batch-table-form tbody .form-checkbox:checked').length;
        var cids = {};
        $('#lx-batch-operations-batch-table-form tbody .form-checkbox').each(function(){
            name = $(this).attr('name');
            if ($(this).prop('checked') == true) {
                cids[name.replace('cmp_', '')] = 1;
            }
            else {
                cids[name.replace('cmp_', '')] = 2;
            }
        });

        if (el.attr('name') == 'check_all_campaigns') {
            if ($('.sticky-enabled .form-item-check-all-campaigns input').prop('checked') == true) {
                totalCmps = $('#cmps-list-table').data('cids');
                if (len < totalCmps) {
                    $('#cmps-list-table tbody').prepend('<tr class="sel-row"><td colspan="7"> You\'ve selected all ' + len + ' campaigns on this page. <a href="#" class="sel-all-action">Select all <strong>' + totalCmps + '</strong> campaigns.</a></td></tr>');
                }
            }
            else {
                if ($('tr').is('.sel-row')) {
                    $('.sel-row').remove();
                } 
            }
        }
        else if ($('tr').is('.sel-row')) {
            $('.sel-row').remove();
            $('.form-item-check-all-campaigns .form-checkbox').prop('checked', false);
        }

        $.post(location.href, {box_cids: cids}, function (data) {
            data = jQuery.parseJSON(data);
            if (data.result == 'ok') {
                checkEditBtn(data.selected);

                if (data.selected == 0) {
                    if ($('tr').is('.sel-row')) {
                        $('.sel-row').remove();
                    }                            
                }
            }
        });
    }

    function saveAllCids() {
        waitNum();
        filtered = $('#cmps-list-table').data('filtered');
        params = {box_cids: 'all'};
        
        if (filtered) {
            params['filtered'] = 1;
        }
                
        $.post(location.href, params, function (data) {
            data = jQuery.parseJSON(data);
            if (data.result == 'ok') {                    
                checkEditBtn(data.selected);
                $('.sel-row').html('<td colspan="7"> You\'ve selected all <strong>' + data.selected + ' campaigns</strong>. <a href="#" class="sel-all-cancel-action">Clear selection<strong></a></td>');
            }
        });        
    }

    function resetAllCids() {
        params = {box_cids: 'reset'};
                
        if ($('tr').is('.sel-row')) {
            $('.sel-row').remove();
        }                
        checkEditBtn(0);
        $.post(location.href, params, function (data) {});        
    }

    function checkEditBtn(len) {
        if (len) {
            $('.actions-panel').addClass('edit-mode');
            $('.batch-ops-btn .selected-cmps .txt .seled').html(len).show();
            if (len < 10) {
                $('.ops-list').width(186);
            }
            else {
                $('.ops-list').width(195);
            }
            
            $('.batch-ops-btn .selected-cmps .icon').hide();
            $('.batch-ops-btn .edit-btn .fa-sort-desc').show();
        }
        else {
            $('.actions-panel').removeClass('edit-mode');
        }
    }
    
    $('#cmps-list-table').on('click', '.sel-all-action', function (e) {
        e.preventDefault();
        saveAllCids();
    })
    
    $('#cmps-list-table').on('click', '.sel-all-cancel-action', function (e) {
        e.preventDefault();
        $('.close-select').trigger('click');
    })
    
    //При клике на чекбокс в шапке таблицы
    $('select#edit-per-page--2, select#edit-per-page-bottom').on('change', function() {
        var url = window.location.href;
        var params = getQueryParams(url);
        params.per_page = this.value;
        $.cookie('MC_per_page', this.value,{ expires: 3650});
        var queryPars = setQueryParams(params);
        url = url.split('?')[0];
        window.location.href = url + '?' + queryPars;
    });

    //При клике на чекбокс в шапке таблицы
    $('.sticky-enabled .form-item-check-all-campaigns input').on('click', function() {
      $('#lx-batch-operations-batch-table-form .form-checkbox').prop('checked', $(this).prop('checked'));
    });

    //Автосабмит формы при изменении селекта
//    $('select#edit-batch-operation').on('change', function() {
//      $('#edit-submit-button').click();
//    });
//    $('select#edit-batch-operation-bottom').on('change', function() {
//      $('#edit-submit-button-bottom').click();
//    });
    function popupConfirmForm() {
      $('#page #content').addClass('bg-blured');
      $('body').append('<div class="overlay-over-blured"><div class="overlay-over-blured-content"><div class="conf-text">Are you sure you want to delete the selected campaigns?</div><div class="conf-buttons"><a class = "confirm-no" href = "">Cancel</a><a href = "" class = "confirm-yes">Confirm</a></div></div></div>');
      $('.confirm-no').on('click', function(e) {
        closePopupConfirmForm();
        e.preventDefault();
      });
      $('.confirm-yes').on('click', function(e) {
        $('#edit-submit-button-bottom').click();
        e.preventDefault();
      });
    }

    function closePopupConfirmForm() {
      $('#page #content').removeClass('bg-blured');
      $('.overlay-over-blured').remove();
    }

    //Первичный disable селектов
    if (!$("#lx-batch-operations-batch-table-form input:checked" ).length) {
      setBatchOperationSelectDisabled();
    }

    //Снимаем дизейблд с селектов
    $('#lx-batch-operations-batch-table-form .form-type-checkbox input').on('click', function() {
//      if (!$("#lx-batch-operations-batch-table-form input:checked" ).length) {
//        setBatchOperationSelectDisabled();
//      } else {
//        $('select#edit-batch-operation').prop( "disabled", false );
//        $('select#edit-batch-operation').parent('.form-item').removeClass('form-disabled');
//        $('select#edit-batch-operation-bottom').prop( "disabled", false );
//        $('select#edit-batch-operation-bottom').parent('.form-item').removeClass('form-disabled');
//      }
      saveCids($(this));
    });
    
    
    $('.batch-ops-btn a.control-btn').on('click', function() {
        $('select#edit-batch-operation').prop( "disabled", false );
        $('select#edit-batch-operation').parent('.form-item').removeClass('form-disabled');
        $('select#edit-batch-operation-bottom').prop( "disabled", false );
        $('select#edit-batch-operation-bottom').parent('.form-item').removeClass('form-disabled');        
        $('select#edit-batch-operation-bottom').val($(this).data('id'));
        if ($(this).data('id') == 1) { //Операция удаления
          popupConfirmForm();
        } else {
          $('#edit-submit-button-bottom').click();
        }
    });
    
    $('.close-select').on('click', function(e) {
      e.preventDefault();  
      $('#lx-batch-operations-batch-table-form .form-checkbox').prop('checked', false);
      resetAllCids();

    });    
});


function setBatchOperationSelectDisabled() {
  $('select#edit-batch-operation').prop( "disabled", true );
  $('select#edit-batch-operation').parent('.form-item').addClass('form-disabled');
  $('select#edit-batch-operation-bottom').prop( "disabled", true );
  $('select#edit-batch-operation-bottom').parent('.form-item').addClass('form-disabled');
}

function getQueryParams(url){
    var qparams = {},
        parts = (url||'').split('?'),
        qparts, qpart,
        i=0;

    if(parts.length <= 1 ){
        return qparams;
    }else{
        qparts = parts[1].split('&');
        for(i in qparts){

            qpart = qparts[i].split('=');
            qparams[decodeURIComponent(qpart[0])] =
                decodeURIComponent(qpart[1] || '');
        }
    }

    return qparams;
};

function setQueryParams(pars) {
  var queryString = '';
  for (key in pars) {
    if (queryString == '') {
      queryString += key + '=' + pars[key];
    } else {
      queryString += '&' + key + '=' + pars[key];
    }
  }
  return queryString;
};