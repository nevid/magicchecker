<?php

//list of all available batch operations
function lx_batch_operations_get_list() {
    return array(
        0 => t('Choose operation'),
        1 => t('Delete'),
        2 => t('Change status to:'),
        3 => t('- Active'),
        4 => t('- Under Review'),
        5 => t('- Show Safe Page to All'),
        6 => t('- Show Money Page to All'),
    );
}

//Get confirmation text by batch operation id
function lx_batch_operations_get_op_confirmation_text($op_id) {
    $texts = array(
        1 => t('Do you want to delete selected campaigns?'),
        3 => t('Do you want to change status to "Active" for selected campaigns?'),
        4 => t('Do you want to change status to "Under Review" for selected campaigns?'),
        5 => t('Do you want to change status to "Show Safe Page to All" for selected campaigns?'),
        6 => t('Do you want to change status to "Show Money Page to All" for selected campaigns?'),
    );
    return $texts[$op_id];
}

function lx_batch_operations_get_op_page_title($op_id) {
    $texts = array(
        1 => t('Deleting campaigns...'),
        3 => t('Updating status...'),
        4 => t('Updating status...'),
        5 => t('Updating status...'),
        6 => t('Updating status...'),
    );
    return $texts[$op_id];
}

function lx_batch_operations_get_op_finish_text($op_id) {
    $texts = array(
        1 => t('Selected campaigns were successfully deleted.'),
        3 => t('Selected campaigns were successfully updated.'),
        4 => t('Selected campaigns were successfully updated.'),
        5 => t('Selected campaigns were successfully updated.'),
        6 => t('Selected campaigns were successfully updated.'),
    );
    return $texts[$op_id];
}

function lx_batch_operations_get_without_batch_cnt($op_id) {
    $texts = array(
        1 => 5,
        3 => 50,
        4 => 50,
        5 => 50,
        6 => 50,
    );
    return $texts[$op_id];
}

function lx_batch_operations_get_op_function_name_by_id($op_id) {
  $operations = array(
    1 => 'lx_batch_operations_delete_operation',
    3 => 'lx_batch_operations_update_status_operation',
    4 => 'lx_batch_operations_update_status_operation',
    5 => 'lx_batch_operations_update_status_operation',
    6 => 'lx_batch_operations_update_status_operation',
  );
  return $operations[$op_id];
}


function lx_batch_operations_delete_operation($cid, &$context = null) {
    lx_domains_cmp_delete_campaign($cid);
    
    if (!empty($context)) {
        $context['results']['operations_performed']++;
        $context['message'] = 'Campaign with id "' . $cid . '" deleted...';
    }    
}

function lx_batch_operations_update_status_operation($cid, &$context = null) {

    $ops_data = $_SESSION['lx_cmp_batch_operation'];
    switch($ops_data['name']) {
      case 3:
        $status = 7;
      break;
      case 4:
        $status = 101;
      break;
      case 5:
        $status = 102;
      break;
      case 6:
        $status = 103;
      break;
    }
    $user = user_load($ops_data['uid']);
    lx_domains_cmp_user_set_status_manually($user, $cid, $status);
    if (!empty($context)) {
        $context['results']['operations_performed']++;
        $context['message'] = 'Campaign with id "' . $cid . '" updated...';
    }    
}

//Batch Example operation
function lx_batch_operations_example_operation($cid, &$context = null) {
    for ($i = 0; $i < 99999999; $i++) {}
    
    if (!empty($context)) {
        $context['results']['operations_performed']++;
        $context['message'] = 'Operation <em>' . $context['results']['operations_performed'] . '</em> performed successfully.';
    }    
}