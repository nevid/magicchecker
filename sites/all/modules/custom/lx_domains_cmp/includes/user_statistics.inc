<?php

function lx_domains_caleran_dates_range_form($form, $form_state, $acc, $cid = null) {
    global $user;
    drupal_add_css(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/css/caleran.min.css');
    
    $user_created = (int)((time() - $acc->created)/86400);
    drupal_add_js('window.user_created='.$user_created.';', 'inline');
    
    
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/js/moment.min.js');
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/js/caleran.min.js');
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/js/caleran-show.js');
    
    $period_type = 2;
    if ($user->uid == 1) {
        $period_type = 3;
    }
$date_ranges = array(
//                         6 => 'Last week', 
                         8 => 'Today', 
                         7 => 'Yesterday', 
                         2 => 'Last 7 days', 
                         4 => 'Last 30 days',
//                         1 => 'This week',
                         3 => 'This month',
                         4 => 'Last 30 days', 
                         5 => 'Last month',
                         10 => 'All time',
                         100 => 'Custom date range'
                    );
    if (isset($_GET['period_type']) && is_numeric($_GET['period_type']) && in_array($_GET['period_type'], array_keys($date_ranges))) {
        $period_type = $_GET['period_type'];
    }    
    
    $form['fs'] = array('#type' => 'fieldset',
        '#attributes' => array('class' => array('pad12 container-inline form-wrapper'))
    );    
    
    $def_value = date('01.m.Y') .' - ' .date('d.m.Y');
    if (isset($_GET['dates']) && preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4} \- [0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $_GET['dates'])) {
        $def_value = $_GET['dates'];
    }
    
    $form['fs']['period_type'] = array('#type' => 'select',
        '#title' => '<i class="fa fa-calendar" aria-hidden="true"></i>',
        '#options' => $date_ranges,
        '#default_value' => $period_type,
        '#weight' => 1
        
        );
    
    $form['fs']['dates'] = array('#type' => 'textfield',
//        '#title' => '<i class="fa fa-calendar" aria-hidden="true"></i><span>Period&nbsp;&nbsp;&nbsp;</span>',
        '#default_value' => $def_value,
        '#weight' => 2,
        '#prefix' => '<div class="dates-wrapper"' .($period_type < 100 ? ' style="display:none"' : '') .'>',
        '#suffix' => '</div>'
        );
    
    $form['#method'] = 'get';
    
    $form['fs']['submit'] = array('#type' => 'submit',
        '#value' => 'Show',
        '#weight' => 3
        
    );
    
    if ($period_type == 100) {
        $form['#attributes']['class'][] = 'custom-shown';
    }
    
    return $form;
}

// shows user statistics by days
function lx_domains_user_total_stat($acc) {
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/js/tip_window.js');
    
    date_default_timezone_set('UTC');
    
    try {
        
    $date_from = date('01.m.Y');
    $date_to = date('d.m.Y');
    
    
    
    if (isset($_GET['period_type']) && is_numeric($_GET['period_type'])) {
        if ($_GET['period_type'] == 100 && isset($_GET['dates']) && preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4} \- [0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $_GET['dates'])) {
            $dates = explode(' - ', $_GET['dates']);
            $date_from = $dates[0];
            $date_to = $dates[1];
        }
        else {
            if ($_GET['period_type'] == 1) {
                $date_from =  date("d.m.Y", strtotime("this Monday"));
               //воскресенье прошлой недели
               $date_to = date('d.m.Y');
            }
            else if ($_GET['period_type'] == 6) {
               //понедельник прошлой недели
               $date_from =  date("d.m.Y", strtotime("last Monday"));
               //воскресенье прошлой недели
               $date_to =  date("d.m.Y", strtotime("last Sunday"));            
            }
            else if ($_GET['period_type'] == 2) {
               $date_from = date('d.m.Y', time() - 86400*6);
               $date_to = date('d.m.Y');
            }
            else if ($_GET['period_type'] == 3) {
                $date_from = date('01.m.Y');
                $date_to = date('d.m.Y');                
            }
            else if ($_GET['period_type'] == 4) {
                $date_from = date('d.m.Y', time() - 86400*30);
                $date_to = date('d.m.Y');
            }
            else if ($_GET['period_type'] == 10) {                
                $date_from = date('d.m.Y', $acc->created);
                $date_to = date('d.m.Y');
            }
            else if ($_GET['period_type'] == 5) {
               $date_from =  date("d.m.Y", strtotime("first day of last month"));
               //воскресенье прошлой недели
               $date_to =  date("d.m.Y", strtotime("last day of last month")); 
            }
            else if ($_GET['period_type'] == 7) {
               $date_from =  date("d.m.Y", time() - 86400);
               //воскресенье прошлой недели
               $date_to =  date("d.m.Y", time() - 86400); 
            }
            else if ($_GET['period_type'] == 8) {
               $date_from =  date("d.m.Y", time());
               //воскресенье прошлой недели
               $date_to =  date("d.m.Y", time()); 
            }
        }
    }    
    
    
    $date_from = explode('.', $date_from);
    $date_from = mktime(0,0,0, ltrim($date_from[1], '0'), ltrim($date_from[0], '0'), $date_from[2]);
    
    $date_to = explode('.', $date_to);
    $date_to = mktime(0,0,0, ltrim($date_to[1], '0'), ltrim($date_to[0], '0'), $date_to[2]);          
        


    $form = drupal_get_form('lx_domains_caleran_dates_range_form',$acc);
    $output = drupal_render($form);
    
    $is_admin = user_access('administer users');
//    $is_admin = false;

    $col_names = array('hosting_clicks' => 'VPN/Proxy',
        'country_clicks' => "Visitor's Location",
        'bot_clicks' => 'Bots',
        'adplex_clicks' => 'Adplexity',
        'total_blocked' => 'Total blocked',
        'total' => 'Total clicks',
        'isp_clicks' => 'ISP/Organizations',
        'referer_clicks' => 'Referer',
        'ip_clicks' => 'IP List',
        'device_os_clicks' => 'Devices',
        'url_clicks' => 'URL Substrings',
        'ua_clicks' => 'User Agent',
        'time_clicks' => 'Time of Day',
        'status_clicks' => 'Status',
        'headers_clicks' => 'MFN',
        'light_clicks' => 'MFL',
        'super_clicks' => 'MFS',
        'tz_clicks' => 'Timezone',
        'js_mf_clicks' => 'Magic JS Filter',
        'js_luminati_clicks' => 'Luminati',
        'browser_clicks' => 'Browsers',
        'efilter1_clicks' => 'Languages',
//        'efilter2_clicks' => 'ND22',
//        'efilter3_clicks' => 'ND23',
//        'efilter4_clicks' => 'ND24',
//        'efilter5_clicks' => 'ND25',        
    );
    
    if (!$is_admin) {
        unset($col_names['adplex_clicks']);
    }

    
    $rows = array(0 => array());

    $total_row = array('created' => '<b>Total for period:</b>',
        'total_clicks' => 0,
        'adplex_clicks' => 0,
        'hosting_clicks' => 0,
        'country_clicks' => 0,
        'bot_clicks' => 0,
        'isp_clicks' => 0,
        'referer_clicks' => 0,
        'ip_clicks' => 0,
        'device_os_clicks' => 0,
        'url_clicks' => 0,
        'ua_clicks' => 0,
        'time_clicks' => 0,
        'status_clicks' => 0,
        'headers_clicks' => 0,
        'light_clicks' => 0,
        'super_clicks' => 0,
        'tz_clicks' => 0,
        'js_mf_clicks' => 0,
        'js_luminati_clicks' => 0,
        'browser_clicks' => 0,
        'efilter1_clicks' => 0,
//        'efilter2_clicks' => 0,
//        'efilter3_clicks' => 0,
//        'efilter4_clicks' => 0,
//        'efilter5_clicks' => 0,        
        
        'total_blocked' => 0,
    );    
    
    $res = db_select('domain_compaigns_users_stat', 's');
    $res->addExpression('SUM(total_clicks)', 'total_clicks');
    $res->addExpression('SUM(adplex_clicks)', 'adplex_clicks');
    $res->addExpression('SUM(hosting_clicks)', 'hosting_clicks');
    $res->addExpression('SUM(country_clicks)', 'country_clicks');
    $res->addExpression('SUM(bot_clicks)', 'bot_clicks');
    $res->addExpression('SUM(isp_clicks)', 'isp_clicks');
    $res->addExpression('SUM(referer_clicks)', 'referer_clicks');
    $res->addExpression('SUM(ip_clicks)', 'ip_clicks');
    $res->addExpression('SUM(device_os_clicks)', 'device_os_clicks');
    $res->addExpression('SUM(url_clicks)', 'url_clicks');
    $res->addExpression('SUM(ua_clicks)', 'ua_clicks');
    $res->addExpression('SUM(time_clicks)', 'time_clicks');
    $res->addExpression('SUM(status_clicks)', 'status_clicks');
    $res->addExpression('SUM(headers_clicks)', 'headers_clicks');
    $res->addExpression('SUM(light_clicks)', 'light_clicks');
    $res->addExpression('SUM(super_clicks)', 'super_clicks');    
    $res->addExpression('SUM(tz_clicks)', 'tz_clicks');    
    $res->addExpression('SUM(js_mf_clicks)', 'js_mf_clicks');    
    $res->addExpression('SUM(js_luminati_clicks)', 'js_luminati_clicks'); 
    $res->addExpression('SUM(browser_clicks)', 'browser_clicks');
    $res->addExpression('SUM(efilter1_clicks)', 'efilter1_clicks');
//    $res->addExpression('SUM(efilter2_clicks)', 'efilter2_clicks');
//    $res->addExpression('SUM(efilter3_clicks)', 'efilter3_clicks');
//    $res->addExpression('SUM(efilter4_clicks)', 'efilter4_clicks');
//    $res->addExpression('SUM(efilter5_clicks)', 'efilter5_clicks');       

    $res->condition('created', $date_from, '>=');
    $res->condition('created', $date_to, '<=');
    $res->condition('uid', $acc->uid);
    $r = $res->execute()->fetchObject();  
    $r = (array)$r;

    $total_blocked = $r['adplex_clicks'] + $r['hosting_clicks'] + $r['country_clicks'] + $r['bot_clicks'] + $r['isp_clicks'] + $r['referer_clicks'] + $r['ip_clicks'] + $r['device_os_clicks'] + $r['url_clicks']+$r['ua_clicks'] + $r['time_clicks'] + $r['status_clicks'] + $r['headers_clicks'] + $r['light_clicks'] + $r['super_clicks'] + $r['tz_clicks'] + $r['js_mf_clicks'] + $r['js_luminati_clicks'] + $r['browser_clicks'] + $r['efilter1_clicks']; // + $r['efilter2_clicks'] + $r['efilter3_clicks'] + $r['efilter4_clicks'] + $r['efilter5_clicks'];

    $total_row = array('created' => '<b>Total for period:</b>',
                    'total_clicks' => $r['total_clicks'],
                    'adplex_clicks' => $r['adplex_clicks'],
                    'hosting_clicks' => $r['hosting_clicks'],
                    'country_clicks' => $r['country_clicks'],
                    'bot_clicks' => $r['bot_clicks'],
                    'isp_clicks' => $r['isp_clicks'],
                    'referer_clicks' => $r['referer_clicks'],
                    'ip_clicks' => $r['ip_clicks'],
                    'device_os_clicks' => $r['device_os_clicks'],
                    'url_clicks' => $r['url_clicks'],
                    'ua_clicks' => $r['ua_clicks'],
                    'time_clicks' => $r['time_clicks'],
                    'status_clicks' => $r['status_clicks'],
                    'headers_clicks' => $r['headers_clicks'],
                    'light_clicks' => $r['light_clicks'],
                    'super_clicks' => $r['super_clicks'],
                    'tz_clicks' => $r['tz_clicks'],
                    'js_mf_clicks' => $r['js_mf_clicks'],
                    'js_luminati_clicks' => $r['js_luminati_clicks'],
                    'browser_clicks' => $r['browser_clicks'],
                    'efilter1_clicks' => $r['efilter1_clicks'],
//                    'efilter2_clicks' => $r['efilter2_clicks'],
//                    'efilter3_clicks' => $r['efilter3_clicks'],
//                    'efilter4_clicks' => $r['efilter4_clicks'],
//                    'efilter5_clicks' => $r['efilter5_clicks'],
                    'total_blocked' => $total_blocked,
    );          
    
    
    $total_blocked = 0;

    if (date('d.m.Y') == date('d.m.Y', $date_to)) {
    
        db_set_active('stata');
        $res = db_select('day_cmp_stat_' .date('Y_m_d'), 'm');
        $res->addExpression('SUM(total_clicks)', 'total_clicks');
        $res->addExpression('SUM(adplex_clicks)', 'adplex_clicks');
        $res->addExpression('SUM(hosting_clicks)', 'hosting_clicks');
        $res->addExpression('SUM(country_clicks)', 'country_clicks');
        $res->addExpression('SUM(bot_clicks)', 'bot_clicks');
        $res->addExpression('SUM(isp_clicks)', 'isp_clicks');
        $res->addExpression('SUM(referer_clicks)', 'referer_clicks');
        $res->addExpression('SUM(ip_clicks)', 'ip_clicks');
        $res->addExpression('SUM(device_os_clicks)', 'device_os_clicks');
        $res->addExpression('SUM(url_clicks)', 'url_clicks');
        $res->addExpression('SUM(ua_clicks)', 'ua_clicks');
        $res->addExpression('SUM(time_clicks)', 'time_clicks');
        $res->addExpression('SUM(status_clicks)', 'status_clicks');
        $res->addExpression('SUM(headers_clicks)', 'headers_clicks');
        $res->addExpression('SUM(light_clicks)', 'light_clicks');
        $res->addExpression('SUM(super_clicks)', 'super_clicks');
        $res->addExpression('SUM(tz_clicks)', 'tz_clicks');
        $res->addExpression('SUM(js_mf_clicks)', 'js_mf_clicks');
        $res->addExpression('SUM(js_luminati_clicks)', 'js_luminati_clicks');
        $res->addExpression('SUM(browser_clicks)', 'browser_clicks');
        $res->addExpression('SUM(efilter1_clicks)', 'efilter1_clicks');
//        $res->addExpression('SUM(efilter2_clicks)', 'efilter2_clicks');
//        $res->addExpression('SUM(efilter3_clicks)', 'efilter3_clicks');
//        $res->addExpression('SUM(efilter4_clicks)', 'efilter4_clicks');
//        $res->addExpression('SUM(efilter5_clicks)', 'efilter5_clicks');          
        $res->condition('uid', $acc->uid);
        $r = $res->execute()->fetchObject();

        db_set_active('default');
        
        

        if ($r->total_clicks) {        
            $r = (array)$r;

            $total_blocked = $r['adplex_clicks'] + $r['hosting_clicks'] + $r['country_clicks'] + $r['bot_clicks'] + $r['isp_clicks'] + $r['referer_clicks'] + $r['ip_clicks'] + $r['device_os_clicks'] + $r['url_clicks']+$r['ua_clicks'] + $r['time_clicks'] + $r['status_clicks'] + $r['headers_clicks'] + $r['light_clicks'] + $r['super_clicks'] + $r['tz_clicks'] + $r['js_mf_clicks'] + $r['js_luminati_clicks'] + $r['browser_clicks'] + $r['efilter1_clicks']; // + $r['efilter2_clicks'] + $r['efilter3_clicks'] + $r['efilter4_clicks'] + $r['efilter5_clicks'];

            if (!$is_admin) {
                $r['bot_clicks'] += $r['adplex_clicks'];
                unset($r['adplex_clicks']);
            }    

            foreach ($r as $k => $v) {
                $total_row[$k] += $v;

                if (!in_array($k, array('total_clicks', 'total_blocked'))) {
                    $r[$k] = array('class' => array('hide-650'), 'data' => mcfn($v) .' <span class="percent">(' . mcfn($v / $r['total_clicks'] * 100, 2, false) . '%)</span>');
                }
            }


            $total_row['total_blocked'] += $total_blocked;

            $r = array('created' => date('d.m.Y')) + $r;
            $r['total_blocked'] = array('class' => array('total_blocked'), 'data-type' => $col_names['total_blocked'], 'data-value' => $total_blocked, 'data' => mcfn($total_blocked) .' <span class="percent">(' . mcfn($total_blocked / $r['total_clicks'] * 100, 2, false) . '%)</span>');
            $r['total_clicks'] = array('class' => array('total'), 'data-type' => $col_names['total'], 'data-value' => $r['total_clicks'], 'data' => mcfn($r['total_clicks']));

            if (!isset($_GET['page'])) {
                $rows[] = array('data-date' => date('d.m.Y'), 'data-cmps-link' => '/user/' .$acc->uid .'/statistics/' .date('Y-m-d'), 'class' => array('day-row'), 'data' => $r);
            }    
        }
    }

    
    $res = db_select('domain_compaigns_users_stat', 's');
//    $res->join('domain_compaigns', 'dc', 'dc.cid=s.cid');
    $res->fields('s', array('created'));
    
    $res->addExpression('SUM(total_clicks)', 'total_clicks');
    $res->addExpression('SUM(adplex_clicks)', 'adplex_clicks');
    $res->addExpression('SUM(hosting_clicks)', 'hosting_clicks');
    $res->addExpression('SUM(country_clicks)', 'country_clicks');
    $res->addExpression('SUM(bot_clicks)', 'bot_clicks');
    $res->addExpression('SUM(isp_clicks)', 'isp_clicks');
    $res->addExpression('SUM(referer_clicks)', 'referer_clicks');
    $res->addExpression('SUM(ip_clicks)', 'ip_clicks');
    $res->addExpression('SUM(device_os_clicks)', 'device_os_clicks');
    $res->addExpression('SUM(url_clicks)', 'url_clicks');
    $res->addExpression('SUM(ua_clicks)', 'ua_clicks');
    $res->addExpression('SUM(time_clicks)', 'time_clicks');
    $res->addExpression('SUM(status_clicks)', 'status_clicks');
    $res->addExpression('SUM(headers_clicks)', 'headers_clicks');
    $res->addExpression('SUM(light_clicks)', 'light_clicks');
    $res->addExpression('SUM(super_clicks)', 'super_clicks');  
    $res->addExpression('SUM(tz_clicks)', 'tz_clicks');
    $res->addExpression('SUM(js_mf_clicks)', 'js_mf_clicks');
    $res->addExpression('SUM(js_luminati_clicks)', 'js_luminati_clicks'); 
    $res->addExpression('SUM(browser_clicks)', 'browser_clicks');
    $res->addExpression('SUM(efilter1_clicks)', 'efilter1_clicks');
//    $res->addExpression('SUM(efilter2_clicks)', 'efilter2_clicks');
//    $res->addExpression('SUM(efilter3_clicks)', 'efilter3_clicks');
//    $res->addExpression('SUM(efilter4_clicks)', 'efilter4_clicks');
//    $res->addExpression('SUM(efilter5_clicks)', 'efilter5_clicks');    

    $res->condition('created', $date_from, '>=');
    $res->condition('created', $date_to, '<=');
    $res->condition('uid', $acc->uid);
    
    $res->groupBy('created');
    $res = $res->extend('PagerDefault')->limit(31);

    $res->orderBy('created', 'DESC');
    $data = $res->execute()->fetchAll();



    if (sizeof($data)) {
        foreach ($data as $_kk => $r) {
            $r = (array) $r;

            $total_blocked = $r['adplex_clicks'] + $r['hosting_clicks'] + $r['country_clicks'] + $r['bot_clicks'] + $r['isp_clicks'] + $r['referer_clicks'] + $r['ip_clicks'] + $r['device_os_clicks'] + $r['url_clicks']+$r['ua_clicks'] + $r['time_clicks'] + $r['status_clicks'] + $r['headers_clicks'] + $r['light_clicks'] + $r['super_clicks'] + $r['tz_clicks'] + $r['js_mf_clicks'] + $r['js_luminati_clicks'] + $r['browser_clicks'] + $r['efilter1_clicks']; // + $r['efilter2_clicks'] + $r['efilter3_clicks'] + $r['efilter4_clicks'] + $r['efilter5_clicks'];
            
            if (!$is_admin) {
                $r['bot_clicks'] += $r['adplex_clicks'];
                unset($r['adplex_clicks']);
            }            

            foreach ($r as $kk => $vv) {

                if (!in_array($kk, array('cid', 'created', 'total_clicks', 'total_blocked'))) {
                    $r[$kk] = array('class' => array('detailed hide-650'), 'data-type' => $col_names[$kk], 'data-value' => $vv, 'data' => $r['total_clicks'] ? mcfn($vv) . ' <span class="percent">(' . mcfn($vv / $r['total_clicks'] * 100, 2, false) . '%)</span>' : 0);
                }
            }

            $link_date = date('Y-m-d', $r['created']);
            $r['created'] = date('d.m.Y', $r['created']);
            $r['total_blocked'] = array('class' => array('total_blocked'), 'data-type' => $col_names['total_blocked'], 'data-value' => $total_blocked, 'data' => mcfn($total_blocked) . ' <span class="percent">(' . mcfn($total_blocked / $r['total_clicks'] * 100, 2, false) . '%)</span>');
            $r['total_clicks'] = array('class' => array('total'), 'data-type' => $col_names['total'], 'data-value' => $r['total_clicks'], 'data' => mcfn($r['total_clicks']));         
            unset($r['cid']);

            $rows[] = array('data-date' => $r['created'], 'data-cmps-link' => '/user/' .$acc->uid .'/statistics/' .$link_date, 'class' => array('day-row'), 'data' => $r);
        }
    }

    if (sizeof($rows)) {
        if ($total_row['total_clicks']) {
            
            if (!$is_admin) {
                $total_row['bot_clicks'] += $total_row['adplex_clicks'];
                unset($total_row['adplex_clicks']);
            }                        
            
            if ($is_admin) {
                $total_row['adplex_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['adplex_clicks'], 'data-value' => $total_row['adplex_clicks'], 'data' => mcfn($total_row['adplex_clicks']) . ' <span class="percent">(' . mcfn($total_row['adplex_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            }
            
            
            
            $total_row['hosting_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['hosting_clicks'], 'data-value' => $total_row['hosting_clicks'], 'data' => mcfn($total_row['hosting_clicks']) . ' <span class="percent">(' . mcfn($total_row['hosting_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['country_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['country_clicks'], 'data-value' => $total_row['country_clicks'], 'data' => mcfn($total_row['country_clicks']) . ' <span class="percent">(' . mcfn($total_row['country_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['bot_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['bot_clicks'], 'data-value' => $total_row['bot_clicks'], 'data' => mcfn($total_row['bot_clicks']) . ' <span class="percent">(' . mcfn($total_row['bot_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['isp_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['isp_clicks'], 'data-value' => $total_row['isp_clicks'], 'data' => mcfn($total_row['isp_clicks']) . ' <span class="percent">(' . mcfn($total_row['isp_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['referer_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['referer_clicks'], 'data-value' => $total_row['referer_clicks'], 'data' => mcfn($total_row['referer_clicks']) . ' <span class="percent">(' . mcfn($total_row['referer_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['ip_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['ip_clicks'], 'data-value' => $total_row['ip_clicks'], 'data' => mcfn($total_row['ip_clicks']) . ' <span class="percent">(' . mcfn($total_row['ip_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['device_os_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['device_os_clicks'], 'data-value' => $total_row['device_os_clicks'], 'data' => mcfn($total_row['device_os_clicks']) . ' <span class="percent">(' . mcfn($total_row['device_os_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['url_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['url_clicks'], 'data-value' => $total_row['url_clicks'], 'data' => mcfn($total_row['url_clicks']) . ' <span class="percent">(' . mcfn($total_row['url_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['ua_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['ua_clicks'], 'data-value' => $total_row['ua_clicks'], 'data' => mcfn($total_row['ua_clicks']) . ' <span class="percent">(' . mcfn($total_row['ua_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['time_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['time_clicks'], 'data-value' => $total_row['time_clicks'], 'data' => mcfn($total_row['time_clicks']) . ' <span class="percent">(' . mcfn($total_row['time_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['status_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['status_clicks'], 'data-value' => $total_row['status_clicks'], 'data' => mcfn($total_row['status_clicks']) . ' <span class="percent">(' . mcfn($total_row['status_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['headers_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['headers_clicks'], 'data-value' => $total_row['headers_clicks'], 'data' => mcfn($total_row['headers_clicks']) . ' <span class="percent">(' . mcfn($total_row['headers_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['light_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['light_clicks'], 'data-value' => $total_row['light_clicks'], 'data' => mcfn($total_row['light_clicks']) . ' <span class="percent">(' . round($total_row['light_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['super_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['super_clicks'], 'data-value' => $total_row['super_clicks'], 'data' => mcfn($total_row['super_clicks']) . ' <span class="percent">(' . mcfn($total_row['super_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            
            $total_row['tz_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['tz_clicks'], 'data-value' => $total_row['tz_clicks'], 'data' => mcfn($total_row['tz_clicks']) . ' <span class="percent">(' . mcfn($total_row['tz_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['js_mf_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['js_mf_clicks'], 'data-value' => $total_row['js_mf_clicks'], 'data' => mcfn($total_row['js_mf_clicks']) . ' <span class="percent">(' . mcfn($total_row['js_mf_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['js_luminati_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['js_luminati_clicks'], 'data-value' => $total_row['js_luminati_clicks'], 'data' => mcfn($total_row['js_luminati_clicks']) . ' <span class="percent">(' . mcfn($total_row['js_luminati_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');

            $total_row['browser_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['browser_clicks'], 'data-value' => $total_row['browser_clicks'], 'data' => mcfn($total_row['browser_clicks']) . ' <span class="percent">(' . mcfn($total_row['browser_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            $total_row['efilter1_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['efilter1_clicks'], 'data-value' => $total_row['efilter1_clicks'], 'data' => mcfn($total_row['efilter1_clicks']) . ' <span class="percent">(' . mcfn($total_row['efilter1_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
//            $total_row['efilter2_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['efilter2_clicks'], 'data-value' => $total_row['efilter2_clicks'], 'data' => mcfn($total_row['efilter2_clicks']) . ' <span class="percent">(' . mcfn($total_row['efilter2_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
//            $total_row['efilter3_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['efilter3_clicks'], 'data-value' => $total_row['efilter3_clicks'], 'data' => mcfn($total_row['efilter3_clicks']) . ' <span class="percent">(' . mcfn($total_row['efilter3_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
//            $total_row['efilter4_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['efilter4_clicks'], 'data-value' => $total_row['browser4_clicks'], 'data' => mcfn($total_row['efilter4_clicks']) . ' <span class="percent">(' . mcfn($total_row['efilter4_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
//            $total_row['efilter5_clicks'] = array('class' => array('detailed hide-650'), 'data-type' => $col_names['efilter5_clicks'], 'data-value' => $total_row['efilter5_clicks'], 'data' => mcfn($total_row['efilter5_clicks']) . ' <span class="percent">(' . mcfn($total_row['efilter5_clicks'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');
            
            $total_row['total_blocked'] = array('class' => array('total_blocked'), 'data-type' => $col_names['total_blocked'], 'data-value' => $total_row['total_blocked'], 'data' => mcfn($total_row['total_blocked']) . ' <span class="percent">(' . mcfn($total_row['total_blocked'] / $total_row['total_clicks'] * 100, 2, false) . '%)</span>');

            $total_row['total_clicks'] = array('class' => array('total'), 'data-type' => $col_names['total'], 'data-value' => $total_row['total_clicks'], 'data' => mcfn($total_row['total_clicks']));
        

            if (!$is_admin) {
                unset($total_row['adplex_clicks']);
            }        

            $rows[0] = array('class' => array('total-row'), 'data' => $total_row);

            $headers = array(array('class' => array('nohid'), 'data' => 'Date'),
                array('class' => array('nohid'), 'data' => $col_names['total']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['adplex_clicks']),
                array('class' => array('hide-650'), 'data' => $col_names['hosting_clicks']),
                array('class' => array('hide-650'), 'data' => $col_names['country_clicks']),
                array('class' => array('hide-650'), 'data' => $col_names['bot_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['isp_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['referer_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['ip_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['device_os_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['url_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['ua_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['time_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['status_clicks']),
                array('class' => array('hide-650'), 'data' => $col_names['headers_clicks']),
                array('class' => array('hide-650'), 'data' => $col_names['light_clicks']),
                array('class' => array('hide-650'), 'data' => $col_names['super_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['tz_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['js_mf_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['js_luminati_clicks']),
                
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['browser_clicks']),
                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['efilter1_clicks']),
//                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['efilter2_clicks']),
//                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['efilter3_clicks']),
//                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['efilter4_clicks']),
//                array('class' => array('hidbydefault hide-650'), 'data' => $col_names['efilter5_clicks']),
                
                array('class' => array('nohid'), 'data' => $col_names['total_blocked'])
            );

            if (!$is_admin) {
                unset($headers[2]);
            }

            $grafics = '';


                // web-сайт скрипта графиков: http://www.chartjs.org/docs/#bar-chart-introduction
                drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') . '/js/charts.min.js');
                drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') . '/js/show_charts2.js');
                $grafics = '<fieldset class="pad22 fs-grafs"><div class="graf-1"><div class="pre-graphs-txt" style="text-align: center">Click on bars and line dots to view detailed day statistics.</div>
        <canvas id="canvas"></canvas>
    </div>
    <div class="graf-2">
        <canvas id="pie"></canvas>
    </div></fieldset>';

            if (in_array($_GET['period_type'], array(7,8))) {
                $output .= '<style>.total-row {display: none}</style>';
            }

            $output .= $grafics . '<fieldset class="table-fs stat-table user-stat-table new-look align-right"><div class="column-switcher clearfix"></div><div class="clearfix">' . theme('table', array('class' => array('statistics-table'), 'header' => $headers, 'rows' => $rows)) .theme('pager') . '</div></fieldset>';
            
            $output .= '<div class="show-full-str show-full-str-large">'
                        . '     <div class="close-wrapper"><a class="close" href="#">x</a></div>'
                        . '     <div class="content-data"></div>'
                        . '</div>';            
        }    
        else {
            $output .= 'Not enough data to build statistics.';
        }
    } else {
        $output .= 'No statistic found.';
    }

    return $output;    
    
    }
    catch (Exception $e) {
//        print_r($e); die();
    }
}

function lx_domains_user_total_stat_campaigns($acc, $date) {
    date_default_timezone_set('UTC');
    
    $output = '';
    if (preg_match('/[0-9]{4}\-[0-9]{2}\-[0-9]{2}/', $date)) {
        
        $data = null;

        if ($date == date('Y-m-d')) {
            db_set_active('stata');
            
            $res = db_select('day_cmp_stat_' .date('Y_m_d'), 's');
            $res->fields('s');
            $res->condition('s.uid', $acc->uid);
            $res->condition('s.total_clicks', 0, '>');
            $data = $res->orderBy('s.total_clicks', 'DESC')->execute()->fetchAll();                    
            
            db_set_active('default');
            $date = explode('-', $date);
            $date = mktime(0,0,0,ltrim($date[1], '0'), ltrim($date[2], '0'), $date[0]);            
        }
        else {
            $date = explode('-', $date);
            $date = mktime(0,0,0,ltrim($date[1], '0'), ltrim($date[2], '0'), $date[0]);

            $res = db_select('domain_compaigns_cmp_stat', 's');
            $res->join('domain_compaigns', 'c', 's.cid=c.cid');
            $res->fields('s');
            $res->fields('c', array('name'));
            $res->condition('s.uid', $acc->uid);
            $res->condition('s.total_clicks', 0, '>');
            $res->condition('s.created', $date);
            $data = $res->orderBy('s.total_clicks', 'DESC')->execute()->fetchAll();
        }
        $rows = array();
        foreach ($data as $r) {
            $total_blocked = 0;
            $r = (array)$r;

            foreach ($r as $rk => $rv) {
                if (!in_array($rk, array('id', 'created', 'uid', 'cid','total_clicks', 'name'))) {
                    $total_blocked += $rv;
                }
            }
            
            if (!isset($r['name'])) {
                $r['name'] = db_select('domain_compaigns', 'c')
                                ->fields('c', array('name'))
                                ->condition('cid', $r['cid'])
                                ->execute()->fetchField();
                
                if (!$r['name']) {
                    $r['name'] = 'Campaign is deleted';
                }
            }

            $rows[] = array($r['name'], mcfn($r['total_clicks']), mcfn($total_blocked) .' <span class="percent">(' .mcfn($total_blocked / $r['total_clicks'] * 100, 2, false) .'%)</span>');
        }

        $output = '<h2>'.date('d.m.Y', $date).'</h2><div class="new-look align-right">' .theme('table', array('header' => array('Campaign', 'Total clicks', 'Safe clicks'), 'rows' => $rows)) .'</div>';
    }
    else {
        $output = 'No statistics found for selected period.';
    }
    
    print $output;
}

//function __set_users_day_stat() {
//    
//    $uids = db_query('SELECT DISTINCT(uid) as uid FROM {domain_compaigns_cmp_stat}')->fetchAll();
//    foreach ($uids as $u) {
//        $res = db_select('domain_compaigns_cmp_stat', 's');
//    //    $res->join('domain_compaigns', 'dc', 'dc.cid=s.cid');
//        $res->fields('s', array('created', 'uid'));
//        $res->addExpression('SUM(total_clicks)', 'total_clicks');
//        $res->addExpression('SUM(adplex_clicks)', 'adplex_clicks');
//        $res->addExpression('SUM(hosting_clicks)', 'hosting_clicks');
//        $res->addExpression('SUM(country_clicks)', 'country_clicks');
//        $res->addExpression('SUM(bot_clicks)', 'bot_clicks');
//        $res->addExpression('SUM(isp_clicks)', 'isp_clicks');
//        $res->addExpression('SUM(referer_clicks)', 'referer_clicks');
//        $res->addExpression('SUM(ip_clicks)', 'ip_clicks');
//        $res->addExpression('SUM(device_os_clicks)', 'device_os_clicks');
//        $res->addExpression('SUM(url_clicks)', 'url_clicks');
//        $res->addExpression('SUM(ua_clicks)', 'ua_clicks');
//        $res->addExpression('SUM(time_clicks)', 'time_clicks');
//        $res->addExpression('SUM(status_clicks)', 'status_clicks');
//        $res->addExpression('SUM(headers_clicks)', 'headers_clicks');
//        $res->addExpression('SUM(light_clicks)', 'light_clicks');
//        $res->addExpression('SUM(super_clicks)', 'super_clicks');    
//        $res->condition('s.uid', $u->uid);    
//        $res->groupBy('s.created', 's.uid');
//        $res->orderBy('created', 'ASC');
//        $res->orderBy('uid', 'ASC');
//        $data = $res->execute()->fetchAll();    
//        
//        if (sizeof($data)) {
//            foreach ($data as $r) {
//                $r = (array)$r;
//                
//                db_insert('domain_compaigns_users_stat')
//                    ->fields($r)->execute();
//            }
//        }
//    } 
//    
//    print 'ok';
//}