<?php

function lx_payments_tarifs_list($plans_packet = '') {
    global $user;
    
    drupal_add_css(drupal_get_path('module', 'lx_domains_cmp') .'/css/pricing.css');
    
    $nodes = node_load_multiple(array(), array('type' => 'tarif', 'status' => 1));
    
    
    $output = '<fieldset class="pad12"><div class="pricing-table princig-table-flat spaced">';
    
    $i = 1;
    
    $buy_function_exists = function_exists('lx_payments_pay_form');
    
    $user = user_load($user->uid);
    $user_has_special = false;
    
    $can_buy = true;
    if ($user->field_active_status['und'][0]['value'] == 200) {
        $can_buy = false;
        drupal_set_message('You have active subscription already. If you want to change pricing plan you need cancel your active subscription on Payments page.');
    }        
    
    $packets = explode(',', variable_get('lx_payment_pricing_packs', 'nonepack'));
    foreach ($packets as $k => $v) {
        $packets[$k] = trim($v);
    }
    
    foreach ($nodes as $node) {
        $skip_plan = false;
        $is_special = @(int)$node->field_special_tarif['und'][0]['value'];
        
        if ($is_special) {
            if ($user->field_current_tarif_id['und'][0]['nid'] == $node->nid) {
                $user_has_special = true;
            }
            
            continue;
        }    
        
        // если специальный пакет тарифов (например, для кибер понедельника или черной пятницы)
        if ($plans_packet) {
           if (strpos($node->field_stripe_plan_id['und'][0]['value'], $plans_packet) === false) {
               continue;
           } 
        }
        else {
            // если обычные тарифы, то нужно исключить все спецпакеты
            foreach ($packets as $pack) {
                if (strpos($node->field_stripe_plan_id['und'][0]['value'], $pack) !== false) {
                    $skip_plan = true;
                    break;
                }
            }
        }
        
        if ($skip_plan) continue;
        
        
        $output .= '<div class="col-md-3">
                <div class="plan">
                    <h3>' .$node->title .($i == 1 ? '<em class="desc">Most Popular</em>' : '')  .'<span>$' .$node->field_tarif_cost['und'][0]['value'] .'</span></h3>
                    <ul>
                        <li>Unlimited access for <b>' .$node->field_renewal_frequency_txt['und'][0]['value'] .'</b></li>
                        <li>Total for period $' .$node->field_total_for_period['und'][0]['value'] .'</li>
                        <li>Discount: $' .$node->field_discount['und'][0]['value'] .'</li>
                        <li>' .($node->nid == 263 ? '10 Millions click per day included.<br />Extra clicks for $1 per million.' : 'Unlimited access') .'</li>
                        <li>Spy services & bots protection</li>
                        <li>Competitor’s detecting</li>
                        <li>24 hours support</li>
                    </ul>
                    ' .($buy_function_exists && $can_buy ? lx_payments_pay_form($node) : '') .'
                </div>
            </div>';
        
        $i++;
    }      
    
    if ($user_has_special && ($user->uid != 1)) {
        $output = '<fieldset class="pad12"><div style="font-size: 16px">You have special pricing plan.<br />If you want to change it, <a href="mailto:info@magicchecker.com?subject=I+want+to+change=pricing+plan">write to us</a>, please.<br /><br /><a href="/renew">Back to Renew page</a>';
    }
    
    $output .= '</div></fieldset>';
    
    drupal_set_title('Pricing plans');
    
    return $output;
}


function lx_payments_prolong_page() {
    global $user;
    
    drupal_add_css(drupal_get_path('module', 'lx_domains_cmp') .'/css/pricing.css');
    
    $user = user_load($user->uid);
    
    if ($user->field_active_status['und'][0]['value'] == 200) {
        drupal_set_message('You have active subscription already. If you want to resubscribe you need cancel your active subscription.');
        drupal_goto('user/' .$user->uid .'/payments');
    }    
    
    $output = '';
    
    $cur_tarif = (int)@$user->field_current_tarif_id['und'][0]['nid'];
    
    if ($cur_tarif) {
        $output = '<fieldset class="pad12"><div class="pricing-table princig-table-flat">';
        
        $node = node_load($cur_tarif);
        
        $discount = @$node->field_discount['und'][0]['value'];
        
        $output .= '<p style="text-align: center">Your current tariff is</p><div class="col-md-3">
                <div class="plan">
                    <h3>' .$node->title .'<span>$' .$node->field_tarif_cost['und'][0]['value'] .'</span></h3>                     
                    <ul>
                        <li>Unlimited access for <b>' .$node->field_renewal_frequency_txt['und'][0]['value'] .'</b></li>
                        <li>Total for period $' .$node->field_total_for_period['und'][0]['value'] .'</li>
                        ' .($discount ? '<li>Discount: $' .$node->field_discount['und'][0]['value'] .'</li>' : '') .'
                        <li>' .($node->nid == 263 ? '10 Millions click per day included.<br />Extra clicks for $1 per million.' : 'Unlimited access') .'</li>
                        <li>Spy services & bots protection</li>
                        <li>Competitor’s detecting</li>
                        <li>24 hours support</li>                            
                    </ul>
                    ' .(function_exists('lx_payments_pay_form') ? lx_payments_pay_form($node) : '') .'   
                </div>
            </div><p style="text-align:center">You can renew it or <a href="/tarifs' .(isset($_GET['chk2']) ? '?chk2' : '') .'">choose another pricing plan</a>...</p>
            ';     
        
        $output .= '</div></fieldset>';
        
//        if (user_access('administer users')) {
//            $output .= '<script src="https://www.2checkout.com/static/checkout/javascript/direct.min.js"></script>';
//        }
    }
    else {
        drupal_goto('tarifs');
    }
    
    return $output;
}

// получение оплаты от платежной системы
function lx_payments_confirm_payments() {
    
    if ($success_paid) {
        lx_payments_give_days_to_user($uid, $days, $tarif_id);
    }    
}


// устанавливает активность аккаунта на кол-во дней
function lx_payments_give_days_to_user($uid, $days, $tarif_id = 0) {
    $acc = user_load($uid);
    
    $actve_till = (int)$acc->field_active_till['und'][0]['value'];
    
    if ($actve_till > time()) {
        $acc->field_active_till['und'][0]['value'] = $acc->field_active_till['und'][0]['value'] + $days*86400;
    }
    else {
        $acc->field_active_till['und'][0]['value'] = time() + $days*86400;
    }
    
    if ($tarif_id) {
        $acc->field_current_tarif_id['und'][0]['value'] = $tarif_id;
    }    
    
    user_save($acc);
}