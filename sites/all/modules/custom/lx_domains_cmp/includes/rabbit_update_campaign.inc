<?php

/**
 *  Отправляет запрос на обновление объекта кампании в index.php всех серверов
 */

//require_once '/home/api173magic/data/www/api.magicchecker.com/vendor/autoload.php';
//require_once '/home/user173magic/data/www/magicchecker.com/sites/all/modules/custom/lx_domains_cmp/sconfig.php';
//
//use PhpAmqpLib\Connection\AMQPStreamConnection;
//use PhpAmqpLib\Message\AMQPMessage;


// обновляет статус всем кампаниям пользователя
// например при блокировке за неуплату или при включении после оплаты
function __set_status_all_user_campaigns($acc, $op, $set_status_to = null) {
//    $uid = is_object($acc) ? $acc->uid : $acc;
//    
//    $db = new PDO("mysql:host=" .LX_MYSQLI_HOST .";dbname=" .LX_MYSQLI_DB, LX_MYSQLI_USER, LX_MYSQLI_PASS);  
//    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );      
//    
//    if ($op == 'start_all') {
//        // оплата поступила - переводим все кампании в статус, который был до блокировки
//        $sth = $db->prepare('UPDATE ' .LX_MYSQLI_DB_PREFIX .'domain_compaigns SET status=stored_status WHERE uid=:uid AND status=:status');
//        $vals = array('uid' => $uid, 'status' => 403);
//        $sth->execute($vals);
//        
//        _rabbit_update_campaign(0, $uid);
//    }    
//    else {
//        // какой-то вид блокировки. определяется статусом $set_status_to
//        $sth = $db->prepare('UPDATE ' .LX_MYSQLI_DB_PREFIX .'domain_compaigns SET stored_status=status, status=:status WHERE uid=:uid');
//        $vals = array('status' => $set_status_to, 'uid' => $uid);
//        $sth->execute($vals);        
//        
//        _rabbit_update_campaign(0, $uid);
//
//        if ($set_status_to == 403) {
//            
//            $sth = $db->prepare('SELECT uid, name, mail FROM ' .LX_MYSQLI_DB_PREFIX .'users WHERE uid=:uid');
//            $vals = array('uid' => $uid);
//            $sth->execute($vals);
//            
//            $acc = $sth->fetch(PDO::FETCH_OBJ);
//            
//            $subject = variable_get('notify_campaigns_403_mail_subject', '');
//            $message = variable_get('notify_campaigns_403_mail_text', '');
//
//            $message = nl2br($message);
//
//            $headers = "MIME-Version: 1.0\r\n";
//            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
//
//            $headers .= "From: MagicChecker.com <info@magicchecker.com>\r\n";
//            $headers .= "To: " .$acc->mail . "\r\n";
//
//            mail($acc->mail, $subject, $message, $headers);             
//            
//            
//            $subject = 'User account is suspended.';
//            $message =  "User " .$acc->name ." is suspended. <br /><br />https://clients.magicchecker.com/user/" .$acc->uid ."/edit"
//                       . "<br /><br />MagicChecker.com";
//
//
//            /* Для отправки HTML-почты вы можете установить шапку Content-type. */
//            $headers = "MIME-Version: 1.0\r\n";
//            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
//
//            /* дополнительные шапки */
//            $headers .= "From: MagicChecker.com <info@magicchecker.com>\r\n";
//            $headers .= "To: info@magicchecker.com\r\n";            
//            mail('info@magicchecker.com', $subject, $message, $headers);
//        }         
//    }
//    
//    $db = null;
}


function _rabbit_update_campaign($cmp_id, $uid = null, $delete = false) {    
//    global $_api_hosts; // defined in scongig.php
//
//    $own_stamps = null;
//    
//    $db = new PDO("mysql:host=" .LX_MYSQLI_HOST .";dbname=" .LX_MYSQLI_DB, LX_MYSQLI_USER, LX_MYSQLI_PASS);  
//    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
//
//    if (empty($uid)) {
//        $STH = $db->prepare("SELECT * FROM " .LX_MYSQLI_DB_PREFIX ."domain_compaigns WHERE cid=:cid");
//        $fields = array('cid' => $cmp_id);  
//    }
//    else {
//        $STH = $db->prepare("SELECT * FROM " .LX_MYSQLI_DB_PREFIX ."domain_compaigns WHERE uid=:uid");
//        $fields = array('uid' => $uid);
//    }
//    $STH->execute($fields);
//    
//    $vars = array();
//    while ($var = $STH->fetch(PDO::FETCH_OBJ)) {
//        if (!empty($var) && in_array($var->status, array(7, 101, 102, 103))) { 
//            $var->data = unserialize($var->data);
//            
//            if (empty($own_stamps)) {
//                $has_own_stamps = $db->query("SELECT field_user_stamps_value FROM " .LX_MYSQLI_DB_PREFIX ."field_data_field_user_stamps WHERE entity_id=" .$var->uid)->fetchColumn();
//                if ($has_own_stamps) {
//                    $own_stamps = array();
//                    $rows = explode("\n", $has_own_stamps);
//                    if (sizeof($rows)) {
//                        foreach ($rows as $r) {
//                            if (trim($r)) {
//                                $r = explode('||', trim($r));
//                                if (!isset($own_stamps[$r[0]])) {
//                                    $own_stamps[$r[0]] = array();
//                                }
//                                if (!isset($own_stamps[$r[0]][$r[1]])) {
//                                    $own_stamps[$r[0]][$r[1]] = array();
//                                }
//                                if (!isset($own_stamps[$r[0]][$r[1]][$r[2]])) {
//                                    $own_stamps[$r[0]][$r[1]][$r[2]] = array();
//                                }
//                                if (!isset($own_stamps[$r[0]][$r[1]][$r[2]][$r[3]])) {
//                                    $own_stamps[$r[0]][$r[1]][$r[2]][$r[3]] = array();
//                                }
//                                
//                                $own_stamps[$r[0]][$r[1]][$r[2]][$r[3]][] = $r[4];
//                            }
//                        }
//                    }
//                }    
//            }
//            
//            if (!empty($own_stamps)) {
//                $var->own_stamps = $own_stamps;
//            }
//
//            
//        }
//        else {
//            $_var = new stdClass;
//            $_var->cid = $var->cid;
//            $_var->status = !empty($var) ? $var->status : 0;
//
//            $var = $_var;
//        }
//        
//        $var->delete_campaign = 0;
//        
//        if ($delete) {
//            $var->delete_campaign = 1;
//        }
//        
//        $vars[] = $var;
//    }    
//    
//    $STH = null;
//    $db = null;
//
//    foreach ($_api_hosts as $_host) {
//        try {
//            $_host = explode('|',$_host); 
//            $_connection = new AMQPStreamConnection($_host[0], $_host[1], $_host[2], $_host[3]);
//            $_channel = $_connection->channel();
//
//            $_channel->queue_declare('update_campaign', false, true, false, false);
//
//            foreach ($vars as $var) {
//                $_msg = new AMQPMessage(serialize($var), array('delivery_mode' => 2));
//                $_channel->basic_publish($_msg, '', 'update_campaign');
//            }
//            $_channel->close();
//            $_connection->close();    
//        }
//        catch (Exception $e) {
//            
//        }
//    }    
}

/// удаление кампании с боевых
// $cmps - массив id кампаний
function _rabbit_delete_campaign($cmps) {    
//    global $_api_hosts; // defined in scongig.php
//
//    $vars = array();
//
//    foreach ($cmps as $cmp_id) {
//        $_var = new stdClass;
//        $_var->cid = $cmp_id;
//        $_var->status = 0;
//        $_var->delete_campaign = 1;    
//
//        $vars[$cmp_id] = $_var;
//    }    
//
//
//    foreach ($_api_hosts as $_host) {
//        try {
//            $_host = explode('|',$_host); // 88.99.31.4|5672|admin173|kwAEsDjtWuG3NC
//            $_connection = new AMQPStreamConnection($_host[0], $_host[1], $_host[2], $_host[3]);
//            $_channel = $_connection->channel();
//
//            $_channel->queue_declare('update_campaign', false, true, false, false);
//
//            foreach ($vars as $var) {
//                $_msg = new AMQPMessage(serialize($var), array('delivery_mode' => 2));
//                $_channel->basic_publish($_msg, '', 'update_campaign');
//            }
//            $_channel->close();
//            $_connection->close();    
//        }
//        catch (Exception $e) {
//            
//        }
//    }    
}