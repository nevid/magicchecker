<?php

define('AUD_QUEUE_TIME_NOTIFY', 30);
define('AUD_QUEUE_NOTIFY_MAIL', 'alexei.vertal@yandex.ru');

// добавление кампании в очередь на добавление (a)/обновление (u)/удаление (d) на боевых
// $op = 0 - добавление/обновление
// $op = 1 - удаление
function lx_domains_cmp_add_to_aud_queue($cid, $op = 0) {
    db_query('INSERT IGNORE INTO {cmp_update_delete_queue} (created, cid, op) VALUES (:created, :cid, :op)', [':created' => time(), ':cid' => $cid, ':op' => $op]);
}

function lx_domains_cmp_aud_test_notify($op, $cmps) {
    $subject = 'AUD queue: campaigns ' .($op == 0 ? 'added/updated' : 'deleted');
    $mess = 'Hi!<br /><br />'
            . 'Campaigns ' .implode('<br />', $cmps) . ' ' .($op == 0 ? 'added/updated' : 'deleted')
            . '<br /><br />'
            . 'MagicChecker.com team';

    /* Для отправки HTML-почты вы можете установить шапку Content-type. */
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

    /* дополнительные шапки */
    $headers .= "From: MagicChecker.com <info@magicchecker.com>\r\n";

    mail(AUD_QUEUE_NOTIFY_MAIL, $subject, $mess, $headers);      
}

function lx_domains_cmp_process_aud_queue($op, $limit = 1) {
    $cmps = lx_domains_cmp_get_from_aud_queue($op, $limit);
    if (sizeof ($cmps)) {
        include_once './sites/all/modules/custom/lx_domains_cmp/includes/rabbit_update_campaign.inc';
        
        foreach ($cmps as $cmp_id) {
            if ($op == 1) {
                // удаление (по базе друпала)
                lx_domains_cmp_delete_campaign_process($cmp_id);
            }
            else {
                // обновление на боевых
                _rabbit_update_campaign($cmp_id);
            }
        }

        if ($op == 0) {
            variable_set('lx_aud_update_last_sent', date('Y-m-d H:i:s') .': ' .sizeof($cmps));
        }
        else {
            // удаление на боевых
            _rabbit_delete_campaign($cmps);
            variable_set('lx_aud_delete_last_sent', date('Y-m-d H:i:s') .': ' .sizeof($cmps));
            // уведомление об удалении
//            lx_domains_cmp_aud_test_notify($op, $cmps);
        }
    }    
}

// выборка кампаний из очереди (при этом запись сразу удаляется)
function lx_domains_cmp_get_from_aud_queue($op = 0, $limit = 1) {
    $cids = [];
    $res = db_query('SELECT id, cid FROM {cmp_update_delete_queue} WHERE op=:op ORDER BY id ASC LIMIT 0, '.$limit, [':op' => $op])->fetchAll();
    if (sizeof($res)) {
        foreach ($res as $r) {
           $cids[$r->cid] = $r->cid; 
           db_delete('cmp_update_delete_queue')
                ->condition('id', $r->id)
                ->execute();
        }
    }
    
    return $cids;
}

// очистка очереди
function lx_domains_cmp_flush_aud_queue($first_item_time) {
    db_query('TRUNCATE {cmp_update_delete_queue}');
}

function lx_domains_cmp_aud_notify_on_mail($data) {
    if ($data->cnt && ((time() - $data->mn) >= AUD_QUEUE_TIME_NOTIFY)) {
        $mail_already_sent = variable_get('cmp_aud_notify_on_mail_at', 0);
        if ($mail_already_sent < time()) {
            $subject = 'AUD queue alert!';
            $mess = 'Hi!<br /><br />'
                    . 'AUD queue status:<br /><br />'
                    . 'total: ' .$data->cnt .'<br />'
                    . 'minDate: ' .date('Y-m-d H:i:s', $data->mn).'<br />'
                    . 'maxDate: ' .date('Y-m-d H:i:s', $data->mx)
                    . '<br /><br />'
                    . 'MagicChecker.com team';

            /* Для отправки HTML-почты вы можете установить шапку Content-type. */
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

            /* дополнительные шапки */
            $headers .= "From: MagicChecker.com <info@magicchecker.com>\r\n";

            mail(AUD_QUEUE_NOTIFY_MAIL, $subject, $mess, $headers);    
            
            variable_set('cmp_aud_notify_on_mail_at', time() + 300); // письмо шлем не чаще чем через 5 минут
        }    
    }    
}

// состояние очереди: сколько записей в ней, и даты первой и последней записи в очереди
function lx_domains_cmp_aud_queue_status($op = 0, $notify_on_mail = false) {
    $res = db_query('SELECT count(id) as cnt, min(created) as mn, max(created) as mx FROM {cmp_update_delete_queue} WHERE op=:op', [':op' => $op])->fetchObject();
    
    $output = ['total' => 0, 'minDate' => '', 'maxDate' => ''];
    if (!empty($res)) {
        $output['total'] = $res->cnt;
        $output['minDate'] = $res->mn ? date('Y-m-d H:i:s', $res->mn) : '';
        $output['maxDate'] = $res->mx ? date('Y-m-d H:i:s', $res->mx) : '';
        $output['lastUpdate'] = ($op == 0) ? variable_get('lx_aud_update_last_sent', '') : variable_get('lx_aud_delete_last_sent', '');
        
        if ($notify_on_mail && ($op == 0)) {
            lx_domains_cmp_aud_notify_on_mail($res);
        }        
    }
    
    return $output;
}

