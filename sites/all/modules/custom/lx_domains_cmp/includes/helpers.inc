<?php

require '/home/api173magic/data/www/api.magicchecker.com/vendor/autoload.php';

use MaxMind\Db\Reader;  

function helperCountryByIp($real_ip) {
    $databaseFile = '/etc/php5/GeoIP/GeoIP2-Country_20160524/GeoIP2-Country.mmdb';
    $reader = new Reader($databaseFile);
    $data = $reader->get($real_ip);
    $reader->close();
    
    $country = '';
    $blocked = 0;
    
    if (isset($data['country'])) {
        $country = $data['country']['iso_code'];  
    }
    else if (isset($data['registered_country'])) {
        $country = $data['registered_country']['iso_code'];  
    }
    
    return $country;
}