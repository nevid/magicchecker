<?php

/*
 * Теги к кампаниям 
 * nfrd_cmp_tags_users - таблица тегов юзеро
 * nfrd_cmp_tags - связка кампании и тегов
 */

function lx_tag_autocomplete() {
    global $user;
    
    $string = strtolower(trim(check_plain($_GET['term'])));
    $exclude = strtolower(trim(check_plain($_GET['exclude'])));
    
    if ($exclude) {
        $exclude = explode(',',$exclude);
        foreach ($exclude as $k => $v) {
            if (!trim($exclude[$k])) {
                unset($exclude[$k]);
            }
        }
    }
    else {
        $exclude = array();
    }
    
    $tags_list = campaign_tags_get_user_tags($user->uid);

    $matches = array();
    if (strlen($string) >= 1) {
        if (sizeof($tags_list)) {
            foreach ($tags_list as $k => $c) {
              if (strpos(strtolower($c), $string) !== false && !in_array($k, $exclude)) {  
                  $matches[] = (object)array('id' => $k, 'value' => check_plain($c));
              }    
              if (sizeof($matches) == 10) break;
            }
        }    
    }

    drupal_json_output($matches);    
}

// получаем теги кампании
function campaign_tags_get_campaign_tags($cid) {
    $tags = array();
    $res = db_select('cmp_tags_users', 't');
    $res->join('cmp_tags', 'ct', 'ct.tag_id=t.id');
    $res->fields('t', array('id', 'tag'));
    $res->condition('ct.cid', $cid);
    $data = $res->execute()->fetchAll();
    if (sizeof($data)) {
        foreach ($data as $r) {
           $tags[$r->id] = $r->tag; 
        }
    }
    
    return $tags;
}

// получаем все теги юзера
function campaign_tags_get_user_tags($uid) {
    $tags = array();
    $res = db_select('cmp_tags_users', 't')
                ->fields('t', array('id', 'tag'))
                ->condition('uid', $uid)
                ->execute()->fetchAll();
    if (sizeof($res)) {
        foreach ($res as $r) {
           $tags[$r->id] = $r->tag; 
        }
    }
    
    return $tags;
}

// получаем теги юзера по id тегов
function campaign_tags_get_tags_by_ids($ids) {
    global $user;
    $tags = array();
    $res = db_select('cmp_tags_users', 't')
                ->fields('t', array('id', 'tag'))
                ->condition('uid', $user->uid)
                ->condition('id', $ids, 'IN')
                ->execute()->fetchAll();
    if (sizeof($res)) {
        foreach ($res as $r) {
           $tags[] = $r; 
        }
    }
    
    return $tags;
}

// добавление нового тэга в базу
function campaign_tags_add_new($uid, $cid, $tag) {
    $tag_id = (int)db_select('cmp_tags_users', 'u')
                ->fields('u', array('id'))
                ->condition('uid', $uid)
                ->condition('tag', $tag)
                ->execute()->fetchField();

    if (!$tag_id) {
        $tag_id = db_insert('cmp_tags_users')
                    ->fields(array('uid' => $uid, 'tag' => $tag))
                    ->execute();
    }    

    db_query('INSERT IGNORE INTO {cmp_tags} (cid, tag_id) '
            . '         VALUES (:cid, :id)', array(':cid' => $cid, ':id' => $tag_id));    
}

// сохраняем теги кампании
function campaign_tags_set($uid, $cid, $tags) {
    db_delete('cmp_tags')
        ->condition('cid', $cid)
        ->execute();
    
    
    if (sizeof($tags)) {
        foreach ($tags as $tag) {
            if (is_numeric($tag)) {
                db_query('INSERT IGNORE INTO {cmp_tags} (cid, tag_id) '
                        . '         VALUES (:cid, :id)', array(':cid' => $cid, ':id' => $tag));
            }
            else {
                campaign_tags_add_new($uid, $cid, $tag);
            }
        }
    }
}

function campaign_tags_get_tag_id_by_name($tag) {
    global $user;
    
    $id = (int)db_select('cmp_tags_users', 'u')
                ->fields('u', array('id'))
                ->condition('uid', $user->uid)
                ->condition('tag', $tag)
                ->execute()->fetchField();
    
    return $id;
}