<?php

function __get_remote_server_types() {
    return ['workers'   => LX_WORKER_RABBIT_CONNECTION,
            'mediators' => LX_MEDIATOR_RABBIT_CONNECTION];    
}

function __get_defined_server_types_ips($server_type) {
    $ips = [];
    $data = file(LX_REMOTE_SERVERS_IPS_FOLDER .'/' .$server_type .'_ips.txt');
    foreach ($data as $ip) {
        $ips[] = trim($ip);
    }
    
    return $ips;
}

function __build_api_hosts() {
    $ips = [];
    $server_types = __get_remote_server_types();
    
    foreach ($server_types as $st_name => $st_data) {
        $server_ips = __get_defined_server_types_ips($st_name);
        foreach ($server_ips as $ip) {
            $ips[] = trim($ip) .'|' .$st_data;
        }
    }
    
    return $ips;
}