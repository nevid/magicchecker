<?php

require_once getcwd() .'/sites/all/libraries/phpclickhouse/include.php';
//require(drupal_get_path('module', 'lx_domains_cmp') . '/sconfig.php');

function ch_user_query($query, $values = [],$res_type = 'rows', $with_total_count = false, $use_clients_config = true) {
    global $click_house_config;
    global $click_house_webuser_config;
    static $db;
    
    $output = '';
    $s_time = microtime(true);
    
    $total_count = -1;

    try {
        if ($query) {
            if (empty($db)) {
                $config = $use_clients_config ? $click_house_webuser_config : $click_house_config;
                $db = new ClickHouseDB\Client($config);
                $db->database('default');
                $db->setTimeout(600);      // 1500 ms
                $db->setConnectTimeOut(5); // 5 seconds   
            }    

            $res = $db->select($query, $values);
            
            if ($with_total_count) {
//                $count_query = '';
//                $count_query_begin = strpos($query, '(');
//                $count_query_end = strpos($query, 'ORDER');
//                $count_query = str_replace(' id ', ' count(id) as cnt ',substr($query, $count_query_begin+1, ($count_query_end-$count_query_begin-2)));
//                $res_cnt = $db->select($count_query);
//                $total_count = $res_cnt->fetchOne();
                $total_count = $res->countAll();
            }

            if ($res_type == 'rows') {
                $output = $res->rows();
            }    
        }
    }
     catch (Exception $e) {
        lx_magic_log2('Clickhouse user log error', 'Clickhouse user log error', $e->getMessage(), 200, true);
        print 'error';
        die();
     }

    $db = null;
    
    $execution_time = number_format((microtime(true) - $s_time)*1000,2);
    return ['result' => $output, 'execution_time' => $execution_time, 'total_count' => $total_count];
}

function ch_user_query_csv($query) {
    global $click_house_config;
    static $db;
    
    $output = '';
    $s_time = microtime(true);

    try {
        if ($query) {
            if (empty($db)) {
                $db = new ClickHouseDB\Client($click_house_config);
                $db->database('default');
                $db->setTimeout(600);      // 1500 ms
                $db->setTimeout(600);       // 10 seconds
                $db->setConnectTimeOut(600); // 5 seconds   
            }    

            $filename = dirname(__FILE__) .'/csv_export/' .time() .'.csv';
            
//            $fields = '';
//            $fields_end = strpos($query, 'FROM');
//            $fields = str_replace('SELECT', '',substr($query, 0, ($fields_end-1)));
//            $f = fopen($filename, 'w');
//            fputs($f, $fields ."\n");
//            fclose($f);
            
            $WriteToFile=new ClickHouseDB\WriteToFile($filename);
            $db->select($query,[],null,$WriteToFile);
            
            exec('gzip ' .$filename);
        }
    }
     catch (Exception $e) {
         print_r($data);
         print "\n\n";
         print substr($e->getMessage(), 0, 512);
         die();
     }

    $db = null;    
    
    $execution_time = number_format((microtime(true) - $s_time)*1000,2);
    return ['filename' => $filename.'.gz', 'execution_time' => $execution_time];
}

// получаем смещение по таймзоне, если установлено
function lx_get_user_timezone_offset($acc) {
    $offset = 0;
    
    if ($acc->timezone) {
        $z = new DateTimeZone($acc->timezone);
        $c = new DateTime(null, $z);
        $offset = $z->getOffset($c);
    }    
    
    return $offset;
}

function lx_domains_cmp_ch_show_log($acc, $cid, $date) {
    global $user;
    
    drupal_add_css(drupal_get_path('module', 'lx_domains_cmp') .'/css/blur.css');
//    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp').'/js/tip_window.js');
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/js/blur.js');

//    if ($user->uid == 1) {
//        if (isset($_GET['op']) && ($_GET['op'] == 'Export')) {
//            lx_domains_cmp_export_log($acc, $cid, $date);
//        }
//    }
    
    $output = '';

    $cmp = __can_view_users_campaigns($acc, $cid);
    $is_admin = user_access('administer users');
    
    $first = 0;
    $last = 0;
    
    if (!empty($cmp)) {

        if (!isset($_GET['ajax'])) {
            $form = drupal_get_form('lx_domains_ch_cmp_log_filters', $cmp);
            $output .= drupal_render($form) .'<div class="ajaxContent"></div>';
            $output .= '<div class="show-full-str">'
                    . '     <div class="close-wrapper"><a class="close" href="#">x</a></div>'
                    . '     <div class="content-data"></div>'
                    . '</div>';
            $dst = drupal_get_destination();
            return $output;
        }
        else {
            print lx_domains_cmp_ch_show_log_content($acc, $cid, $date);
            die();
        }
        
    } else {
        drupal_goto('user/' . $acc->uid . '/campaigns');
    }

    return $output;
}

function lx_domains_cmp_ch_show_log_content($acc, $cid, $date) {
    global $user;

//    if ($user->uid == 1) {
//        if (isset($_GET['op']) && ($_GET['op'] == 'Export')) {
//            lx_domains_cmp_export_log($acc, $cid, $date);
//        }
//    }
    
    $cmp = __can_view_users_campaigns($acc, $cid);
    if (empty($cmp)) {
        print '<script>location.href="/user/' . $acc->uid . '/campaigns";</script>';
    }
    
    $date_from = null;
    $date_to = null;
    
    $ch_cur_show_date = $date;
    $ch_cur_date_range = [];
    
    $period_type = isset($_GET['period_type']) ? (int)$_GET['period_type'] : 8;
    if (!$period_type) {
        $period_type = 8;
    }
    
    
    
    if ($period_type == 100 && isset($_GET['dates']) && preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4} \- [0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $_GET['dates'])) {
        $dates = explode(' - ', $_GET['dates']);
        $date_from = implode('-',array_reverse(explode('.', $dates[0])));
        $date_to = implode('-',array_reverse(explode('.', $dates[1])));

    }
    else {
        if ($period_type == 1) {
            $date_from =  date("Y-m-d", strtotime("this Monday"));
           //воскресенье прошлой недели
           $date_to = date('Y-m-d');
        }
        else if ($period_type == 6) {
           //понедельник прошлой недели
           $date_from =  date("Y-m-d", strtotime("last Monday"));
           //воскресенье прошлой недели
           $date_to =  date("Y-m-d", strtotime("last Sunday"));            
        }
        else if ($period_type == 2) {
           $date_from = date('Y-m-d', time() - 86400*6);
           $date_to = date('Y-m-d');
        }
        else if ($period_type == 3) {
            $date_from = date('Y-m-01');
            $date_to = date('Y-m-d');                
        }
        else if ($period_type == 4) {
            $date_from = date('Y-m-d', time() - 86400*30);
            $date_to = date('Y-m-d');
        }
        else if ($period_type == 10) {                
            $date_from = date('Y-m-d', $acc->created);
            $date_to = date('Y-m-d');
        }
        else if ($period_type == 5) {
           $date_from =  date("Y-m-d", strtotime("first day of last month"));
           //воскресенье прошлой недели
           $date_to =  date("Y-m-d", strtotime("last day of last month")); 
        }
        else if ($period_type == 7) {
           $date_from =  date("Y-m-d", time() - 86400);
           //воскресенье прошлой недели
           $date_to =  date("Y-m-d", time() - 86400); 
        }
        else if ($period_type == 8) {
           $date_from =  date("Y-m-d", time());
           //воскресенье прошлой недели
           $date_to =  date("Y-m-d", time()); 
        }
    }
    
    
    
    
    if ($date_from && $date_to) {
        if ($date_from != $date_to) { 

            $date_from_ts = strtotime($date_from);
            $date_to_ts = strtotime($date_to);
            while($date_from_ts <= $date_to_ts) {
                $ch_cur_date_range[] = date('Y-m-d', $date_from_ts);
                $date_from_ts += 86400;
                
                // ограничиваем поиск семью днями
                if (sizeof($ch_cur_date_range) == 7) {
                    break;
                }
            }
        }
        else {
            $ch_cur_date_range[] = $date_from;
        }        
    }
    
    // проверяем с учетом таймзон
    $_timezone_checked = lx_domains_cmp_apply_timezone_to_range($acc, $ch_cur_date_range);
    $range_more_then = $_timezone_checked['range_more_then'];
    $range_less_then = $_timezone_checked['range_less_then'];
    $ch_cur_date_range = $_timezone_checked['ch_cur_date_range'];
        
    
    if (!in_array($ch_cur_show_date, $ch_cur_date_range)) {
        $ch_cur_show_date = $ch_cur_date_range[sizeof($ch_cur_date_range) -1];
    }
    else if (isset($_GET['form_id'])) {
        $ch_cur_show_date = $ch_cur_date_range[sizeof($ch_cur_date_range) -1];
    }
    
    
//    if (!preg_match('/^2[0-9]{3}\-[0-9]{2}-[0-9]{2}$/', $date) && ) {
//        drupal_goto('user/' . $acc->uid . '/campaigns');
//    }
    
    $extra = array('country' => 'country', 
                   'isp' => 'isp_org', 
                   'os' => 'ua_os', 
                   'browser' => 'ua_browser', 
                   'ua' => 'user_agent', 
                   'referer' => 'user_referer', 
                   'url' => 'user_page_url');    

    $output = '<div class="overlay-over-blured"></div>';

    $cmp = __can_view_users_campaigns($acc, $cid);
    $is_admin = user_access('administer users');
    
    $first = 0;
    $last = 0;

    if (!empty($cmp)) {
        try {
            $db = new PDO("mysql:host=" . LX_MYSQLI_STAT_HOST . ";dbname=" . LX_MYSQLI_STAT_DB, LX_MYSQLI_STAT_USER, LX_MYSQLI_STAT_PASS);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $reasons = __get_reasons_translate() + array(103 => 'Money Page to All');
            
            
            if (!$is_admin) {
                $reasons[1] = $reasons[4];
            }    

            $where_params = array();
            $where = array();

            if (isset($_GET['blockedby']) && is_numeric($_GET['blockedby']) && ($_GET['blockedby'] > -1)) {
                if ($_GET['blockedby'] == 1000) {
                    $where[] = 'block_reason>{block_reason} AND block_reason<{block_reason2}';
                    $where_params['block_reason'] = 0;
                    $where_params['block_reason2'] = 103;
                }
                else {
                    if (!$is_admin && ($_GET['blockedby'] == 4)) {
                        $where[] = '((block_reason={block_reason1}) OR (block_reason={block_reason2}) )';
                        $where_params['block_reason1'] = 1;  
                        $where_params['block_reason2'] = 4;  
                    }
                    else {
                        $where[] = 'block_reason={block_reason}';
                        $where_params['block_reason'] = (int)$_GET['blockedby'];                    
                    }    
                }
            }
            
            if (isset($_GET['userip']) && trim($_GET['userip'])) {
                if (is_valid_ip(trim($_GET['userip'])) != 'unknown') {
                    $where[] = 'real_ip={real_ip}';
                    $where_params['real_ip'] = "'".trim($_GET['userip']) ."'";
                }
                else {
                    drupal_set_message('Wrong IP format',' error');
                    drupal_goto('user/' .$acc->uid .'/campaigns/' .$cid .'/stastistics/log/' .arg(6));
                }
            }
            
            if (isset($_GET['selfield']) && $_GET['selfield'] && in_array($_GET['selfield'], array_keys($extra)) && isset($_GET['selfieldval']) && trim($_GET['selfieldval'])) {
                if ( !in_array($_GET['selfield'], ['referer', 'url'])) {
                    $where[] =  $extra[$_GET['selfield']] ."='{" .$extra[$_GET['selfield']] ."}'";
                    $where_params[$extra[$_GET['selfield']]] = check_plain($_GET['selfieldval']);
                }
                else {
                    $where[] =  "positionCaseInsensitiveUTF8(". $extra[$_GET['selfield']].", '{" .$extra[$_GET['selfield']] ."}') > 0" ;
                    $where_params[$extra[$_GET['selfield']]] = str_replace('&amp;','&',$_GET['selfieldval']);                    
                }
            }

            $sort_order = 'DESC';

            if (isset($_GET['last'])) {
                $where[] = 'id<{id}';
                $where_params['id'] = (int)$_GET['last'];  
            }
            else if (isset($_GET['first'])) {
                $where[] = 'id>{id}';
                $where_params['id'] = (int)$_GET['first'];       
                $sort_order = 'ASC';
            }            
            
            if ($cmp->reset_stat) {
                $where[] = 'click_date>{reset_stat}';
                $where_params['reset_stat'] = "'".$cmp->reset_stat ."'"; 
            }
            
            
            $_query = 'SELECT id, click_date, real_ip, country, isp_org, block_reason, ua_os, ua_browser, accept_lang, user_agent, user_referer, user_page_url'
                        . '                     FROM clicklog WHERE id IN (SELECT id FROM clicklog PREWHERE event_date = {event_date} AND click_date<= {click_date} AND click_date>= {click_date_to} AND cid={cid} ' .(sizeof($where) ? ' WHERE ' .implode(' AND ', $where) : '') .' ORDER BY id ' .$sort_order .' LIMIT {limit}) ORDER BY id DESC';
            
            
            $where_params['event_date'] = "'" .$ch_cur_show_date ."'";
            $where_params['click_date'] = "'" .$range_less_then ."'";
            $where_params['click_date_to'] = "'" .$range_more_then ."'";
            $where_params['cid'] = "'".$cid."'";
                   
            
            $where_params['limit'] = 101;
            
            $data = ch_user_query($_query, $where_params);

            $rows = array();
            $headers = array();
            $objects = array();
             
            
            if (sizeof($data)) {   
                $objects = $data['result'];                
                $cnt = sizeof($objects);
                
//                if ($cnt < 101 && (sizeof($ch_cur_date_range) > 1) && !in_array($per)) { // $per - ???
                if ($cnt < 101 && (sizeof($ch_cur_date_range) > 1)) {
                    $cur_date_key = array_search($ch_cur_show_date, $ch_cur_date_range);
                    $cur_date_key --;
                    while (isset($ch_cur_date_range[$cur_date_key]) && $cnt < 101) {
                        $ch_cur_show_date = $ch_cur_date_range[$cur_date_key];
                        $where_params['event_date'] = "'" .$ch_cur_show_date ."'";
                        $where_params['limit'] = 101 - $cnt;
                        
                        $data = ch_user_query($_query, $where_params);
                        if (sizeof($data)) {   
                            $objects += $data['result']; 
                            $cnt += sizeof($data['result']);
                        }
                        
                        $cur_date_key --;
                    }
                }
                
                if ($cnt == 101) {
                    unset($objects[100]);
                }                
                
                
                foreach ($objects as $r) {
                    if (!$first) {
                        $first = $r['id'];
                    }
                    
                    foreach ($r as $kk => $rr) {
                        if ($rr == 'N_') {
                            $r[$kk] = '';
                        }
                    }

                    $last = $r['id'];
                    
                    $rows[] = array(
//                                        $r['click_date'],
                                        lx_format_date_timezone($r['click_date'], $acc->timezone),
                                        $r['real_ip'],
                                        $r['country'],
                                        $r['isp_org'],
                                        isset($reasons[$r['block_reason']]) ? $reasons[$r['block_reason']] : '',
                                        $r['ua_os'],
                                        $r['ua_browser'],
                                        __normalize_accept_lang($r['accept_lang']),
                                        '<span class="long-trimmed">' .htmlspecialchars($r['user_agent'], ENT_QUOTES) .'</span>',
                                        '<span class="long-trimmed">' .htmlspecialchars($r['user_referer'], ENT_QUOTES) .'</span>',
                                        '<span class="long-trimmed">' .htmlspecialchars($r['user_page_url'], ENT_QUOTES).'</span>',
                            );
                }

                unset($objects);
                

            }
            
            $dst = drupal_get_destination();
            $dst = rtrim(str_replace('ajax=1','',$dst['destination']), '?');
            
            $_timezone = 'UTC+0';
            if ($acc->timezone) {
                $timezones = timezone_list();     
                $_timezone = $timezones[$acc->timezone];
            }
            
            $output .= '<div class="preinfo">'
//                        . '<div class="total-found-rows">Matches found: ' .$cnt_obj->rows .'</div>' 
                        .'<div class="today-delay">Time zone: <strong>' .$_timezone .'</strong><a href="/user/' .$acc->uid .'/edit?destination=' . urlencode($dst) .'">change</a>Log updates once per 1-2 minutes.</div>'
                        .'</div>';
            
            if (sizeof($rows)) {
//                if (!$is_admin) {
                    $headers = array('Date', 'User&nbsp;IP', 'Country', 'ISP/Org', 'Blocked', 'OS', 'Browser', 'Language','User-Agent', 'Referer', 'URL');
//                }
                
                $limit = 100;
                $output .= '<fieldset class="table-fs stat-table stat-log-table new-look">'.theme('table', array('header' => $headers, 'rows' => $rows)) . __get_user_ch_pager($cnt, $limit, $first, $last, $ch_cur_show_date) .'</fieldset>';
            } else {
                $output .= '<fieldset class="table-fs stat-table"><p style="margin-bottom: 10px; padding: 10px 10px 0;">Nothing found. Change your search options.</p></fieldset>';
            }
            
            $db = null;
            $sth = null;
            
            print $output;
            
        } catch (PDOException $e) {
            lx_magic_log2('Clickhouse user log error', 'Clickhouse user log error', $e->getMessage(), 200, true);
            print 'error';
        }
    } else {
        print '<script>location.href="/user/' . $acc->uid . '/campaigns";</script>';
    }  
}


function __normalize_accept_lang($lang) {
    $res = '';
    
    if ($lang) {
        $res = substr($lang, 0, 2);
    }
    
    return $res;
}

function lx_domains_ch_cmp_log_filters($form, $form_state, $cmp) {
    global $user;
    
    drupal_add_css(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/css/caleran.min.css');
    
    $cmp_created = (int)((time() - $cmp->created)/86400);
    if ($cmp_created > 60) $cmp_created = 60;    
    
    if (user_access('administer users') || ($user->uid == 9)) {
        $cmp_created = 360;
    }
    
    
    drupal_add_js('window.user_created='.$cmp_created.';', 'inline');
    
    
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/js/moment.min.js');
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/js/caleran.min.js');
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/js/caleran-show.js');
    
    $period_type = 8;

    $date_ranges = array(
//                         6 => 'Last week', 
                         8 => 'Today', 
                         7 => 'Yesterday', 
                         2 => 'Last 7 days', 
//                         4 => 'Last 30 days',
//                         1 => 'This week',
//                         3 => 'This month',
//                         4 => 'Last 30 days', 
//                         5 => 'Last month',
//                         10 => 'All time',
                         100 => 'Custom'
                    );
    if (isset($_GET['period_type']) && is_numeric($_GET['period_type']) && in_array($_GET['period_type'], array_keys($date_ranges))) {
        $period_type = $_GET['period_type'];
    }    
    
    $form['fs'] = array('#type' => 'fieldset',
        '#attributes' => array('class' => array('pad12 container-inline form-wrapper'))
    );    
    
    $def_value = '';
    $def_value_vis = '';
    if (isset($_GET['dates']) && preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4} \- [0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $_GET['dates'])) {
        $def_value = $_GET['dates'];
        
        $dates_arr = explode('-', $_GET['dates']);
        $dates_arr[0] = trim($dates_arr[0]);
        $dates_arr[1] = trim($dates_arr[1]);
        
        
        $dates_arr_0 = explode('.', $dates_arr[0]);
        $dates_arr_1 = explode('.', $dates_arr[1]);
        
        unset($dates_arr_0[2]);
        unset($dates_arr_1[2]);
        $def_value_vis = implode('.', $dates_arr_0) . '-' .implode('.', $dates_arr_1);
    }
    
    $form['fs']['period_type'] = array('#type' => 'select',
        '#title' => '<i class="fa fa-calendar" aria-hidden="true"></i>',
        '#options' => $date_ranges,
        '#default_value' => $period_type,
//        '#weight' => 1
        
        );
    
    $form['fs']['dates'] = array('#type' => 'textfield',
//        '#title' => '<i class="fa fa-calendar" aria-hidden="true"></i><span>Period&nbsp;&nbsp;&nbsp;</span>',
        '#default_value' => $def_value,
//        '#weight' => 2,
        '#prefix' => '<div class="dates-wrapper" style="margin:0; width: 0">',
        '#suffix' => '</div>',
        '#attributes' => ['placeholder' => 'Click to select dates range...', 'class' => ['limitto7', 'limitto7-with-vis'], 'style' => 'width: 1px;padding: 0;border: none;background: transparent;']
        );
    
    $form['fs']['dates_vis'] = array('#type' => 'textfield',
//        '#title' => '<i class="fa fa-calendar" aria-hidden="true"></i><span>Period&nbsp;&nbsp;&nbsp;</span>',
        '#default_value' => $def_value_vis,
//        '#weight' => 2,
        '#prefix' => '<div class="dates-wrapper-vis"' .($period_type < 100 ? ' style="display:none"' : '') .'>',
        '#suffix' => '</div>',
        '#attributes' => ['placeholder' => 'Dates range', 'class' => ['limitto7']]
        );      
    
    if ($period_type == 100) {
        $form['#attributes']['class'][] = 'custom-shown';
    }    
    
    
    
    // =======
    
    
    
    
    
    
    
    
    
//    $form['fs']['show_blocked'] = array('#type' => 'checkbox',
//        '#title' => 'Show only blocked',
//        '#default_value' => isset($_GET['show_blocked']) && is_numeric($_GET['show_blocked']) ? $_GET['show_blocked'] : 0
//        );
    
    $is_admin = user_access('administer users');
    
    $options = __get_reasons_translate();
    
    $options[-1] = 'Show All';
    
    $options[1000] = 'Show All Blocked';
    
    if (!$is_admin) {
        unset($options[1]);
        
        // MFS
        unset($options[15]);
    }    
    
    // NEW
//    if ($is_admin) {
//        unset($options[14]);
//    }
    
    $form['fs']['blockedby'] = array('#type' => 'select',
        '#title' => '<i class="fa fa-ban" aria-hidden="true"></i><span>Block reason</span>',
        '#options' => $options,
        '#default_value' => isset($_GET['blockedby']) && is_numeric($_GET['blockedby']) ? $_GET['blockedby'] : -1
        );
    
    $form['fs']['userip'] = array('#type' => 'textfield',
        '#title' => 'IP',
//        '#title_display' => 'invisible',
        '#default_value' => isset($_GET['userip']) ? check_plain($_GET['userip']) : '',
        '#attributes' => array('placeholder' => 'User IP'),
        '#maxlength' => 39,
        '#size' => 15
        );
    
    $form['fs']['selfield'] = array('#type' => 'select',
        '#title' => '<i class="fa fa-search" aria-hidden="true"></i>',
        '#options' => array('' => 'Choose field', 
                            'country' => 'Country',
                            'isp' => 'Isp/Org',
                            'os' => 'OS',
                            'browser' => 'Browser',
                            'ua' => 'User-Agent',
                            'referer' => 'Referer',
                            'url' => 'Url Substrings'
            ),
        
        
        '#default_value' => isset($_GET['selfield']) ? check_plain($_GET['selfield']) : ''
        );    
    
    $form['fs']['selfieldval'] = array('#type' => 'textfield',
//            '#title' => 'User IP',
            '#title_display' => 'invisible',
            '#default_value' => isset($_GET['selfieldval']) ? str_replace('&amp;','&',check_plain($_GET['selfieldval'])) : '',
            '#attributes' => array('placeholder' => 'Selected field contains...'),
            '#maxlength' => 128
        );
    
    $form['fs']['submit'] = array('#type' => 'submit',
        '#value' => 'Apply filter'
        );
    
//    if ($is_admin) {
//        $form['fs']['export'] = array('#type' => 'submit',
//            '#value' => 'Export'
//        );
//    }
//    
//    $form['fs']['mu'] = array('#type' => 'markup',
//        '#markup' => '<div class="operations operations-float-right"><span class="action-btn">Actions</span>'
//            . '       <ul class="width-174">'
//        . '         <li><a href="/user/' .arg(1) . '/campaigns/' .arg(3) . '/stastistics">Statistics</a></li>'
//            . '          <li><a href="/user/' .arg(1) . '/campaigns/' .arg(3) . '/edit">Edit campaign</a></li>'
//            . '      </ul></div>'
//        );
    
    $form['#method'] = 'get';
    $form['#id'] = 'lx-domains-cmp-log-filters';
    
    return $form;
}


// формирует список страниц под таблицей click_house
function __get_user_ch_pager($cnt, $limit, $first_id, $last_id, $search_date) {
    $output = '';
    
    if ($cnt > 100 || isset($_GET['first'])) {

        $output = '<div class="item-list"><ul class="pager">';

        $url = explode('?', $_SERVER['REQUEST_URI']);
        
        $page_url = $url[0];
        $page_url = explode('/', $page_url);
        unset($page_url[sizeof($page_url)-1]);
        $page_url[] = $search_date;
        $page_url = implode('/', $page_url);
        
        
        if (isset($url[1])) {
            $params = explode('&', $url[1]);
            
            foreach ($params as $k => $v) {
                $v = explode('=', $v);
                if (in_array($v[0], array('op', 'form_build_id', 'form_token', 'form_id', 'last', 'first', 'ajax'))) {
                    unset($params[$k]);
                }
            }
            
            $page_url .= '?' . implode('&', $params);
        } else {
            $page_url .= '?';
        }



        if (isset($_GET['last']) || (isset($_GET['first']) && ($cnt > 100))) {
            $output .= '<li><a class="ajaxed" href="' . $page_url . '&first=' .$first_id .'"><<<</a></li>';
        }

        if (isset($_GET['first']) || (isset($_GET['last']) && $cnt > 100) || (!isset($_GET['first']) && !isset($_GET['last']))) {
            $output .= '<li><a class="ajaxed" href="' . $page_url . '&last=' .$last_id .'">>>></a></li>';
        }

        $output .= '</ul></div>';
    }


    return $output;
}