<?php

function lx_domains_cmp_confirm_mail($uid, $timestamp, $hashed_pass) {
    global $user;
    
    $account = user_load($uid);
    $mail_confirmed = @(int)$account->field_email_confirmed['und'][0]['value'];
    
    if (!$mail_confirmed) {
        if ($account->uid && $hashed_pass == user_pass_rehash($account->pass, $timestamp, $account->name, $account->uid)) {
            $account->field_email_confirmed['und'][0]['value'] = 1;
            user_save($account);

            // Display default welcome message.
            drupal_set_message(t('You\'ve confirmed your Email address successfuly.'));
        }
    }
    drupal_goto('<front>');
}