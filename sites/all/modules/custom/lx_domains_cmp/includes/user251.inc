<?php

//статистику по странам по каждой кампании риалтайм (ну не прям риалтайм, но например раз в 5 мин),  за текущие сутки
//то есть, я отдаю вам запросом айди кампании - ответом получаю:
//гео + тотал кликс 

//https://clients.magicchecker.com/personal-251-campaigns-countries?cid=9e55d117f3a0533608f6d2fecc539c3b&stoken=sf43fg9jsb32DW3

function lx_domains_251_campaign_country_group() {
    $token = check_plain($_GET['stoken']);

//    if (!isset($_GET['test'])) die('test mode');
    
    if ($token == 'sf43fg9jsb32DW3') {
        $cid = check_plain($_GET['cid']);
        
        try {
            $db = new PDO("mysql:host=" . LX_MYSQLI_STAT_HOST . ";dbname=" . LX_MYSQLI_STAT_DB, LX_MYSQLI_STAT_USER, LX_MYSQLI_STAT_PASS);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);            
            
            
            
            $set_max_to_zero = false;
            $table_name = 'day_user_251_' .date('Y_m_d');
            if (date('Y_m_d') != date('Y_m_d', time() - 600)) {
                $table_name = 'day_user_251_' .date('Y_m_d', time() - 600);
                $set_max_to_zero = true;
            }
//            
            $this_time_max_id = $db->query('SELECT MAX(id) as mx FROM ' .$table_name)->fetchObject();
            $this_time_max_id = $this_time_max_id->mx;


            
//            // выбираем из дневной  
            
            
            $last_id_sth = $db->prepare('SELECT last_id FROM auser_251_cmp_country_ids WHERE cid=:cid');
            $last_id_sth->bindParam(':cid', $cid, PDO::PARAM_STR);
            $last_id_sth->execute();
            $last_id = $last_id_sth->fetchObject();
            
            if (!empty($last_id)) {
                $last_id = $last_id->last_id;
            }
            else {
                $last_id = 0;
            }
            
            
            
            $_query = 'SELECT count(id) as cnt, country FROM ' . $table_name . ' WHERE id>:id1 AND id <= :id2 AND cid=:cid GROUP BY country';
            $sth = $db->prepare($_query);


            $sth->bindParam(':id1', $last_id, PDO::PARAM_INT);
            $sth->bindParam(':id2', $this_time_max_id, PDO::PARAM_INT);
            $sth->bindParam(':cid', $cid, PDO::PARAM_STR);
            $sth->execute();

            $output = '';
            while ($r = $sth->fetchObject()) {
                $output .= $r->country .',' .$r->cnt ."\n";
            }    

            if ($output) {
                print $output;
                $f = fopen('/home/user173magic/data/www/magicchecker.com/sites/all/modules/custom/lx_domains_cmp/includes/countries.log', 'a');
                fputs($f, date('Y-m-d H:i:s') ."\t" .$cid ."\t" .$last_id ."\t" .$this_time_max_id ."\n");
                fputs($f, $output ."\n\n");
                fclose($f);
            }
//            else {
//                print('none');
//            }                

            if ($set_max_to_zero) {
                $this_time_max_id = 0;
            }

            $cmp_sth = $db->prepare('INSERT IGNORE INTO auser_251_cmp_country_ids (cid, last_id) VALUES (:cid, :last_id) ON DUPLICATE KEY UPDATE last_id=VALUES(last_id)');
            $cmp_sth->bindParam(':cid', $cid, PDO::PARAM_STR);
            $cmp_sth->bindParam(':last_id', $this_time_max_id, PDO::PARAM_INT);
            $cmp_sth->execute();

            
            
            $sth = null;
            $last_id_sth=null;
            $cmp_sth=null;
            $db = null;
            
        } catch (PDOException $e) {
            print_r($e); //'error';
            die('error');
        }        
    }
}

