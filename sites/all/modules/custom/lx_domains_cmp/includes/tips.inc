<?php

function _lx_domains_cmp_match_path($pages) {
    $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));    
    $pages = drupal_strtolower($pages);

    // Compare the lowercase internal and lowercase path alias (if any).
    $page_match = drupal_match_path($path, $pages);
    if ($path != $_GET['q']) {
      $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
    }

    if ($page_match ) return true;
    
    return false;  
}

function _lx_domains_cmp_get_tips() {
    $res = db_select('node', 'n');
    $res->join('field_data_field_pages_to_show', 's', 's.entity_id=n.nid');
    $res->fields('s', array('entity_id', 'field_pages_to_show_value'));
    $res->condition('n.type', 'tips');
    $res->condition('n.status', 1);
    $pages = $res->execute()->fetchAll();     
//    
    $tips = '';
    
    if (sizeof($pages)) {
        foreach ($pages as $page) {
            if (_lx_domains_cmp_match_path($page->field_pages_to_show_value)) {
                $tips = _lx_domains_cmp_build_tips($page->entity_id);
                
                if ($tips) {
                    drupal_add_css(drupal_get_path('module', 'lx_domains_cmp') .'/css/jquery.qtip.min.css');
                    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/js/jquery.qtip.min.js');
                    drupal_add_js($tips, 'inline');
                }
                
                break;
            }
        }
    }
}

function _lx_domains_cmp_build_tips($nid) {
//    $memcache = new Memcache;
//    $memcache->pconnect(LX_DOMAINS_MEMCACHE_IP, LX_DOMAINS_MEMCACHE_PORT) or die("Could not connect");
//    $tips = $memcache->get('tips_' . $nid);
//    if (!empty($tips)) {
//        $tips = $memcache->get('tips_' . $nid);
//    }
//    else {
        $node = node_load($nid);
        
        // генерим массив подсказок вида
        // $tips = [
        //            ['selector' => 'jquery selector', 'content' => 'tips content', 'links' => [array_of_links]]
        //            ['selector' => 'jquery selector', 'content' => 'tips content', 'links' => [array_of_links]]
        //            .....
        //         ];
        
        $tips_ids = @$node->field_tips['und'];
        if (sizeof($tips_ids)) {
            foreach ($tips_ids as $tip_id) {
                $tip = field_collection_item_load($tip_id['value']);
                $rule = @trim($tip->field_jquery_rule['und'][0]['value']);
                $text = @trim($tip->field_tip['und'][0]['value']);
                $links = @trim($tip->field_tip_links['und'][0]['value']);
                
                if ($rule && $text) {
                    $links_arr = array();
                    if ($links) {
                        $links = explode("\n", $links);
                        if (sizeof($links)) {
                            foreach ($links as $link) {
                                if (trim($link)) {
                                    $link = explode('|', $link);
                                    if ((sizeof($link) == 2) && trim($link[0]) && trim($link[1])) {
                                        $links_arr[] = '<a target="_blank" href="' .trim($link[1]) .'">' .trim($link[0]) .'</a>';
                                    }
                                }
                            }
                        }
                    }
                    
                    $text = explode("\n", $text);
                    foreach ($text as $k => $v) {
                        $text[$k] = '<p class="r-' .$k .'">' .trim($v) .'</p>';
                    }
                    
                    $text =  implode('', $text) .(sizeof($links_arr) ? '<p class="extra-lniks">'.implode('', $links_arr) .'</p>' : '');
                    
                    $tips[] = "jQuery('" .$rule ."').append('<i class=\"qick-tip fa fa-info-circle\" aria-hidden=\"true\"></i>'); jQuery('" .$rule ." .qick-tip').qtip({content: {text: '" .str_replace("'", "\'", $text) ."'}, hide: { fixed: true, delay: 300}, position: {my: 'bottom center', adjust: {y: -22, x: -10}, viewport: jQuery(window)}});";
                } 
            }
            
            if (sizeof($tips)) {
                $tips = "jQuery(document).ready(function() {" .implode("\n", $tips) ."});";
//                $memcache_result = $memcache->replace('tips_' . $nid, $tips, false, 30*86400);
//                if($memcache_result == false ) {
//                    $result = $memcache->set('tips_' . $nid, $tips, false, 30*86400);
//                }                   
            }
            
        }
//    }
    
    return $tips;
}

