<?php

// выводит папки пользоателя над списком кампаний
function lx_domains_cmp_show_folders($uid) {
    
    $output = '';
    
    $res = db_query('SELECT DISTINCT(status) as sts FROM {domain_compaigns} WHERE uid=:uid ORDER BY status ASC', array(':uid' => $uid))->fetchAll();
    
    if (sizeof($res)) {
        
        $statuses = array(7 => '<i class="fa fa-play" aria-hidden="true"></i>',
                          101 => '<i class="fa fa-pause" aria-hidden="true"></i>',
                          102 => '<i class="fa fa-shield" aria-hidden="true"></i>',
                          103 => '<i class="fa fa-usd" aria-hidden="true"></i>');
        
        $output = '<ul class="cmp-folders">'
                . '   <li><a' .((!isset($_GET['status']) && !isset($_GET['folder'])) ? ' class="active-folder"' : '') .' href="/user/' .$uid .'/campaigns">ALL</a></li>';
    
        foreach ($res as $r) {
            if (isset($statuses[$r->sts])) {
                $output .= '<li><a' .(isset($_GET['status']) && $_GET['status'] == $r->sts ? ' class="active-folder"' : '') .' href="/user/' .$uid .'/campaigns?status=' .$r->sts .'">' .$statuses[$r->sts] .'</a></li>';
            }    
        }
        
        $output .= '</ul>';        
    }
    
    return $output;
}
