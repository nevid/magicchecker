<?php

require_once(drupal_get_path('module', 'lx_domains_cmp') . '/includes/helpers.inc');


function lx_magic_track_user() {
    global $user;
    
    if ($user->uid > 20 && $user->uid <> 27) {
        $url = substr($_SERVER['REQUEST_URI'], 0, 1024);
        if (strpos($url, '/js/') === false) {
            lx_magic_log2($url, '', '', 100);
        }    
    }    
}

function lx_magic_log2($short, $short_user = '', $info = '', $type = 3, $send_mail = false, $server = '88.99.56.17', $acc_uid = null) {
    global $user;
    
    $ip = ip_address();
    $country = helperCountryByIp($ip);
    $uid = $acc_uid ? $acc_uid : $user->uid;
    
    try {
        db_insert('magic_log')
            ->fields(array('uid' => $uid,
                              'type' => $type,
                              'ip' => $ip,
                              'country' => $country,
                              'server' => $server,
                              'short_descr' => $short,
                              'short_descr_user' => $short_user,
                              'info' => $info,
                ))->execute();
    }
    catch (Exception $ex){

    }

        
    if ($send_mail) {
        lx_magic_log_send_mail($uid, $ip, $type, $short, $info, $server);
    }
}

function lx_magic_log($short, $info = '', $type = 3, $send_mail = false, $server = '88.99.56.17', $acc_uid = null) {
    global $user;
    
    $ip = ip_address();
    $uid = $acc_uid ? $acc_uid : $user->uid;
    
    db_insert('magic_log')
        ->fields(array('uid' => $uid,
                          'type' => $type,
                          'ip' => $ip,
                          'server' => $server,
                          'short_descr' => $short,
                          'info' => $info,
            ))->execute();

        
    if ($send_mail) {
        lx_magic_log_send_mail($uid, $ip, $type, $short, $info, $server);
    }
}

function lx_magic_log_send_mail($uid, $ip, $type, $short, $info, $server) {
    /* тема/subject */
    
    $types = lx_magic_log_types();
    
    $subject = 'MagicLog - ' .$server .' - ' .$types[$type] .': '  .substr($short, 0, 128);
    $message = 'Uid: ' .$uid .'<br />';
    $message .= 'IP: ' .$ip .'<br />';
    $message .= 'Type: ' .$types[$type] .'<br /><br />';
    $message .= 'Short: ' .$short .'<br /><br/>';
    $message .= strpos($info, '<br') !== false ? $info : nl2br($info);
    
    $message .= '<br /><br />MagicChecker.com';


    /* Для отправки HTML-почты вы можете установить шапку Content-type. */
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

    /* дополнительные шапки */
    $headers .= "From: info@magicchecker.com\r\n";
    $to = 'info@magicchecker.com';
    $headers .= "To: " . $to . "\r\n";

    mail($to, $subject, $message, $headers);    
}

function lx_magic_log_types() {
    return array(1 => 'Error',
                 2 => 'Warning',
                 3 => 'Client action',
                 4 => 'Payment',
                 5 => 'Log In',
                 6 => 'Log Out',
                 7 => 'Registration',
                 10 => 'Stripe',
                 20 => 'Duplicate acc',
                 100 => 'track',
                 200 => 'clicklog'
                 );
}