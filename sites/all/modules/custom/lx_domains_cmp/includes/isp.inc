<?php

function lx_isp_autocomplete() {
    $matches = array();
    
    $string = strtolower(trim(check_plain($_GET['term'])));
    $string = str_replace('&amp;','&',$string);
    if (strlen($string) >= 3) {    
        
    
//    $res = db_select('isps', 'i')
//                ->fields('i', array('isp', 'org'))
//                ->condition(
//                    db_or()
//                    ->condition('isp', '%' .db_like($string) .'%', 'LIKE')
//                    ->condition('org', '%' .db_like($string) .'%', 'LIKE')
//                )
//                ->range(0,10)
//                ->execute()->fetchAll();
                    
    
        $res = db_query('SELECT isp, org FROM {isps} WHERE isp LIKE :isp OR org LIKE :org LIMIT 0,10', array(':isp' => '%' .db_like($string) .'%', ':org' => '%' .db_like($string) .'%'))->fetchAll();
        
        $isp_list = array();

        foreach ($res as $v) {
            if (stripos($v->org,$string) !== false) {
                $isp_list[$v->org] = $v->org;
            }
            else if (stripos($v->isp,$string) !== false) {
                $isp_list[$v->isp] = $v->isp;
            }
        }

      
      foreach ($isp_list as $k => $c) {
        if (strpos(strtolower($c), $string) !== false) {  
            $matches[] = (object)array('id' => $k, 'value' => str_replace('&amp;','&',check_plain($c)));
        }    
        
//        if (sizeof($matches) == 10) break;
      }
    }

    drupal_json_output($matches);
}