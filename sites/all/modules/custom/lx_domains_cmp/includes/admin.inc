<?php

function lx_domains_cmp_trial_users() {
    
    $form['lx_trial_access_users'] = array('#type' => 'textarea',
        '#title' => 'Trial access users',
        '#rows' => 10,
        '#default_value' => variable_get('lx_trial_access_users', ''),
        '#description' => 'One Email per line'
        );
        
    return system_settings_form($form);
}

