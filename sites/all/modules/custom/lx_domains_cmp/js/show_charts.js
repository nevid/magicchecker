jQuery(document).ready(function () {
    
    Chart.defaults.global.tooltips.mode = 'label';
    
    var chartData = {
            labels: [],
            datasets: [
                {
                type: 'bar',
                label: 'Total clicks',
                backgroundColor: "rgba(151,187,205,0.7)",
                data: [],
                borderColor: 'white',
                borderWidth: 2
            },             {
                type: 'line',
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 5,
                pointHitRadius: 10,
                label: 'Total blocks',
                data: []
            }]
        };    
    
    jQuery('.day-row').each(function () {
        chartData['labels'][chartData['labels'].length] = jQuery(this).attr('data-date');
        chartData['datasets'][0]['data'][chartData['datasets'][0]['data'].length] = parseInt(jQuery(this).find('.total').attr('data-value'));
        chartData['datasets'][1]['data'][chartData['datasets'][1]['data'].length] = parseInt(jQuery(this).find('.total_blocked').attr('data-value'));
    });
    
    chartData['datasets'][0]['data'] = chartData['datasets'][0]['data'].reverse();
    chartData['datasets'][1]['data'] = chartData['datasets'][1]['data'].reverse();
    chartData['labels'] = chartData['labels'].reverse();
    
    
    var ctx = document.getElementById("canvas").getContext("2d");
    window.myMixedChart = new Chart(ctx, {
        type: 'bar',
        data: chartData,
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Total clicks/Total blocks per day'
            }
        }
    });
    
    var pieData = {labels: [],
                    datasets: [{data: [],
                                backgroundColor: [
                                    "#00FF00",
                                    "#FF6384",
                                    "#36A2EB",
                                    "#FFCE56",
                                    "#017007",
                                    "#011E70",
                                    "#6F03A7",
                                    "#1D5DBc",
                                    "#05A808",
                                    "#A83305",
                                    "#B856EB",
                                    "#A86905",
                                    '#FF0000',
                                    '#0000FF'
                                ]
                        }]};
                        
                                    
    
//    pieData['labels'][0] = 'Money page - ' + '(' + Math.round((parseInt(jQuery('.total-row .total').attr('data-value')) - parseInt(jQuery('.total-row .total_blocked').attr('data-value')))/parseInt(jQuery('.total-row .total').attr('data-value'))*100, 2) + '%)';
//    pieData['datasets'][0]['data'][pieData['datasets'][0]['data'].length] = parseInt(jQuery('.total-row .total').attr('data-value')) - parseInt(jQuery('.total-row .total_blocked').attr('data-value'));

    jQuery('.total-row .detailed').each(function () {
        v = parseInt(jQuery(this).attr('data-value'));
        if (v != 0) {
            pieData['labels'][pieData['labels'].length] = jQuery(this).attr('data-type') + ' ' + jQuery(this).find('.percent').text();
            pieData['datasets'][0]['data'][pieData['datasets'][0]['data'].length] = v;
        }    
    });    

    var ctx2 = document.getElementById("pie").getContext("2d");
    window.myPie = new Chart(ctx2, {type: 'pie', data: pieData, options: {title: {display: true, text: 'Detailed safe page statistics (selected period)'}}});    
    

    jQuery('#canvas').click(function(evt){
        var activePoints = myMixedChart.getElementsAtEvent(evt);
        if (activePoints.length) {
            pie_id = activePoints[0]._model.label.toString();
            window.myPie.destroy();
            pieData = {labels: [],
                            datasets: [{data: [],
                                        backgroundColor: [
                                            "#00FF00",
                                            "#FF6384",
                                            "#36A2EB",
                                            "#FFCE56",
                                            "#017007",
                                            "#011E70",
                                            "#6F03A7",
                                            "#1D5DBc",
                                            "#05A808",
                                            "#A83305",
                                            "#B856EB",
                                            "#A86905",
                                        ]
                                }]};        

//            pieData['labels'][0] = 'Money page - ' + '(' + Math.round((parseInt(jQuery('.day-row[data-date="' + pie_id + '"] .total').attr('data-value')) - parseInt(jQuery('.day-row[data-date="' + pie_id + '"] .total_blocked').attr('data-value')))/parseInt(jQuery('.day-row[data-date="' + pie_id + '"] .total').attr('data-value'))*100, 2) + '%)';
//            pieData['datasets'][0]['data'][pieData['datasets'][0]['data'].length] = parseInt(jQuery('.day-row[data-date="' + pie_id + '"] .total').attr('data-value')) - parseInt(jQuery('.day-row[data-date="' + pie_id + '"] .total_blocked').attr('data-value'));


            if (jQuery('.day-row[data-date="' + pie_id + '"] .total_blocked').attr('data-value') != '0') {
                jQuery('.day-row[data-date="' + pie_id + '"] .detailed').each(function () {
                    v = parseInt(jQuery(this).attr('data-value'));
                    if (v != 0) {
                        pieData['labels'][pieData['labels'].length] = jQuery(this).attr('data-type') + ' ' + jQuery(this).find('.percent').text();
                        pieData['datasets'][0]['data'][pieData['datasets'][0]['data'].length] = v;
                    }    
                });        

                var ctx2 = document.getElementById("pie").getContext("2d");
                window.myPie = new Chart(ctx2, {type: 'pie', data: pieData, options: {title: {display: true, text: 'Detailed safe page statistics (' + pie_id + ')'}}});   
            }    
            else {
                var c=document.getElementById("pie");
                var ctx=c.getContext("2d");

                ctx.font="20px Tahoma";
                ctx.fillText("Safe page was not shown on this day: " + pie_id,150,20);                
                

            }
        }    
    });       

    jQuery('#pie').mousemove(function(evt){
        var activePoints = myPie.getElementsAtEvent(evt);
        if (activePoints.length) {
            label_txt = activePoints[0]._model.label.toString();
            
            label_txt = label_txt.split(' (');
            label_txt = label_txt[0];
            
            label_txt = jQuery.trim(label_txt);
            jQuery('#lx-domains-stat-dates .form-item-enabled-filters .form-checkboxes .form-type-checkbox').css('background', 'transparent').css('box-shadow', 'none');
            jQuery('#lx-domains-stat-dates .form-item-enabled-filters .form-checkboxes .form-type-checkbox').each(function () {
                cur_label = jQuery(this).find('label').text();
                cur_label = jQuery.trim(cur_label);
                if (label_txt == cur_label) {
                    jQuery(this).css('background', activePoints[0]._model.backgroundColor).css('box-shadow', '0px 0px 2px #ccc');
                    return;
                }
            });
        }    
    });       
    
    jQuery('#pie').mouseout(function () {
        jQuery('#lx-domains-stat-dates .form-item-enabled-filters .form-checkboxes .form-type-checkbox').css('background', 'transparent').css('box-shadow', 'none');
    });
    


});
