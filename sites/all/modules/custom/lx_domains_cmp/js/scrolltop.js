jQuery(document).ready(function(){
    jQuery(".node a").click(function (event) { 
        var id  = jQuery(this).attr('href');
        jQuery('html, body').animate({scrollTop:jQuery(id).position().top}, 800);

    });
    
    
    var obj = jQuery('.quick-menu-wrapper');
    var offset = obj.offset();
    var topOffset = offset.top;
    var marginTop = obj.css("marginTop");

    jQuery(window).scroll(function() {
    var scrollTop = jQuery(window).scrollTop();

      if (scrollTop >= topOffset){

        obj.css({
          marginTop: 0,
          position: 'fixed',
          top: 0
          
        });
      }

      if (scrollTop < topOffset){

        obj.css({
          marginTop: 0,
          position: 'relative'
        });
      }
    });    
    
});