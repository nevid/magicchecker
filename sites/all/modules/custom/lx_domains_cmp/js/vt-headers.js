jQuery(document).ready(function () {
    
    
    function __showOns() {
        if (jQuery(window).width() < 691) {
            jQuery('#lx-domains-cmp-form .resp-accordion strong').each(function () {
                txt = jQuery(this).html();
                fs_class = 'fs-off';
                if (txt.indexOf('(') != -1) {
                    txt2 = txt.split('(');
                    txt2[1] = txt2[1].replace(')', '');
                    if (txt2[1] == 'Off') {
                        txt2[0] = txt2[0] + ' <sup style="color: #e55454">Off</sup>';
                        txt2[1] = '';
                    }
                    else {
                        txt2[0] = txt2[0] + ' <sup style="color: #0e9c57">On</sup>';
                        fs_class = 'fs-on';
                    }

                    jQuery(this).parents('.resp-accordion').addClass(fs_class);
                    jQuery(this).html(txt2[0]);
                    jQuery(this).next().html(txt2[1].replace(/;/g, '<br />'));
                }
                else {
                    if (jQuery(this).find('sup').text() == 'Off') {
                        jQuery(this).parents('.resp-accordion').addClass('fs-off');
                    }
                }
            });

            jQuery('#lx-domains-cmp-form  .fieldset-legend').each(function () {
                txt = jQuery(this).text();

                if (txt.indexOf('(') != -1) {
                    txt2 = txt.split('(');
                    txt = txt2[0]
                }   

                jQuery(this).text(txt);
            });
        }
        else {
            jQuery('#lx-domains-cmp-form .vertical-tab-button strong').each(function () {
                txt = jQuery(this).html();
                if (txt.indexOf('(') != -1) {
                    txt2 = txt.split('(');
                    txt2[1] = txt2[1].replace(')', '');
                    if (txt2[1] == 'Off') {
                        txt2[0] = txt2[0] + ' <sup style="color: #e55454">Off</sup>';
                        txt2[1] = '';
                    }
                    else {
                        txt2[0] = txt2[0] + ' <sup style="color: #0e9c57">On</sup>';
                    }

                    jQuery(this).html(txt2[0]);
                    jQuery(this).next().html(txt2[1].replace(/;/g, '<br />'));
                }
            });

            jQuery('#lx-domains-cmp-form  .fieldset-legend').each(function () {
                txt = jQuery(this).text();

                if (txt.indexOf('(') != -1) {
                    txt2 = txt.split('(');
                    txt = txt2[0]
                }   

                jQuery(this).text(txt);
            });
        }   
    }    
    
    __showOns();
    
    jQuery(window).resize(function(){
        __showOns();
    });
});