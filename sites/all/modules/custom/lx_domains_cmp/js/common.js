jQuery(document).ready(function () {
    
    jQuery('.operations').mouseover(function () {
        if (jQuery(window).width() > 988) {
            jQuery(this).find('ul').show();
        }    
    });
    
    jQuery('.operations').mouseout(function () {
        if (jQuery(window).width() > 988) {
            jQuery(this).find('ul').hide();
        }    
    });
    
    jQuery('.operations .action-btn').click(function (e) {
        e.preventDefault();
        
        if (jQuery(this).next().hasClass('menu-vis')) {
            jQuery(this).next().hide();
            jQuery(this).next().removeClass('menu-vis');
        }
        else {
            jQuery(this).next().show();
            jQuery(this).next().addClass('menu-vis');
        }
        
        return false;
    });    
    
    function _reshowCols(cols) {
        jQuery('.column-switcher').html('<span class="show-hide-txt">Show/hide columns:</span></h2>');
        for (var k in cols) {
            jQuery('.column-switcher').append('<div title="' + (cols[k]['is_shown'] == 1 ? 'Filter is enabled now and has blocks' : 'Filter is not enebled now, but has blocks in this month') + '" class="form-item"' + (cols[k]['hide_box'] ? ' style="display:none"' : '') + '> <input class="form-checkbox"' + (cols[k]['is_shown'] == 1 ? ' checked="checked"' : '') + ' type="checkbox" id="chk-swchk' + cols[k]['num'] + '" name="swchk' + cols[k]['num'] + '" value="' + cols[k]['num'] + '"  /> <label for="chk-swchk' + cols[k]['num'] + '">' + cols[k]['name'] + '</label></div>');
            if (cols[k]['is_shown'] === 0) {
                jQuery('.stat-table table thead th:nth-child(' + cols[k]['num'] + ')').hide();
                jQuery('.stat-table table tbody td:nth-child(' + cols[k]['num'] + ')').hide();                    
            }
        }
    }
    
    jQuery('.column-switcher').on('change', '.form-checkbox', function () {        
       if(this.checked) {
           jQuery('.stat-table table thead th:nth-child(' + jQuery(this).val() + ')').fadeIn('Slow');
           jQuery('.stat-table table tbody td:nth-child(' + jQuery(this).val() + ')').fadeIn('Slow');
           
           var col_num = jQuery(this).val();
           
           jQuery('.stat-table table tbody td:nth-child(' + col_num + ')').addClass('tmp-highlight');
           setInterval(function () {jQuery('.stat-table table tbody td:nth-child(' + col_num + ')').removeClass('tmp-highlight');}, 2000);
       }
       else {
           jQuery('.stat-table table thead th:nth-child(' + jQuery(this).val() + ')').fadeOut('Slow');
           jQuery('.stat-table table tbody td:nth-child(' + jQuery(this).val() + ')').fadeOut('Slow');           
       }
    });
    
    
    jQuery('.td-stat').mouseover(function () {
        jQuery(this).find('.short_stat').show();
    });
    
    jQuery('.td-stat').mouseout(function () {
        jQuery(this).find('.short_stat').hide();
    });
    
    jQuery('.td-stat').click(function () {
        jQuery(this).find('.short_stat').toggleClass('visible');
    });
    
    jQuery('table').on('click', '.status-control .status-control-btn', function () {
        jQuery(this).parents('.status-control').find('.status-control-btn').removeClass('active');
        jQuery(this).addClass('active');
        jQuery(this).parents('.status-control').prev().html(jQuery(this).text());
        jQuery.get(jQuery(this).attr('href'));
        
        return false;
    });
    
    jQuery('.mobile-icon a').click(function () {
        if (jQuery(this).hasClass('shown')) {
            jQuery('.mobile-icon').next().fadeOut('fast');
            jQuery(this).removeClass('shown');
        }
        else {
            jQuery('.mobile-icon').next().fadeIn('fast');
            jQuery(this).addClass('shown');
        }    
        return false;
    });
  
    
    
    function __resize_cmp_table() {
        cmp_table = jQuery('.cmp-list-table');
        if (cmp_table.length) {
            if ((jQuery(window).width() < 900)) {
                if (!jQuery('.cmp-list-table').hasClass('is-appended')) {
                    jQuery('.cmp-list-table tbody tr').each(function () {
                       appendto = jQuery(this).find('.apender');
                       appendto.append('<div class="name-total clearfix">Today total (UTC+0): <b>' + jQuery(this).find('.td-total-total').html() + '</b></div>'); 
                       appendto.append('<div class="name-safe clearfix">Safe page shows: <b>' + jQuery(this).find('.td-total-safe').html() + '</b></div>'); 
                       appendto.append('<div class="name-sts clearfix">'+jQuery(this).find('.td-status').html() + '</div>'); 
                    });

                    jQuery('.name').addClass('appended');
                    jQuery('.td-status').hide();
                    jQuery('.td-total').hide();

                    jQuery('.cmp-list-table').addClass('is-appended');
                }    
            }
            else {
                if (jQuery('.cmp-list-table').hasClass('is-appended')) {
                    jQuery('.apender').html('');
                    jQuery('.name').removeClass('appended');
                    jQuery('.td-status').show();
                    jQuery('.td-total').show();     
                    jQuery('.cmp-list-table').removeClass('is-appended');
                }    
            }
        }   
    }
    
    
    jQuery(window).resize(function(){
        __resize_cmp_table();
    });    
    
    var cols = {};
    switcher = jQuery('.column-switcher');
    if (switcher.length && !cols.length) {
        var i = 1;
        jQuery('.stat-table table:first-child thead th').each(function () {
            if (!jQuery(this).hasClass('nohid')) {
                _state = 1;
                if (jQuery(this).hasClass('hidbydefault')) {
                    _state = 0;
                }
                
                hide_box = 0;
                if (jQuery('.total-row td:nth-child(' + i + ')').attr('data-value') == '0') {
                    hide_box = 1;
                }
                
                cols[jQuery(this).text()] = {name: jQuery(this).text(), is_shown: _state, num: i, hide_box: hide_box};
            }    
            
            i ++;
        });
//        console.log(cols);
        _reshowCols(cols);
    }
    
    __resize_cmp_table();
    
});