jQuery(document).ready(function () {
   function setBgBlured() {
       jQuery('#page #content').addClass('bg-blured');
       jQuery('body').append('<div class="overlay-over-blured"><div class="overlay-over-blured-content">Loading...</div></div>');
    } 
    
    function hideBgBlured() {
        jQuery('#page #content').removeClass('bg-blured');
        jQuery('.overlay-over-blured').remove();
    }
    
    function __resize_log_table() {      
        
        if(jQuery('#branding').width() > 1400) {   
            jQuery('.stat-log-table .overflow-fix').css('width', '100%');
        }
        else {
            if(jQuery(document).width() > 690) {   
                jQuery('.stat-log-table .overflow-fix').width(jQuery('#branding').width());
            }   
            else {
                jQuery('.stat-log-table .overflow-fix').width(jQuery('#branding').width() - 10);
            }
        }         
        console.log('win = ' + jQuery(document).width());
        console.log('brand = ' + jQuery('#branding').width());
    }
    
    jQuery(window).resize(function(){
        __resize_log_table();       
    });        
    
    
    function loadPgContent(wrapper, url) {
        setBgBlured();
        
        if (url.indexOf('?') === -1) {
            url = url + '?ajax=1';
        }
        else {
            url = url + '&ajax=1';
        }

        jQuery.get(url, function (data) {
            if (data !== 'error') {
                jQuery(wrapper).html(data);
                __resize_log_table();  
                hideBgBlured();
                
                jQuery('.long-trimmed').on('click', function () {
                    jQuery('.show-full-str .content-data').text(jQuery(this).text());
                    jQuery('.show-full-str').fadeIn('fast');
                });                     
                
                
                jQuery('.ajaxed').on('click', function (e) {
                    e.preventDefault();
                    jQuery('.show-full-str').fadeOut('fast');
                    loadPgContent('.ajaxContent', this.href);
                    jQuery('html, body').animate({scrollTop:jQuery(wrapper).position().top}, 800);
                });                
                
            }
            else {
                hideBgBlured();
                jQuery(wrapper).html('<fieldset class="table-fs stat-table" style="background: rgba(255,0,0, 0.1)"><p style="margin-bottom: 10px; padding: 10px 10px 0;">Some error occured. Try again later or write to support.</p></fieldset>');
            }
        });
    }  
    
    loadPgContent('.ajaxContent', location.href);  
    jQuery('.show-full-str .close').click(function (e) {
        e.preventDefault();
        jQuery('.show-full-str').fadeOut('fast');
    });    
    
    __resize_log_table();
});