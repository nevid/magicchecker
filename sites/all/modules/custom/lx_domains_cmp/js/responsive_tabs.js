/*global window,Drupal,jQuery*/
/*jslint nomen: true, indent: 2*/
// Easy Responsive Tabs Plugin
// Author: Samson.Onna <Email : samson3d@gmail.com>
// Modified: Miroslav Abrahám <Email : miris@kunago.com>

(function ($) {
  "use strict";
  $.fn.extend({
    responsiveTabs: function (method) {
      // Variables
      method = typeof method !== 'undefined' ? method : 'init';

      // Main function
      this.each(function () {
        var $respTabs = $(this),
          $tabItemh2,
          $tabItemh2exists,
          itemCount = 0,
          count = 0,
          $tabContent,
          $tabItem,
          $accItem,
          tabcount;

        switch (method) {
        case 'init':
          if (!$respTabs.hasClass('resp-tabs')) {
            $respTabs.addClass('resp-tabs');
            $respTabs.children('ul').addClass('resp-tabs-list');
            $respTabs.children('div').addClass('resp-tabs-container');
            $respTabs.find('ul.resp-tabs-list li').addClass('resp-tab-item');
            $respTabs.find('div.resp-tabs-container > fieldset').addClass('resp-tab-content');

            // if the h2 element does not exist
            $tabItemh2exists = $respTabs.find('.resp-tab-content');
            if ($tabItemh2exists.prev('h2.resp-accordion').length === 0) {
              $tabItemh2exists.before('<h2 class="resp-accordion" role="tab"></h2>');
            }

            $respTabs.find('.resp-accordion').each(function () {
              $tabItemh2 = $(this);
              // if the h2 element is empty
              if ($tabItemh2.children().length === 0) {
                $tabItem = $respTabs.find('.resp-tab-item:eq(' + itemCount + ')');
                $accItem = $respTabs.find('.resp-accordion:eq(' + itemCount + ')');
                $accItem.append($tabItem.html());
                $accItem.data($tabItem.data());
                $tabItemh2.attr('aria-controls', 'tab_item-' + itemCount);
                itemCount = itemCount + 1;
              }
            });

            // Assigning the 'aria-controls' to Tab items
            $respTabs.find('.resp-tab-item').each(function () {
              $tabItem = $(this);
              // if aria-controls have not been assigned
              if ($tabItem.is("[role!='tab']")) {
                $tabItem.attr('aria-controls', 'tab_item-' + count);
                $tabItem.attr('role', 'tab');

                // Assigning the 'aria-labelledby' attr to Tab content
                tabcount = 0;
                $respTabs.find('.resp-tab-content').each(function () {
                  $tabContent = $(this);
                  $tabContent.attr('aria-labelledby', 'tab_item-' + tabcount);
                  tabcount = tabcount + 1;
                });
                count = count + 1;
              }
            });

            // Action function
            $respTabs.find("[role=tab]").each(function () {
              // Define variables
              var $tabAria,
                $currentTab;

              // Initial class setup for items
              if ($(this).is('li.selected')) {
                $tabAria = $(this).attr('aria-controls');
                $respTabs.find('.resp-tab-item[aria-controls=' + $tabAria + ']').addClass('resp-tab-active');
                $respTabs.find('.resp-accordion[aria-controls=' + $tabAria + ']').addClass('resp-tab-active').addClass('selected');
                $respTabs.find('.resp-tab-content[aria-labelledby=' + $tabAria + ']').addClass('resp-tab-content-active');
              }

              // Action on item click
              $currentTab = $(this).children('a');
              $currentTab.click(function (event) {

                $currentTab = $(this).parent();
                $tabAria = $currentTab.attr('aria-controls');
                event.preventDefault();

                if (!$currentTab.hasClass('resp-tab-active')) {
                  $respTabs.find('.resp-tab-active').removeClass('resp-tab-active').removeClass('selected');
                  $respTabs.find('.resp-tab-content-active').slideUp().removeClass('resp-tab-content-active resp-accordion-closed');
                  $respTabs.find('.resp-accordion[aria-controls=' + $tabAria + ']').addClass('resp-tab-active').addClass('selected');
                  $respTabs.find('.resp-tab-item[aria-controls=' + $tabAria + ']').addClass('resp-tab-active').addClass('selected');
                  $respTabs.find('.resp-tab-content[aria-labelledby=' + $tabAria + ']').slideDown().addClass('resp-tab-content-active');
                }
              });
            });

            // Window resize function
            if (typeof _ !== 'undefined' && typeof _.debounce !== 'undefined' && $.isFunction(_.debounce)) {
              $(window).resize(_.debounce(function () { $respTabs.find('.resp-accordion-closed').removeAttr('style'); }, 300));
            } else {
              $(window).resize(function () {$respTabs.find('.resp-accordion-closed').removeAttr('style');});
            }
          }
          break;

        case 'destroy':
          if ($respTabs.hasClass('resp-tabs')) {
            $respTabs.find('h2.resp-accordion').remove();
            $respTabs.find('div.resp-tabs-container > fieldset').removeClass('resp-tab-content-active').removeClass('resp-tab-content').removeAttr('aria-labelledby');
            $respTabs.find('ul.resp-tabs-list li').removeClass('resp-tab-active').removeClass('resp-tab-item').removeAttr('aria-controls').removeAttr('role');
            $respTabs.children('div').removeClass('resp-tabs-container');
            $respTabs.children('ul').removeClass('resp-tabs-list');
            $respTabs.removeClass('resp-tabs');
          }
          break;
        }
      });
    }
  });

  function responsiveTabsTrigger() {
    if (window.matchMedia("(max-width: 600px)").matches) {
      $("form div.vertical-tabs").responsiveTabs();
    } else {
      $("form div.vertical-tabs").responsiveTabs('destroy');
    }
  }

  Drupal.behaviors.JSresponsiveTabs = {
    attach: function () {
      if (typeof _ !== 'undefined' && typeof _.debounce !== 'undefined' && $.isFunction(_.debounce)) {
        $(window).resize(_.debounce(responsiveTabsTrigger, 300)).resize();
      } else {
        $(window).resize(responsiveTabsTrigger).resize();
      }
    }
  };
}(jQuery));
