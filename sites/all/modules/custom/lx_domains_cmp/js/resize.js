jQuery(document).ready(function () {
    
    jQuery(window).resize(function() {
       if (jQuery(window).width() < 700) {
           jQuery('.mobile-header').html('<h1>' + jQuery('h1.page-title').text() + '</h1>');
           jQuery('.page-title').hide();
       } 
       else {
           jQuery('.mobile-header').html('');
           jQuery('.page-title').show();
       }
    });    
    
    
    if (jQuery(window).width() < 700) {
        jQuery('.mobile-header').html('<h1>' + jQuery('h1.page-title').text() + '</h1>');
        jQuery('.page-title').hide();
    } 
    else {
        jQuery('.mobile-header').html('');
        jQuery('.page-title').show();
    }    
    
});