Drupal.behaviors.jqueryui_autocomplete = {
    attach: function (context, settings) {
      jQuery('.country-autocomplete', context).autocomplete({
        source: "/country/autocomplete",
        minLength: 2,
        select: function( event, ui ) {
            event.preventDefault();
            jQuery('#' + event.target.id).parents('.form-item').next().append('<div class="selected-itm">' + ui.item.value + ' <a class="del-itm-selected del-country" data-id="' + ui.item.id + '" href="#">x</a></div>');
            jQuery('.form-item-location-list textarea').append(ui.item.id + ',');
            jQuery('#' + event.target.id).val('');
        }
      });
      jQuery('.isp-autocomplete', context).autocomplete({
        source: "/isp/autocomplete",
        minLength: 2,
        select: function( event, ui ) {
            event.preventDefault();
            
            document.getElementById("edit-isp-list").value =  jQuery.trim(jQuery('.form-item-isp-list #edit-isp-list').val() + "\n"  + ui.item.value);
            
//            jQuery('.form-item-isp-list #edit-isp-list').attr('value',);
            jQuery('#' + event.target.id).val('');
        }
      });
    }
  };
  
jQuery(document).ready(function () {
    jQuery('form').on('click', '.del-itm-selected', function () {
        
       if (jQuery(this).hasClass('del-country')) {
           txt = jQuery('.form-item-location-list textarea');
           c = txt.text();
           c = c.toString().replace(jQuery(this).attr('data-id') + ',', '');
           txt.text(c);
       }
        
       jQuery(this).parents('.selected-itm').remove();

       return false;
    });
    

    jQuery('#time-slider').slider({
      range: true,
      values: [ jQuery('#cur-time').attr('data-starttime'), jQuery('#cur-time').attr('data-finishtime') ],
      min: 0,
      max: 288,
      step: 1,
      slide: function( event, ui ) {
        str = '';  
          
        h = parseInt(ui.values[0]/12);
        m = (parseInt(ui.values[0] - h*12)*5);
        
        if (h < 10) {
            str += '0' + h.toString();
        }
        else {
            str += h;
        }
        str += ':';
        if (m < 10) {
            str += '0' + m.toString();
        }
        else {
            str += m;
        }
        str += ' - ';
        
        h = parseInt(ui.values[1]/12);
        m = (parseInt(ui.values[1] - h*12)*5);
        
        if (h < 10) {
            str += '0' + h.toString();
        }
        else {
            str += h.toString();
        }
        str += ':';
        if (m < 10) {
            str += '0' + m.toString();
        }
        else {
            str += m.toString();
        }        
          
        jQuery( "#cur-time" ).html(str);
        jQuery( "#selectedtime" ).attr('value', str);
      }
    });          
    
    jQuery('.os-select .form-checkbox').click(function () {
        if (jQuery(this).prop('checked')) {
            jQuery(this).parents('.os-select').prev().find('.form-checkbox').prop('checked', true);
        }    
        else {
            any_checked = false;
            jQuery(this).parents('.os-select').find('.form-checkbox').each(function () {
                if (jQuery(this).prop('checked')) {
                    any_checked = true;
                }
            });
            
            if (!any_checked) {
                jQuery(this).parents('.os-select').prev().find('.form-checkbox').prop('checked', false);
            }
        }
    });
    
    jQuery('.device-chbx').click(function () {
        if (jQuery(this).prop('checked')) {
            jQuery(this).parents('.form-item').next().find('.form-checkbox').prop('checked', true);
        }    
        else {
            jQuery(this).parents('.form-item').next().find('.form-checkbox').prop('checked', false);
        }
    });


    
    jQuery('.redir-din-tabs a').click(function (e) {
        e.preventDefault();
        jQuery(this).parent('.redir-din-tabs').find('a').removeClass('seltab');
        seltype = jQuery(this).attr('data-type');
        
        jQuery(this).parents('.field-prefix').find('.form-text').removeClass('redirect-type');
        jQuery(this).parents('.field-prefix').find('.form-text').removeClass('showpage-type');
        
        if (seltype == 'redir') {
            jQuery(this).addClass('seltab');
            jQuery(this).parents('.field-prefix').find('.form-text').addClass('redirect-type');
            
            jQuery(this).parent('.redir-din-tabs').prev().attr('value', 1);
            
            jQuery(this).parents('.form-item').next().show();
        }
        else {
            jQuery(this).addClass('seltab');
            jQuery(this).parents('.field-prefix').find('.form-text').addClass('showpage-type');
            
            jQuery(this).parent('.redir-din-tabs').prev().attr('value', 2);
            
            jQuery(this).parents('.form-item').next().hide();
        }

        jQuery(this).parents('.field-prefix').find('.redir-class').hide();
        jQuery(this).parents('.field-prefix').find('.dyn-class').hide();        
        jQuery(this).parents('.field-prefix').find('.' + seltype + '-class').show();
    });
});