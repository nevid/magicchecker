jQuery(document).ready(function () {
    jQuery('.long-trimmed').click(function () {
        jQuery('.show-full-str .content-data').text(jQuery(this).text());
        jQuery('.show-full-str').fadeIn('fast');
    });
    
    jQuery('.wildcards-link').click(function () {
        jQuery('.show-full-str .content-data').html(jQuery('.wildcards').html());
        jQuery('.show-full-str').fadeIn('fast');        
    });
    
    jQuery('.cmp-warn i.fa').click(function () {
        jQuery('.show-full-str .content-data').html(jQuery(this).next('.cmp-warns-list').html());
        jQuery('.show-full-str').fadeIn('fast');
    });     
    
    jQuery('.show-full-str .close').click(function (e) {
        e.preventDefault();
        
        jQuery('.show-full-str').fadeOut('fast');
    });
    
    
    jQuery('.link-tip').click(function (e) {
        e.preventDefault();
        jQuery('.show-full-str .content-data').text(jQuery(this).data('text'));
        jQuery('.show-full-str').fadeIn('fast');
    });
    
    jQuery('.day-row').click(function () {
        lk = jQuery(this).data('cmps-link');
        
        console.log(window.firstClick);
        
        if (lk) {
            jQuery.get(lk, function (data) {
                jQuery('.show-full-str .content-data').html(data);
                
                var $message = jQuery('.show-full-str');

                if ($message.css('display') != 'block') {
                    $message.fadeIn('fast');

                    window.firstClick = true;
                    jQuery(document).bind('click.myEvent', function(e) {
                        if (firstClick && jQuery(e.target).closest('.show-full-str').length == 0) {
                            $message.fadeOut('fast');
                            jQuery(document).unbind('click.myEvent');
                        }
                    });
                }                
                
            });
        }
    });    
});