Drupal.behaviors.jqueryui_autocomplete = {
    attach: function (context, settings) {
       function _check_tags_amount() {
           num = jQuery('.tag-selector .selected-itm').size();
           console.log(num);
           if (num >= 10) {
               jQuery('.tag-autocomplete').attr('placeholder', 'Max 10 Tags');
               jQuery('.tag-autocomplete').attr('disabled', 'disabled');
           }
           else if (num == 0 && jQuery('.tag-autocomplete').hasClass('has-long-state')) {
               jQuery('.tag-autocomplete').addClass('long-placeholder');
               jQuery('.tag-autocomplete').attr('placeholder', 'Separate tags by commas or press Enter');
               jQuery('.tag-autocomplete').removeAttr('disabled');               
           }
           else {
               jQuery('.long-placeholder').removeClass('long-placeholder');
               jQuery('.tag-autocomplete').attr('placeholder', 'Input Tags...');
               jQuery('.tag-autocomplete').removeAttr('disabled');
           }
       }  
       
       function tagExists(new_tag) {
           var already_exists = false;
            jQuery('.tag-selector .selected-itm').each(function () {
                    cur = jQuery(this).text();
//                    console.log('"' + jQuery.trim(cur) + '" = ' + new_tag);
                    if (jQuery.trim(cur) == new_tag) {
                        jQuery(this).addClass('tagExists');
                        setTimeout(function () {jQuery('.tagExists').removeClass('tagExists')}, 500);
                        already_exists = true;
                    }
                });
                
            return already_exists; 
       }
       
        function split( val ) {
          return val.split( /,\s*/ );
        }
        function extractLast( term ) {
          return split( term ).pop();
        }       
        
        
      jQuery('.country-autocomplete', context).autocomplete({
        source: "/country/autocomplete",
        minLength: 2,
        select: function( event, ui ) {
            event.preventDefault();
            jQuery('#' + event.target.id).parents('.form-item').next().append('<div class="selected-itm">' + ui.item.value + ' <a class="del-itm-selected del-country" data-id="' + ui.item.id + '" href="#"><i class="fa fa-times" aria-hidden="true"></i></a></div>');
            jQuery('.form-item-location-list textarea').append(ui.item.id + ',');
            jQuery('#' + event.target.id).val('');
        }
      });
      
      jQuery('.lang-autocomplete', context).autocomplete({
        source: "/lang/autocomplete",
        minLength: 2,
        select: function( event, ui ) {
            event.preventDefault();
            jQuery('#' + event.target.id).parents('.form-item').next().append('<div class="selected-itm">' + ui.item.value + ' <a class="del-itm-selected del-lang" data-id="' + ui.item.id + '" href="#"><i class="fa fa-times" aria-hidden="true"></i></a></div>');
            jQuery('.form-item-langs textarea').append(ui.item.id + ',');
            jQuery('#' + event.target.id).val('');
        }
      });      
      
      jQuery('.isp-autocomplete', context).autocomplete({
        source: "/isp/autocomplete",
        minLength: 2,
        select: function( event, ui ) {
            event.preventDefault();
            document.getElementById("edit-isp-list").value =  jQuery.trim(jQuery('.form-item-isp-list #edit-isp-list').val() + "\n"  + ui.item.value);
            jQuery('#' + event.target.id).val('');
        }
      });
      jQuery('.tag-autocomplete', context).autocomplete({
        source: function( request, response ) {
                        exclude = jQuery('.form-item-tag-list textarea').val();
                        jQuery.getJSON( "/tag/autocomplete?exclude=" + encodeURIComponent(exclude), {
                          term: extractLast( request.term )
                        }, response );
                      },
        minLength: 2,
        select: function( event, ui ) {
            event.preventDefault();
            jQuery('#edit-sel-tag').before('<div class="selected-itm">' + ui.item.value + ' <a class="del-itm-selected del-tag" data-id="' + ui.item.id + '" href="#"><i class="fa fa-times" aria-hidden="true"></i></a></div>');
            jQuery('.form-item-tag-list textarea').append(ui.item.id + ',');
            jQuery('#' + event.target.id).val('');
            _check_tags_amount();
        }
      });  
      
        jQuery('.new-allowed .tag-autocomplete').keypress(function(e) {
            if(e.keyCode==13){
                e.preventDefault();
                
                new_tag = jQuery(this).val();
                already_exists = tagExists(new_tag);
                
                if (!already_exists) {
                    jQuery('#edit-sel-tag').before('<div class="selected-itm">' + new_tag + ' <a class="del-itm-selected del-tag" data-id="' + new_tag + '" href="#"><i class="fa fa-times" aria-hidden="true"></i></a></div>');
                    jQuery('.form-item-tag-list textarea').append(new_tag + ',');
                }    
                jQuery(this).val('');
                _check_tags_amount();
            }
        });   
      
        jQuery('.tag-autocomplete').keyup(function(e) {
            if(e.keyCode==8){
                str = jQuery('.tag-autocomplete').val();
                if (str.length == 0) {
                    jQuery(this).prev('.selected-itm').find('.del-itm-selected').trigger('click');
                }
            }
            else {
                str = jQuery(this).val();
                if (str.indexOf(',') != -1) {
                    new_tag = jQuery(this).val();
                    new_tag = new_tag.replace(',', '');
                    already_exists = tagExists(new_tag);
                    if (!already_exists) {
                        jQuery('#edit-sel-tag').before('<div class="selected-itm">' + new_tag + ' <a class="del-itm-selected del-tag" data-id="' + new_tag + '" href="#"><i class="fa fa-times" aria-hidden="true"></i></a></div>');
                        jQuery('.form-item-tag-list textarea').append(new_tag + ',');
                    }    
                    jQuery(this).val('');
                    _check_tags_amount();
                }                    
            }            
        });   
        
        jQuery('.tag-autocomplete').blur(function () {
            str = jQuery(this).val();
            if (str.length) {
                new_tag = jQuery(this).val();
                new_tag = new_tag.replace(',', '');
                already_exists = tagExists(new_tag);
                if (!already_exists) {
                    jQuery('#edit-sel-tag').before('<div class="selected-itm">' + new_tag + ' <a class="del-itm-selected del-tag" data-id="' + new_tag + '" href="#"><i class="fa fa-times" aria-hidden="true"></i></a></div>');
                    jQuery('.form-item-tag-list textarea').append(new_tag + ',');
                }    
                jQuery(this).val('');
                _check_tags_amount();
            }
        });
        
        jQuery('.tag-selector').click(function () {
            jQuery(this).find('.form-text').focus();
        });
        
        _check_tags_amount();
      
    }
  };
  
jQuery(document).ready(function () {
       function _check_tags_amount() {
           num = jQuery('.tag-selector .selected-itm').size();
//           console.log(num);
           if (num >= 10) {
               jQuery('.tag-autocomplete').attr('placeholder', 'Max 10 Tags');
               jQuery('.tag-autocomplete').attr('disabled', 'disabled');
           }
           else if (num == 0 && jQuery('.tag-autocomplete').hasClass('has-long-state')) {
               jQuery('.tag-autocomplete').addClass('long-placeholder');
               jQuery('.tag-autocomplete').attr('placeholder', 'Separate tags by commas or press Enter');
               jQuery('.tag-autocomplete').removeAttr('disabled');               
           }
           else {
               jQuery('.long-placeholder').removeClass('long-placeholder');
               jQuery('.tag-autocomplete').attr('placeholder', 'Input Tags...');
               jQuery('.tag-autocomplete').removeAttr('disabled');
           }
       }  
       
    jQuery('form').on('click', '.del-itm-selected', function () {
        
       if (jQuery(this).hasClass('del-country')) {
           txt = jQuery('.form-item-location-list textarea');
           c = txt.text();
           c = c.toString().replace(jQuery(this).attr('data-id') + ',', '');
           txt.text(c);
       }
       else if (jQuery(this).hasClass('del-tag')) {
           txt = jQuery('.form-item-tag-list textarea');
           c = txt.text();
           c = c.toString().replace(jQuery(this).attr('data-id') + ',', '');
           txt.text(c);
       }
       else if (jQuery(this).hasClass('del-lang')) {
           txt = jQuery('.form-item-langs textarea');
           c = txt.text();
           c = c.toString().replace(jQuery(this).attr('data-id') + ',', '');
           txt.text(c);
       }
        
       jQuery(this).parents('.selected-itm').remove();
       _check_tags_amount();

       return false;
    });
    

    if (jQuery('#time-slider').length) {
        jQuery('#time-slider').slider({
          range: true,
          values: [ jQuery('#cur-time').attr('data-starttime'), jQuery('#cur-time').attr('data-finishtime') ],
          min: 0,
          max: 288,
          step: 1,
          slide: function( event, ui ) {
            str = '';  

            h = parseInt(ui.values[0]/12);
            m = (parseInt(ui.values[0] - h*12)*5);

            if (h < 10) {
                str += '0' + h.toString();
            }
            else {
                str += h;
            }
            str += ':';
            if (m < 10) {
                str += '0' + m.toString();
            }
            else {
                str += m;
            }
            str += ' - ';

            h = parseInt(ui.values[1]/12);
            m = (parseInt(ui.values[1] - h*12)*5);

            if (h < 10) {
                str += '0' + h.toString();
            }
            else {
                str += h.toString();
            }
            str += ':';
            if (m < 10) {
                str += '0' + m.toString();
            }
            else {
                str += m.toString();
            }        

            jQuery( "#cur-time" ).html(str);
            jQuery( "#selectedtime" ).attr('value', str);
          }
        });  
    }    
    
    jQuery('.os-select .form-checkbox').click(function () {
        if (jQuery(this).prop('checked')) {
            jQuery(this).parents('.os-select').prev().find('.form-checkbox').prop('checked', true);
        }    
        else {
            any_checked = false;
            jQuery(this).parents('.os-select').find('.form-checkbox').each(function () {
                if (jQuery(this).prop('checked')) {
                    any_checked = true;
                }
            });
            
            if (!any_checked) {
                jQuery(this).parents('.os-select').prev().find('.form-checkbox').prop('checked', false);
            }
        }
    });
    
    jQuery('.device-chbx').click(function () {
        if (jQuery(this).prop('checked')) {
            jQuery(this).parents('.form-item').next().find('.form-checkbox').prop('checked', true);
            jQuery(this).parents('.form-item').next().find('.form-type-checkbox').last().find('.form-checkbox').prop('checked', false);
        }    
        else {
            jQuery(this).parents('.form-item').next().find('.form-checkbox').prop('checked', false);
        }
    });


    
    jQuery('.redir-din-tabs a').click(function (e) {
        e.preventDefault();
        jQuery(this).parent('.redir-din-tabs').find('a').removeClass('seltab');
        seltype = jQuery(this).attr('data-type');
        
        jQuery(this).parents('.field-prefix').find('.form-text').removeClass('redirect-type');
        jQuery(this).parents('.field-prefix').find('.form-text').removeClass('showpage-type');
        
        if (seltype == 'redir') {
            jQuery(this).addClass('seltab');
            jQuery(this).parents('.field-prefix').find('.form-text').addClass('redirect-type');
            
            jQuery(this).parent('.redir-din-tabs').prev().attr('value', 1);
            
            jQuery(this).parents('.form-item').next().show();
        }
        else {
            jQuery(this).addClass('seltab');
            jQuery(this).parents('.field-prefix').find('.form-text').addClass('showpage-type');
            
            jQuery(this).parent('.redir-din-tabs').prev().attr('value', 2);
            
            jQuery(this).parents('.form-item').next().hide();
        }

        jQuery(this).parents('.field-prefix').find('.redir-class').hide();
        jQuery(this).parents('.field-prefix').find('.dyn-class').hide();        
        jQuery(this).parents('.field-prefix').find('.' + seltype + '-class').show();
    });
    
    jQuery('.on-off-boxes .chbxes-on').click(function (e) {
        e.preventDefault();
        jQuery('#edit-browsers .form-checkbox').prop('checked', true);
        jQuery('#edit-browsers').find('.form-type-checkbox').last().find('.form-checkbox').prop('checked', false);
    });
    
    jQuery('.on-off-boxes .chbxes-off').click(function (e) {
        e.preventDefault();
        jQuery('#edit-browsers .form-checkbox').prop('checked', false);
    });
    
    
    function array_chunk( input, size ) {	// Split an array into chunks
            // 
            // +   original by: Carlos R. L. Rodrigues

            for(var x, i = 0, c = -1, l = input.length, n = []; i < l; i++){
                    (x = i % size) ? n[c][x] = input[i] : n[++c] = [input[i]];
            }

            return n;
    }
    
    function rebuildBrowsers() {
        
        browsers_cnt = jQuery('#edit-browsers .form-item').length;
        
        if (browsers_cnt) {
            browsers = jQuery('#edit-browsers .form-item');
            browsers_cnt = jQuery('#edit-browsers .form-item').length;

            wrapper_width = jQuery('#edit-browsers-fs').width();
            chunk_size = Math.ceil(browsers_cnt/Math.floor(wrapper_width/210));

            chunks = array_chunk(browsers, chunk_size);

            rebuilt = '';
            for (var i in chunks) {
                rebuilt = rebuilt + '<div class="checkbox-col">';

                for (var j in chunks[i]) {
                    rebuilt = rebuilt + chunks[i][j].outerHTML;
                }

                rebuilt = rebuilt + '</div>';
            }

            jQuery('#edit-browsers').html(rebuilt);  
        }    
    }
    
    
        
    if( jQuery("#lx-domains-cmp-form").length ) {
        jQuery( window ).resize(function() {
          rebuildBrowsers();
        });
        
        if (jQuery('#edit-browsers .form-item').length) {
            jQuery('#edit-browsers-fs legend span').text('Browsers');
        }        
    }      
    
});