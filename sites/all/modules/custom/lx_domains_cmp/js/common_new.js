jQuery(document).ready(function () {
    jQuery('.operations').mouseover(function () {
        if (jQuery(window).width() > 988) {
            jQuery(this).find('ul').show();
        }    
    });
    
    jQuery('.operations').mouseout(function () {
        if (jQuery(window).width() > 988) {
            jQuery(this).find('ul').hide();
        }    
    });
    
    jQuery('.operations .action-btn').click(function (e) {
        e.preventDefault();
        
        if (jQuery(this).next().hasClass('menu-vis')) {
            jQuery(this).next().hide();
            jQuery(this).next().removeClass('menu-vis');
        }
        else {
            jQuery(this).next().show();
            jQuery(this).next().addClass('menu-vis');
        }
        
        return false;
    });    
    
    function _reshowCols(cols) {
        jQuery('.column-switcher').html('<span class="show-hide-txt"><i class="fa fa-eye" aria-hidden="true"></i></span></h2>');
        show_divs = '<div class="column-switcher-chbxs">';
        for (var k in cols) {
            show_divs = show_divs + '<div title="' + (cols[k]['is_shown'] == 1 ? 'Filter is enabled now and has blocks' : 'Filter is not enebled now, but has blocks in this month') + '" class="form-item"' + (cols[k]['hide_box'] ? ' style="display:none"' : '') + '> <input class="form-checkbox"' + (cols[k]['is_shown'] == 1 ? ' checked="checked"' : '') + ' type="checkbox" id="chk-swchk' + cols[k]['num'] + '" name="swchk' + cols[k]['num'] + '" value="' + cols[k]['num'] + '"  /> <label for="chk-swchk' + cols[k]['num'] + '">' + cols[k]['name'] + '</label></div>';
            if (cols[k]['is_shown'] === 0) {
                jQuery('.stat-table table thead th:nth-child(' + cols[k]['num'] + ')').hide();
                jQuery('.stat-table table tbody td:nth-child(' + cols[k]['num'] + ')').hide();                    
            }
        }
        show_divs = show_divs + '</div>';
        jQuery('.column-switcher').append(show_divs);
    }
    
    jQuery('.column-switcher').on('change', '.form-checkbox', function () {        
       if(this.checked) {
           jQuery('.stat-table table thead th:nth-child(' + jQuery(this).val() + ')').fadeIn('Slow');
           jQuery('.stat-table table tbody td:nth-child(' + jQuery(this).val() + ')').fadeIn('Slow');
           
           var col_num = jQuery(this).val();
           
           jQuery('.stat-table table tbody td:nth-child(' + col_num + ')').addClass('tmp-highlight');
           setInterval(function () {jQuery('.stat-table table tbody td:nth-child(' + col_num + ')').removeClass('tmp-highlight');}, 2000);
       }
       else {
           jQuery('.stat-table table thead th:nth-child(' + jQuery(this).val() + ')').fadeOut('Slow');
           jQuery('.stat-table table tbody td:nth-child(' + jQuery(this).val() + ')').fadeOut('Slow');           
       }
    });
    
    
    jQuery('.td-stat').mouseover(function () {
        jQuery(this).find('.short_stat').show();
    });
    
    jQuery('.td-stat').mouseout(function () {
        jQuery(this).find('.short_stat').hide();
    });
    
    jQuery('.td-stat').click(function () {
        jQuery(this).find('.short_stat').toggleClass('visible');
    });
    
    jQuery('table').on('click', '.status-control .status-control-btn', function () {
        jQuery(this).parents('.status-control').find('.status-control-btn').removeClass('active');
        jQuery(this).addClass('active');
        jQuery(this).parents('.status-control').prev().html(jQuery(this).text());
        jQuery.get(jQuery(this).attr('href'));
        
        return false;
    });
    
    function __resize_cmp_table() { 
        cmp_table = jQuery('.cmp-list-table');
        if (cmp_table.length) {
            if ((jQuery(window).width() < 801)) {
                if (!jQuery('.cmp-list-table').hasClass('is-appended')) {
                    jQuery('.cmp-list-table tbody tr').each(function () {
                       appendto = jQuery(this).find('.apender');
                       appendto.append('<div class="total-n-safe"><div class="name-total">Total: <b>' + jQuery(this).find('.td-total-total').html() + '</b></div><div class="name-safe">Safe <span class="xs-hid">clicks</span>: <b>' + jQuery(this).find('.td-total-safe').html() + '</b></div></div>'); 
                       appendto.append('<div class="name-sts clearfix">'+jQuery(this).find('.td-status').html() + '</div>'); 
                    });

                    jQuery('.name').addClass('appended');
                    jQuery('.td-status').hide();
                    jQuery('.td-total').hide();

                    jQuery('.cmp-list-table').addClass('is-appended');
                }    
                
                if ((jQuery(window).width() < 515)) {
                    if (!jQuery('.cmp-list-table').hasClass('is-appended-2')) {
                        jQuery('.cmp-list-table tbody tr').each(function () {
                           appendto = jQuery(this).find('.apender');
                           appendto.append('<div class="appended-btns clearfix">'+jQuery(this).find('.td-actions').html() + '</div>'); 
                        });
                        
                        jQuery('.td-actions').hide();
                        jQuery('.cmp-list-table').addClass('is-appended-2');
                    }
                }
                else {
                    if (jQuery('.cmp-list-table').hasClass('is-appended-2')) {
                        jQuery('.appended-btns').remove();
                        jQuery('.td-actions').show();
                        jQuery('.cmp-list-table').removeClass('is-appended-2');
                    } 
                }                
                
                
            }
            else {
                if (jQuery('.cmp-list-table').hasClass('is-appended')) {
                    jQuery('.apender').html('');
                    jQuery('.name').removeClass('appended');
                    jQuery('.td-status').show();
                    jQuery('.td-total').show();     
                    jQuery('.cmp-list-table').removeClass('is-appended');
                }    
            }
        }   
    }
    
    jQuery('.mobile-icon').click(function () {
        jQuery(this).toggleClass('shown');
        if (jQuery(this).hasClass('shown')) {
            if (!jQuery('.closebtn-wrapper').length) {
                jQuery('#block-lx-domains-cmp-clientmenu .content ul').prepend('<li class="closebtn-wrapper"><a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></li>');
            }    
            jQuery('#block-lx-domains-cmp-clientmenu').removeClass('mobile-hidden');
            
            jQuery('.closebtn-wrapper a').click(function (e) {
                e.preventDefault();
                
                jQuery('#block-lx-domains-cmp-clientmenu .content ul').fadeOut('slow', function () {
                    jQuery('.mobile-icon').toggleClass('shown');
                    jQuery('#block-lx-domains-cmp-clientmenu').addClass('mobile-hidden');
                }); 
            });            
            
            jQuery('#block-lx-domains-cmp-clientmenu .content ul').fadeIn('slow');
            
        }
    });
    
    function __resize_satistics_table() {
        if(jQuery(document).width() > 690) {   
            if (jQuery('#block-lx-domains-cmp-clientmenu').hasClass('mobile-hidden')) {
                jQuery('#block-lx-domains-cmp-clientmenu .content ul').show();
            }    
            
            jQuery('.full-stat-table .overflow-fix').css('width', '100%');
        }
        else {
            
            jQuery('.full-stat-table .overflow-fix').width(jQuery('#content').width() - 20);
        }        
        
        if(jQuery(document).width() > 1400) {   
            jQuery('.stat-log-table .overflow-fix').css('width', '100%');
        }
        else {
            if(jQuery(document).width() > 690) {   
                jQuery('.stat-log-table .overflow-fix').width(jQuery('#branding').width());
            }   
            else {
                jQuery('.stat-log-table .overflow-fix').width(jQuery('#branding').width() - 10);
            }
        }         
        
    }
    
    jQuery(window).resize(function(){
        __resize_cmp_table();
        __resize_satistics_table();       
    });    
    
    var cols = {};
    switcher = jQuery('.column-switcher');
    if (switcher.length && !cols.length) {
        var i = 1;
        jQuery('.stat-table table:first-child thead th').each(function () {
            if (!jQuery(this).hasClass('nohid')) {
                _state = 1;
                if (jQuery(this).hasClass('hidbydefault')) {
                    _state = 0;
                }
                
                hide_box = 0;
                if (jQuery('.total-row td:nth-child(' + i + ')').attr('data-value') == '0') {
                    hide_box = 1;
                }
                
                cols[jQuery(this).text()] = {name: jQuery(this).text(), is_shown: _state, num: i, hide_box: hide_box};
            }    
            
            i ++;
        });
        _reshowCols(cols);
    }
    
    
    __resize_cmp_table();
    __resize_satistics_table();
    
});