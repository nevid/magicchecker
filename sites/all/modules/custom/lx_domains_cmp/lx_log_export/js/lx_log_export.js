$ = jQuery;
$(document).ready(function () {

    $('#pubs-report-link').on('click', function(e) {
        pubsIdPopup();
        e.preventDefault();
    });

    $('#export-report-link').on('click', function(e) {
        var hrefWithoutPars = $(this).attr('href').split('?')[0];
        var hrefWithPars = $(this).attr('href');
        window.location.href = 'http://' + document.domain + generateUrlWithPars(hrefWithoutPars, hrefWithPars);
        e.preventDefault();
    });
    
    $('.export-log').mouseenter(function () {
        $('.actions').addClass('hovered');
    });
    
    $('.actions').mouseleave(function () {
        $(this).removeClass('hovered');
    });

    function generateUrlWithPars(hrefWithoutPars, hrefWithPars) {
        form_data = $("form#lx-domains-cmp-log-filters").serializeArray();
        var cid = hrefWithPars.split('cid=')[1].split('&')[0];
        var repType = hrefWithPars.split('rep_type=')[1].split('&')[0];
        var queryPars = 'cid=' + cid + '&rep_type=' + repType + '&';
        $.each( form_data, function( i, value ){
            if (i != (form_data.length - 1)) {
                queryPars += form_data[i]['name'] + '=' + form_data[i]['value'] + '&';
            } else {
                queryPars += form_data[i]['name'] + '=' + form_data[i]['value'];
            }
        });
        var currentUrl = window.location.pathname.replace('/', '').split('?')[0];
        var dst = currentUrl.replace('://', "%3A%2F%2F") + '%3F' + queryPars.replace(/&/g,"%26").replace(/=/g, "%3D");
        return hrefWithoutPars + '?' + queryPars + "&dst=" + dst;
    }

    function pubsIdPopup() {
        $('#page #content').addClass('bg-blured');

        $('body').append(
            '<div class="overlay-over-blured-pubs" >' +
              '<div class="overlay-over-blured-content-pubs" id = "overlay-over-blured-content-pubs">' +
                '' +
                '<div class="conf-text pubs-id-inputs">' +
                  '<input placeholder="Enter pub’s parameter" id="edit-pubs-id" name="pubsid" value="" size="15" maxlength="128" class="form-text" type="text">' +
                '<div class="pubs-id-text">Enter pub’s parameter</div></div>' +
                  '<div class="conf-buttons"><a class = "confirm-no cancel-pubs-export" href = "">Cancel</a>' +
                  '<a href = "' +  $('#pubs-report-link').attr('href') + '" class = "confirm-yes export-pubs-report">Export</a></div>' +                
              '</div>' +
            '</div>'
        );

        $('a.export-pubs-report').on('click', function(e) {
            e.preventDefault();
            if ($('#edit-pubs-id').val() !== '') {
                var hrefWithoutPars = $('a.export-pubs-report').attr('href').split('?')[0];
                var hrefWithPars = $('a.export-pubs-report').attr('href');
                window.location.href = 'http://' + document.domain + generateUrlWithPars(hrefWithoutPars, hrefWithPars) + '&pubs_id=' + $('#edit-pubs-id').val(); //$('.export-log a#pubs-report-link').attr('href')
            } else {
                $('.pubs-id-text').text('Field is required.');
                $('.pubs-id-text').fadeIn('fast');
            }
        });

        $('a.cancel-pubs-export').on('click', function(e) {
            closePubsIdPopup();
            e.preventDefault();
        });

    }

    function closePubsIdPopup() {
        $('#page #content').removeClass('bg-blured');
        $('.overlay-over-blured-pubs').remove();
    }
});