<?php

//require_once '/home/api173magic/data/www/api.magicchecker.com/v1.0/server_utility.php';
require_once '/home/api173magic/data/www/api.magicchecker.com/vendor/autoload.php';
use MaxMind\Db\Reader;  


// UTILS

function _helper_cidrToRange($cidr) {
  $range = array();
  $cidr = explode('/', $cidr);
  $range[0] = long2ip((ip2long($cidr[0])) & ((-1 << (32 - (int)$cidr[1]))));
  $range[1] = long2ip((ip2long($cidr[0])) + pow(2, (32 - (int)$cidr[1])) - 1);
  return $range;
}

function _helper_cidrToRange_v6($cidr) {
    $range = array();
  
    // Split in address and prefix length
    list($firstaddrstr, $prefixlen) = explode('/', $cidr);

    // Parse the address into a binary string
    $firstaddrbin = inet_pton($firstaddrstr);

    // Convert the binary string to a string with hexadecimal characters
    # unpack() can be replaced with bin2hex()
    # unpack() is used for symmetry with pack() below
    $firstaddrhex = reset(unpack('H*', $firstaddrbin));

    // Overwriting first address string to make sure notation is optimal
    $firstaddrstr = inet_ntop($firstaddrbin);

    // Calculate the number of 'flexible' bits
    $flexbits = 128 - $prefixlen;

    // Build the hexadecimal string of the last address
    $lastaddrhex = $firstaddrhex;

    // We start at the end of the string (which is always 32 characters long)
    $pos = 31;
    while ($flexbits > 0) {
      // Get the character at this position
      $orig = substr($lastaddrhex, $pos, 1);

      // Convert it to an integer
      $origval = hexdec($orig);

      // OR it with (2^flexbits)-1, with flexbits limited to 4 at a time
      $newval = $origval | (pow(2, min(4, $flexbits)) - 1);

      // Convert it back to a hexadecimal character
      $new = dechex($newval);

      // And put that character back in the string
      $lastaddrhex = substr_replace($lastaddrhex, $new, $pos, 1);

      // We processed one nibble, move to previous position
      $flexbits -= 4;
      $pos -= 1;
    }

    // Convert the hexadecimal string to a binary string
    # Using pack() here
    # Newer PHP version can use hex2bin()
    $lastaddrbin = pack('H*', $lastaddrhex);

    // And create an IPv6 address from the binary string
    $lastaddrstr = inet_ntop($lastaddrbin);

    
    $range = array($firstaddrstr, $lastaddrstr);
    
    return $range;
}

// /UTILS

function lx_domains_admin_helpers() {
    return '<p>Admin Helpers</p>'
    . '     <p><ul>'
            . '<li><a href="/admin/content/campaigns/helpers/check-country">Check country by IP</a></li>'
            . '<li><a href="/admin/content/campaigns/helpers/ip-to-long">Convert ip/diapazon to long</a></li>'
            . '</ul>'
            . '</p>';
}

function lx_domains_admin_checks_country() {
    
    $form['ip'] = array('#type' => 'textfield',
        '#title' => 'IP'
        );
    
    $form['#method'] = 'get';
    
    $form['submit'] = array('#type' => 'submit',
        '#value' => 'Check IP'
        );
    
    if (isset($_GET['ip'])) {
        $databaseFile = '/etc/php5/GeoIP/GeoIP2-Country_20160524/GeoIP2-Country.mmdb';
        $reader = new Reader($databaseFile);
        $data = $reader->get(trim($_GET['ip']));
        $reader->close();        
        
        
        $form['mu'] = array('#type' => 'markup',
            '#markup' => '<pre>' .print_r($data, true) .'</pre>'
            );
    }    
    
    return $form;
}


function lx_domains_admin_ip_converts() {
    
    $form = array();
    
    $form['version'] = array('#type' => 'radios',
        '#title' => 'IP version',
        '#options' => array('v4' => 'v4', 'v6' => 'v6'),
        '#default_value' => 'v4',
        '#prefix' => $output
        );
    
    $form['ips'] = array('#type' => 'textarea',
        '#title' => 'IP/Diapazons',
        '#rows' => 10,
        '#required' => true
        );
    
    $form['as_array'] = array('#type' => 'checkbox',
        '#title' => 'Give result as php array'
        );
    
    $form['submit'] = array('#type' => 'submit',
        '#value' => 'Convert'
        );
    
    return $form;
}


function lx_domains_admin_ip_converts_submit() {
    if (isset($_POST['version'])) {

        $ip_version = $_POST['version'];

        $result = array();

        $ips = explode("\n", $_POST['ips']);

        if (sizeof($ips)) {            
            foreach ($ips as $ip) {
                if (!trim($ip)) continue;
                if (strpos($ip, '/') !== false) {
                    if ($ip_version == 'v4') {
                        $result[$ip] = _helper_cidrToRange($ip);
                        $result[$ip][0] = ip2long($result[$ip][0]);
                        $result[$ip][1] = ip2long($result[$ip][1]);
                    }
                    else {
                        $result[$ip] = _helper_cidrToRange_v6($ip);                    
                        $result[$ip][0] = ip2long_v6($result[$ip][0]);
                        $result[$ip][1] = ip2long_v6($result[$ip][1]);                        
                    }
                }
                else if (strpos($ip, '-') !== false) {
                    if ($ip_version == 'v4') {
                        $result[$ip] = explode('-', $ip);
                        
                        $result[$ip][0] = ip2long(trim($result[$ip][0]));
                        $result[$ip][1] = ip2long(trim($result[$ip][1]));
                    }
                    else {
                        $result[$ip] = explode('-', $ip);                    
                        $result[$ip][0] = ip2long_v6(trim($result[$ip][0]));
                        $result[$ip][1] = ip2long_v6(trim($result[$ip][1]));                        
                    }                    
                }
                else {
                    if ($ip_version == 'v4') {
                        $ip_long = ip2long($ip);
                    }
                    else {
                        $ip_long = ip2long_v6($ip);
                    }                   

                    $result[$ip] = $ip_long;
                }
            }

            if (sizeof($result)) {
                
                if (!$_POST['as_array']) {
                    foreach ($result as $r) {

                        $output .= (is_array($r) ? implode(' - ', $r) : $r) ."\n";
                    }
                }
                else {
                    $output .= '$ips = array(';

                    foreach ($result as $r) {
                        $output .= (is_array($r) ? 'array("' .$r[0] .'", "' .$r[1] .'"),' : 'array("' .$r .'"),') ."\n";
                    }                

                    $output .= ');';
                }
            }


        }
        else {
            $output = '<p>Nothing found</p>';
        }


    }
    
    if (sizeof($result) <= 10) {
        drupal_set_message('<p>Result: </p><pre>'.$output.'<pre>');
    }
    else {
        print '<p>Result: </p><pre>'.$output.'<pre>';
        die();
    }
}