jQuery(document).ready(function () {
    jQuery("#edit-dates").caleran({
              startOnMonday: true, 
              autoCloseOnSelect: true, 
              maxDate: moment(),
              minDate: moment().subtract(window.user_created,"days"),
              format: 'DD.MM.YYYY',
              locale: 'en',
              showHeader: false,
              showFooter: false,
              ranges: [
                {
                    title: "This Week",
                    startDate: moment().startOf("week"),
                    endDate: moment()
                },
                {
                    title: "Last 7 days",
                    startDate: moment().subtract(6,"days"),
                    endDate: moment()
                },
                {
                    title: "This month",
                    startDate: moment().startOf("month"),
                    endDate: moment()
                },
                {
                    title: "Last 30 days",
                    startDate: moment().subtract(30,"days"),
                    endDate: moment().subtract(1,"days")
                },
                {
                    title: "Last month",
                    startDate: moment().subtract(1,"months").startOf("month"),
                    endDate: moment().subtract(1,"months").endOf("month")
                }
              ],
              onfirstselect: function(caleran, startDate){
                  if (jQuery("#edit-dates").hasClass('limitto7')) {
                    caleran.config.minDate = startDate.clone().subtract(6,"days");
                    
                    future_date = new Date(startDate);
                    future_date = future_date.setDate(future_date.getDate() + 6);
                    now_date = new Date();
                    if (future_date <= now_date) {
                        caleran.config.maxDate = startDate.clone().add(6,"days");
                    }    
                    
                  }  
                },
             onafterselect: function(caleran, startDate, endDate){
                 if (jQuery('#edit-dates').hasClass('limitto7-with-vis')) {
                     strDate = '';
                    sDate =  new Date(startDate);
                    eDate =  new Date(endDate);
                    
                    strDate = (sDate.getDate() < 10 ? '0' + sDate.getDate() : sDate.getDate()) + '.' + (sDate.getMonth() < 9 ? '0' + (sDate.getMonth() + 1) : (sDate.getMonth() + 1 ) ) + ' - ' + (eDate.getDate() < 10 ? '0' + eDate.getDate() : eDate.getDate()) + '.' + (eDate.getMonth() < 9 ? '0' + (eDate.getMonth() + 1) : (eDate.getMonth() + 1 ) );
                    jQuery('#edit-dates-vis').attr('value', strDate);
                }   
             },
             onafterhide: function(caleran){
                if (jQuery("#edit-dates").hasClass('limitto7')) {
                    caleran.config.maxDate = moment();
                    caleran.config.minDate = moment().subtract(window.user_created,"days");
                }
            }   
      });
      
      jQuery('#edit-period-type').change(function () {
          if (jQuery(this).val() == 100) {
              if (jQuery('#edit-dates').hasClass('limitto7-with-vis')) {
                jQuery('.dates-wrapper-vis').fadeIn('fast');
                jQuery('#edit-dates').trigger('click');
                $("#caleran-ex-11").caleran({target: $("#caleran-ex-11-target")});
              }
              else {
                  if (jQuery('#edit-dates').hasClass('limitto7-with-vis')) {
                      jQuery('.dates-wrapper-vis').fadeIn('fast');
                  }
                  else {
                    jQuery('.dates-wrapper').fadeIn('fast');
                }  
              }
          }
          else {
              if (jQuery('#edit-dates').hasClass('limitto7-with-vis')) {
                    jQuery('.dates-wrapper-vis').fadeOut('fast');
              }
              else {
                    jQuery('.dates-wrapper').fadeOut('fast');
              }
          }
          
          jQuery(this).parents('form').toggleClass('custom-shown');
      });
      
      if (jQuery('#edit-dates').hasClass('limitto7-with-vis')) {
          jQuery('#edit-dates-vis').click(function (e) {
            var caleran = $("#edit-dates").data("caleran");
            caleran.showDropdown(e);
          });
      }
});