jQuery(document).ready(function () {
    jQuery("#edit-dates").caleran({
              startOnMonday: true, 
              autoCloseOnSelect: true, 
              maxDate: moment(),
              minDate: moment().subtract(window.user_created,"days"),
              format: 'DD.MM.YYYY',
              locale: 'en',
              showHeader: false,
              showFooter: false,
              ranges: [
                {
                    title: "This Week",
                    startDate: moment().startOf("week"),
                    endDate: moment()
                },
                {
                    title: "Last 7 days",
                    startDate: moment().subtract(6,"days"),
                    endDate: moment()
                },
                {
                    title: "This month",
                    startDate: moment().startOf("month"),
                    endDate: moment()
                },
                {
                    title: "Last 30 days",
                    startDate: moment().subtract(30,"days"),
                    endDate: moment().subtract(1,"days")
                },
                {
                    title: "Last month",
                    startDate: moment().subtract(1,"months").startOf("month"),
                    endDate: moment().subtract(1,"months").endOf("month")
                }
              ],
//              onfirstselect: function(caleran, startDate){
//                    caleran.config.minDate = startDate.clone().subtract(90,"days");
//                },
      });
      
      jQuery('#edit-period-type').change(function () {
          if (jQuery(this).val() == 100) {
              jQuery('.dates-wrapper').fadeIn('fast');
          }
          else {
              jQuery('.dates-wrapper').fadeOut('fast');
          }
          
          jQuery(this).parents('form').toggleClass('custom-shown');
      });
});