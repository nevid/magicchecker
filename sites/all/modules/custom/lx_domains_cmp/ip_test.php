<?php

// проверка валидности IPv4
function _valid_ip_v4($ip) {
    return preg_match('/^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$/', $ip);
}

// проверка валидности IPv6
function _valid_ip_v6($ip) {
    return preg_match('/(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))/', $ip);
}

// переводIPv6 в long
function ip2long_v6($ip) {
    $ip_n = inet_pton($ip);
    $bin = '';
    for ($bit = strlen($ip_n) - 1; $bit >= 0; $bit--) {
        $bin = sprintf('%08b', ord($ip_n[$bit])) . $bin;
    }

    $dec = '0';
    for ($i = 0; $i < strlen($bin); $i++) {
        $dec = bcmul($dec, '2', 0);
        $dec = bcadd($dec, $bin[$i], 0);
    }
    
    return $dec;
}


$data['ip_list_work'] = array();
$data['ip_list'] = array('9.', '134.67.3.5', '2001:0DB8:AA10:0001:0000:0000:0000:00FB');

foreach ($data['ip_list'] as $ip) {
    if (strpos($ip, '-')) {
        $ips = explode('-', $ip);
        $ips[0] = trim($ips[0]);
        $ips[1] = trim($ips[1]);

        if (_valid_ip_v4($ips[0]) && _valid_ip_v4($ips[1])) {
            $data['ip_list_work'][] = array(ip2long($ips[0]), ip2long($ips[1]));
        }
        else if (_valid_ip_v6($ips[0]) && _valid_ip_v6($ips[1])) {
            $data['ip_list_work'][] = array(ip2long_v6($ips[0]), ip2long_v6($ips[1]));
        }
    }
    else {
        
        if (_valid_ip_v4($ip)) {
            $data['ip_list_work'][] = array(ip2long($ip), ip2long($ip));
        }
        else if (_valid_ip_v6($ip)) {
            $data['ip_list_work'][] = array(ip2long_v6($ip), ip2long_v6($ip));
        }
        else {
            if (preg_match('/^([0-9]{1,3}\.){1,3}$/', $ip)) {
                $octets = explode('.', $ip);
                $ip_from = rtrim($ip, '.');
                $ip_to = $ip_from;
                for ($i_ip = sizeof($octets); $i_ip <= 4; $i_ip ++) {
                    $ip_from .= '.0';
                    $ip_to .= '.255';
                }
                
                print $ip_from .'-' .$ip_to;

                $data['ip_list_work'][] = array(ip2long($ip_from), ip2long($ip_to));
            }
        }
    }
}

print_r($data['ip_list_work']);

?>