<?php

function lx_domains_admin_cmp_list() {
    $statuses = __lx_campaign_statuses(true);
    
    $output = '';
    
    $res = db_select('domain_compaigns', 'd');
    $res->join('users', 'u', 'd.uid=u.uid');
    $res->fields('d');
    $res->addField('u', 'name', 'uname');
    
    
    if (isset($_GET['cid']) && $_GET['cid']) {
        $cid = trim(check_plain($_GET['cid']));
        $res->condition('cid', $cid);
    }
    
    if (isset($_GET['name']) && $_GET['name']) {
        $name = trim(check_plain($_GET['name']));
        $res->condition('name', $name);
    }
    
    if (isset($_GET['status']) && $_GET['status']) {
        $status = trim(check_plain($_GET['status']));
        $res->condition('status', $status);
    }
    
    if (isset($_GET['paid_till']) && $_GET['paid_till']) {
        $_created = trim(check_plain($_GET['paid_till']));
        
        $_created = explode(' ', $_created);
        $_created_d = explode('.', $_created[0]);
        $_created_t = explode(':', $_created[1]);
        $paid_till = mktime((int)ltrim($_created_t[0], '0'), (int)ltrim($_created_t[1], '0'), 0, (int)ltrim($_created_d[1], '0'), (int)ltrim($_created_d[0], '0'), $_created_d[2]);        
        
        $res->condition('paid_till', 0, '>');
        $res->condition('paid_till', $paid_till, '<=');
    }
    
    
    
//    $res->extend('PagerDefault');
//    $res->limit(30);
    $data = $res->execute();
    
    $rows = array();
    if (sizeof($data)) {
        $memcache = new Memcache;
        $memcache->connect(LX_DOMAINS_MEMCACHE_IP, LX_DOMAINS_MEMCACHE_PORT) or die ("Could not connect");      

        foreach ($data as $r) {
            
            $stopped = $r->stopped_at ? date('d.m.Y H:i:s', $r->stopped_at) : '';
            
            $var = $memcache->get('cmp_' .$r->cid);
            $in_memcache =  empty($var) ? 'No' : 'Yes';             
            
            $rows[$r->cid] = array($r->cid,
                          date('d.m.Y H:i', $r->created),
                          l($r->uname, 'user/' .$r->uid),
                          $r->name,
                          $statuses[$r->status] .(($r->status == 7) ? '' : '<br /><i style="font-size: 10px">'.$stopped .'</i>'),
                          '0',
                          '<nobr>Adpl: 0</nobr><br />'
                        . '<nobr>Host: 0</nobr><br />'
                        . '<nobr>Ctry: 0</nobr><br />'
                        . '<nobr>Bots: 0</nobr><br />',
                          
                          $in_memcache .($in_memcache == 'Yes' ? ' <a href="/admin/content/campaigns/' .$r->cid .'/dumpmem">dump</a>' : ''),
                
                          '<div class="operations"><span class="action-btn">Operations</span>'
                    . '       <ul>'
                    .'          <li><a href="/user/' .$r->uid .'/campaigns/' .$r->cid .'/edit?destination=admin/content/campaigns">Edit</a></li>'
                    . '         <li>' .($r->status == 7 ? '<a href="/admin/content/campaigns/' .$r->cid .'/change-status?set=100&destination=admin/content/campaigns">Stop</a>' : '<a href="/admin/content/campaigns/' .$r->cid .'/change-status?set=7&destination=admin/content/campaigns">Activate</a>') .'</li>'
                    . '         <li><a href="/user/' .$r->uid .'/campaigns/' .$r->cid .'/stastistics">Stat</a></li>'
                . '             <li><a href="/user/' .$r->uid .'/campaigns/' .$r->cid .'/stastistics/log/' .date('Y-m-d') .'">Log</a></li>'
                . '             <li><a href="/user/' .$r->uid .'/campaigns/' .$r->cid .'/delete?destination=admin/content/campaigns">Delete</a></li>'
                . '      </ul></div>'               
                );
        }   

        $memcache->close();

        $headers = array('campaign id', 
                         'created', 
                         'user',
                         'name', 
                         'status', 
                         'Today total', 
                         'Blocks', 
//                         'Hosting', 
//                         'Country', 
//                         'Bots', 
                         'In mem', '');
        
        if (sizeof($rows)) {
            $campaigns_stat = __get_campaigns_cnt_stat(array_keys($rows), true);            
            if (sizeof($campaigns_stat)) {
                foreach ($campaigns_stat as $cid => $r) {
                    $rows[$cid][5] = $r['today']['all'];
                    $rows[$cid][6] = '<nobr>Adpl: ' .$r['today'][1] .'</nobr><br />'
                                    . '<nobr>Host: ' .$r['today'][2] .'</nobr><br />'
                                    . '<nobr>Ctry: ' .$r['today'][3] .'</nobr><br />'
                                    . '<nobr>Bots: ' .$r['today'][4] .'</nobr><br />';
                            
//                            $r['today'][1];
//                    $rows[$cid][7] = $r['today'][2];
//                    $rows[$cid][8] = $r['today'][3];
//                    $rows[$cid][9] = $r['today'][4];
                }
            }        
        }
        
        $output .= theme('table', array('header' => $headers, 'rows' => $rows)) .theme('pager');
    }
    
    if (!sizeof($rows)) {
        $output .= '<p>List empty</p>';
    } 
    
    $search_form = drupal_get_form('lx_domains_admin_cmp_search_form');
    
    $output = drupal_render($search_form) .$output;
    
    return '<p style="text-align: right">Server time: ' .date('d.m.Y H:i:s') .'</p>' .$output;
}



function lx_domains_admin_cmp_search_form($form, &$form_state) {
    global $user;
    
    $statuses = __lx_campaign_statuses();   
    
    $form['fs'] = array('#type' => 'fieldset');
    
    $form['fs']['cid'] = array('#type' => 'textfield', 
        '#title' => 'Campaign id',
        '#default_value' => isset($_GET['cid']) ? trim($_GET['cid']) : '',
    );
    
    
    
//    $form['uiditem'] = array('#type' => 'item', 
//        '#title' => 'User',
//        '#markup' => l($user_name, 'user/'.$cmp->uid),
//    );

    
    // cmp name
    $form['fs']['name'] = array('#type' => 'textfield',
        '#title' => 'Name',
        '#default_value' => isset($_GET['name']) ? trim($_GET['name']) : '',
        );
    
    // cmp status
    $form['fs']['status'] = array('#type' => 'select',
        '#title' => 'Status',
        '#options' => array('' => 'All') + $statuses,
        '#default_value' => isset($_GET['status']) ? trim($_GET['status']) : '',
        );  
    
    
    // cmp stopped_at
    $form['fs']['stopped_at'] = array('#type' => 'textfield',
        '#title' => 'Stopped at selected date',
        '#default_value' => isset($_GET['paid_till']) ? trim($_GET['stopped_at']) : '',
        '#attributes' => array('placeholder' => date('d.m.Y H:i'))
        
        );    
    
    $form['#method'] = 'get';
    
    
    $form['fs']['submit'] = array('#type' => 'submit',
        '#value' => 'Search',
        '#weight' => 100
        );
    
    
    return $form;
}

// Delete campaign
function lx_domains_admin_cmp_delete_form($form, &$form_state, $cmp_id = null) {
    $name = db_select('domain_compaigns', 'c')
                ->fields('c', array('name'))
                ->condition('cid', $cmp_id)
                ->execute()->fetchField();
    
    drupal_set_title('Are you really want to delete campaign "' .$name .'"');
    
    $form['cid'] = array('#type' => 'hidden',
        '#default_value' => $cmp_id
        );
    
    $form['name'] = array('#type' => 'hidden',
        '#default_value' => $name
        );
    
    $form['submit'] = array('#type' => 'submit',
        '#value' => t('Delete')
        );
    
    return $form;
}

function lx_domains_admin_cmp_delete_form_submit($form, &$form_state) {
    
    __lx_domains_cmp_delete_from_memcache($form_state['values']['cid'], true);
    
    db_delete('domain_compaigns')
        ->condition('cid', $form_state['values']['cid'])
        ->execute();
    
    db_delete('domain_compaigns_cmp_stat')
        ->condition('cid', $form_state['values']['cid'])
        ->execute();
    
    drupal_set_message('Campaign deleted.');
}

function lx_domains_admin_change_status($cmp_id) {
    $sts = (int)$_GET['set'];
    
    $statuses = __lx_campaign_statuses();    
    
    if (!in_array($sts, array_keys($statuses))) {
        drupal_access_denied();
    }
    
    db_update('domain_compaigns')
        ->fields(array('status' => $sts))
        ->condition('cid', $cmp_id)
        ->execute();
    
    if (in_array($sts, array(10, 100))) {
        db_update('domain_compaigns')
        ->fields(array('stopped_at' => time()))
        ->condition('cid', $cmp_id)
        ->execute();
        __lx_domains_cmp_delete_from_memcache($cmp_id);        
    }
    else {
        __lx_domains_cmp_add_to_memcache($cmp_id);
    }
    
    if (isset($_GET['destination'])) {
        drupal_set_message('Status changed to ' .$statuses[$sts]);
        drupal_goto($_GET['destination']);
    }    
}

function lx_domains_admin_current_errors() {
    $output = 'No errors';
    
    $memcache = new Memcache;
    $memcache->connect(LX_DOMAINS_MEMCACHE_IP, LX_DOMAINS_MEMCACHE_PORT) or die ("Could not connect");      
    $errors = $memcache->get('cmperrors');    
    
    if (isset($_GET['reset_errors'])) {
        $memcache->delete('cmperrors');    
        $memcache->delete('cmp_not_found');    
        drupal_set_message('Errors cleared.');
        drupal_goto('/admin/content/current-errors');
    }
    else {
        $errors = $memcache->get('cmperrors'); 
        
        if (!empty($errors)) {
            $output = '<h2>Errors</h2><p style="text-align: right"><a href="/admin/content/current-errors?reset_errors=1">Clear errors</a></p>' .print_r($errors, true);
        }
        
        $cmp_not_found = $memcache->get('cmp_not_found'); 
        if (!empty($cmp_not_found)) {
            $output .= '<h2>Campaigns not found</h2><p style="text-align: right"><a href="/admin/content/current-errors?reset_errors=1">Clear errors</a></p>' .print_r($cmp_not_found, true);
        }        
    }
    
    $memcache->close();        
    
    return '<pre>' .$output .'</pre>';
}

function lx_domains_admin_dump_cmp_mem_var($cmp_id) {
    $memcache = new Memcache;
    $memcache->connect(LX_DOMAINS_MEMCACHE_IP, LX_DOMAINS_MEMCACHE_PORT) or die ("Could not connect");     
    $var = $memcache->get('cmp_' .$cmp_id);
    $memcache->close();
    
    return '<pre>' .print_r($var, true) .'</pre>';
}

function lx_domains_admin_checks_list() {
    $output = 'List is empty.';
    $show_from = isset($_GET['from']) ? check_plain($_GET['from']) : '10min';
    $headers = array();
    
    $reasons = __get_reasons_translate();
    $block = array(0 => 'No', 1 => 'Yes');
    
    // 1. берем статистику за сегодня из базы статистики
    try {
        $db = new PDO("mysql:host=" .LX_MYSQLI_STAT_HOST .";dbname=" .LX_MYSQLI_STAT_DB, LX_MYSQLI_STAT_USER, LX_MYSQLI_STAT_PASS);  
        $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  

//        $table_name = '';
//        $rows = array();
//        if ($show_from = 'day') {
//            // выбираем из дневной
//            $table_name = 'day_stat_' .date('Y_m_d');
//        }
//        else {
            // выбираем из 10тиминутной
            $ten_minutes_table_name = '';
            $ten_minutes_table_name = date('i');
            $table_name = 'mem_stat_' .$ten_minutes_table_name[0] .'0';             
//        }
        
        $sth = $db->prepare('SELECT * FROM ' .$table_name .' ORDER BY click_date DESC LIMIT 0, 100');      
        $sth->execute();
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) { 
          // Parse the result set
           if (empty($headers)) {
              $headers = array_keys($row); 
           }
           
           $row['is_blocked'] = $block[$row['is_blocked']];
           $row['block_reason'] = $reasons[$row['block_reason']];
           
           $rows[] = $row;
          
        }    

        if (sizeof($rows)) {
            $output = theme('table', array('header' => $headers, 'rows' => $rows));
        }
        $sth = null;
        $db = null;
    }
    catch(PDOException $e) {  
        drupal_set_message($e->getMessage(), 'error');
    }         
    
    return $output;
}


// aggregated days stat
function lx_domains_cmp_admin_day_stat() {
    global $user;

    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') . '/charts/chartist.min.js');
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') . '/charts/tooltip.js');
    drupal_add_css(drupal_get_path('module', 'lx_domains_cmp') . '/charts/chartist.min.css');
    drupal_add_css(drupal_get_path('module', 'lx_domains_cmp') . '/charts/tooltip.css');


    $output = '';


    
    $form = drupal_get_form('lx_domains_stat_dates', $cid);
    $output = drupal_render($form);
    


    $col_names = array('hosting_clicks' => 'VPN/Proxy',
        'country_clicks' => "Visitor's Location",
        'bot_clicks' => 'Bots',
        'adplex_clicks' => 'Adplexity',
        'total_blocked' => 'Total blocked',
        'total' => 'Total clicks',
        'isp_clicks' => 'ISP/Organizations',
        'referer_clicks' => 'Referer',
        'ip_clicks' => 'IP List',
        'device_os_clicks' => 'Devices',
        'url_clicks' => 'URL Substrings',
        'ua_clicks' => 'User Agent',
        'time_clicks' => 'Time of Day',
        'status_clicks' => 'Status',
        'headers_clicks' => 'Magic Filter',
    );

    
    $rows = array(0 => array());

    if (isset($_GET['period']) && preg_match('/[0-9]{4}\_[0-9]{2}/', $_GET['period'])) {
        $dt = explode('_', $_GET['period']);
    } else {
        $dt = explode('_', date('Y_m'));
    }

    $total_row = array('created' => '<b>Total for period:</b>',
        'total_clicks' => 0,
        'adplex_clicks' => 0,
        'hosting_clicks' => 0,
        'country_clicks' => 0,
        'bot_clicks' => 0,
        'isp_clicks' => 0,
        'referer_clicks' => 0,
        'ip_clicks' => 0,
        'device_os_clicks' => 0,
        'url_clicks' => 0,
        'ua_clicks' => 0,
        'time_clicks' => 0,
        'status_clicks' => 0,
        'headers_clicks' => 0,
        'total_blocked' => 0,
    );
    $total_blocked = 0;

//    if (implode('_', $dt) == date('Y_m')) {
//        $today = __get_campaigns_cnt_stat(array($cid), true);
//
//        $total_blocked = 0;
//        foreach ($today[$cid]['today'] as $k => $v) {
//            if (is_numeric($k)) {
//                $total_blocked += $v;
//            }
//        }
//
////            $total_blocked = $today[$cid]['today'][1] + $today[$cid]['today'][2] + $today[$cid]['today'][3] + $today[$cid]['today'][4] + $today[$cid]['today'][5] + $today[$cid]['today'][6] + $today[$cid]['today'][7] + $today[$cid]['today'][4];
//
//        $total_row = array('created' => '<b>Total for period:</b>',
//            'total_clicks' => $today[$cid]['today']['all'],
//            'adplex_clicks' => $today[$cid]['today'][1],
//            'hosting_clicks' => $today[$cid]['today'][2],
//            'country_clicks' => $today[$cid]['today'][3],
//            'bot_clicks' => $today[$cid]['today'][4] + $today[$cid]['today'][1],
//            'isp_clicks' => $today[$cid]['today'][5],
//            'referer_clicks' => $today[$cid]['today'][6],
//            'ip_clicks' => $today[$cid]['today'][7],
//            'device_os_clicks' => $today[$cid]['today'][8],
//            'url_clicks' => $today[$cid]['today'][9],
//            'ua_clicks' => $today[$cid]['today'][11],
//            'time_clicks' => $today[$cid]['today'][10],
//            'status_clicks' => $today[$cid]['today'][12],
//            'total_blocked' => $total_blocked,
//        );
//
//
//        $r = array('class' => array('day-row'), 'data-date' => date('d.m.Y'), 'data' => array('created' => date('d.m.Y'),
//                'total_clicks' => array('class' => array('total'), 'data-type' => $col_names['total'], 'data-value' => $today[$cid]['today']['all'], 'data' => $today[$cid]['today']['all']),
//                'adplex_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['adplex_clicks'], 'data-value' => $today[$cid]['today'][1], 'data' => $today[$cid]['today'][1] . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($today[$cid]['today'][1] / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'hosting_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['hosting_clicks'], 'data-value' => $today[$cid]['today'][2], 'data' => $today[$cid]['today'][2] . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($today[$cid]['today'][2] / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'country_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['country_clicks'], 'data-value' => $today[$cid]['today'][3], 'data' => $today[$cid]['today'][3] . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($today[$cid]['today'][3] / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'bot_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['bot_clicks'], 'data-value' => $today[$cid]['today'][4], 'data' => ($today[$cid]['today'][4] + $today[$cid]['today'][1]) . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round(($today[$cid]['today'][4] + $today[$cid]['today'][1]) / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'isp_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['isp_clicks'], 'data-value' => $today[$cid]['today'][5], 'data' => $today[$cid]['today'][5] . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($today[$cid]['today'][5] / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'referer_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['referer_clicks'], 'data-value' => $today[$cid]['today'][6], 'data' => $today[$cid]['today'][6] . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($today[$cid]['today'][6] / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'ip_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['ip_clicks'], 'data-value' => $today[$cid]['today'][7], 'data' => $today[$cid]['today'][7] . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($today[$cid]['today'][7] / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'device_os_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['device_os_clicks'], 'data-value' => $today[$cid]['today'][8], 'data' => $today[$cid]['today'][8] . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($today[$cid]['today'][8] / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'url_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['url_clicks'], 'data-value' => $today[$cid]['today'][9], 'data' => $today[$cid]['today'][9] . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($today[$cid]['today'][9] / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'ua_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['ua_clicks'], 'data-value' => $today[$cid]['today'][11], 'data' => $today[$cid]['today'][11] . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($today[$cid]['today'][11] / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'time_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['time_clicks'], 'data-value' => $today[$cid]['today'][10], 'data' => $today[$cid]['today'][10] . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($today[$cid]['today'][10] / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'status_clicks' => array('class' => array('detailed'), 'data-type' => $col_names['status_clicks'], 'data-value' => $today[$cid]['today'][12], 'data' => $today[$cid]['today'][12] . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($today[$cid]['today'][12] / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' )),
//                'total_blocked' => array('class' => array('total_blocked'), 'data-type' => $col_names['total_blocked'], 'data-value' => $total_blocked, 'data' => $total_blocked . ($today[$cid]['today']['all'] ? ' <span class="percent">(' . round($total_blocked / $today[$cid]['today']['all'] * 100, 2) . '%)</span>' : '' ))
//            //                    'log' => l('Show log', 'user/' .$acc->uid .'/campaigns/' .$cid .'/stastistics/log/' .date('Y-m-d'))
//        ));
//
//        $rows[] = $r;
//    }

    $res = db_select('domain_compaigns_total_stat', 's');
    $res->fields('s');

    $from = mktime(0, 0, 0, ltrim($dt[1], '0'), 1, $dt[0]);
    $to = mktime(23, 59, 59, ltrim($dt[1], '0'), date('t', $from), $dt[0]);

    $res->condition('created', $from, '>=');
    $res->condition('created', $to, '<=');

    $res->orderBy('created', 'DESC');
    $data = $res->execute()->fetchAll();



    if (sizeof($data)) {
        foreach ($data as $_kk => $r) {
            $r = (array) $r;

            $total_blocked = $r['adplex_clicks'] + $r['hosting_clicks'] + $r['country_clicks'] + $r['bot_clicks'] + $r['isp_clicks'] + $r['referer_clicks'] + $r['ip_clicks'] + $r['device_os_clicks'] + $r['url_clicks']+$r['ua_clicks'] + $r['time_clicks'] + $r['status_clicks'] +  + $r['headers_clicks'];

            foreach ($r as $k => $v) {
                if (!in_array($k, array('created', 'log', 'cid'))) {
                    $total_row[$k] += $v;
                }
            }


            $total_row['total_blocked'] += $total_blocked;

            foreach ($r as $kk => $vv) {

                if (!in_array($kk, array('cid', 'created', 'total_clicks', 'total_blocked'))) {
                    $r[$kk] = array('class' => array('detailed'), 'data-type' => $col_names[$kk], 'data-value' => $vv, 'data' => $r['total_clicks'] ? $vv . ' <span class="percent">(' . round($vv / $r['total_clicks'] * 100, 2) . '%)</span>' : 0);
                }
            }

            $r['created'] = date('d.m.Y', $r['created']);
            $r['total_blocked'] = array('class' => array('total_blocked'), 'data-type' => $col_names['total_blocked'], 'data-value' => $total_blocked, 'data' => $total_blocked . ' <span class="percent">(' . round($total_blocked / $r['total_clicks'] * 100, 2) . '%)</span>');
            $r['total_clicks'] = array('class' => array('total'), 'data-type' => $col_names['total'], 'data-value' => $r['total_clicks'], 'data' => $r['total_clicks']);
//                $r['log'] = l('Show log', 'user/' .$acc->uid .'/campaigns/' .$cid .'/stastistics/log/' .date('Y-m-d', $r['created']));                
            unset($r['cid']);

            $rows[] = array('data-date' => $r['created'], 'class' => array('day-row'), 'data' => $r);
        }
    }


    if (sizeof($rows)) {
        if ($total_row['total_clicks']) {
            $total_row['adplex_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['adplex_clicks'], 'data-value' => $total_row['adplex_clicks'], 'data' => $total_row['adplex_clicks'] . ' <span class="percent">(' . round($total_row['adplex_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['hosting_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['hosting_clicks'], 'data-value' => $total_row['hosting_clicks'], 'data' => $total_row['hosting_clicks'] . ' <span class="percent">(' . round($total_row['hosting_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['country_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['country_clicks'], 'data-value' => $total_row['country_clicks'], 'data' => $total_row['country_clicks'] . ' <span class="percent">(' . round($total_row['country_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['bot_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['bot_clicks'], 'data-value' => $total_row['bot_clicks'], 'data' => $total_row['bot_clicks'] . ' <span class="percent">(' . round($total_row['bot_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['isp_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['isp_clicks'], 'data-value' => $total_row['isp_clicks'], 'data' => $total_row['isp_clicks'] . ' <span class="percent">(' . round($total_row['isp_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['referer_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['referer_clicks'], 'data-value' => $total_row['referer_clicks'], 'data' => $total_row['referer_clicks'] . ' <span class="percent">(' . round($total_row['referer_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['ip_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['ip_clicks'], 'data-value' => $total_row['ip_clicks'], 'data' => $total_row['ip_clicks'] . ' <span class="percent">(' . round($total_row['ip_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['device_os_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['device_os_clicks'], 'data-value' => $total_row['device_os_clicks'], 'data' => $total_row['device_os_clicks'] . ' <span class="percent">(' . round($total_row['device_os_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['url_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['url_clicks'], 'data-value' => $total_row['url_clicks'], 'data' => $total_row['url_clicks'] . ' <span class="percent">(' . round($total_row['url_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['ua_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['ua_clicks'], 'data-value' => $total_row['ua_clicks'], 'data' => $total_row['ua_clicks'] . ' <span class="percent">(' . round($total_row['ua_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['time_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['time_clicks'], 'data-value' => $total_row['time_clicks'], 'data' => $total_row['time_clicks'] . ' <span class="percent">(' . round($total_row['time_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['status_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['status_clicks'], 'data-value' => $total_row['status_clicks'], 'data' => $total_row['status_clicks'] . ' <span class="percent">(' . round($total_row['status_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');
            $total_row['headers_clicks'] = array('class' => array('detailed'), 'data-type' => $col_names['headers_clicks'], 'data-value' => $total_row['headers_clicks'], 'data' => $total_row['headers_clicks'] . ' <span class="percent">(' . round($total_row['headers_clicks'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');

            $total_row['total_blocked'] = array('class' => array('total_blocked'), 'data-type' => $col_names['total_blocked'], 'data-value' => $total_row['total_blocked'], 'data' => $total_row['total_blocked'] . ' <span class="percent">(' . round($total_row['total_blocked'] / $total_row['total_clicks'] * 100, 2) . '%)</span>');

            $total_row['total_clicks'] = array('class' => array('total'), 'data-type' => $col_names['total'], 'data-value' => $total_row['total_clicks'], 'data' => $total_row['total_clicks']);
        }


        $rows[0] = array('class' => array('total-row'), 'data' => $total_row);
        $headers = array(array('class' => array('nohid'), 'data' => 'Date'),
            array('class' => array('nohid'), 'data' => $col_names['total']),
            array('class' => array('hidbydefault'), 'data' => $col_names['adplex_clicks']),
            $col_names['hosting_clicks'],
            $col_names['country_clicks'],
            $col_names['bot_clicks'],
            array('class' => array('hidbydefault'), 'data' => $col_names['isp_clicks']),
            array('class' => array('hidbydefault'), 'data' => $col_names['referer_clicks']),
            array('class' => array('hidbydefault'), 'data' => $col_names['ip_clicks']),
            array('class' => array('hidbydefault'), 'data' => $col_names['device_os_clicks']),
            array('class' => array('hidbydefault'), 'data' => $col_names['url_clicks']),
            array('class' => array('hidbydefault'), 'data' => $col_names['ua_clicks']),
            array('class' => array('hidbydefault'), 'data' => $col_names['time_clicks']),
            array('class' => array('hidbydefault'), 'data' => $col_names['status_clicks']),
            array('class' => array('hidbydefault'), 'data' => $col_names['headers_clicks']),
            array('class' => array('nohid'), 'data' => $col_names['total_blocked'])
        );

        $grafics = '';


            // web-сайт скрипта графиков: http://www.chartjs.org/docs/#bar-chart-introduction
            drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') . '/js/charts.min.js');
            drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') . '/js/show_charts.js');
            $grafics = '<fieldset class="pad22"><div class="graf-1"><div class="pre-graphs-txt" style="text-align: center">Click on bars and line dots to view detailed day statistics.</div>
    <canvas id="canvas"></canvas>
</div>
<div class="graf-2">
    <canvas id="pie"></canvas>
</div></fieldset>';


        $output .= $grafics . '<fieldset class="table-fs stat-table"><div class="column-switcher clearfix"></div><div class="clearfix">' . theme('table', array('header' => $headers, 'rows' => $rows)) . '</div></fieldset>';
    } else {
        $output .= 'No statistic found.';
    }

    return $output;
}