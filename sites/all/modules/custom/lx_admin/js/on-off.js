jQuery(document).ready(function () {
    jQuery('.on-off-boxes .chbxes-on').click(function (e) {
        e.preventDefault();
        jQuery('#lx-admin-log-filter-form .form-checkbox').prop('checked', true);
    });
    
    jQuery('.on-off-boxes .chbxes-off').click(function (e) {
        e.preventDefault();
        jQuery('#lx-admin-log-filter-form .form-checkbox').prop('checked', false);
    });    
});