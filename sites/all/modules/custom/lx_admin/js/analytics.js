jQuery(document).ready(function () {
    jQuery('body').on('click', '.change-month', function (e) {
        e.preventDefault();
        mon = jQuery(this).data('mon');        
        jQuery.get('/admin/lxadmin/analytics-month/' + mon, function (data) {
            jQuery('.total-info-month').replaceWith(data);
        })
    });
    jQuery('body').on('click', '.js-users-list', function (e) {
        e.preventDefault();
        url = jQuery(this).attr('href');        
        jQuery.get(url, function (data) {
            jQuery('.users-list').html(data);
        })
    });
});