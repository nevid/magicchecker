<?php

//require_once '/home/user173magic/data/www/magicchecker.com/sites/all/libraries/phpclickhouse/include.php';

require_once getcwd() .'/sites/all/libraries/phpclickhouse/include.php';
require(drupal_get_path('module', 'lx_domains_cmp') . '/sconfig.php');

define('LX_ADMIN_NUM_WHERE', 12);

function ch_query($query, $values = '',$res_type = 'rows', $with_total_count = false) {
    static $db;
    global $click_house_config;
    
    $output = '';
    $s_time = microtime(true);
    
    $total_count = -1;

    try {
        if ($query) {
            if (empty($db)) {
                $db = new ClickHouseDB\Client($click_house_config);
                $db->database('default');
                $db->setTimeout(600);      // 1500 ms
                $db->setTimeout(600);       // 10 seconds
                $db->setConnectTimeOut(600); // 5 seconds   
            }    

            $res = $db->select($query);
            
            if ($with_total_count) {
                $count_query = '';
                $count_query_begin = strpos($query, '(');
                $count_query_end = strpos($query, 'ORDER');
                $count_query = str_replace(' id ', ' count(id) as cnt ',substr($query, $count_query_begin+1, ($count_query_end-$count_query_begin-2)));
                $res_cnt = $db->select($count_query);
                $total_count = $res_cnt->fetchOne();
                $total_count = $total_count['cnt'];
            }

            if ($res_type == 'rows') {
                $output = $res->rows();
            }    
        }
    }
     catch (Exception $e) {
         print_r($data);
         print "\n\n";
         print substr($e->getMessage(), 0, 512);
         die();
     }

    $db = null;    
    
    $execution_time = number_format((microtime(true) - $s_time)*1000,2);
    return ['result' => $output, 'execution_time' => $execution_time, 'total_count' => $total_count];
}

function ch_query_csv($query) {
    static $db;
    global $click_house_config;
    
    $output = '';
    $s_time = microtime(true);

    try {
        if ($query) {
            if (empty($db)) {
                $db = new ClickHouseDB\Client($click_house_config);
                $db->database('default');
                $db->setTimeout(600);      // 1500 ms
                $db->setTimeout(600);       // 10 seconds
                $db->setConnectTimeOut(600); // 5 seconds   
            }    

            $filename = dirname(__FILE__) .'/csv_export/' .time() .'.csv';
            
//            $fields = '';
//            $fields_end = strpos($query, 'FROM');
//            $fields = str_replace('SELECT', '',substr($query, 0, ($fields_end-1)));
//            $f = fopen($filename, 'w');
//            fputs($f, $fields ."\n");
//            fclose($f);
            
            $WriteToFile=new ClickHouseDB\WriteToFile($filename);
            $db->select($query,[],null,$WriteToFile);
            
            exec('gzip ' .$filename);
        }
    }
     catch (Exception $e) {
         print_r($data);
         print "\n\n";
         print substr($e->getMessage(), 0, 512);
         die();
     }

    $db = null;    
    
    $execution_time = number_format((microtime(true) - $s_time)*1000,2);
    return ['filename' => $filename.'.gz', 'execution_time' => $execution_time];
}

function lx_admin_ch_recent_log() {
    global $ch_event_date;
//    $query = "SELECT id, click_date, real_ip, country, isp_org, block_reason, ua_device, ua_os, ua_browser, user_agent, user_referer, user_page_url FROM clicklog WHERE id IN (SELECT id FROM clicklog WHERE event_date='2018-05-23' ORDER BY id DESC LIMIT 0, 100)  ORDER BY id DESC";
//    ch_execute_query($query);
    
    $query = '';
    
    try {
    
        $to_csv = isset($_GET['op']) && ($_GET['op'] == 'To CSV' ) ? 1 : 0;
        $output = 'List is empty.';
        $headers = array();


        $res = ch_query('DESCRIBE TABLE clicklog');

        foreach ($res['result'] as $v) {
//            if ($v['name'] == 'id') continue;
            $table_fields[$v['name']] = $v['name'];
        }

        $reasons = __get_reasons_translate();
        $block = array(0 => 'No', 1 => 'Yes');

        $show_fields = array();
        if (isset($_GET['fields'])) { 
            $show_fields = $_GET['fields'];
        }    
        else {    
            $show_fields = array('click_date', 'uid', 'remote_addr', 'x_forwarded', 'proxy', 'real_ip', 'country', 'isp_org', 'is_blocked', 'block_reason', 'ua_device', 'ua_os', 'ua_osv', 'ua_browser', 'ua_browserv', 'user_agent', 'accept_lang', 'all_headers', 'headers_browser', 'extime', 'errtxt');    
        }

        $form = drupal_get_form('lx_admin_ch_filter_form', $table_fields, $show_fields);
        $output = drupal_render($form); 

        $rows = array();
        $res = null;
        
        $ch_event_date = '';
        
        
        if (isset($_GET['period_type']) && is_numeric($_GET['period_type'])) {
            if ($_GET['period_type'] == 100 && isset($_GET['dates']) && preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4} \- [0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $_GET['dates'])) {
                $dates = explode(' - ', $_GET['dates']);
                $date_from = $dates[0];
                $date_to = $dates[1];
                
                $ch_event_date = "(event_date >= '" .implode('-',array_reverse(explode('.', $date_from))) ."') AND (event_date <= '" .implode('-',array_reverse(explode('.', $date_to))) ."')";
            }
            else {
                if ($_GET['period_type'] == 1) {
                    $date_from =  date("Y-m-d", strtotime("this Monday"));
                   //воскресенье прошлой недели
                   $date_to = date('Y-m-d');
                }
                else if ($_GET['period_type'] == 6) {
                   //понедельник прошлой недели
                   $date_from =  date("Y-m-d", strtotime("last Monday"));
                   //воскресенье прошлой недели
                   $date_to =  date("Y-m-d", strtotime("last Sunday"));            
                }
                else if ($_GET['period_type'] == 2) {
                   $date_from = date('Y-m-d', time() - 86400*6);
                   $date_to = date('Y-m-d');
                }
                else if ($_GET['period_type'] == 3) {
                    $date_from = date('Y-m-01');
                    $date_to = date('Y-m-d');                
                }
                else if ($_GET['period_type'] == 4) {
                    $date_from = date('Y-m-d', time() - 86400*30);
                    $date_to = date('Y-m-d');
                }
                else if ($_GET['period_type'] == 10) {                
                    $date_from = date('Y-m-d', $acc->created);
                    $date_to = date('Y-m-d');
                }
                else if ($_GET['period_type'] == 5) {
                   $date_from =  date("Y-m-d", strtotime("first day of last month"));
                   //воскресенье прошлой недели
                   $date_to =  date("Y-m-d", strtotime("last day of last month")); 
                }
                else if ($_GET['period_type'] == 7) {
                   $date_from =  date("Y-m-d", time() - 86400);
                   //воскресенье прошлой недели
                   $date_to =  date("Y-m-d", time() - 86400); 
                }
                else if ($_GET['period_type'] == 8) {
                   $date_from =  date("Y-m-d", time());
                   //воскресенье прошлой недели
                   $date_to =  date("Y-m-d", time()); 
                }
                
                if ($date_from != $date_to) { 
                    $ch_event_date = "(event_date >= '" .$date_from ."') AND (event_date <= '" .$date_to ."')";
                }
                else {
                    $ch_event_date = "event_date = '" .$date_from ."'";
                }
            }
        }
        else {
            $ch_event_date = "event_date = '" .date('Y-m-d') ."'";
        }
    
    

        if (isset($_GET['select_type']) && $_GET['select_type'] == 'count') {
            $query = 'SELECT count(id) as cnt, ' .implode(', ', $show_fields) ." FROM clicklog PREWHERE $ch_event_date";
        }
        else {
            $query = 'SELECT ' .implode(', ', $show_fields) .' FROM clicklog  WHERE id IN (!SUBQUERY!)';
            $subquery = "SELECT id FROM clicklog PREWHERE $ch_event_date";
            $subquery_conds = [];
        }

        for ($i = 1; $i <= LX_ADMIN_NUM_WHERE; $i ++) {
            if (isset($_GET['condition_field_' .$i]) && ($_GET['condition_op_' .$i] != '') && ($_GET['condition_val_' .$i] != '' )) {
                if (strpos($_GET['condition_val_' .$i], '||') === false) {
                    if (strpos($_GET['condition_op_' .$i], 'LIKE') !== false) {
                        $subquery_conds[] = trim($_GET['condition_field_' .$i])." " .$_GET['condition_op_' .$i] ." '%" .db_like(trim($_GET['condition_val_' .$i])) ."%'";
                    }
                    else if (strpos($_GET['condition_op_' .$i], 'IN') !== false) {
                        $subquery_conds[] = trim($_GET['condition_field_' .$i])." IN (" .db_like(trim($_GET['condition_val_' .$i])) .")";
                    }
                    else {
//                        $res->condition($_GET['condition_field_' .$i], trim($_GET['condition_val_' .$i]), $_GET['condition_op_' .$i]);
                        $cond_val = trim($_GET['condition_val_' .$i]);
                        $subquery_conds[] = trim($_GET['condition_field_' .$i]) .$_GET['condition_op_' .$i] .(is_numeric($cond_val) ? $cond_val : "'$cond_val'");
                    }
                }
                else {
                    $or = explode('||', $_GET['condition_val_' .$i]);
                    $or_conds = [];
                    
                    foreach ($or as $or_1) {
                        if (strpos($_GET['condition_op_' .$i], 'LIKE') !== false) {
                            $or_conds[] = trim($_GET['condition_field_' .$i])." " .$_GET['condition_op_' .$i] ." '%" .db_like(trim($or_1)) ."%'";
                        }
                        else if (strpos($_GET['condition_op_' .$i], 'IN') !== false) {
                            $or_conds[] = trim($_GET['condition_field_' .$i])." IN (" .db_like(trim($or_1)) .")";
                        }
                        else {
                            $cond_val = trim($or_1);
                            $or_conds[] = trim($_GET['condition_field_' .$i]) .$_GET['condition_op_' .$i] .(is_numeric($cond_val) ? $cond_val : "'$cond_val'");
                        }
                        
                    }

                    $subquery_conds[] = '(' .implode(') OR (', $or_conds) .')';
                }
                $last_cond_num ++;
            }

        }
        
        if (sizeof($subquery_conds)) {
            if (isset($_GET['select_type']) && $_GET['select_type'] == 'count') {
                $query .= 'WHERE (' .implode(') AND (', $subquery_conds) .') ';
            }
            else {
                $subquery .= 'WHERE (' .implode(') AND (', $subquery_conds) .') ';
            }
        }
        
        $page = isset($_GET['page']) ? $_GET['page'] : 0;
        $limit = (isset($_GET['limit']) ? $_GET['limit'] : 50);        
        
        if (isset($_GET['select_type']) && $_GET['select_type'] == 'count') {
            $query .= ' GROUP BY ' .implode(', ', $show_fields) .' ORDER BY cnt DESC';// LIMIT '  .($page ? $page*$limit .',' : '' )   .$limit;
        }
        else {    
            $subquery .= ' ORDER BY id DESC ';
        }    
//  
//
        if (!$to_csv) {
//            $limit = isset($_GET['limit']) ? $_GET['limit'] : 50;
//            $res->limit($limit);
            if (isset($_GET['select_type']) && $_GET['select_type'] == 'count') {
                
            }
            else {
                $subquery .= ' LIMIT '  .($page ? $page*$limit .',' : '' )   .$limit;
                
                $query = str_replace('!SUBQUERY!', $subquery, $query) .' ORDER BY id DESC';
            }    
        }    
        else {
            if (isset($_GET['select_type']) && $_GET['select_type'] == 'count') {
                
            }
            else {
//                $subquery .= ' LIMIT 10';
                $query = str_replace('!SUBQUERY!', $subquery, $query) .' ORDER BY id DESC';
            }    
            
        }
//
//        if (isset($_GET['select_type']) && $_GET['select_type'] == 'count') {
//            foreach ($show_fields as $field) {
//                $res->groupBy($field);
//            }
//
//            $res->orderBy('count', 'DESC');
//        }
//        else {
//            $res->orderBy('click_date', 'DESC');
//        }
//
//        $data = $res->execute();
//        
        
//        
//        
//
//        $csv = '';
//        if (isset($_GET['tocsv'])) {
//            header("Content-type: application/x-download");
//            header("Content-Disposition: attachment; filename=result.csv");
//            $csv = fopen('php://output', 'w');
//        }        
        
        if (!$to_csv) {
            $with_count_query = strpos($query, 'count(') === false;
            $res = ch_query($query, [], 'rows', $with_count_query);
            $data = $res['result'];

            foreach ($data as $r) {
                $r = (array)$r;

                if (isset($_GET['tocsv'])) {

                    if (empty($headers)) {
                        $headers = array_keys($r);
                        fputcsv($csv, $headers);
                    }            
                    fputcsv($csv, $r);
                }
                else {        
                    foreach ($r as $kk => $vv) {
                        $r[$kk] = strlen($vv) > 20 ? '<span class="long-trimmed">' .$vv .'</span>' : $vv;
                    }

                    if (isset($r['is_blocked'])) {
                        $r['is_blocked'] = $block[$r['is_blocked']];
                    }
                    if (isset($r['block_reason'])) {
                        $r['block_reason'] = $reasons[$r['block_reason']];
                    }    

                    if (!isset($r['count'])) {
                        $rows[] = $r;
                    }
                    else {
                        $cnt = $r['count'];
                        unset($r['count']);
                        $rows[]  = array('count' => $cnt) + $r;
                    }
                }    

            }



            if (sizeof($rows)) {
                drupal_add_js(drupal_get_path('module', 'lx_domains_cmp').'/js/tip_window.js');
                $output .= '<div class="show-full-str">'
                            . '     <div class="close-wrapper"><a class="close" href="#">x</a></div>'
                            . '     <div class="content-data"></div>'
                            . '</div>';            

                if (isset($_GET['select_type']) && $_GET['select_type'] == 'count') {
                    $show_fields = array('count') + $show_fields;
                }

                $per_page = (isset($_GET['limit']) ? $_GET['limit'] : 50);   // строк на страницу
                $total_rows = $res['total_count']; // всего строк
                $element = 0;     // номер пэйджера

                pager_default_initialize($total_rows, $per_page, $element);
                $pager = theme('pager');            

                $output .= '<div style="clear: both; float: none; padding-top: 20px">'.$query.'</div>';
                $output .= '<div style="clear: both; float: none; padding-top: 20px"><span>Execution time: ' .$res['execution_time'] .'ms. Total: ' .number_format($res['total_count'],0,'.', ' ') .'</span></div>'.theme('table', array('header' => $show_fields, 'rows' => $rows)) .$pager;// .theme('pager');
            }
        
        }
        else {
            $res = ch_query_csv($query);
            $output .= '<div style="clear: both; float: none; padding-top: 20px">Execution time: ' .$res['execution_time'] .'ms.<br /><a href="' .str_replace('/home/user173magic/data/www/', 'https://clients.', $res['filename']) .'">Download file</a></div>';
        }
    
    }
    catch (Exception $e) {
        print_r($e);
    }

    
    return $output;    
    
    
}


function lx_admin_ch_filter_form($form, $form_state, $table_fields = array(), $selected_fields = array()) {
//    global $ch_event_date;
    
    
    drupal_add_css(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/css/caleran.min.css');
    
    $log_created = strtotime('2018-01-01');
    drupal_add_js('window.user_created='.$log_created.';', 'inline');
    
    
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/js/moment.min.js');
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/js/caleran.min.js');
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp') .'/caleran/js/caleran-show.js');    
    drupal_add_js(drupal_get_path('module', 'lx_admin') .'/js/on-off.js');    
    
//    if (isset($_GET['event_date'])) {
//        $ch_event_date = check_plain($_GET['event_date']);
//    }
//    else {
//        $ch_event_date_res = ch_query('SELECT max(event_date) as ed FROM clicklog');
//        $ch_event_date = $ch_event_date_res['result'][0]['ed'];
//    }
    


    $date_ranges = array(
//                         6 => 'Last week', 
                         8 => 'Today', 
                         7 => 'Yesterday', 
                         2 => 'Last 7 days', 
                         4 => 'Last 30 days',
//                         1 => 'This week',
                         3 => 'This month',
                         4 => 'Last 30 days', 
                         5 => 'Last month',
                         10 => 'All time',
                         100 => 'Custom date  '
                    );
    
    $period_type = 8;
    if (isset($_GET['period_type']) && is_numeric($_GET['period_type']) && in_array($_GET['period_type'], array_keys($date_ranges))) {
        $period_type = $_GET['period_type'];
    }    
    
    $form['fs'] = array('#type' => 'fieldset',
        '#attributes' => array('class' => array('pad12 container-inline form-wrapper'))
    );    
    
    $def_value = date('d.m.Y');
    if (isset($_GET['dates']) && preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4} \- [0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $_GET['dates'])) {
        $def_value = $_GET['dates'];
    }
    
    $form['fs']['period_type'] = array('#type' => 'select',
        '#title' => '<i class="fa fa-calendar" aria-hidden="true"></i>',
        '#options' => $date_ranges,
        '#default_value' => $period_type,
        '#weight' => 1
        
        );
    
    $form['fs']['dates'] = array('#type' => 'textfield',
//        '#title' => '<i class="fa fa-calendar" aria-hidden="true"></i><span>Period&nbsp;&nbsp;&nbsp;</span>',
        '#default_value' => $def_value,
        '#weight' => 2,
        '#prefix' => '<div class="dates-wrapper"' .($period_type < 100 ? ' style="display:none"' : '') .'>',
        '#suffix' => '</div>',
        '#weight' => 2
        );    
    
    
    
    
    
    $form['mu'] = array('#type' => 'markup',
        '#markup' => '<style type="text/css" media="all">@import url("/sites/all/modules/custom/lx_admin/css/style.css");</style>'
        );
    
    
//    $tables = array($table_name);

    
//    $form['event_date'] = array('#type' => 'textfield',
//        '#title' => 'Дата',
//        '#default_value' => $ch_event_date,
//       // '#suffix' => '<script>jQuery(document).ready(function () {jQuery("#edit-table").change(function () { location.href="/admin/content/etalon-db-query?table=" + jQuery(this).val();}); });</script>'
//        );    
    
    $operations = array('=' => '=',
                        '<>' => '<>',
                        '>' => '>',
                        '>=' => '>=',
                        '<' => '<',
                        '<=' => '<=',
                        'IN' => 'IN',
                        'LIKE' => 'LIKE',
                        'NOT LIKE' => 'NOT LIKE'
        );
    
    $selects = array(
//                     'SELECT SUM(cnt) as cnt ' => 'Показать количество',
                     'fields' => 'Показать поля',        
                     'count' => 'Показать количество',
            );  
    
    
    $form['fs']['select_type'] = array('#type' => 'select',
        '#title' => 'Вид запроса',
        '#title_display' => 'invisible',
        '#options' => $selects,
        '#default_value' => isset($_GET['select_type']) ? $_GET['select_type'] : 'fields',
        '#weight' => 3
        );
    
//    $form['sort_order'] = array('#type' => 'select',
//        '#title' => 'Порядок сортировки',
//        '#options' => array('' => 'Выберите', 'ASC' => 'ASC', 'DESC' => 'DESC'),
//        '#default_value' => isset($_GET['sort_order']) ? $_GET['sort_order'] : '',
//        );    
    
    $form['fs']['limit'] = array('#type' => 'select',
        '#title' => 'выводить по',
        '#options' => array(20 => 20, 50 => 50, 100 => 100, 500 => 500, 1000 => 1000),
        '#default_value' => isset($_GET['limit']) ? $_GET['limit'] : 100,
        '#weight' => 4
        );    
    
//    $form['show_query'] = array('#type' => 'checkbox',
//        '#title' => 'Показывать только сформированный запрос',
//        '#default_value' => isset($_GET['show_query']) ? $_GET['show_query'] : 0,
//        );    
    
    $form['fs']['submit'] = array('#type' => 'submit',
        '#value' => 'Show',
        '#weight' => 5,
//        '#suffix' => isset($_GET['select_type']) ? ' <a style="margin-left: 20px" class="button" href="' .str_replace('&tocsv=1','',$_SERVER['REQUEST_URI']) .'&tocsv=1">To CSV</a>' : ''
        );    
    $form['fs']['csv'] = array('#type' => 'submit',
        '#value' => 'To CSV',
        '#weight' => 6,
//        '#suffix' => isset($_GET['select_type']) ? ' <a style="margin-left: 20px" class="button" href="' .str_replace('&tocsv=1','',$_SERVER['REQUEST_URI']) .'&tocsv=1">To CSV</a>' : ''
        );    
    
    $form['onoff'] = array('#type' => 'markup',
        '#markup' => '<div class="on-off-boxes"><a href="#" class="chbxes-on">Enable All</a> | <a href="#" class="chbxes-off">Disable All</a></div>'
        );
    
    $form['fields'] = array('#type' => 'checkboxes',
        '#title' => 'Показать выбранные поля',
        '#options' => $table_fields,
        '#default_value' => isset($_GET['fields']) ? $_GET['fields'] : $selected_fields,
        );
        
    
    $form['conds'] = array('#type' => 'markup',
        '#markup' => '<strong>Условия</strong><br />'
        );
    
    for ($i = 1; $i <= LX_ADMIN_NUM_WHERE; $i ++) {
        $form['fs_'.$i] = array('#type' => 'fieldset',
            );
                
                
        $form['fs_'.$i]['condition_field_' .$i] = array('#type' => 'select',
        '#options' => array('' => 'Поле') + $table_fields,
        '#default_value' => isset($_GET['condition_field_' .$i]) ? $_GET['condition_field_' .$i] : '',
        '#prefix' => '<div class="cond-inline">',
        '#suffix' => '</div>'
        );
        
        $form['fs_'.$i]['condition_op_' .$i] = array('#type' => 'select',
        '#options' => array('' => 'Операция') + $operations,
        '#default_value' => isset($_GET['condition_op_' .$i]) ? $_GET['condition_op_' .$i] : '',
        '#prefix' => '<div class="cond-inline">',
        '#suffix' => '</div>'            
        );
        
        $form['fs_'.$i]['condition_val_' .$i] = array('#type' => 'textfield',
            '#default_value' => isset($_GET['condition_val_' .$i]) ? $_GET['condition_val_' .$i] : '',
            '#attributes' => array('placeholder' => 'Введите значение. Для OR используйте ||'),
            '#prefix' => '<div class="cond-inline clear-right">',
            '#suffix' => '</div>',
            '#size' => 40,
            '#maxlength' => 1000
            );
        
    }    
    
    $form['#method'] = 'get';
    $form['#id'] = 'lx-admin-log-filter-form';
    
    return $form;
}

function lx_admin_ch_process_list() {
    $res = ch_query('SHOW PROCESSLIST');
    
    print_r($res);
    die();
}