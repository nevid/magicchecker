<?php


function lx_admin_iss_options() {
    $form['iss_risk_countries'] = array('#type' => 'textfield',
        '#title' => 'Risk countries',
        '#default_value' => variable_get('iss_risk_countries', 'US,LT,LV,EE,PL,SE'),
        '#description' => 'Example: US,LT,LV'
        );
    
    $ips = db_query('SELECT count(ip) FROM {users_ip_blacklist}')->fetchField();
    
    $form['iss_risk_ips'] = array('#type' => 'textarea',
        '#title' => 'Risk IPs',
        '#default_value' => '',
        '#description' => 'IPs loaded: ' .$ips .'<br />One IP per line'
        );
    
    $form['iss_calc_rules'] = array('#type' => 'textarea',
        '#title' => 'ARL calculate rules',
        '#default_value' =>  variable_get('iss_calc_rules', ''),
        '#description' => 'One IP per line');
    
    $form['recalc_all'] = array('#type' => 'checkbox',
        '#title' => 'Recalculate all ARLs',
        '#return_value' => 1
        );
    
    $form['submit'] = array('#type' => 'submit',
        '#value' => 'Save'
        );
    
    return $form;
}

function lx_admin_iss_options_submit($form, &$form_state) {
    variable_set('iss_risk_countries', trim(check_plain($form_state['values']['iss_risk_countries'])));
    if ($form_state['values']['iss_risk_ips']) {
        $ips = trim(check_plain($form_state['values']['iss_risk_ips']));
        if ($ips) {
            $ips = explode("\n", $ips);
            foreach ($ips as $ip) {
                if (trim($ip)) {
                    db_query('INSERT IGNORE INTO {users_ip_blacklist} (ip) VALUES (:ip)', array(':ip' => trim($ip)));
                }
            }
        }
    }
    
    variable_set('iss_calc_rules', trim(check_plain($form_state['values']['iss_calc_rules'])));
    
    if ($form_state['values']['recalc_all']) {
        db_update('users_iss')
            ->fields(array('recalc_risk' => 1))
            ->execute();
        
        // у кого нет единиц, убираем условие проверки
        db_query('UPDATE {users_iss} SET recalc_risk=0 where risk_country+risk_ip+duplicate_acc+several_countries+pay_on_reg_date+several_vpns=0');
    }
    
    
    drupal_set_message('Options were saved.');
}


function lx_admin_iss_users_list() {

    $header = array(
                array('data' => 'UID',     'field' => 'i.uid', 'sort' => 'desc'),
                'Email',
                array('data' => 'ARL',     'field' => 'i.total_risk'),
                array('data' => 'Risk countries', 'field' => 'i.risk_country'),
                array('data' => 'Risk IPs',           'field' => 'i.risk_ip'),
                array('data' => 'Dup. accs',           'field' => 'i.duplicate_acc'),
                array('data' => 'Countries>=2',           'field' => 'i.several_countries'),
                array('data' => 'Pay on reg',           'field' => 'i.pay_on_reg_date'),
                array('data' => 'VPNs>=2',           'field' => 'i.several_vpns'),
              ); 
 
    $res = db_select('users', 'u');
    $res->join('users_iss', 'i', 'u.uid=i.uid');
    $res->fields('u', array('mail'));
    $res->fields('i');
    $res->condition('i.uid', 0, '>');
    $res = $res->extend('PagerDefault')
                    ->limit(30); 
    $res = $res->extend('TableSort')->orderByHeader($header);
    $data = $res->execute();


    $rows = array();
    foreach ($data as $r) {
      $rows[] = array(
          $r->uid,
          '<a href="/user/' .$r->uid .'" target="_blank">' .$r->mail .'</a>',
          $r->total_risk,
          $r->risk_country ? '<i class="fa fa-check" aria-hidden="true"></i>' : '&nbsp;',
          $r->risk_ip ? '<i class="fa fa-check" ></i>' : '&nbsp;',
          $r->duplicate_acc ? '<i class="fa fa-check"></i>' : '&nbsp;',
          $r->several_countries ? '<i class="fa fa-check"></i>' : '&nbsp;',
          $r->pay_on_reg_date ? '<i class="fa fa-check"></i>' : '&nbsp;',
          $r->several_vpns ? '<i class="fa fa-check"></i>' : '&nbsp;',
      );
    }

    $output = theme('table', array('header' => $header, 'rows' => $rows));
    $output .= theme('pager');
        return $output;
}

function lx_admin_iss_duplicates_list() {
    
//    if (isset($_GET['intercom'])) {
//        require_once(drupal_get_path('module', 'lx_intercom') .'/includes/users.inc');
//        $extra = array();
//        $account = user_load(1656);
//        
//        try {
//            lx_intercom_create_user($account, $extra); 
//        }
//        catch (Exception $e) {
//            print $e->getMessage();
//        }
//        
//        print 'ok';
//        die();
//    }
    
    $_get_show_all = isset($_GET['show_all']) ? 1 : 0;
    $_get_show_only_notapproved = isset($_GET['show_only_notapproved']) ? 1 : 0;
    $_get_acc = isset($_GET['acc']) ? $_GET['acc'] : 0;
    
    if ($_get_acc) {
        if (!is_numeric($_get_acc) && valid_email_address($_get_acc)) {
            $_get_acc = db_select('users', 'u')
                            ->fields('u', array('uid'))
                            ->condition('mail', $_get_acc)
                            ->execute()->fetchField();
        }
    }
    
    if (is_numeric($_get_acc) && $_get_acc) {
        $_get_show_all = 0;
        $_get_show_only_notapproved = 0;
    }
    else if ($_get_show_all) {
        $_get_show_only_notapproved = 0;
    }

    
    
    $res = db_select('users_duplicates', 'd');
    $res->distinct('ip');
    $res->fields('d',array('ip'));
        
    
    if ($_get_acc) {
        $res->condition('uid', $_get_acc);
    }
    else if (!$_get_show_all) {
        $res->condition('approved', 0);
    }    
    
    $res = $res->extend('PagerDefault')
                    ->limit(20); 
    
    $res->orderBy('added', 'DESC');

    $data = $res->execute();
    
    $rows = array();
    
    $statuses = array(1 => 'Not Paid',
                      100 => 'Trial/Unsubs',
                      200 => 'Subscribed',
                      1000 => 'Trial',
                      1001 => 'Unsubscribed'
        );
    
    foreach ($data as $r) {
        $row = [];
        
        if (!$_get_show_only_notapproved) {
            $uids = db_query('SELECT u.uid, u.mail, u.created, s.field_active_status_value FROM {users} u, {field_data_field_active_status} s'
                . '             WHERE u.uid=s.entity_id AND uid IN (SELECT uid FROM {users_duplicates} WHERE ip=:ip) ORDER BY uid DESC', array(':ip' => $r->ip))->fetchAll();
        }
        else {
            $uids = db_query('SELECT u.uid, u.mail, u.created, s.field_active_status_value FROM {users} u, {field_data_field_active_status} s'
                . '             WHERE u.uid=s.entity_id AND uid IN (SELECT uid FROM {users_duplicates} WHERE ip=:ip AND approved=:appr) ORDER BY uid DESC', array(':ip' => $r->ip, ':appr' => 0))->fetchAll();            
        }
        
        $row[] = array('data' => $r->ip, 'rowspan' => sizeof($uids));
        $row[] = array('data' => '<a href="/user/' .$uids[0]->uid .'" target="_blank">'.$uids[0]->uid .'</a>');
        $row[] = array('data' => '<a href="mailto:' .$uids[0]->mail .'" target="_blank" title="Write Email to user">' .$uids[0]->mail .'</a>');
        $row[] = array('data' => date('d.m.Y',$uids[0]->created));
        
        $sts = $uids[0]->field_active_status_value;
        if ($sts == 100) {
            $has_payments = db_query('SELECT nid FROM {node} WHERE uid=:uid AND type=:type LIMIT 0,1', array(':uid' => $uids[0]->uid, ':type' => 'payment'))->fetchField();
            $sts = $has_payments ? 1001 : 1000;
        }
        
        $row[] = array('data' => '<a href="/user/' .$uids[0]->uid .'/payments" target="_blank" title="User\'s payments page">'.$statuses[$sts] .'</a>');
        
        $approved = (int)db_select('users_duplicates', 'd')
                        ->fields('d', array('approved'))
                        ->condition('ip', $r->ip)
                        ->condition('uid', $uids[0]->uid)
                        ->execute()->fetchField();
        
        $approved = $approved ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<a href="/admin/lxadmin/iss/duplicates/approve/' .$uids[0]->uid .'/' .$r->ip .'/as-duplicate">Approve duplicate</a>';
        
        $row[] = array('data' => $approved);
        
        
        $row[] = array('data' => '<a href="/admin/lxadmin/iss/duplicates/server-info/' .$r->ip .'" target="_blank">Server Data</a>', 'rowspan' => sizeof($uids));        
        $rows[] = $row;
        
        
        unset($uids[0]);
        if (sizeof($uids)) {
            foreach ($uids as $uid) {
                $row = [];
                $row[] = array('data' => '<a href="/user/' .$uid->uid .'" target="_blank">'.$uid->uid .'</a>');
                $row[] = array('data' => '<a href="mailto:' .$uid->mail .'" target="_blank" title="Write Email to user">' .$uid->mail .'</a>');
                $row[] = array('data' => date('d.m.Y',$uid->created));
                
                $sts = $uid->field_active_status_value;
                if ($sts == 100) {
                    $has_payments = db_query('SELECT nid FROM {node} WHERE uid=:uid AND type=:type LIMIT 0,1', array(':uid' => $uids[0]->uid, ':type' => 'payment'))->fetchField();
                    $sts = $has_payments ? 1001 : 1000;
                }                
                
                
                $row[] = array('data' => '<a href="/user/' .$uid->uid .'/payments" target="_blank" title="User\'s payments page">'.$statuses[$sts] .'</a>');
                
                $approved = (int)db_select('users_duplicates', 'd')
                                ->fields('d', array('approved'))
                                ->condition('ip', $r->ip)
                                ->condition('uid', $uid->uid)
                                ->execute()->fetchField();

                $approved = $approved ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<a href="/admin/lxadmin/iss/duplicates/approve/' .$uid->uid .'/' .$r->ip .'/as-duplicate">Approve duplicate</a>';

                $row[] = array('data' => $approved);                
                
                $rows[] = $row;
            }
        }
        
        $row = [];
        $row[] = array('data' => '', 'colspan' => 7, 'style' => 'background: #efefef');
        $rows[] = $row;
    }
    
    $form = drupal_get_form('lx_admin_filter_dups');
    $output = render($form);
    $header = array('Server IP', 'UID', 'Email', 'Created', 'Status','Approved','Operations');
    $output .= theme('table', array('header' => $header, 'rows' => $rows));
    $output .= theme('pager');    
    return $output;
}

function lx_admin_iss_duplicates_by_ip($ip) {
    if (preg_match('/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/', $ip)) {
        
        drupal_set_title('Server IP: ' .$ip);
        
        $isp = db_query('SELECT isp FROM {users_ips} WHERE ip=:ip LIMIT 0,1', array(':ip' => $ip))->fetchField();
        
        $output = '<h3>ISP: ' .$isp .'</h3>';
        
        $data = file_get_contents('http://88.99.166.16/get_hosts_by_curl_remote_addr_as3fglaweuIUd.php?ip='.$ip);
        //if (trim($data) && (strpos($data, 'none_new') === false) && (strpos($data, 'error') === false)) {
            $data = unserialize($data);
            $header = array('uid', 'host', 'curl_remote_addr', 'server_addr', 'user', 'home', 'server_name', 'server_software', 'server_protocol', 'document_root');
            
            $output .= theme('table', array('header' => $header, 'rows' => $data));
            
            return $output;
        //}
        //else {
          //  return $data;
        //}
        }
        else {
        return 'Wrong ip format';
    }
}

function lx_admin_filter_dups() {
    
    $acc = isset($_GET['acc']) && $_GET['acc'] ? 1 : 0;
    
    $form['fs'] = array('#type' => 'fieldset',
        '#attributes' => array('class' => array('pad12 container-inline form-wrapper'))
        );
    
    $form['fs']['acc'] = array('#type' => 'textfield',
        '#attributes' => array('placeholder' => 'uid or email'),
        '#default_value' => isset($_GET['acc']) ? $_GET['acc'] : ''
        
        );
    $form['fs']['show_all'] = array('#type' => 'checkbox',
        '#title' => 'Show all',
        '#return_value' => 1,
        '#default_value' => isset($_GET['show_all']) && !$acc ? 1 : 0
        );
    $form['fs']['show_only_notapproved'] = array('#type' => 'checkbox',
        '#title' => 'Show only not approved',
        '#return_value' => 1,
        '#default_value' => isset($_GET['show_only_notapproved']) && !$acc && !isset($_GET['show_all']) ? 1 : 0
        );
    
    $form['#method'] = 'get';
    
    $form['fs']['submit'] = array('#type' => 'submit',
        '#value' => 'Save'
        );
    
    return $form;
}

// подтверждаем дубль акка
function lx_admin_iss_duplicates_approve_user($form, $form_state, $uid, $ip) {
    
    $mail = db_select('users', 'u')->fields('u', array('mail'))->condition('uid', $uid)->execute()->fetchField();
    
    $form['mu'] = array('#type' => 'markup',
        '#markup' => '<h3>User <b>' .$mail .' (uid: ' .$uid .')</b> will be approved as duplicate account</h3>'
        );
    
    $form['uid'] = array('#type' => 'hidden',
        '#default_value' => $uid
        );
    
    $form['mail'] = array('#type' => 'hidden',
        '#default_value' => $mail
        );
    
    $form['ip'] = array('#type' => 'hidden',
        '#default_value' => $id
        );
    
    $form['ban_type'] = array('#type' => 'radios',
        '#options' => array(1 => 'Give one day for payment and send a warning letter.',
                            2 => 'Ban right now. No letter will be sent.',
                            3 => 'Do nothing. Just mark as duplicate.'),
        '#default_value' => 1
        );
    
    
    $form['mu2'] = array('#type' => 'markup',
        '#markup' => '<br />'
        );    
    
    $form['submit'] = array('#type' => 'submit',
        '#value' => 'Confirm'
        );
    
    return $form;
}


// ПРВОВЕРИТЬ БАН ДУБЛЕЙ АККОВ!!!!
function lx_admin_iss_duplicates_approve_user_submit($form, $form_state) {
    
    require_once(drupal_get_path('module', 'lx_intercom') .'/includes/users.inc');
    
    try {
        $uid = $form_state['values']['uid'];

        db_query('UPDATE {users_duplicates} SET approved=:approved WHERE uid=:uid', array(':approved' => 1, ':uid' => $uid));
        db_query('UPDATE {users_iss} SET duplicate_acc=:dup, recalc_risk=:risk WHERE uid=:uid', array(':dup' => 1, ':risk' => 1, ':uid' => $uid));

        $acc = user_load($uid);

        // обновляем инфо по юзеру
        if ($form_state['values']['ban_type'] == 1) {
            $__fields = array('duplicate_acc' => 2);
            lx_intercom_update_user_local($acc->uid, $__fields);

            $acc->field_active_till['und'][0]['value'] = strtotime(date('Y-m-d', time()-86400*2)); // ????
            user_save($acc);
        }
        else if ($form_state['values']['ban_type'] == 2) {

            require_once(drupal_get_path('module', 'lx_domains_cmp') .'/includes/rabbit_update_campaign.inc');


            $__fields = array('plan' => 'none-gone', 'duplicate_acc' => 1);
            lx_intercom_update_user_local($acc->uid, $__fields); 

            if ($acc->field_active_status['und'][0]['value'] != 1) {
                $acc->field_active_status['und'][0]['value'] = 1;
                user_save($acc);

                __set_status_all_user_campaigns($acc->uid, 'not_paid', 403, false);           
            }         

        }
        else {
            $__fields = array('duplicate_acc' => 3);
            lx_intercom_update_user_local($acc->uid, $__fields);
        }
    
    }
    catch (Exception $e) {
        drupal_set_message($e->getMessage(), 'error');
    }
    
    drupal_set_message('User ' .$form_state['values']['mail'] .' (uid: ' .$uid .') marked as duplicate.');
    drupal_goto('admin/lxadmin/iss/duplicates');
}