<?php

function lx_admin_aud_queue() {
    include_once './sites/all/modules/custom/lx_domains_cmp/includes/cmp_queue.inc';
    
    $data = lx_domains_cmp_aud_queue_status();
    
    $output = '<h2>Add/Update<h2>';
    $output .= '<p>Campaigns in add/update queue: <strong>' .$data['total'] .'</strong></p>';
    $output .= '<p>Min Date: <strong>' .$data['minDate'] .'</strong></p>';
    $output .= '<p>Max Date: <strong>' .$data['maxDate'] .'</strong></p>';
    $output .= '<p>Last update: <strong>' .$data['lastUpdate'] .'</strong></p>';
    
    $data = lx_domains_cmp_aud_queue_status(1);
    $output .= '<br /><h2>Delete<h2>';
    $output .= '<p>Campaigns in add/update queue: <strong>' .$data['total'] .'</strong></p>';
    $output .= '<p>Min Date: <strong>' .$data['minDate'] .'</strong></p>';
    $output .= '<p>Max Date: <strong>' .$data['maxDate'] .'</strong></p>';
    $output .= '<p>Last update: <strong>' .$data['lastUpdate'] .'</strong></p>';

    return $output;
}