<?php

require_once '/home/api173magic/data/www/api.magicchecker.com/vendor/autoload.php';
require_once '/home/user173magic/data/www/magicchecker.com/sites/all/modules/custom/lx_domains_cmp/sconfig.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
//use PhpAmqpLib\Message\AMQPMessage;

function lx_admin_traffic_ip_info($ip) {
    
    $ip_info = file_get_contents('http://check.magicchecker.com/v2.2/get_ip_info_sf3azD32dv52dfgDFe.php?ip=' .$ip);
        
    $output = '';
    if (trim($ip_info)) {
        $ip_info = (array)json_decode($ip_info);
        if (isset($ip_info['status']) && ($ip_info['status'] == 'ok')) {
            return $ip_info['country'];
        }
    }      
    
    return "NODATA";
}

function lx_admin_traffic_stat_clickhouse($array_only = false) {
    require_once drupal_get_path('module', 'lx_admin') .'/includes/click_house.inc';
    
    $res = ch_query("SELECT count(id) as cnt, headers_browser FROM clicklog PREWHERE event_date='" .date('Y-m-d', time()-60) ."' WHERE click_date > '" .date('Y-m-d H:i:00', time()-60) ."'  GROUP BY headers_browser");
    
    $output = '';
    
    if (!isset($_GET['js'])) {
        $output = '<br /><br /><h1>Last minute (clickhouse)</h1>'
                . '            <table class="min-table" style="width: 500px; border: none">'
                . '                 <thead>'
                . '                     <tr>'
                . '                         <td>Server</td><td>Total for minute</td><td>Average per second</td>'
                . '                     </tr>'
                . '                 </thead>';
    }
    
    $data_by_ips = [];
    
    if (sizeof($res)) {
        
        foreach ($res['result'] as $r) {
            if ($array_only) {
                $data_by_ips[$r['headers_browser']] = $r['cnt'];
            }
            else {
                $country = lx_admin_traffic_ip_info($r['headers_browser']);
                $data_by_ips[$country .': ' .$r['headers_browser']] = $r['cnt'];
            }    
        }
        
        ksort($data_by_ips);
    }
    
    if ($array_only) {
        return $data_by_ips;
    }
    
    
    if (isset($_GET['js'])) {
        print json_encode($data_by_ips); die();
    }
    else if (!$array_only && !isset($_GET['js'])) {
        $i = 0;
        foreach ($data_by_ips as $k => $v) {
            $output .= '<tr><td>' .$k .'</td><td class="c-cnt-' .$i .'">' .$v .'</td><td>' .round($v/60) .' r/s'  .'</td></tr>';        
            $i ++;
        }   
        
        $output .= '</table>';     
    }

    return $output;
    
}

function lx_admin_traffic_stat($array_only = false) {
//    require_once drupal_get_path('module', 'lx_admin') .'/includes/click_house.inc';
    
    if (!$array_only && !isset($_GET['js'])) {
        drupal_add_js(drupal_get_path('module', 'lx_admin') .'/js/traffic.js');
        
        lx_admin_traffic_stat_clickhouse();
    }
    
    
    $output = '';
    
    $cur_table = 'mem_clicklog_' .date('YmdHi');    
    try {   
       db_set_active('stata');

       $res = db_query('SELECT count(id) as cnt, headers_browser FROM ' .$cur_table .' WHERE click_date>=:click_date GROUP BY headers_browser', [':click_date' => date('Y-m-d H:i:s',time() - 5)])->fetchAll();

       db_set_active('default');

    }
    catch (Exception $e) {
        $output = $e->getMessage(); 
    }
    

    if (!isset($_GET['js'])) {
        $output = '<h1>Last second (mysql)</h1><table class="sec-table" style="width: 500px; border: none">'
                . '                 <thead>'
                . '                     <tr>'
                . '                         <td>Server</td><td>Total for second</td>'
                . '                     </tr>'
                . '                 </thead>';;
    }
    
    $data_by_ips = [];
    
    if (sizeof($res)) {
        
        foreach ($res as $r) {
            if ($array_only) {
                $data_by_ips[$r->headers_browser] = round($r->cnt/5);
            }
            else {
                $country = lx_admin_traffic_ip_info($r->headers_browser);
                $data_by_ips[$country .': ' .$r->headers_browser] = round($r->cnt/5);
            }    
        }
        
        ksort($data_by_ips);
    }
    
    if ($array_only) {
        return $data_by_ips;
    }
    
    
    if (isset($_GET['js'])) {
        print json_encode($data_by_ips); die();
    }
    else if (!$array_only && !isset($_GET['js'])) {
        $i = 0;
        foreach ($data_by_ips as $k => $v) {
            $output .= '<tr><td>' .$k .'</td><td class="cnt-' .$i .'">' .$v .'</td></tr>';        
            $i ++;
        }   
        
        $output .= '</table>';
        
        $output .= lx_admin_traffic_stat_clickhouse();        
    }
    
    return $output;
}


// проверяет текущую таблицу в памяти на 16м, есть ли данные от всех серверов
// если нет, то уведомляем нас
function lx_admin_traffic_stat_notify() {
//    global $_api_hosts;
    require_once(LX_REMOTE_SERVERS_API_HOSTS_PATH);
    $api_hosts = __get_defined_server_types_ips('workers');
    
    $working_ips = lx_admin_traffic_stat(true);
    
//    $api_hosts = [];
    

//    foreach ($_api_hosts as $v) {
//        $v = explode('|', $v);
//        
//        if ($v[0] != '88.99.166.16') {
//            $api_hosts[] = $v[0];
//        }
//    }
        
    
//    unset($working_ips['165.22.99.198']); // test data

    
    if (sizeof($api_hosts) != sizeof($working_ips)) {
        $diff = array_diff($api_hosts, array_keys($working_ips));
        
        if (!empty($diff) && is_array($diff)) {
            $result = implode("<br />", $diff);
            
            
            $subject = 'DB Log Alert!';
            $mess = 'The next servers has no data for last check:<br /><br />' .$result .'<br /><br />Kill db_logger2.php process for defined ips on 16th<br /><br />';

            /* Для отправки HTML-почты вы можете установить шапку Content-type. */
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

            /* дополнительные шапки */
            $headers .= "From: MagicChecker.com <info@magicchecker.com>\r\n";

//            mail('alexei.vertal@yandex.ru, vitalii.gres@icloud.com', $subject, $mess, $headers);             
            mail(LX_ADMIN_NOTIFY_MAIL, $subject, $mess, $headers);             
            
        }
    }
            
    print 'ok';
    
}

function lx_admin_rabbit_api_call($ip, $port, $username, $password) {

    $URL='http://' .$ip .':1' .$port .'/api/queues?sort=message_stats.publish_details.rate&sort_reverse=true&columns=name,message_stats.publish_details.rate,message_stats.deliver_get_details.rate,messages,messages_ready,memory,state';

    
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$URL);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout after 5 seconds
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    $result=curl_exec ($ch);
    
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    curl_close ($ch);    
    
    return ['http_code' => $status_code, 'data' => $result];
}

function lx_admin_rabbit_stat_parse_answer($data) {
    $qs = json_decode($data['data']);
    
    $result = [];
    
    if (is_array($qs)) {
        foreach ($qs as $q) {
            $q = (array)$q;
            
            $result[$q['name']] = [
                                'name' => $q['name'],
                                'status' => $q['state'],
                                'memory' => $q['memory'],
                                'messages' => $q['messages'],
                                'in_out' => $q['message_stats']->publish_details->rate .' / ' .$q['message_stats']->deliver_get_details->rate,
                                'out' => $q['message_stats']->deliver_get_details->rate
                              ];
        }
    }
    
    return $result;
}

function lx_admin_rabbit_stat_table($ip, $qs) {
    $tbl_id = 't-'.str_replace('.','',$ip);
    
    $tbls_td_width = [200,100,100,100,100];
    
    $output = '<fieldset class="table-fs stat-table new-look blue-header cmp-list-table"><table>'
            . ' <thead>'
            . ' <tr>'
            . '     <th>' .$ip .'</th><th>Status</th><th>Memory</th><th>Queue Length</th><th>In/Out</th>'
            . '</tr>'
            . '</thead>';
    
    $i = 0;
    foreach ($qs as $name => $row) {
        $output .= '<tr class="' .($i%2 == 0 ? 'odd' : 'even') .'">';
        $j = 0;
        foreach ($row as $k => $v) {
            if ($k == 'out') continue;
            $output .= '<td style="width: ' .$tbls_td_width[$j] .'px" class="' .$tbl_id .'-' .$name .'-' .$k .'">' .$v .'</td>';
            $j ++;
        }
        $output .= '</tr>';
        $i++;
    }
    
    
    $output .= '</table></fieldset>';
    
    return $output;
}

function lx_admin_rabbit_stat_data() {
//    global $_api_hosts;
    require_once(LX_REMOTE_SERVERS_API_HOSTS_PATH);
    $_api_hosts = __build_api_hosts();    
    
    $queues_ar = [];
    
    foreach ($_api_hosts as $host) {
        $host = explode('|', $host);
        
        $data = lx_admin_rabbit_api_call($host[0], $host[1],$host[2], $host[3]);

        $queues_ar[$host[0]] = lx_admin_rabbit_stat_parse_answer($data);
    }    
    
    return $queues_ar;
}

function lx_admin_rabbit_stat() {
    
    drupal_add_js(drupal_get_path('module', 'lx_admin') .'/js/rabbit.js');
    
    $tbls = [];
    $queues_ar = lx_admin_rabbit_stat_data();
    
    foreach ($queues_ar as $ip => $data) {
        $tbls[] = lx_admin_rabbit_stat_table($ip, $data);
    }
    
    $output = '<table><tr><td style="padding: 20px !important; border-bottom: none !important; vertical-align: top">';
    
    $tbls_cols = array_chunk($tbls, 5);
    
    $output .= implode('', $tbls_cols[0]);
    
    $output .= '</td><td style="padding: 20px !important; border-bottom: none !important; border-right: none !important; vertical-align: top">';
    
    $output .= implode('', $tbls_cols[1]);
    
    $output .= '</td></tr></table>';
    
    return $output;
    
//    $connection = new AMQPStreamConnection('104.248.32.132', 5672, 'admin173', 'kwAEsDjtWuG3NC');
//    $channel = $connection->channel();
////    list($queue, $messageCount, $consumerCount) = $channel->queue_declare('log_request', true);
//    $data = $channel->queue_declare('log_request', true);
//    
//    $channel->close();
//    $connection->close();
    
/*
 * curl -i -u admin173:kwAEsDjtWuG3NC 'http://104.248.32.132:15672/api/queues?sort=message_stats.publish_details.rate&sort_reverse=true&columns=name,message_stats.publish_details.rate,message_stats.deliver_get_details.rate,messages,messages_ready,memory,state'
 * 
 */    
    
}

function lx_admin_rabbit_stat_js() {
    $queues_ar = lx_admin_rabbit_stat_data();
    
    $to_js = [];
    
    foreach ($queues_ar as $ip => $qs) {
        $tbl_id = 't-'.str_replace('.','',$ip);
        foreach ($qs as $queue => $row) {
            foreach ($row as $k => $v) {
                $to_js[$tbl_id.'-'.$queue.'-'.$k] = $v;
            }
        }    
    }
    
    print json_encode($to_js);
}

function lx_admin_rabbit_send_kill_command($ip, $queue_name) {
    $url = '';
    
    if (in_array($queue_name, ['log_request'])) {
        $url = 'http://88.99.166.16/restart-logger-urWEfnr4dEr90kj-Rtssw3.php?ip=' .urlencode($ip);
    }
    else if (in_array($queue_name, ['set_campaign_status', 'get_campaign'])) {
        $real_processes_names = ['set_campaign_status' => 'set_campaign_status', 'get_campaign' => 'give_cmp_to_index2'];
        $url = 'https://clients.magicchecker.com/restart-status-bgjt8ERW84df0f-ss4D32dfu7?ip=' .urlencode($ip) .'&proc=' .urlencode($real_processes_names[$queue_name]);
    }
    
    $res = file_get_contents($url);
    
    return $res;
}

// поверяем работу очередей, если какая-то выбивается за верхний порог - отсылаем запрос на завершение процесса
function lx_admin_rabbit_cron_monitor() {
//    global $_api_hosts;
    require_once(LX_REMOTE_SERVERS_API_HOSTS_PATH);
    $_api_hosts = __build_api_hosts();        
    
    $to_send = [];
    $critical = false;
    $resp_from_16 = '';
    
    
    $porog = [
        'log_request' => 5000,
        'update_campaign' => 20,
        'get_campaign' => 20,
        'set_campaign_status' => 50
        ];
    
    $queues_ar = [];
    
    foreach ($_api_hosts as $host) {
        $host = explode('|', $host);
        
        $data = lx_admin_rabbit_api_call($host[0], $host[1],$host[2], $host[3]);

        $parsed = lx_admin_rabbit_stat_parse_answer($data);
        
        foreach ($parsed as $queue_name => $vals) {
//            if (($host[0] == '178.128.178.209') && ($queue_name == 'get_campaign')) { // test data !
//            if (($host[0] == '88.99.166.16') && ($queue_name == 'set_campaign_status')) { // test data !
//                $vals['messages'] = 100000;
//            }    
            
            $critical = false;
            
            if ($vals['messages'] > 0) {
                
                $ip = explode('.', $host[0]);
                
                $to_send = $queue_name .' (' .$vals['messages'] .') on ' .$host[0];
                
                if ($vals['messages'] > $porog[$queue_name]) {
                    if ($vals['out'] == 0) {
                        $resp_from_16 = lx_admin_rabbit_send_kill_command($host[0], $queue_name);                    
                        $critical = true;
                    }    
                }
                
                
                $subject = 'Queues Alert!';
                $mess = 'Queue too long!<br /><br />' . $to_send .'<br /><br />' .($critical ? 'Command is sent.' : '');
                
                if ($resp_from_16) {
                    $mess .= '<br /><br />'.nl2br($resp_from_16);
                }
                
                /* Для отправки HTML-почты вы можете установить шапку Content-type. */
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

                /* дополнительные шапки */
                $headers .= "From: MagicChecker.com <info@magicchecker.com>\r\n";


    //            mail('alexei.vertal@yandex.ru, vitalii.gres@icloud.com', $subject, $mess, $headers);             
                mail(LX_ADMIN_NOTIFY_MAIL, $subject, $mess, $headers);   
                        
                
                
            }            
        }
    }    
    
}

function lx_admin_db_logger_send_mail() {
    $username = check_plain($_POST['user']);
    $pass = check_plain($_POST['pass']);
    $mess = nl2br(check_plain($_POST['mail']));
    
//    print_r($_POST);
    
    if (($username == 'dEhecs9dj') && ($pass == 'fkrur843Hds3hjdsn34fvdD')) {
            $subject = 'Queue Restart Result';
//            $mess = 'Queue too long!<br /><br />' . $to_send .'<br /><br />' .($critical ? 'Command is sent.' : '');
            /* Для отправки HTML-почты вы можете установить шапку Content-type. */
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

            /* дополнительные шапки */
            $headers .= "From: MagicChecker.com <info@magicchecker.com>\r\n";


//            mail('alexei.vertal@yandex.ru, vitalii.gres@icloud.com', $subject, $mess, $headers);             
            mail(LX_ADMIN_NOTIFY_MAIL, $subject, $mess, $headers);   
            
//            print "!OK!";
    }
    else {
//        print 'error';
        die();
    }
}

function lx_admin_rabbit_init_process_restart() {
    $ip = $_GET['ip'];
    $proc_name = $_GET['proc'];

    if (preg_match('/^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$/', $ip)) {

        try {
            $fname = '/home/user173magic/data/queues_to_kill/'.str_replace('.', '_', $ip) .'_'.$proc_name;
            if (!file_exists($fname)) {
                $f = fopen($fname, 'w');
                fputs($f, $ip.':'.$proc_name);
                fclose($f);

                print 'IP logged';
            }
            else {
                print 'File ' .$ip .'_'.$proc_name .' already exists. Waiting for action on 16.';
            }
        }
        catch (Exception $e) {
            'Error while IP ' .$ip .'_'.$proc_name .' logging: ' .$e->getMessage();
        }
    }
    else {
        print 'Bad IP sent!' ."\n\n" .htmlspecialchars($ip, ENT_QUOTES);
    }    
}