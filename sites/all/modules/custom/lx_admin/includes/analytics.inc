<?php

function lx_admin_analytics_page() {
    drupal_add_js(drupal_get_path('module', 'lx_admin') .'/js/analytics.js');
        
    // находим общее кол-во юзеров
    $all = array();
    $res = db_query('SELECT count(u.uid) as cnt, s.field_active_status_value'
            . '             FROM {users} u, {field_data_field_active_status} s'
            . '             WHERE u.uid=s.entity_id AND u.uid > :uid1 AND u.uid <> :uid2'
            . '             GROUP BY s.field_active_status_value', array(':uid1' => 20, ':uid2' => 27))->fetchAll();
    
    $total_users = 0;
    foreach ($res as $r) {
        $all[$r->field_active_status_value] = $r->cnt;
        $total_users += $r->cnt;
    }
    
    $output .= '<div class="gen-analyt-wrapper"><fieldset class="total-info"  style="display:inline-block; width: 300px; margin-right: 40px; vertical-align: top"><legend>All Time</legend>';
    $output .= '<div class="fieldset-wrapper">'
            . '    <table>'
            . '     <tr class="odd"><td><strong>Total registered:</strong></td><td>' .$total_users .'</td></tr>'
            . '     <tr class="even"><td><strong>Subscribed:</strong></td><td>' .$all[200] .' (' .round($all[200]/$total_users*100) .'%)' .'</td></tr>'
            . '     <tr class="odd"><td><strong>Trial or paid single period:</strong></td><td>' .$all[100] .' (' .round($all[100]/$total_users*100) .'%)' .'</td></tr>'
            . '     <tr class="even"><td><strong>Went away:</strong></td><td>' .$all[1] .' (' .round($all[1]/$total_users*100) .'%)' .'</td></tr>'
            . '    </table>';
    
    
    
//    $campaigns_stat = db_query('SELECT sum(total_clicks) as total_clicks,  sum(adplex_clicks + hosting_clicks+country_clicks+bot_clicks + isp_clicks+referer_clicks+ip_clicks+device_os_clicks+url_clicks+ua_clicks+time_clicks+status_clicks+ headers_clicks+light_clicks+super_clicks) as total_block, uid, cid FROM {domain_compaigns_cmp_stat}'
//            . '                         WHERE uid IN (SELECT entity_id FROM {field_data_field_active_status} WHERE field_active_status_value=:status AND entity_id > :uid1 AND entity_id <> :uid2) GROUP BY uid, cid  HAVING total_clicks > 0', array(':status' => 100, ':uid1' => 20, ':uid2' => 27))->fetchAll();
    $uids_stat = db_query('SELECT DISTINCT(uid) as uid FROM {domain_compaigns_cmp_stat}'
            . '                         WHERE uid IN (SELECT entity_id FROM {field_data_field_active_status} WHERE field_active_status_value=:status AND entity_id > :uid1 AND entity_id <> :uid2)', array(':status' => 100, ':uid1' => 20, ':uid2' => 27))->fetchAll();
    
    $uids_has_traf = array();
    foreach ($uids_stat as $c) {
        if (!isset($uids_has_traf[$c->uid])) {
            $uids_has_traf[$c->uid] = $c->uid;
        }
    }
    
    $uids_stat = db_query('SELECT DISTINCT(uid) as uid FROM {domain_compaigns}'
            . '                         WHERE uid IN (SELECT entity_id FROM {field_data_field_active_status} WHERE field_active_status_value=:status AND entity_id > :uid1 AND entity_id <> :uid2)', array(':status' => 100, ':uid1' => 20, ':uid2' => 27))->fetchAll();
    
    $uids_has_campaigns = array();
    foreach ($uids_stat as $c) {
        if (!isset($uids_has_campaigns[$c->uid]) && !in_array($c->uid, $uids_has_campaigns)) {
            $uids_has_campaigns[$c->uid] = $c->uid;
        }
    }
    
    $output .= '<h2>Trial users statistcs</h2>';
    $output .= '<table>'
            . '     <tr class="odd"><td><strong>Has campaigns with clicks:</strong></td><td>' .sizeof($uids_has_traf) .' (' .round(sizeof($uids_has_traf)/$all[100]*100) .'%)' .'</td></tr>'
            . '     <tr class="even"><td><strong>Created campaigns:</strong></td><td>' .sizeof($uids_has_campaigns) .' (' .round(sizeof($uids_has_campaigns)/$all[100]*100) .'%)' .'</td></tr>'
            . '     <tr class="odd"><td><strong>No activity:</strong></td><td>' .round($all[100] - sizeof($uids_has_campaigns)) .' (' .round(($all[100] - sizeof($uids_has_campaigns))/$all[100]*100) .'%)' .'</td></tr>'
            . '    </table>';    
    
    $uids_stat = db_query('SELECT DISTINCT(uid) as uid FROM {domain_compaigns_cmp_stat}'
            . '                         WHERE uid IN (SELECT entity_id FROM {field_data_field_active_status} WHERE field_active_status_value=:status AND entity_id > :uid1 AND entity_id <> :uid2)', array(':status' => 1, ':uid1' => 20, ':uid2' => 27))->fetchAll();
    
    $uids_has_traf = array();
    foreach ($uids_stat as $c) {
        if (!isset($uids_has_traf[$c->uid])) {
            $uids_has_traf[$c->uid] = $c->uid;
        }
    }
    
    $uids_stat = db_query('SELECT DISTINCT(uid) as uid FROM {domain_compaigns}'
            . '                         WHERE uid IN (SELECT entity_id FROM {field_data_field_active_status} WHERE field_active_status_value=:status AND entity_id > :uid1 AND entity_id <> :uid2)', array(':status' => 1, ':uid1' => 20, ':uid2' => 27))->fetchAll();
    
    $uids_has_campaigns = array();
    foreach ($uids_stat as $c) {
        if (!isset($uids_has_campaigns[$c->uid]) && !in_array($c->uid, $uids_has_campaigns)) {
            $uids_has_campaigns[$c->uid] = $c->uid;
        }
    }
    
    $output .= '<h2>Went users statistcs</h2>';
    $output .= '<table>'
            . '     <tr class="odd"><td><strong>Has campaigns with clicks:</strong></td><td>' .sizeof($uids_has_traf) .' (' .round(sizeof($uids_has_traf)/$all[1]*100) .'%)' .'</td></tr>'
            . '     <tr class="even"><td><strong>Created campaigns:</strong></td><td>' .sizeof($uids_has_campaigns) .' (' .round(sizeof($uids_has_campaigns)/$all[1]*100) .'%)' .'</td></tr>'
            . '     <tr class="odd"><td><strong>No activity:</strong></td><td>' .round($all[1] - sizeof($uids_has_campaigns)) .' (' .round(($all[1] - sizeof($uids_has_campaigns))/$all[1]*100) .'%)' .'</td></tr>'
            . '    </table>';     
    
    
    $output .= '</fieldset>';
    
    
    // за текущий или выбранный месяц:
    $cur_month = lx_admin_general_analytics_month(date('Y-m'));
    
    $output .= $cur_month;
        
    
    $output .= '</div>';
    $output .= '</fieldset>';
    
    
    $output .= lx_admin_analytics_payments_stat('all');
    
    $output .= '</div><br /><br /><div class="users-list"></div>';    
    
    return $output;
}    

function lx_admin_general_analytics_month($month, $js = false) {
    $output = '';
    
    $mon_from = explode('-', $month);
    $mon_from = mktime(0,0,0, ltrim($mon_from[1], '0'), 1, $mon_from[0]);
    $mon_to = explode('-', date("Y-m-t", $mon_from));
    
    $mon_to = mktime(23,59,59, ltrim($mon_to[1], '0'), ltrim($mon_to[2], '0'), $mon_to[0]);
    
    
    $all = array();
    $res = db_query('SELECT count(u.uid) as cnt, s.field_active_status_value'
            . '             FROM {users} u, {field_data_field_active_status} s'
            . '             WHERE u.uid=s.entity_id AND u.uid > :uid1 AND u.uid <> :uid2 AND'
            . '                   u.created >= :d1 AND u.created <= :d2'
            . '             GROUP BY s.field_active_status_value', 
                            array(':uid1' => 20, ':uid2' => 27, ':d1' => $mon_from, ':d2' => $mon_to))->fetchAll();  
    
    $total_users = 0;
    foreach ($res as $r) {
        $all[$r->field_active_status_value] = $r->cnt;
        $total_users += $r->cnt;
    }    
    
    
    $calked_prev = explode('-', $month);
    $calked_prev[1] = ltrim($calked_prev[1], '0');
    $calked_prev[1] --;
    if ($calked_prev[1] <= 0) {
        $calked_prev[0] --;
        $calked_prev[1] = 12;
    }
    if ($calked_prev[1] < 10) {
        $calked_prev[1] = '0' .$calked_prev[1];
    }
    
    $calked_prev = $calked_prev[0] .'-' .$calked_prev[1];
    
    $calked_next = explode('-', $month);
    $calked_next[1] = ltrim($calked_next[1], '0');
    $calked_next[1] ++;
    if ($calked_next[1] > 12) {
        $calked_next[0] ++;
        $calked_next[1] = 1;
    }
    if ($calked_next[1] < 10) {
        $calked_next[1] = '0' .$calked_next[1];
    }
    
    $calked_next = $calked_next[0] .'-' .$calked_next[1];
    
    
    $next_mon = ($month == date('Y-m')) ? '' : '&nbsp;&nbsp;&nbsp;<a class="change-month" data-mon="' .$calked_next .'" href="#">>></a>';
    $prev_mon = ($month == '2017-01') ? '' : '<a class="change-month" data-mon="' .$calked_prev .'" href="#"><<</a>&nbsp;&nbsp;&nbsp;';
    
    $output .= '<fieldset class="total-info-month" style="display:inline-block; width: 300px; margin-right: 40px; vertical-align: top"><legend>' .$prev_mon .date('F Y', $mon_from) .$next_mon  .'</legend>';
    $output .= '<div class="fieldset-wrapper">'
            . '    <table>'
            . '     <tr class="odd"><td><a class="js-users-list" href="/admin/lxadmin/analytics/js-list?mon_from=' .$mon_from .'&mon_to=' .$mon_to .'"><strong>Total registered:</strong></a></td><td>' .$total_users .'</td></tr>'
            . '     <tr class="even"><td><a class="js-users-list" href="/admin/lxadmin/analytics/js-list?mon_from=' .$mon_from .'&mon_to=' .$mon_to .'&status=200"><strong>Subscribed:</strong></a></td><td>' .$all[200] .' (' .round($all[200]/$total_users*100) .'%)' .'</td></tr>'
            . '     <tr class="odd"><td><a class="js-users-list" href="/admin/lxadmin/analytics/js-list?mon_from=' .$mon_from .'&mon_to=' .$mon_to .'&status=100"><strong>Trial or paid single period:</strong></a></td><td>' .$all[100] .' (' .round($all[100]/$total_users*100) .'%)' .'</td></tr>'
            . '     <tr class="even"><td><a class="js-users-list" href="/admin/lxadmin/analytics/js-list?mon_from=' .$mon_from .'&mon_to=' .$mon_to .'&status=1"><strong>Went away:</strong></a></td><td>' .$all[1] .' (' .round($all[1]/$total_users*100) .'%)' .'</td></tr>'
            . '    </table>';
    
    
    
    
    $uids_stat = db_query('SELECT DISTINCT(uid) as uid FROM {domain_compaigns_cmp_stat}'
            . '                         WHERE uid IN (SELECT s.entity_id FROM {users} u, {field_data_field_active_status} s WHERE u.uid=s.entity_id AND u.created >= :d1 AND u.created <= :d2 AND s.field_active_status_value=:status AND s.entity_id > :uid1 AND s.entity_id <> :uid2)', array(':d1' => $mon_from, ':d2' => $mon_to, ':status' => 100, ':uid1' => 20, ':uid2' => 27))->fetchAll();
    
    $uids_has_traf = array();
    foreach ($uids_stat as $c) {
        if (!isset($uids_has_traf[$c->uid])) {
            $uids_has_traf[$c->uid] = $c->uid;
        }
    }
    
    $uids_stat = db_query('SELECT DISTINCT(uid) as uid FROM {domain_compaigns}'
            . '                         WHERE uid IN (SELECT s.entity_id FROM {users} u, {field_data_field_active_status} s WHERE u.uid=s.entity_id AND u.created >= :d1 AND u.created <= :d2 AND s.field_active_status_value=:status AND s.entity_id > :uid1 AND s.entity_id <> :uid2)', array(':d1' => $mon_from, ':d2' => $mon_to, ':status' => 100, ':uid1' => 20, ':uid2' => 27))->fetchAll();
    
    $uids_has_campaigns = array();
    foreach ($uids_stat as $c) {
        if (!isset($uids_has_campaigns[$c->uid]) && !in_array($c->uid, $uids_has_campaigns)) {
            $uids_has_campaigns[$c->uid] = $c->uid;
        }
    }
    
    $output .= '<h2>Trial users statistcs</h2>';
    $output .= '<table>'
            . '     <tr class="odd"><td><strong>Has campaigns with clicks:</strong></td><td>' .sizeof($uids_has_traf) .' (' .round(sizeof($uids_has_traf)/$all[100]*100) .'%)' .'</td></tr>'
            . '     <tr class="even"><td><strong>Created campaigns:</strong></td><td>' .sizeof($uids_has_campaigns) .' (' .round(sizeof($uids_has_campaigns)/$all[100]*100) .'%)' .'</td></tr>'
            . '     <tr class="odd"><td><strong>No activity:</strong></td><td>' .round($all[100] - sizeof($uids_has_campaigns)) .' (' .round(($all[100] - sizeof($uids_has_campaigns))/$all[100]*100) .'%)' .'</td></tr>'
            . '    </table>';        
    
    
    $uids_stat = db_query('SELECT DISTINCT(uid) as uid FROM {domain_compaigns_cmp_stat}'
            . '                         WHERE uid IN (SELECT s.entity_id FROM {users} u, {field_data_field_active_status} s WHERE u.uid=s.entity_id AND u.created >= :d1 AND u.created <= :d2 AND s.field_active_status_value=:status AND s.entity_id > :uid1 AND s.entity_id <> :uid2)', array(':d1' => $mon_from, ':d2' => $mon_to, ':status' => 1, ':uid1' => 20, ':uid2' => 27))->fetchAll();
    
    $uids_has_traf = array();
    foreach ($uids_stat as $c) {
        if (!isset($uids_has_traf[$c->uid])) {
            $uids_has_traf[$c->uid] = $c->uid;
        }
    }
    
    $uids_stat = db_query('SELECT DISTINCT(uid) as uid FROM {domain_compaigns}'
            . '                         WHERE uid IN (SELECT s.entity_id FROM {users} u, {field_data_field_active_status} s WHERE u.uid=s.entity_id AND u.created >= :d1 AND u.created <= :d2 AND s.field_active_status_value=:status AND s.entity_id > :uid1 AND s.entity_id <> :uid2)', array(':d1' => $mon_from, ':d2' => $mon_to, ':status' => 1, ':uid1' => 20, ':uid2' => 27))->fetchAll();
    
    $uids_has_campaigns = array();
    foreach ($uids_stat as $c) {
        if (!isset($uids_has_campaigns[$c->uid]) && !in_array($c->uid, $uids_has_campaigns)) {
            $uids_has_campaigns[$c->uid] = $c->uid;
        }
    }
    
    $output .= '<h2>Went users statistcs</h2>';
    $output .= '<table>'
            . '     <tr class="odd"><td><strong>Has campaigns with clicks:</strong></td><td>' .sizeof($uids_has_traf) .' (' .round(sizeof($uids_has_traf)/$all[1]*100) .'%)' .'</td></tr>'
            . '     <tr class="even"><td><strong>Created campaigns:</strong></td><td>' .sizeof($uids_has_campaigns) .' (' .round(sizeof($uids_has_campaigns)/$all[1]*100) .'%)' .'</td></tr>'
            . '     <tr class="odd"><td><strong>No activity:</strong></td><td>' .round($all[1] - sizeof($uids_has_campaigns)) .' (' .round(($all[1] - sizeof($uids_has_campaigns))/$all[100]*100) .'%)' .'</td></tr>'
            . '    </table>';       
    
    if ($js) {
        print $output;
    }
    else {
        return $output;
    }    
}

function lx_admin_analytics_users_list() {
    $mon_from = (int)$_GET['mon_from'];
    $mon_to = (int)$_GET['mon_to'];
    $status = (int)$_GET['status'];

    if ($status) {
        $res = db_query('SELECT u.created, u.uid, u.name, u.mail '
                . '             FROM {users} u, '
                . '                  {field_data_field_active_status} s'
                . '             WHERE u.uid=s.entity_id AND u.created>=:d1 AND u.created <= :d2 AND s.field_active_status_value=:status'
                . '             ORDER BY u.created DESC',
                            array(':d1' => $mon_from, ':d2' => $mon_to, ':status' => $status))->fetchAll();
    }
    else {
        $res = db_query('SELECT u.created, u.uid, u.name, u.mail '
                . '             FROM {users} u'
                . '             WHERE u.created>=:d1 AND u.created <= :d2'
                . '             ORDER BY u.created DESC',
                            array(':d1' => $mon_from, ':d2' => $mon_to))->fetchAll();        
    }
    
    $rows = array();
    foreach ($res as $r) {
        $rows[$r->uid] = array( date('d.m.Y', $r->created),
                        '<a href="/user/' .$r->uid .'/campaigns" target="_blank">' .$r->name .'</a>',
                        '<a href="mailto:' .$r->mail .'">' .$r->mail .'</a>',
                        '',
                        0,
                        0,
                        0,
                        '<a href="/user/' .$r->uid .'/edit">Edit</a>',
                        '<a href="/user/' .$r->uid .'/track">Track</a>',
            );
    }
    
    if (sizeof($rows)) {
        $res = db_select('field_data_field_referred_from', 'f')
                    ->fields('f', array('entity_id', 'field_referred_from_value'))
                    ->condition('entity_id', array_keys($rows), 'IN')
                    ->execute()->fetchAll();

        foreach ($res as $r) {
            $rows[$r->entity_id][3] = $r->field_referred_from_value;
        }        
        
        $res = db_query('SELECT count(cid) as cnt, uid FROM {domain_compaigns} '
                . '             WHERE uid IN (' .implode(',', array_keys($rows)) .') GROUP BY uid')->fetchAll();
        
        foreach ($res as $r) {
            $rows[$r->uid][4] = $r->cnt;
        }        
        
        
        $campaigns_stat = db_query('SELECT sum(total_clicks) as total_clicks,  '
                . '                            sum(adplex_clicks + hosting_clicks+country_clicks+bot_clicks + isp_clicks+referer_clicks+ip_clicks+device_os_clicks+url_clicks+ua_clicks+time_clicks+status_clicks+ headers_clicks+light_clicks+super_clicks) as total_block, '
                . '                            uid '
                . '                             FROM {domain_compaigns_cmp_stat}'
                . '                         WHERE uid IN (' .implode(',', array_keys($rows)) .') GROUP BY uid')->fetchAll();
        
        foreach ($campaigns_stat as $r) {
            $rows[$r->uid][5] = $r->total_clicks;
            $rows[$r->uid][6] = $r->total_block;
        }
        
        $statuses = array(0 => 'Total regestered in month',
                          1 => 'Went users',
                          100 => 'Trial or single period',
                          200 => 'Subscribed'
            );
        
        print '<h2 style="clear:both; width: 100%">' .$statuses[$status] .'</h2>' .theme('table', array('header' => array('Regestered', 'User', 'mail', 'Referred from', 'Campaigns','Total', 'Blocked','', ''), 'rows' => $rows));
    }
    else {
        print 'Nothing found';
    }
    
    
}

// выводит инфо по использованию фильтров и типов кампаний
function lx_admin_analytics_campaigns_page() {
//    $res = db_select('domain_compaigns', 'c')
//                ->fields('c')
//                ->execute();
    $uniq_users = [];
    $total_users = [];
    $res = db_query('SELECT * FROM {domain_compaigns} WHERE uid in (SELECT entity_id FROM {field_data_field_active_till} WHERE field_active_till_value > ' .time() .')');
    
    $stat = array('LP Both Redirect' => 0,
                  'LP Both Show Content' => 0,
                  'LP Mixed' => 0,
                  'Bots' => 0,
                  'Proxy/VPN' => 0,
                  'Magic Filter Light' => 0,
                  'Magic Filter Normal' => 0,
                  'Magic Filter Super' => 0,
                  'Timezone' => 0,
                  'JS Magic Filter' => 0,
                  'Luminati' => 0,
                  'Pass FB Prefetch' => 0,
                  'Location' => 0,
                  'Devices' => 0,
                  'ISP' => 0,
                  'IP' => 0,
                  'User Agent' => 0,
                  'Referrer' => 0,
                  'Blank Referer' => 0,
                  'URL Substrings' => 0,
                  'Time of Day' => 0,
        );
    
    $total = 0;
    
    foreach ($res as $r) {
        $total ++;
        
        $total_users[$r->uid] = $r->uid;
        
        $data = unserialize($r->data);
        
        if ($data['safe_page_show_type'] == 1 && $data['money_page_show_type'] == 1) {
            $stat['LP Both Redirect'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['LP Both Redirect'] = [];
            }
            $uniq_users['LP Both Redirect'][$r->uid] = $r->uid;
        }
        else if ($data['safe_page_show_type'] == 2 && $data['money_page_show_type'] == 2) {
            $stat['LP Both Show Content'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['LP Both Show Content'] = [];
            }
            $uniq_users['LP Both Show Content'][$r->uid] = $r->uid;            
        }
        else {
            $stat['LP Mixed'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['LP Mixed'] = [];
            }
            $uniq_users['LP Mixed'][$r->uid] = $r->uid;             
        }
        
        if ($data['hosting']) {
            $stat['Proxy/VPN'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Proxy/VPN'] = [];
            }
            $uniq_users['Proxy/VPN'][$r->uid] = $r->uid;              
        }
        
        if ($data['bots']) {
            $stat['Bots'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Bots'] = [];
            }
            $uniq_users['Bots'][$r->uid] = $r->uid;                   
        }
        
        if ($data['magic_filter_light']) {
            $stat['Magic Filter Light'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Magic Filter Light'] = [];
            }
            $uniq_users['Magic Filter Light'][$r->uid] = $r->uid;                               
        }
        
        if ($data['magic_filter_normal']) {
            $stat['Magic Filter Normal'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Magic Filter Normal'] = [];
            }
            $uniq_users['Magic Filter Normal'][$r->uid] = $r->uid;             
        }
        
        if ($data['pass_fb_prefetch']) {
            $stat['Pass FB Prefetch'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Pass FB Prefetch'] = [];
            }
            $uniq_users['Pass FB Prefetch'][$r->uid] = $r->uid;             
        }
        
        
        if ($data['magic_filter_super']) {
            $stat['Magic Filter Super'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Magic Filter Super'] = [];
            }
            $uniq_users['Magic Filter Super'][$r->uid] = $r->uid;             
        }        
        
        if ($data['js_timezone']) {
            $stat['Timezone'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Timezone'] = [];
            }
            $uniq_users['Timezone'][$r->uid] = $r->uid;             
        }        
        
        if ($data['js_magic']) {
            $stat['JS Magic Filter'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['JS Magic Filter'] = [];
            }
            $uniq_users['JS Magic Filter'][$r->uid] = $r->uid;             
        }        
        
        if ($data['js_luminati']) {
            $stat['Luminati'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Luminati'] = [];
            }
            $uniq_users['Luminati'][$r->uid] = $r->uid;             
        }        
        
        if ($data['location_status']) {
            $stat['Location'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Location'] = [];
            }
            $uniq_users['Location'][$r->uid] = $r->uid;             
        }
        
        if (sizeof($data['devices_work'])) {
            $stat['Devices'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Devices'] = [];
            }
            $uniq_users['Devices'][$r->uid] = $r->uid;              
        }

        if ($data['isp_list_type']) {
            $stat['ISP'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['ISP'] = [];
            }
            $uniq_users['ISP'][$r->uid] = $r->uid;               
        }        

        if ($data['ip_list_type']) {
            $stat['IP'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['IP'] = [];
            }
            $uniq_users['IP'][$r->uid] = $r->uid;               
        }        

        if ($data['ref_list_type']) {
            $stat['Referrer'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Referrer'] = [];
            }
            $uniq_users['Referrer'][$r->uid] = $r->uid;              
        }        

        if ($data['clk_blank']) {
            $stat['Blank Referer'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Blank Referer'] = [];
            }
            $uniq_users['Blank Referer'][$r->uid] = $r->uid;                  
        }        

        if ($data['urlsbs_list_type']) {
            $stat['URL Substrings'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['URL Substrings'] = [];
            }
            $uniq_users['URL Substrings'][$r->uid] = $r->uid;               
        }        

        if ($data['ua_list_type']) {
            $stat['User Agent'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['User Agent'] = [];
            }
            $uniq_users['User Agent'][$r->uid] = $r->uid;             
        }        

        if ($data['time_type']) {
            $stat['Time of Day'] ++;
            
            if (!isset($uniq_users)) {
                $uniq_users['Time of Day'] = [];
            }
            $uniq_users['Time of Day'][$r->uid] = $r->uid;                         
        }        
    }
    
    $rows = array();
    foreach ($stat as $k => $v) {
        $rows[] = array($k, $v, round($v/$total*100) .'%', sizeof($uniq_users[$k]), round(sizeof($uniq_users[$k])/sizeof($total_users)*100) .'%',);
    }
    
    $output = '<div><strong>Total users:</strong> ' .sizeof($total_users) .'. <strong>Total campaigns:</strong> ' .$total .'</div>';
    $output .= theme('table', array('header' => array('Parameter', 'Campaigns Amount', 'Percent of total', 'Users Amount', 'Percent of total'), 'rows' => $rows));
    
    return $output;
}

// разово заливает кампании пользователей в campaigns_links
function lx_admin_once_import() {
    
    if (isset($_GET['reset'])) {
        variable_set('smart_asses_last_id', 0);
        db_query('TRUNCATE {campaigns_links}');
    }
    
    $from_id = variable_get('smart_asses_last_id', 0);
    
    try {
        $db = new PDO("mysql:host=88.99.166.16;dbname=magicstat_log", 'magicstatlog', 'm22xLYKLe8a8dZHB');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        
        $last_id = $db->query('SELECT MAX(id) as mid FROM ausers_hosts')->fetchObject();
        $last_id = $last_id->mid;

        $result = $db->query('SELECT DISTINCT uid, cid, host, curl_remote_addr FROM ausers_hosts WHERE id>' .$from_id .' AND id <=' .$last_id .' AND uid > 157 AND uid <> 251 ORDER BY id ASC');

        
        
        foreach ($result as $r) {
            $r['host'] = str_replace('www.', '', $r['host']);
            $money_name = explode('.', $r['host']);
            unset($money_name[sizeof($money_name) -1]);        
            $money_name = implode('.', $money_name);
            if (!in_array($money_name, array('google', 'example', 'play.google', 'itunes.apple', 'facebook', 'your-domain', 'youtube', 'yandex', 'com-main', 'baidu', 'vk'))) {
                db_insert('campaigns_links')
                ->fields(array( 'uid' => $r['uid'],
                                  'cid' => $r['cid'],
                                  'domain' => $r['host'],
                                  'name_only' => $money_name,
                                  'server_ip' => $r['curl_remote_addr']
                                ))->execute();   
            }    
        }     
        
        $db = null;
        
        variable_set('smart_asses_last_id', $last_id);
        
    } catch (PDOException $e) {
        print_r($e);
        die();
    }            
    
}

// дубликаты регистраций
function lx_admin_analytics_smart_asses() {
    
    if (isset($_GET['update'])) {
//        db_query('TRUNCATE {campaigns_links}');
        lx_admin_once_import();
        drupal_set_message('Data is updated.');
        drupal_goto('/admin/lxadmin/analytics/smart-asses');
    }    
    
    drupal_add_js(drupal_get_path('module', 'lx_domains_cmp').'/js/tip_window.js');
    
    $group_field = 'name_only';
    if (isset($_GET['gb'])) {
        $group_field = check_plain($_GET['gb']);
    }
    
    $res = null;
    
    if ($group_field == 'userip') {
        $res = db_query('SELECT count(DISTINCT uid) as cnt, ip as gf FROM {magic_log} WHERE uid NOT IN (0,1,6, 9, 27, 153, 154) AND ip<>:empty AND ip NOT IN (:ip2) GROUP by ip order by cnt desc LIMIT 0, 50', array(':empty' => '', ':ip2' => array('162.221.61.196', '64.128.115.196', '5.45.69.170')))->fetchAll();
    }
    else {
        $res = db_query('SELECT count(DISTINCT uid) as cnt, ' .$group_field .' as gf FROM {campaigns_links} WHERE uid NOT IN (0, 1,6, 9, 27, 153, 154) AND ' .$group_field .'<>:empty GROUP by ' .$group_field .' order by cnt desc LIMIT 0, 50', array(':empty' => ''))->fetchAll();
    }

    $rows = array();
    foreach ($res as $r) {
        if ($r->cnt > 1) {
            
            try {
                if ($group_field == 'userip') {
                    $res2 = db_query('SELECT u.uid, u.name, u.mail FROM {users} u, {magic_log} m'
                            . '                WHERE u.uid NOT IN (0,1,6,9,27, 153, 154) AND u.uid=m.uid AND m.ip=:ip ORDER BY u.uid DESC', array(':ip' => $r->gf))->fetchAll();
                }
                else {
                    $res2 = db_query('SELECT u.uid, u.name, u.mail, c.name as cmp_name, c.data '
                            . '                 FROM {users} u, '
                            . '                      {domain_compaigns} c,'
                            . '                      {campaigns_links} l'
                            . '          WHERE u.uid=c.uid AND'
                            . '                l.cid=c.cid AND'
                            . '                l.' .$group_field .'=:name AND u.uid NOT IN (0,1,6,9,27, 153, 154) ORDER BY u.uid DESC', array(':name' => $r->gf))->fetchAll();
                }    
            }
            catch (Exception $e) {
                print_r($e); die();
            }

            $nested = array();

            foreach ($res2 as $r2) {
                if (!isset($nested[$r2->uid])) {
                    $nested[$r2->uid] = array('name' => '<a target="_blank" href="/user/' .$r2->uid .'/edit">'.$r2->name .'</a>',
                                               'links' => '',
                                               '<a target="_blank" href="/user/' .$r2->uid .'/cmapaigns">Campaigns</a>');
                    $_d = unserialize($r2->data);

                    if ($group_field != 'userip') {
                        $nested[$r2->uid]['links'] = '<a target="_blank" href="/user/' .$r2->uid .'/cmapaigns/' .$r2->cid .'/edit"><span class="long-trimmed">' .$_d['money_page'] .'</span></a>';
                    }    
                }
            }

            if (sizeof($nested) > 1) {
                $rows[] = array($r->cnt, '<a href="#">' .$r->gf .'</a>', theme('table', array('rows' => $nested)));
            }    
        }    
    }

    return '<style>ul.links {margin-bottom: 20px; padding: 0; margin-left: 0} ul.links li {float: left; margin-right: 20px;}</style><ul class="links">'
    . '        <li><a style="' .($group_field == 'name_only' ? 'font-weight: bold' : '') .'" href="/admin/lxadmin/analytics/smart-asses?gb=name_only">Group By Domain Name</a></li>'
    . '        <li><a style="' .($group_field == 'domain' ? 'font-weight: bold' : '') .'" href="/admin/lxadmin/analytics/smart-asses?gb=domain">Group By Domain</a></li>'
    . '        <li><a style="' .($group_field == 'server_ip' ? 'font-weight: bold' : '') .'" href="/admin/lxadmin/analytics/smart-asses?gb=server_ip">Group By Server IP</a></li>'
    . '        <li><a style="' .($group_field == 'userip' ? 'font-weight: bold' : '') .'" href="/admin/lxadmin/analytics/smart-asses?gb=userip">Group By User IP</a></li>'
            . '</ul>'.theme('table', array('header' => array('User count', 'Group By Value', 'Users & Campaigns'), 'rows' => $rows));
}


function lx_admin_analytics_payments_stat($type = 'all') {
    $output .= '<fieldset style="display:inline-block; width: 300px; margin-right: 40px;  vertical-align: top"><legend>Payments By Month</legend>';
    $output .= '<div class="fieldset-wrapper">';

    
//    $block = block_load('views', 'payments_by_month-block_1');
//    $blocks = _block_render_blocks(array($block));
//    $blocks_build = _block_get_renderable_array($blocks);
//    unset($blocks_build[$module_name . '_' . $block_delta]['#theme_wrappers']);
//    $mon_block = drupal_render($blocks_build);    
//    
//    preg_match_all('/>([^<]+)<\/a>\s+\(([^\)]+)\)\s+</', $mon_block, $m);
    
    $res = db_query('SELECT COUNT(n.nid) as cnt, SUM(p.field_payment_amount_value) as sm, DATE_FORMAT(FROM_UNIXTIME(n.created), \'%Y-%m\') as dt'
            . '             FROM {node} n, {field_data_field_payment_amount} p '
            . '             WHERE n.type=:type AND n.status=:status AND n.nid=p.entity_id AND n.created >= :created GROUP BY dt ORDER BY dt DESC', [':type' => 'payment', ':status' => 1, ':created' => strtotime("first day of -13 month")])->fetchAll();
    
    
    $output .= '<table>';
    
    foreach ($res as $k => $r) {
        $output .= '<tr class="' .($k%2 == 0 ? 'odd' : 'even') .'"><td>' .$r->dt .'</td><td>' .$r->cnt .'</td><td>$' .number_format($r->sm, 0, '.', ' ') .'</td></tr>';
    }
    
    $output .= '</table>';
    
    $output .= '</div></fieldset>';
    
    
    $output .= '<fieldset style="display:inline-block; width: 300px; margin-right: 40px;  vertical-align: top"><legend>Recent Payments</legend>';
    $output .= '<div class="fieldset-wrapper">';

    
//    $block = block_load('views', 'payments_by_month-block_2');
//    $blocks = _block_render_blocks(array($block));
//    $blocks_build = _block_get_renderable_array($blocks);
//    unset($blocks_build[$module_name . '_' . $block_delta]['#theme_wrappers']);
//    $mon_block = drupal_render($blocks_build);    
//    
//    preg_match_all('/>([^<]+)<\/a>\s+\(([^\)]+)\)\s+</', $mon_block, $m);
    
    $res = db_query('SELECT COUNT(n.nid) as cnt, SUM(p.field_payment_amount_value) as sm, DATE_FORMAT(FROM_UNIXTIME(n.created), \'%Y-%m-%d\') as dt'
            . '             FROM {node} n, {field_data_field_payment_amount} p '
            . '             WHERE n.type=:type AND n.status=:status AND n.nid=p.entity_id AND n.created >= :created GROUP BY dt ORDER BY dt DESC', [':type' => 'payment', ':status' => 1, ':created' => strtotime("-13 day")])->fetchAll();
    
    
    $output .= '<table>';
    
    foreach ($res as $k => $r) {
        $output .= '<tr class="' .($k%2 == 0 ? 'odd' : 'even') .'"><td>' .$r->dt .'</td><td>' .$r->cnt .'</td><td>$' .number_format($r->sm, 0, '.', ' ') .'</td></tr>';
    }    
    
//    foreach ($m[1] as $k => $v) {
//        $output .= '<tr class="' .($k%2 == 0 ? 'odd' : 'even') .'"><td>' .$v .'</td><td>' .$m[2][$k] .'</td></tr>';
//    }
    
    $output .= '</table>';

    
    $output .= '</div></fieldset>';    
    
    return $output;
}