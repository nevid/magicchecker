<?php


function lx_admin_update_users_ips() {
    try {
        $last_id = variable_get('last_magic_log_id', 1144000);
        
        $new_last_id = db_query('SELECT MAX(id) FROM {magic_log}')->fetchField();
        
        db_query('INSERT IGNORE INTO {users_ips} (uid, ip, type) SELECT DISTINCT uid, ip, 0 from nfrd_magic_log WHERE type<>:type AND id > :id', array(':type' => 10, ':id' => $last_id));
        db_query('INSERT IGNORE INTO {users_ips} (uid, ip, type) SELECT uid, ip, 1 FROM nfrd_magic_log WHERE type=:type AND ip NOT LIKE :stripeip AND ip NOT IN (:checkout) AND id > :id', array(':type' => 10, ':stripeip' => db_like('54.187.') .'%', ':checkout' => array('64.128.115.196','162.221.61.196'),':id' => $last_id));
        
        variable_set('last_magic_log_id', $new_last_id);
        
        
        // получаем хосты пользователей
        $data = file_get_contents('http://88.99.166.16/get_users_hosts_cdvr2sSee2r8.php');
        if (trim($data) && (strpos($data, 'none_new') === false) && (strpos($data, 'error') === false)) {
            $data = explode("\n", $data);
            if (sizeof($data)) {
                foreach ($data as $r) {
                    $r = trim($r);
                    if ($r) {
                        $r = explode(';', $r);
                        if (sizeof($r) == 2 && preg_match('/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/', $r[1])) {
                            db_query('INSERT IGNORE INTO {users_ips} (uid, ip, type) VALUES (:uid, :ip, :type)', array(':uid' => $r[0], ':ip' => $r[1], ':type' => 111));
                        }
                    }
                }
            }
        }        
        
        
        $res = db_select('users_ips', 'u')
                ->fields('u', array('id', 'ip'))
                ->condition('country', NULL, 'IS NULL')
                ->range(0, 100)
                ->execute()->fetchAll();
        
//        print_r($res);
        
        $k = 0;
        while (sizeof($res)) {
            $k ++;
            
            foreach ($res as $r) {
                $ip_info = file_get_contents('http://api199.magicchecker.com/v2.1/get_ip_info_sf3azD32dv52dfgDFe.php?ip=' .$r->ip);
                if (trim($ip_info)) {
                    $ip_info = (array)json_decode($ip_info);
//                    print_r($ip_info);
                    
                    if (isset($ip_info['status']) && ($ip_info['status'] == 'ok')) {
                        
                        $fields = [
                            'country' => $ip_info['country'] ? $ip_info['country'] : 'N_',
                            'isp' => ($ip_info['isp']->isp == $ip_info['isp']->org) ? $ip_info['isp']->isp : $ip_info['isp']->isp .'/' .$ip_info['isp']->org,
                            'is_hosting' => ($ip_info['hosting'] || $ip_info['ovpn'] || $ip_info['tor']) ? 1 : 0
                                ];
                        
                        db_update('users_ips')
                            ->fields($fields)
                            ->condition('id', $r->id)
                            ->execute();
                        
                    }
                    else {
                        print $r->ip ."<br />";
                        print_r($ip_info);
                    }
                }
            }    
            
            if ($k == 30) break;
            
            $res = db_select('users_ips', 'u')
                    ->fields('u', array('id', 'ip'))
                    ->condition('country', NULL, 'IS NULL')
                    ->range(0, 100)
                    ->execute()->fetchAll();            
            
//            break;
        }
        
    }
    catch (Exception $e) {
        print $e->getMessage();
    }
    
    
//    if (sizeof($res)) {
//        print_r($res);
//    }
    
    print 'ok';
}

function lx_admin_get_ip_info() {
    if (isset($_GET['ip'])) {
        $ip = trim($_GET['ip']);
        $ip_info = file_get_contents('http://api199.magicchecker.com/v2.1/get_ip_info_sf3azD32dv52dfgDFe.php?ip=' .$ip.'&with_bots&with_ptr');
        
        $output = '';
        if (trim($ip_info)) {
            $ip_info = (array)json_decode($ip_info);
            if (isset($ip_info['status']) && ($ip_info['status'] == 'ok')) {
                $output = print_r($ip_info, true);
            }
            else {
                $output = $r->ip ."<br />";
                $output .= print_r($ip_info, true);
            }
        }      
        else {
            $output = 'No responce.';
        }
        
        
        
        $form['ip_info'] = array('#type' => 'markup',
            '#markup' => '<pre>' .$output .'</pre>'
            );
    }
    
    $form['ip'] = array('#type' => 'textfield',
        '#title' => 'Input IP'
        );
    $form['submit'] = array('#type' => 'submit',
        '#value' => 'Check IP'
        );
    
    $form['#method'] = 'get';
    
    return $form;
}