<?php

/* 
 * Add or remove listeners for worker servers
 * 
 */


function _lx_admin_backup_file($oldname, $newname, $rename = true) {
    if (file_exists($newname)) {
        unlink($newname);   
    }     
 
    if ($rename) {
    rename($oldname, $newname);
}
    else {
        $old_file_contents = file_get_contents($oldname);
        $f = fopen($newname, 'w');
        fputs($f, $old_file_contents);
        fclose($f);
    }
}

function lx_admin_servers_manage($op, $ip) {
//    global $_api_hosts;
    
    if (isset($_GET['ryisUe']) && ($_GET['ryisUe'] == 'IUY798Hdf4999Ds')) {
        
    }
    else {
        die();
    }
    
    $server_type = isset($_GET['type']) ? trim($_GET['type']) : 'workers';
    if (!in_array($server_type, ['workers', 'mediators'])) {
        die('Wrong server type!');
            }
        
            
    require_once(LX_REMOTE_SERVERS_API_HOSTS_PATH);
    $_api_hosts = __get_defined_server_types_ips($server_type);    
    
    if (empty($_api_hosts)) {
        die('__get_defined_server_types_ips(' .$server_type .') returned empty array!');
            }
            
    $ip_exists = in_array($ip, $_api_hosts);
            
    if ($op == 'add') {
        if (!$ip_exists) {
            
            // бэкапим старый файл
            $old_file = LX_REMOTE_SERVERS_IPS_FOLDER .'/' .$server_type .'_ips.txt';
            $new_file = LX_REMOTE_SERVERS_IPS_FOLDER .'/' .$server_type .'_ips-backup.txt';
            _lx_admin_backup_file($old_file, $new_file, false);
            
            $f = fopen($old_file, 'a');
            fputs($f, "\n$ip");
            fclose($f);
//            chmod(LX_ADMIN_WORKERS_IPS_FILE_NAME, 0777);
            
            if ($server_type == 'workers') {
                // "уведомляем" сервер базы о новом ip для листенера (медиатора)
                $mediators = __get_defined_server_types_ips('mediators'); 
                foreach ($mediators as $m_ip) {
                    $res = file_get_contents(str_replace(['!IP!', '!OP!'], [$m_ip, $op], LX_ADMIN_DB_SERVER_URL) .$ip);
                    print "Called mediator: " .str_replace(['!IP!', '!OP!'], [$m_ip, $op], LX_ADMIN_DB_SERVER_URL) .$ip .". Responce: $res<br />";
                }    
            }    
            
            
            $subject = 'New server added!';
            $mess = 'New server ' .$ip .' added as ' .$server_type
                    . '<br /><br />'
                    . 'MagicChecker.com team';

            /* Для отправки HTML-почты вы можете установить шапку Content-type. */
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

            /* дополнительные шапки */
            $headers .= "From: MagicChecker.com <info@magicchecker.com>\r\n";

            mail(LX_ADMIN_NOTIFY_MAIL, $subject, $mess, $headers);                
            
            print $mess;
        }
    }
    else if ($op == 'del') {
        
        if ($ip_exists) {
            print "Ip exists<br >";
            $new_ips = [];
            
            foreach ($_api_hosts as $cur_ip) {
                if ($cur_ip != $ip) {           // удалили искомый
                    $new_ips[] = $cur_ip;
            }
            }            
            
            // бэкапим старый файл
            $old_file = LX_REMOTE_SERVERS_IPS_FOLDER .'/' .$server_type .'_ips.txt';
            $new_file = LX_REMOTE_SERVERS_IPS_FOLDER .'/' .$server_type .'_ips-backup.txt';
            _lx_admin_backup_file($old_file, $new_file);                       
            
            // бэкапим старый файл 
            print "File backuped " .$old_file ." to " .$new_file .".<br >";
            
            $f = fopen($old_file, 'w');
            fputs($f, implode("\n", $new_ips));
            fclose($f);  
            chmod($new_file, 0777);
            
            // "уведомляем" сервер базы о новом ip для листенера (удалить из слушания)
            if ($server_type == 'workers') {
                // "уведомляем" сервер базы о новом ip для листенера (медиатора)
                $mediators = __get_defined_server_types_ips('mediators'); 
                foreach ($mediators as $m_ip) {
                    $res = file_get_contents(str_replace(['!IP!', '!OP!'], [$m_ip, $op], LX_ADMIN_DB_SERVER_URL) .$ip);
                    print "Called mediator: " .str_replace(['!IP!', '!OP!'], [$m_ip, $op], LX_ADMIN_DB_SERVER_URL) .$ip .". Responce: $res<br />";
                    print 'Server deleted from workers on ' .$m_ip .'<br />';
                }    
            }            
            
            print 'Server deleted!';
            
            
            $subject = 'Server deleted!';
            $mess = 'Server ' .$ip .' deleted from ' .$server_type
                    . '<br /><br />'
                    . 'MagicChecker.com team';

            /* Для отправки HTML-почты вы можете установить шапку Content-type. */
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

            /* дополнительные шапки */
            $headers .= "From: MagicChecker.com <info@magicchecker.com>\r\n";

            mail(LX_ADMIN_NOTIFY_MAIL, $subject, $mess, $headers);
            print "<br />mail sent";
            
        }        
    }
    else {
        print "operation $op not found";
}
}

//print 'ok';
