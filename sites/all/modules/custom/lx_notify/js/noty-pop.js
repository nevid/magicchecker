jQuery(document).ready(function () {
    if (jQuery(window).width() > 750) {
        jQuery('.show-noty-popup').click(function (e) {
            e.preventDefault();
            jQuery('body').append('<div class="noty-pop-win"><div class="close-wrapper"><a class="close" href="#">x</a></div><div class="content-data"><p class="noty-load">Loading...</p></div></div>');
            jQuery('.noty-pop-win').addClass('show-full-str');
            jQuery('.noty-pop-win').fadeIn('fast');
            jQuery.get(jQuery(this).attr('href')+'/new', function (data) {
                jQuery('.noty-pop-win .content-data').html(data);
                jQuery('.show-noty-popup sup').remove();
                jQuery('.noty-list li').click(function () {
                    location.href=jQuery('.noty-view-all').attr('href');
                });
            });
        });

        jQuery('body').on('click', '.noty-pop-win .close', function () {
            jQuery('.noty-pop-win').fadeIn('fast');
            jQuery('.noty-pop-win').remove();
        });
    }    
});