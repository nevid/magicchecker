jQuery(document).ready(function () {
    jQuery('.mark-as-read').click(function (e) {
        e.preventDefault();
        jQuery(this).parents('li').removeClass('active').addClass('just-read');
        jQuery(this).hide();        
        jQuery.get(jQuery(this).attr('href'));
    });
    
    jQuery('body').on('mouseleave', '.just-read', function () {
        jQuery(this).removeClass('just-read').addClass('read');
    });
});