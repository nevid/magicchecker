<?php

/* 
 * Уведомления пользователей
 * таблица: user_notifies
 */

function lx_notify_noify_form() {
    $form['whom'] = array('#type' => 'radios',
        '#title' => 'Send to',
        '#options' => array(1 => 'One user', 2 => 'All active users'),
        '#default_value' => 1
        );

    $form['type'] = array('#type' => 'radios',
        '#title' => 'Type',
        '#options' => array(1 => 'Notification', 2 => 'Alert'),
        '#default_value' => 1
        );
    
    $form['send_mail'] = array('#type' => 'checkbox',
        '#title' => 'Send e-mail notification'
        );
   
    $form['username'] = array('#type' => 'textfield',
        '#title' => 'User name',
        '#autocomplete_path' => 'user/autocomplete',
        );
    
    $form['message'] = array('#type' => 'textarea',
        '#title' => 'Message',
        '#rows' => 10,
        '#cols' => 80,
        '#required' => true
        );
    
    $form['submit'] = array('#type' => 'submit',
        '#value' => 'Send message'
        );
    
    return $form;
}

function lx_notify_noify_form_validate($form, &$form_state) {
    if ($form_state['values']['whom'] == 1 && trim($form_state['values']['username']) == '') {
        form_set_error('username', 'Input user name!');
    }
}

function lx_notify_noify_form_submit($form, &$form_state) {
        
    if ($form_state['values']['whom'] == 1) {
        $uid = db_select('users', 'u')->fields('u', array('uid'))
                    ->condition('name', check_plain($form_state['values']['username']))->execute()->fetchField();
        if ($uid) {
            lx_notify_add_notification($uid, $form_state['values']['type'], $form_state['values']['message'], $form_state['values']['send_mail']);
            drupal_set_message('Message sent.');
        }
        else {
            drupal_set_message('Error occured: user not found. Try again.');
        }
    }
    else {
        lx_notify_add_notification('all', $form_state['values']['type'], $form_state['values']['message'], $form_state['values']['send_mail']);
        drupal_set_message('Message sent.');
    }
}

function lx_notify_add_notification($uid, $type, $message, $send_mail = false) {
    $mails = array();
    if ($uid == 'all') {
        $res = db_select('users', 'u');
        $res->fields('u', array('uid', 'mail'));
        $res->join('field_data_field_active_status', 's', 's.entity_id=u.uid');
        $res->condition('u.status', 1);
        $res->condition('s.field_active_status_value', 1, '>');
        $data = $res->execute();
        
        $res = db_insert('user_notifies')
            ->fields(array('type' => $type, 'message' => $message))
            ->execute();        
        
        foreach ($data as $d) {
            db_insert('user_notifies_status')
                ->fields(array('notify_id' => $res, 'uid' => $d->uid))
                ->execute();
            
            if ($send_mail) {
                $mails[] = $d->mail;
            }
        }
    }
    else {
        $res = db_insert('user_notifies')
                ->fields(array('type' => $type, 'message' => $message))
                ->execute();
        
        db_insert('user_notifies_status')
            ->fields(array('notify_id' => $res, 'uid' => $uid))
            ->execute();
        
        if ($send_mail) {
                $mails[] = db_select('users', 'u')
                            ->fields('u', array('mail'))
                            ->condition('uid', $uid)
                            ->execute()->fetchField();
        }
    }
    
    if ($send_mail) {
        foreach ($mails as $mailto) {
            $data = array('mail_type' => ($type == 1) ? 'notification' : 'alert',
                          'message' => $message);
            lx_noty_send_email($mailto, $data);
        }
    }
}

function lx_notify_noify_list() {
    $output = '<ul class="action-links"><li><a href="/admin/lxadmin/notify/add?destination=admin/lxadmin/notify">Add notification</a></li></ul>';
    
    $types = array(1 => 'news', 2 => 'alert');
    
    $res = db_select('user_notifies', 'n')
                ->fields('n')
                ->extend('PagerDefault')
                ->limit(50)
                ->orderBy('id', 'DESC')->execute();
    
    $rows = array();
    foreach ($res as $r) {
        $rows[] = array($r->id, '<nobr>'.$r->date.'</nobr>', $types[$r->type], $r->message, '');
    }
    
    if (sizeof($rows)) {
        foreach ($rows as $k => $r) {
            $total = db_query('SELECT count(uid) FROM {user_notifies_status} WHERE notify_id=:notify_id', array(':notify_id' => $r[0]))->fetchField();
            $read =  db_query('SELECT count(uid) FROM {user_notifies_status} WHERE notify_id=:notify_id AND status=:status', array(':notify_id' => $r[0], ':status' => 2))->fetchField();
            $rows[$k][4] = $read .'/' .$total;
        }
    }
    
    if (sizeof($rows)) {
        $output .= theme('table', array('header' => array('id', 'date', 'type', 'message', 'Read'), 'rows' => $rows)) .theme('pager');
    }    
    
    return $output;
}

