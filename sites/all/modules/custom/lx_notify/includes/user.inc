<?php

function lx_notify_user_list($acc, $type = 'all', $newonly = 0) {
    global $user;
    
    if ($acc->uid != $user->uid && !user_access('aminister users')) {
        drupal_goto('user/' .$user->uid .'/campaigns');
    }
    
    $output = '';
    
//    try {
    
    $res = db_select('user_notifies', 'n');
    $res->join('user_notifies_status', 's', 'n.id=s.notify_id');
    $res->fields('n');
    $res->fields('s', array('status'));
    $res->condition('s.uid', $acc->uid);
    
    if ($newonly) {
        $res = $res->extend('PagerDefault')->limit(3);
    }
    else {
        $res = $res->extend('PagerDefault')->limit(30);
    }
    
    $data = $res->orderBy('n.id', 'DESC')->execute()->fetchAll();
    
//    }
//    catch (Exception $e) {
//        print($e->message); die();
//    }
    
    $rows = array();
    foreach ($data as $r) {
        $rows[] = $r;
    }
    
    if (sizeof($rows)) {
        $output = '<ul class="noty-list">';
        $i = 0;
        foreach ($rows as $r) {
            $output .= '<li class="' .($i%2 == 0 ? 'even' : 'odd') .' ' .($r->status == 1 ? 'active' : 'read') .'">'
                    . '     <div class="noty-status' .($r->type == 1 ? ' noty-info' : ' noty-alert') .'">' .($r->type == 1 ? '<i class="fa fa-info" aria-hidden="true"></i>' : '<i class="fa fa-exclamation" aria-hidden="true"></i>') .'</div>'
                    . '     <div class="message">'
                    //.($r->status == 1 ? '<a class="mark-as-read" href="/user/' .$acc->uid .'/notifications/' .$r->id .'/mark-as-read"><i class="fa fa-check-square-o" aria-hidden="true"></i> mark as read</a>' : '')
                    . '         <div class="date-wrapper"><i class="fa fa-clock-o" aria-hidden="true"></i> <span class="date">' .$r->date .'</span></div>'
                    . '         <div class="mess">' .nl2br($r->message) .'</div>'
                    . '     </div>'
                    . '</li>';
            
            if ($r->status == 1) {
                    db_update('user_notifies_status')
                        ->fields(array('status' => 2))
                        ->condition('notify_id', $r->id)
                        ->condition('uid', $acc->uid)
                        ->execute();
            }
            
            $i ++;
        }
        $output .= '</ul>';
        if (!$newonly) {
            $output .= theme('pager');
        }    
    }    
    else {
        if ($newonly) {
            $output = '<p style="text-align: center">You have no new notifications.</p>';
        }
        else {
            $output = '<p>You have no notifications yet.</p>';
        }    
    }
    
    if ($newonly) {
//        if (sizeof($rows)) {
            $output .= '<div style="height:35px">&nbsp;</div><div class="noty-view-all-wrapper"><a class="noty-view-all" href="/user/' .$user->uid .'/notifications">View All</a></div>';
//        }
        print $output;
        die();
    }
    
    return $output;    
}

function lx_notify_mark_as_read($acc, $notify_id) {
    global $user;
    
    if ($acc->uid == $user->uid || user_access('administer users')) {
        db_update('user_notifies_status')
            ->fields(array('status' => 2))
            ->condition('notify_id', $notify_id)
            ->condition('uid', $acc->uid)
            ->execute();
    }
}