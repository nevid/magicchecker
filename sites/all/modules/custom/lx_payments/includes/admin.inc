<?php

// page for showing views
function lx_payments_log() {
    return '';
}


function lx_payments_options() {
    
    $form['lx_payments_suceess_page_text'] = array('#type' => 'textarea',
        '#title' => 'Success payment page text',
        '#rows' => 10,
        '#description' => 'Aviable variables: !days - amount of days to prolong account, !till - date tarif prolonged till, !uid - user ID',
        '#default_value' => variable_get('lx_payments_suceess_page_text', '')
        );
    $form['lx_payments_failed_page_text'] = array('#type' => 'textarea',
        '#title' => 'Failed payment page text',
        '#rows' => 10,
        '#default_value' => variable_get('lx_payments_failed_page_text', '')
//        '#description' => 'Aviable variables: !days - amount of days to prolong account'
        );
    $form['lx_payments_pay_notify_on_site'] = array('#type' => 'textarea',
        '#title' => 'Payment notification on site',
        '#rows' => 10,
        '#default_value' => variable_get('lx_payments_pay_notify_on_site', ''),
        '#description' => 'Aviable variables: !date - subscription date end with ban'
        );
    $form['notify_payment_mail_subject'] = array('#type' => 'textfield',
        '#title' => 'Payment notification mail subject',
        '#default_value' => variable_get('notify_payment_mail_subject', '')
        );
    $form['notify_payment_mail_text'] = array('#type' => 'textarea',
        '#title' => 'Payment notification mail text',
        '#rows' => 10,
        '#default_value' => variable_get('notify_payment_mail_text', ''),
        '#description' => 'Aviable variables: !date - subscription date end with ban'
        );    
    $form['notify_campaigns_403_mail_subject'] = array('#type' => 'textfield',
        '#title' => 'Account suspended notification mail subject',
        '#default_value' => variable_get('notify_campaigns_403_mail_subject', '')
        );
    $form['notify_campaigns_403_mail_text'] = array('#type' => 'textarea',
        '#title' => 'Account suspended notification mail text',
        '#rows' => 10,
        '#default_value' => variable_get('notify_campaigns_403_mail_text', '')
        );    
    
    $form['lx_payment_ORDER_CREATED_subj'] = array('#type' => 'textfield',
        '#title' => 'New order created subject',
        '#default_value' => variable_get('lx_payment_ORDER_CREATED_subj', '')
        );
    $form['lx_payment_ORDER_CREATED_mess'] = array('#type' => 'textarea',
        '#title' => 'New order created message',
        '#rows' => 10,
        '#default_value' => variable_get('lx_payment_ORDER_CREATED_mess', '')
        );    
    
    $form['lx_payment_RECURRING_INSTALLMENT_SUCCESS_subj'] = array('#type' => 'textfield',
        '#title' => 'Successful recurring payment subject',
        '#default_value' => variable_get('lx_payment_RECURRING_INSTALLMENT_SUCCESS_subj', '')
        );
    $form['lx_payment_RECURRING_INSTALLMENT_SUCCESS_mess'] = array('#type' => 'textarea',
        '#title' => 'Successful recurring payment message',
        '#rows' => 10,
        '#default_value' => variable_get('lx_payment_RECURRING_INSTALLMENT_SUCCESS_mess', '')
        );    
    
    $form['lx_payment_RECURRING_INSTALLMENT_FAILED_subj'] = array('#type' => 'textfield',
        '#title' => 'Failes recurring payment subject',
        '#default_value' => variable_get('lx_payment_RECURRING_INSTALLMENT_FAILED_subj', '')
        );
    $form['lx_payment_RECURRING_INSTALLMENT_FAILED_mess'] = array('#type' => 'textarea',
        '#title' => 'Failed recurring payment message',
        '#rows' => 10,
        '#default_value' => variable_get('lx_payment_RECURRING_INSTALLMENT_FAILED_mess', '')
        );    
    
    $form['lx_payment_FRAUD_STATUS_CHANGED_subj'] = array('#type' => 'textfield',
        '#title' => 'Fraud status changed notification mail subject',
        '#default_value' => variable_get('lx_payment_FRAUD_STATUS_CHANGED_subj', '')
        );
    $form['lx_payment_FRAUD_STATUS_CHANGED_mess'] = array('#type' => 'textarea',
        '#title' => 'Fraud status changed notification mail message',
        '#rows' => 10,
        '#default_value' => variable_get('lx_payment_FRAUD_STATUS_CHANGED_mess', '')
        );    
    
    
    $form['lx_payment_pricing_packs'] = array('#type' => 'textfield',
        '#title' => 'Pricing packs',
        '#default_value' => variable_get('lx_payment_pricing_packs', 'nonepack')
        );    
    
    return system_settings_form($form);
}
