<?php

$tarif = null;

if ($node->field_payment_tarif['und'][0]['nid'] != 210) {
    $tarif = node_load($node->field_payment_tarif['und'][0]['nid']);
}    
else {
    $inv_id = @$node->field_invoice_id['und'][0]['nid'];
    if (!empty($inv_id)) {
        $tarif = node_load($node->field_invoice_id['und'][0]['nid']);
    }
}

$html = '
<html>
<head>
<style>
body {font-family: Tahoma;
	font-size: 10pt;
}
p {	margin: 0pt; }

td { vertical-align: top; }

table thead tr td { 
border-top: 3px solid #efa740;
background: #5974a3;
text-align: center;
}

table thead td { 
        color: #fff;
	text-align: center;
        border-right: 2px solid #fff
}

table.items tr.items-row td {
    background: #f7f8fb;
    border-bottom: 1px solid #e4e8ef;
    font-size: 8pt;
    border-right: 2px solid #fff
}

table.items tr.totals td {
        background: #fafafa;
        border-bottom: 3px solid #e4e8ef;
        border-right: 2px solid #fff	
}


.items tr.totals td {
font-size: 9pt;
    text-align: right;
}

.items td.cost {
	text-align: "." center;
}
</style>
</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
<hr><br><br>
<table width="100%"><tr>
<td width="50%" style="font-weight: normal; font-size: 8pt; line-height: 10pt"><span style="font-weight: bold; font-size: 12pt; line-height: 26pt"><br>BUYER:</span><br><br>' .nl2br($node->field_payment_billing_address['und'][0]['value']) .'<br />' .$acc->mail .'</td>
<td width="50%" style="text-align: right; font-weight: normal; font-size: 8pt; line-height: 10pt"><img width="90px" src="/files/logo-front.png"><br><br>
MTechs LLC<br />
690 S Highway 89, Suite 200<br />
Box 7414<br />
Jackson, WY 83002, USA<br />
info@magicchecker.com
</td>
</tr></table>
</htmlpageheader>
<htmlpagefooter name="myfooter">
<hr>
</htmlpagefooter>
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
<br />
<br />
<table width="100%" style="border: none" cellpadding="0">
<tr><td style="width: 120px; text-align: right; font-weight: bold; font-size: 8pt; line-height: 12pt">INVOICE NUMBER:</td><td style="padding-left: 10px;font-weight: normal; font-size: 8pt; line-height: 12pt">' .$node->field_payment_our_order_number['und'][0]['value'] .'</td></tr>
<tr><td style="width: 120px; text-align: right;font-weight: bold; font-size: 8pt; line-height: 12pt">DATE OF ISSUE:</td><td style="padding-left: 10px;font-weight: normal; font-size: 8pt; line-height: 12pt">' .date('m/d/Y', $node->field_payment_date['und'][0]['value']) .'</td></tr>
<tr><td style="width: 120px; text-align: right;font-weight: bold; font-size: 8pt; line-height: 12pt">DATE OF SALE:</td><td style="padding-left: 10px;ont-weight: normal; font-size: 8pt; line-height: 12pt">' .date('m/d/Y', $node->field_payment_date['und'][0]['value']) .'</td></tr>
<tr><td style="width: 120px; text-align: right;font-weight: bold; font-size: 8pt; line-height: 12pt">PAYMENT DUE DATE:</td><td style="padding-left: 10px;font-weight: normal; font-size: 8pt; line-height: 12pt">' .date('m/d/Y', $node->field_payment_date['und'][0]['value'] + 3*86400) .'</td></tr>
</table>
<br />
<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; border: none" cellpadding="8">
<thead>
<tr>
<td  width="40%">Name of Service</td>
<td  width="20%">Quantity</td>
<td width="20%">Unit Price, USD</td>
<td  width="20%" style="border-right: none">Total Price, USD</td>
</tr>
</thead>
<tbody>
<!-- ITEMS HERE -->
<tr class="items-row">
<td align="left">' .($tarif->type == 'tarif' ? 'MagicChecker Subscription' : $tarif->title) .'</td>
<td align="right">1</td>
<td align="right">$' .$node->field_payment_total['und'][0]['value'] .'</td>
<td align="right" style="border-right: none">$' .$node->field_payment_total['und'][0]['value'] .'</td>
</tr>

<!-- END ITEMS HERE -->
<tr class="totals">
<td colspan="2" style="text-align: left; font-weight: bold">Total:</td>
<td>$' .$node->field_payment_total['und'][0]['value'] .'</td>
<td style="border-right: none">$' .$node->field_payment_total['und'][0]['value'] .'</td>
</tr>
</tbody>
</table>
<br />
<br />
<br />
<br />
<p style="font-weight: bold; font-size: 14pt; text-align: center">THANK YOU VERY MUCH!</p>
</body>
</html>
';
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//define('_MPDF_PATH','../');
require_once drupal_get_path('module', 'lx_payments') . '/includes/vendor/autoload.php';
$mpdf = new mPDF('c','A4','','',20,15,48,25,10,10);

//$mpdf->SetProtection(array('print'));
$mpdf->SetTitle('MagicChecker - Invoice ' .$node->field_payment_our_order_number['und'][0]['value']);
//$mpdf->SetAuthor("Acme Trading Co.");
//$mpdf->SetWatermarkText("Paid");
//$mpdf->showWatermarkText = true;
//$mpdf->watermark_font = 'DejaVuSansCondensed';
//$mpdf->watermarkTextAlpha = 0.1;
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($html);

$filename = 'MagicChecker - Invoice ' .$node->field_payment_our_order_number['und'][0]['value'] .'.pdf';

$mpdf->Output($filename, 'D'); 
exit;