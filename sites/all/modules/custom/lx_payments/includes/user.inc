<?php

function lx_payment_notify_user($mail_type, $data, $acc_id = 0, $payment_type = 'tarif') {
    $acc = null;

    if ($acc_id) {
        $acc = user_load($acc_id);
    }
    else {
        $merchant_order_id = explode('-', $data['vendor_order_id']);  
        $uid = $merchant_order_id[0];
        $acc = user_load($uid);
    }    
    $user_mail = $acc->mail;
    
    if ($user_mail) {
        $subject = variable_get('lx_payment_' .$mail_type .'_subj', '');
        $mess = variable_get('lx_payment_' .$mail_type .'_mess', '');
        $vars = array();
        $vals = array();
        if ($mail_type == 'ORDER_CREATED') {   
            if ($payment_type == 'tarif') {
                $mess = str_replace(array('!uid', '!till'), array($acc->uid, date('d.m.Y', $acc->field_active_till['und'][0]['value'])), $mess);
            }
            else {
                $mess = str_replace('Your account renewed until !till.', '', $mess);
            }
        }
        else if ($mail_type == 'RECURRING_INSTALLMENT_SUCCESS') {
            $mess = str_replace(array('!uid', '!till'), array($acc->uid, date('d.m.Y', $acc->field_active_till['und'][0]['value'])), $mess);
        }
        else if ($mail_type == 'RECURRING_INSTALLMENT_FAILED') {
        }
        else if ($mail_type == 'FRAUD_STATUS_CHANGED') {

        }

        if (sizeof($vals) && sizeof($vars)) {
            $mess = str_replace($vars, $vals, $mess);
        }
        
        $mess = nl2br($mess);

        /* Для отправки HTML-почты вы можете установить шапку Content-type. */
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

        /* дополнительные шапки */
        $headers .= "From: MagicChecker.com <info@magicchecker.com>\r\n";
//        $headers .= "To: " . $user_mail . "\r\n";

    
        mail($user_mail, $subject, $mess, $headers); 
    }    
}

function lx_successfull_payment() {
    
//    $hashSecretWord = 'KJG47343GJ4373T43J23H7'; //2Checkout Secret Word
//    $hashSid = 103122199; //2Checkout account number
//    
//    $product = node_load($_REQUEST['li_0_product_id']);
//    
//    $hashTotal = number_format($product->field_total_for_period['und'][0]['value'], 2, '.', ''); //Sale total to validate against
//    $hashOrder = $_REQUEST['order_number']; //2Checkout Order Number
//    $StringToHash = strtoupper(md5($hashSecretWord . $hashSid . $hashOrder . $hashTotal));
//    
//    $req_data = print_r($_REQUEST, true);
//    if ($StringToHash != $_REQUEST['key']) {
//        lx_magic_log('Payment failed: Hash Mismatch', $req_data, 4, true);
//        
//        drupal_goto('failed-payment');
//    } else {
//      $res = lx_payments_create_payment($_REQUEST);      
//      if ($res) {
          
//          drupal_goto('successful-payment');
//      }   
//      else {
//          lx_magic_log('Payment failed: Order Exists', $req_data, 4, true);
//          drupal_goto('failed-payment');
//      }
//    }
    
    return 'You successfully paid.';
}

// создает инфо об оплате
function lx_payments_create_payment($request_data) {
    
    $order_exists = (int)db_select('field_data_field_payment_system_order_numbe', 'f')
                        ->fields('f', array('entity_id'))
                        ->condition('field_payment_system_order_numbe_value', $request_data['order_number'])
                        ->execute()->fetchField();
//    $order_exists = false;
    
    if (!$order_exists) {
        $node = new stdClass();
        $node->type = 'payment';
        node_object_prepare($node);

        $node->language = LANGUAGE_NONE;
        
        if (isset($request_data['state'])) {
            unset($request_data['state']);
        }    
        if (isset($request_data['zip'])) {
            unset($request_data['zip']);
        }    
        if (isset($request_data['street_address'])) {
            unset($request_data['street_address']);
        }    
        if (isset($request_data['city'])) {
            unset($request_data['city']);
        }    
        if (isset($request_data['country'])) {
            unset($request_data['country']);
        }    
        if (isset($request_data['ip_country'])) {
            unset($request_data['ip_country']);
        }    
        if (isset($request_data['first_name'])) {
            unset($request_data['first_name']);
        }    
        if (isset($request_data['last_name'])) {
            unset($request_data['last_name']);
        }    
        if (isset($request_data['card_holder_name'])) {
            unset($request_data['card_holder_name']);
        }    
        
        $node->body[LANGUAGE_NONE][0]['value'] = print_r($request_data, true);
        $node->status = 1;  // необязательно

        $merchant_order_id = explode('-', $request_data['merchant_order_id']);

        $node->uid = trim($merchant_order_id[0]);
        
        $node->field_payment_date['und'][0]['value'] = time();

        $node->field_payment_amount['und'][0]['value'] = $request_data['total'];
        $node->field_payment_total['und'][0]['value'] = $request_data['total'];
        
        
        $_order_id = $merchant_order_id[2];
        if ($_order_id == 5) {
            $_order_id = 46;
        }
        else if ($_order_id == 9) {
            $_order_id = 49;
        }
        
        $node->field_payment_tarif['und'][0]['nid'] = $_order_id;

        $node->field_payment_our_order_number['und'][0]['value'] = $request_data['merchant_order_id'];
        $node->field_payment_system_order_numbe['und'][0]['value'] = $request_data['order_number'];
        $node->field_payment_system_invoice_id['und'][0]['value'] = $request_data['invoice_id'];


        $name = db_select('users', 'u')
                    ->fields('u', array('name'))
                    ->condition('uid', $merchant_order_id[0])
                    ->execute()->fetchField();

        $tarif = node_load($_order_id);

        $node->title = date('Y-m-d H:i:s') .' - ' .$name .': ' .$tarif->title;
        node_save($node);  
        
        $acc = user_load($merchant_order_id[0]);
        
        
        $cur_val = @(int)$acc->field_active_till['und'][0]['value'];
        
        $date_from = @date('d.m.Y', $acc->field_active_till['und'][0]['value']);
        if ($cur_val) {
            $acc->field_active_till['und'][0]['value'] += 86400*$tarif->field_renewal_frequency_days['und'][0]['value'];
        }
        else {
            $acc->field_active_till['und'][0]['value'] = time() + 86400*$tarif->field_renewal_frequency_days['und'][0]['value'];
        }
        
        $acc->field_active_status['und'][0]['value'] = 100;
        $acc->field_current_tarif_id['und'][0]['nid'] = $_order_id;
        
        user_save($acc);
        
        // обновляем статусы кампаний
        require_once(drupal_get_path('module', 'lx_domains_cmp') .'/includes/rabbit_update_campaign.inc');
        __set_status_all_user_campaigns($acc, 'start_all');        
        
        $_SESSION['prolong_days'] = $tarif->field_renewal_frequency_days['und'][0]['value'];
        
        lx_magic_log('Payment success: $' .$request_data['total'] .'. User subscription prolonged for ' .$tarif->field_renewal_frequency_days['und'][0]['value'] .' days (from ' .$date_from .' till ' .date('d.m.Y', $acc->field_active_till['und'][0]['value']) .')', $req_data, 4, true);
        
        
        return true;
    }
    
    return false;
}

function lx_payment_success() {
    global $user;
    
    $user = user_load($user->uid);
    return '<fieldset class="pad12">' .nl2br(str_replace(array('!till', '!uid'), array(date('d.m.Y', $user->field_active_till['und'][0]['value']), $user->uid), variable_get('lx_payments_suceess_page_text', ''))) .'</fieldset>';
}

function lx_payment_failed() {
    return '<fieldset class="pad12">' .nl2br(variable_get('lx_payments_failed_page_text', '')) .'</fieldset>';
}


function lx_payments_result_payment() {

    $data = print_r($_POST, true);
    $log_file = dirname(__FILE) .'/payments_test.log';
    $f = fopen($log_file, 'a');
    fputs($f, $data);
    fputs($f, "\n\n\n");
    fclose($f);
//    
//    print 'ok'; die();
    
    $insMessage = array();
    foreach ($_POST as $k => $v) {
        $insMessage[$k] = $v;
    }

    # Validate the Hash
    try {
        $hashSecretWord = "KJG47343GJ4373T43J23H7"; # Input your secret word
        $hashSid = 103122199; #Input your seller ID (2Checkout account number)
        $hashOrder = $insMessage['sale_id'];
        $hashInvoice = $insMessage['invoice_id'];
        $StringToHash = strtoupper(md5($hashOrder . $hashSid . $hashInvoice . $hashSecretWord));

        if ($StringToHash != $insMessage['md5_hash']) {
            $insMessage = print_r($insMessage, true);
            $merchant_order_id = explode('-', $request_data['vendor_order_id']);
            lx_magic_log('Payment failed: Hash Mismatch', $insMessage, 4, true, '88.99.56.17', $merchant_order_id[0]);        
        }
        else {

            $func_state = 'lx_payments_result_payment_' .$_POST['message_type'];

            if (function_exists($func_state)) {
                $func_state($insMessage);
            }
            else {
                $insMessage = print_r($insMessage, true);
                $merchant_order_id = explode('-', $request_data['vendor_order_id']);
                lx_magic_log('Payment state function not found!', $insMessage, 4, true, '88.99.56.17', $merchant_order_id[0]);  
            }
        }    
    }
    catch (Exception $e) {
        $err = print_r($insMessage, true).print_r($e, true);
        $merchant_order_id = explode('-', $insMessage['vendor_order_id']);
        lx_magic_log('Payment (Result payment FAILD!): ' .$_POST['message_type'] .' -> ' .$e->getMessage(), $err, 4, true, '88.99.56.17', $merchant_order_id[0]);        
    }
        
}

/// PAYMENT STATE functions

function lx_payments_result_payment_ORDER_CREATED($insMessage) {
    $res = lx_payments_create_payment2($insMessage);     
    if ($res) {}   
    else {
        lx_magic_log('Payment failed: Order ' .$insMessage['sale_id'] .' exists', $insMessage, 4, true);
    }    
}


function lx_payments_result_payment_FRAUD_STATUS_CHANGED($insMessage) {
    
    $res = db_select('field_data_field_payment_system_order_numbe', 'pn');
    $res->join('node', 'n', 'n.nid=pn.entity_id');
    $res->fields('n', array('nid'));
    $res->condition('field_payment_system_order_numbe_value', $insMessage['sale_id']);
    $nid = $res->execute()->fetchField();
    $payment_node = node_load($nid);
    
    lx_magic_log('Payment: FRAUD_STATUS_CHANGED to ' .$insMessage['fraud_status'] .' (Sale id: ' .$insMessage['sale_id'] .'/' .$insMessage['invoice_id'] .').', '', 4, true, '88.99.56.17', $payment_node->uid);    
    
    switch ($insMessage['fraud_status']) {
        case 'pass':
            # Do something when sale passes fraud review.
            break;
        case 'fail':
            if ($payment_node->field_payment_tarif['und'][0]['nid'] != 210) {
                // не прошел реккурентный платеж
                $acc = user_load($payment_node->uid);

                require_once('/home/user173magic/data/www/magicchecker.com/sites/all/modules/custom/lx_payments/2checkout/lib/Twocheckout.php');
                Twocheckout::username(CHECKOUT_API_USERNAME);
                Twocheckout::password(CHECKOUT_API_PASS);

                if ($acc->field_recurrent_sale_id['und'][0]['value']) {
                    $args = array(
                        'sale_id' => $acc->field_recurrent_sale_id['und'][0]['value']
                    );
                    try {
                        $result = Twocheckout_Sale::stop($args);

                        if ($result['response_code'] == 'OK') {

                        }
                        else {
                            $result = print_r($result, true);
                            lx_magic_log('Payment: FRAUD_STATUS_CHANGED FAILED (Sale id: ' .$account->field_recurrent_sale_id['und'][0]['value'] .'). '  .$result, '', 4, true); 
                        }
                    } catch (Twocheckout_Error $e) {
                        lx_magic_log('Payment: FRAUD_STATUS_CHANGED FAILED (Sale id: ' .$account->field_recurrent_sale_id['und'][0]['value'] .'). ' .$e->getMessage(), '', 4, true); 
                    }   
                }      

                $acc->field_active_status['und'][0]['value'] = 100;
                $acc->field_active_till['und'][0]['value'] = time() - 2*86400; // даем 2 дня на оплату
                $acc->field_recurrent_sale_id['und'][0]['value'] = '';
                user_save($acc);
            }
            else {
                // не прошел платеж по счету
                // меняем статус на "новый", чтобы снова можно было его оплатить
                $invoice = node_load($payment_node->field_invoice_id['und'][0]['nid']);
                if (!empty($invoice)) {
                    $invoice->field_invoice_status['und'][0]['value'] = 1;
                    node_save($invoice);
                }
            }
            
            lx_payment_notify_user(FRAUD_STATUS_CHANGED, $insMessage, $payment_node->uid);
            # Do something when sale fails fraud review.
            break;
        case 'wait':     
            break;
    }    
}



function lx_payments_result_payment_SHIP_STATUS_CHANGED($insMessage) {
    $merchant_order_id = explode('-', $insMessage['vendor_order_id']);
    lx_magic_log('Payment: SHIP_STATUS_CHANGED (Sale id: ' .$insMessage['sale_id'] .'/' .$insMessage['invoice_id'] .').', '', 4, true, '88.99.56.17', $merchant_order_id[0]);     
}

function lx_payments_result_payment_INVOICE_STATUS_CHANGED($insMessage) {
    $merchant_order_id = explode('-', $insMessage['vendor_order_id']);
    lx_magic_log('Payment: INVOICE_STATUS_CHANGED (Sale id: ' .$insMessage['sale_id'] .'/' .$insMessage['invoice_id'] .').', '', 4, true, '88.99.56.17', $merchant_order_id[0]);       
}

function lx_payments_result_payment_REFUND_ISSUED($insMessage) {
    $merchant_order_id = explode('-', $insMessage['vendor_order_id']);
    lx_magic_log2('Payment: REFUND_ISSUED (Sale id: ' .$insMessage['sale_id'] .'/' .$insMessage['invoice_id'] .').', 'Refund issued.', '', 4, true, '88.99.56.17', $merchant_order_id[0]);     
    
    require_once(drupal_get_path('module', 'lx_intercom') .'/includes/users.inc');    
    lx_intercom_inc_refund_issued($merchant_order_id[0], $insMessage['invoice_id']);
}

function lx_payments_result_payment_RECURRING_INSTALLMENT_SUCCESS($insMessage) {
    
    $res = lx_payments_create_payment2($insMessage);     
    if ($res) {
        $merchant_order_id = explode('-', $insMessage['vendor_order_id']);
        lx_magic_log('Payment: RECURRING_INSTALLMENT_SUCCESS (Sale id: ' .$insMessage['sale_id'] .'/' .$insMessage['invoice_id'] .').', '', 4, true, '88.99.56.17', $merchant_order_id[0]);         
    }   
    else {
        lx_magic_log('Payment failed: Recurring payment failed! ' .$insMessage['sale_id'] .'/' .$insMessage['invoice_id'] .' exists', $insMessage, 4, true);
    }         
}

function lx_payments_result_payment_RECURRING_INSTALLMENT_FAILED($insMessage) {
    $res = db_select('field_data_field_payment_system_order_numbe', 'pn');
    $res->join('node', 'n', 'n.nid=pn.entity_id');
    $res->fields('n', array('uid'));
    $res->condition('field_payment_system_order_numbe_value', $insMessage['sale_id']);
    $uid = $res->execute()->fetchField();    
    
    
    $acc = user_load($uid);

    require_once('/home/user173magic/data/www/magicchecker.com/sites/all/modules/custom/lx_payments/2checkout/lib/Twocheckout.php');
    Twocheckout::username(CHECKOUT_API_USERNAME);
    Twocheckout::password(CHECKOUT_API_PASS);
    
    $reccurrent_type = 'not active user reccurent';

    if ($acc->field_recurrent_sale_id['und'][0]['value'] == $insMessage['sale_id']) {
        $args = array(
            'sale_id' => $acc->field_recurrent_sale_id['und'][0]['value']
        );
        try {
            $result = Twocheckout_Sale::stop($args);

            if ($result['response_code'] == 'OK') {

            }
            else {
                $result = print_r($result, true);
                lx_magic_log2('Payment: RECURRING_INSTALLMENT_FAILED error (Sale id: ' .$acc->field_recurrent_sale_id['und'][0]['value'] .'). ', 'Payment for next subscription period failed. You will be unsubscribed.', $result, 4, true); 
            }
        } catch (Twocheckout_Error $e) {
            lx_magic_log('Payment: RECURRING_INSTALLMENT_FAILED error (Sale id: ' .$acc->field_recurrent_sale_id['und'][0]['value'] .'). ', $e->getMessage(), 4, true); 
        }   
        $reccurrent_type = 'active user reccurent';
    }      

    
    $merchant_order_id = explode('-', $insMessage['vendor_order_id']);
    lx_magic_log('Payment: RECURRING_INSTALLMENT_FAILED (Sale id: ' .$insMessage['sale_id'] .'/' .$insMessage['invoice_id'] .'). ' .$reccurrent_type, '', 4, true, '88.99.56.17', $merchant_order_id[0]);      
    lx_payment_notify_user(RECURRING_INSTALLMENT_FAILED, $insMessage);
}

function lx_payments_result_payment_RECURRING_STOPPED($insMessage) {
    $res = db_select('field_data_field_payment_system_order_numbe', 'pn');
    $res->join('node', 'n', 'n.nid=pn.entity_id');
    $res->fields('n', array('uid'));
    $res->condition('field_payment_system_order_numbe_value', $insMessage['sale_id']);
    $uid = $res->execute()->fetchField();    
    
    
    $acc = user_load($uid);    
    
    $reccurrent_type = 'not active user reccurent';
    if ($acc->field_recurrent_sale_id['und'][0]['value'] == $insMessage['sale_id']) {
        $acc->field_active_status['und'][0]['value'] = 100;
        $acc->field_recurrent_sale_id['und'][0]['value'] = '';    
        $acc->field_payment_method['und'][0]['value'] = '';
        user_save($acc);        
        $reccurrent_type = 'active user reccurent';
    }    
    
    
    $merchant_order_id = explode('-', $insMessage['vendor_order_id']);
    lx_magic_log2('Payment: RECURRING_STOPPED (Sale id: ' .$insMessage['sale_id'] .'/' .$insMessage['invoice_id'] .'). ' .$reccurrent_type, 'Subscription stopped.', '', 4, true, '88.99.56.17', $merchant_order_id[0]);    
    
    try {
        require_once(drupal_get_path('module', 'lx_intercom') .'/includes/users.inc');
        lx_intercom_inc_user_recc_stop($uid, date('Y-m-d H:i:s'));
    } catch (Exception $ex) {

    }
}

function lx_payments_result_payment_RECURRING_COMPLETE($insMessage) {
    $merchant_order_id = explode('-', $insMessage['vendor_order_id']);
    lx_magic_log('Payment: RECURRING_COMPLETE (Sale id: ' .$insMessage['sale_id'] .'/' .$insMessage['invoice_id'] .').', '', 4, true, '88.99.56.17', $merchant_order_id[0]);
}

function lx_payments_result_payment_RECURRING_RESTARTED($insMessage) {
    $merchant_order_id = explode('-', $insMessage['vendor_order_id']);
    lx_magic_log('Payment: RECURRING_RESTARTED (Sale id: ' .$insMessage['sale_id'] .'/' .$insMessage['invoice_id'] .').', '', 4, true, '88.99.56.17', $merchant_order_id[0]);
}

/////


// создает инфо об оплате (result-url)
function lx_payments_create_payment2($request_data) {
    
    $order_exists = (int)db_select('field_data_field_payment_system_invoice_id', 'f')
                        ->fields('f', array('entity_id'))
                        ->condition('field_payment_system_invoice_id_value', $request_data['invoice_id'])
                        ->execute()->fetchField();
    
    if (!$order_exists) {
        $node = new stdClass();
        $node->type = 'payment';
        node_object_prepare($node);

        $node->language = LANGUAGE_NONE;        
        
        $req_data = '';
        
        $node->body[LANGUAGE_NONE][0]['value'] = print_r($request_data, true);
        $node->status = 1;  

        $merchant_order_id = explode('-', $request_data['vendor_order_id']);

        $node->uid = trim($merchant_order_id[0]);
        
        $node->field_payment_date['und'][0]['value'] = strtotime($request_data['sale_date_placed']);
        $node->field_payment_amount['und'][0]['value'] = $request_data['item_list_amount_1']; //invoice_usd_amount
        $node->field_payment_total['und'][0]['value'] = $request_data['item_list_amount_1'];
        
        $_order_id = $merchant_order_id[2];
        if ($_order_id == 5) {
            $_order_id = 46;
        }
        else if ($_order_id == 9) {
            $_order_id = 49;
        }
        
        $tarif = node_load($_order_id);
        
        $acc = user_load($merchant_order_id[0]);      
        
        $node->title = date('Y-m-d H:i:s') .' - ' .$acc->name .': ' .$tarif->title .'(' .$request_data['sale_id'] .'/' .$request_data['invoice_id'] .')';
        
        $node->field_payment_our_order_number['und'][0]['value'] = $request_data['vendor_order_id'];
        $node->field_payment_system_order_numbe['und'][0]['value'] = $request_data['sale_id'];
        $node->field_payment_system_invoice_id['und'][0]['value'] = $request_data['invoice_id'];   
               
        
        if ($tarif->type == 'tarif') {
            $node->field_payment_tarif['und'][0]['nid'] = $_order_id;
            node_save($node);  

            $acc->field_active_till['und'][0]['value'] = strtotime($request_data['item_rec_date_next_1']);
            $acc->field_active_status['und'][0]['value'] = 200;
            $acc->field_current_tarif_id['und'][0]['nid'] = $_order_id;
            $acc->field_recurrent_sale_id['und'][0]['value'] = $request_data['sale_id'];

            user_save($acc);

            // обновляем статусы кампаний
            require_once(drupal_get_path('module', 'lx_domains_cmp') .'/includes/rabbit_update_campaign.inc');
            __set_status_all_user_campaigns($acc, 'start_all');        
        }
        else {
            $node->field_invoice_id['und'][0]['nid'] = $tarif->nid; // сохраняем привязку оплаты к нашему счету (тип Invoice)
            $node->field_payment_tarif['und'][0]['nid'] = 210; // тариф Invoices (заглушка для оплаты счетов)
            node_save($node);    
            
            // меняем статус счета на оплаченный
            $tarif->field_invoice_status['und'][0]['value'] = 2;
            node_save($tarif);
        }
        
        if ($tarif->type == 'tarif') {
            lx_magic_log2('Payment: ' .$request_data['message_type'] .' (Sale id: ' .$request_data['sale_id'] .'/' .$request_data['invoice_id'] .'). Summa: $' .$request_data['item_list_amount_1'] .'. User subscribed till ' .date('d.m.Y', $acc->field_active_till['und'][0]['value']), 'Payment for $' .$request_data['item_list_amount_1'] .': subscription till ' .date('d.m.Y', $acc->field_active_till['und'][0]['value']), $req_data, 4, true, '88.99.56.17', $acc->uid);
        }
        else {
            lx_magic_log2('Payment (invoice): ' .$request_data['message_type'] .' (Sale id: ' .$request_data['sale_id'] .'/' .$request_data['invoice_id'] .'). Summa: $' .$request_data['item_list_amount_1'] .'.', 'Payment for $' .$request_data['item_list_amount_1'], $req_data, 4, true, '88.99.56.17', $acc->uid);
        }
        
        lx_payment_notify_user($request_data['message_type'], $request_data, 0, $tarif->type);
        
        try {
            // обновляем данные для Intercom
            require_once(drupal_get_path('module', 'lx_intercom') .'/includes/users.inc');
            lx_intercom_inc_user_payment($node);            
        } catch (Exception $ex) {

        }
        
        return true;
    }
    else {
        $merchant_order_id = explode('-', $request_data['vendor_order_id']);
        lx_magic_log('Payment: ' .$request_data['message_type'] .' (Sale id: ' .$request_data['sale_id'] .'/' .$request_data['invoice_id'] .'). Summa: $' .$request_data['item_list_amount_1'] .'. USER PERIOD NOT RENEWED!', $req_data, 4, true, '88.99.56.17', $merchant_order_id[0]);
    }
    
    return false;
}


function lx_payment_unsubscribe() {
    global $user;
    
    if (($user->uid != arg(1)) && !user_access('administer users')) {
        drupal_set_message('Page not found');
        drupal_goto('user/' .$user->uid .'/campaigns');
    }
    
    $acc = user_load(arg(1));
    $pricing_plan = db_select('node', 'n')
                        ->fields('n', array('title'))
                        ->condition('nid', $acc->field_current_tarif_id['und'][0]['nid'])
                        ->execute()->fetchField();
    
    $form['account'] = array('#type' => 'value',
        '#value' => $acc
        );
    
    $form['fs'] = array('#type' => 'fieldset',
        '#title' => 'Do you really want to unsubscribe from "' .$pricing_plan .'" pricing plan?'
        );
    
    $form['submit'] = array('#type' => 'submit',
        '#value' => 'Confirm'
        );
    
    return $form;
}

function lx_payment_unsubscribe_submit(&$form, &$form_state) {
    
    require_once('/home/user173magic/data/www/magicchecker.com/sites/all/modules/custom/lx_payments/2checkout/lib/Twocheckout.php');
    

    Twocheckout::username(CHECKOUT_API_USERNAME);
    Twocheckout::password(CHECKOUT_API_PASS);
    
    $account = $form_state['values']['account'];

    if ($account->field_recurrent_sale_id['und'][0]['value']) {
        $args = array(
            'sale_id' => $account->field_recurrent_sale_id['und'][0]['value']
        );
        try {
            $result = Twocheckout_Sale::stop($args);

            if ($result['response_code'] == 'OK') {
                $account->field_active_status['und'][0]['value'] = 100;
                $account->field_recurrent_sale_id['und'][0]['value'] = '';
                user_save($account);   
                drupal_set_message('You successfully unsubscribed from pricing plan.');
            }
            else {
                $result = print_r($result, true);
                lx_magic_log('Payment: RECURRING_STOPPED FAILED (Sale id: ' .$account->field_recurrent_sale_id['und'][0]['value'] .'). '  .$result, '', 4, true); 
                drupal_set_message('Something went wrong. Write to us and we\'ll solve the problem', 'error');            
            }
        } catch (Twocheckout_Error $e) {
            lx_magic_log('Payment: RECURRING_STOPPED FAILED (Sale id: ' .$account->field_recurrent_sale_id['und'][0]['value'] .'). ' .$e->getMessage(), '', 4, true); 
            drupal_set_message('Something went wrong. Write to us and we\'ll solve the problem', 'error');
        }   
    }
    else {
        drupal_set_message('You have no active subscription now!', 'error');
    }
    drupal_goto('user/' .$account->uid .'/payments');
}