<?php

// https://github.com/mpdf

$html = '
<html>
<head>
<style>
body {font-family: sans-serif;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0.1mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
table thead td { background-color: #EEEEEE;
	text-align: center;
	border: 0.1mm solid #000000;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: right;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
<img src="line.jpg"><br><br>
<table width="100%"><tr>
<td width="50%" style="font-weight: normal; font-size: 7pt; line-height: 10pt"><span style="font-weight: normal; font-size: 12pt; line-height: 26pt"><br>BUYER:</span><br><br>Acme Trading Co.<br />123 Anystreet<br />Your City<br />GD12 4LP</td>
<td width="50%" style="text-align: right; font-weight: normal; font-size: 7pt; line-height: 10pt"><img width="90px" src="logo-front.png"><br><br>
Office 29, Clifton House,<br>
Fitzwilliam Street Lower,<br>
Dublin 2, Republic of Ireland
</td>
</tr></table>
</htmlpageheader>
<htmlpagefooter name="myfooter">
<img src="line-bot.jpg">
</div>
</htmlpagefooter>
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
<table width="100%" style="font-family: serif;" cellpadding="0"><tr>
<td width="45%">
    <span style="font-weight: bold; font-size: 7pt; line-height: 10pt">Invoice #:</span> <span style="font-weight: normal; font-size: 7pt; line-height: 10pt">234683343/34542343</span><br />
<span style="font-weight: bold; font-size: 7pt; line-height: 10pt">Payment date:</span> <span style="font-weight: normal; font-size: 7pt; line-height: 10pt">10.10.2017</span>    
</td>
<td width="10%">&nbsp;</td>
<td width="45%"></td>
</tr></table>
<br />
<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
<thead>
<tr>
<td align="left" width="40%">Name of Service</td>
<td align="right" width="20%">Quantity</td>
<td align="right" width="20%">Unit Price</td>
<td align="right" width="20%">Total Price</td>
</tr>
</thead>
<tbody>
<!-- ITEMS HERE -->
<tr>
<td align="left" style="font-size: 7pt">Subscription by "Month" plan</td>
<td align="right" style="font-size: 7pt">1</td>
<td align="right" style="font-size: 7pt">199 USD</td>
<td align="right" style="font-size: 7pt">199 USD</td>
</tr>

<!-- END ITEMS HERE -->
<tr>
<td class="totals" colspan="2" style="font-size: 7pt">Total:</td>
<td class="totals" style="font-size: 7pt">199 USD</td>
<td class="totals" style="font-size: 7pt">199 USD</td>
</tr>
</tbody>
</table>
</body>
</html>
';
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//define('_MPDF_PATH','../');
require_once dirname(__FILE__) . '/vendor/autoload.php';
$mpdf = new mPDF('c','A4','','',20,15,48,25,10,10);
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("Acme Trading Co. - Invoice");
$mpdf->SetAuthor("Acme Trading Co.");
//$mpdf->SetWatermarkText("Paid");
//$mpdf->showWatermarkText = true;
//$mpdf->watermark_font = 'DejaVuSansCondensed';
//$mpdf->watermarkTextAlpha = 0.1;
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($html);
$mpdf->Output(); exit;
exit;