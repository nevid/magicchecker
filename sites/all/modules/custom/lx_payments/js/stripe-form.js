jQuery(document).ready(function () { 
    var stripe = Stripe(window.stripeKey);  

 var elements = stripe.elements();

  var elementStyles = {
    base: {
        fontSize: '16px',
        fontWeight: '300'
    },
    invalid: {
      color: '#E25950',

      '::placeholder': {
        color: '#FFCCA5',
      },
    },
  };

  var elementClasses = {
    focus: 'focused',
    empty: 'empty',
    invalid: 'invalid',
  };

  var cardNumber = elements.create('cardNumber', {
    iconStyle: "solid",  
    style: elementStyles,
    classes: elementClasses,
    placeholder: 'Card Number'
  });
  cardNumber.mount('#card-number');

  var cardExpiry = elements.create('cardExpiry', {
    style: elementStyles,
    classes: elementClasses,
  });
  cardExpiry.mount('#card-expiry');

  var cardCvc = elements.create('cardCvc', {
    style: elementStyles,
    classes: elementClasses,
  });
  cardCvc.mount('#card-cvc');
  
  var cardElements = [cardNumber, cardExpiry, cardCvc];

  var form = document.getElementById('payment-form');
  

  var savedErrors = {};
  cardElements.forEach(function(element, idx) {
    element.on('change', function(event) {
       var errorMessage = document.getElementById('card-errors');  
      if (event.error) {
//        error.classList.add('visible');
        savedErrors[idx] = event.error.message;
        errorMessage.innerText = event.error.message;
        jQuery('#card-errors').slideDown(200);
      } else {
        savedErrors[idx] = null;

        // Loop over the saved errors and find the first one, if any.
        var nextError = Object.keys(savedErrors)
          .sort()
          .reduce(function(maybeFoundError, key) {
            return maybeFoundError || savedErrors[key];
          }, null);

        if (nextError) {
          // Now that they've fixed the current error, show another one.
          errorMessage.innerText = nextError;
        } else {
          // The user fixed the last error; no more errors.
//          error.classList.remove('visible');
            errorMessage.innerText = '';
            jQuery('#card-errors').slideUp(200);
        }
      }
    });
  });



//    card.addEventListener('change', function(event) {
//      var displayError = document.getElementById('card-errors');
//      if (event.error) {
//        displayError.textContent = event.error.message;
//      } else {
//        displayError.textContent = '';
//      }
//    });
    

    
    form.addEventListener('submit', function(event) {
      event.preventDefault();
      
        jQuery('#edit-submit').attr('disabled', 'disabled');
        
        jQuery('#address-errors').slideUp();
        jQuery('#agree-errors').slideUp();
        jQuery('#card-errors').slideUp();        
        
        user_agreed = jQuery('#edit-agree-to-terms').prop('checked');
      
        if (user_agreed) {
            use_existing = false;
            has_input_errors = false;

            if (!! jQuery('.form-item-choice input[name="choice"]')) {
                if (jQuery('.form-item-choice input[name="choice"]:checked').val() == 1) {
                    use_existing = true;
                }
            }

            if (!use_existing) {
                if (jQuery('#edit-name').val() == '') {
                    jQuery('#edit-name').addClass('error');
                    has_input_errors = true;
                }
                if (jQuery('#edit-city').val() == '') {
                    jQuery('#edit-city').addClass('error');
                    has_input_errors = true;
                }
                if (jQuery('#edit-zip').val() == '') {
                    jQuery('#edit-zip').addClass('error');
                    has_input_errors = true;
                }
                if (jQuery('#edit-address').val() == '') {
                    jQuery('#edit-address').addClass('error');
                    has_input_errors = true;
                }
                if (jQuery('#edit-country').val() == 'US' && jQuery('#edit-state').val() == '') {
                    jQuery('#edit-state').addClass('error');
                    has_input_errors = true;
                }
            }    

            if (!has_input_errors) {
                var ownerInfo = {
                  owner: {
                    name: document.getElementById('edit-name').value,
                    address: {
                      line1: document.getElementById('edit-address').value,
                      city: document.getElementById('edit-city').value,
                      postal_code: document.getElementById('edit-zip').value,
                      country: document.getElementById('edit-country').value,
                      state: document.getElementById('edit-state').value,
                    },
                    email: document.getElementById('edit-mail').value
                  },
                };      

                payment_type = jQuery('#edit-payment-method').val();

                if (payment_type == 'card' && !use_existing) {
                    stripe.createSource(cardElements[0], ownerInfo).then(function(result) {
                      if (result.error) {
                        jQuery('#edit-submit').removeAttr('disabled');  
                        // Inform the user if there was an error
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                        jQuery('#card-errors').slideDown(200);
                        jQuery('#edit-submit').removeAttr('disabled');
                      } else {
                        // Send the source to your server
                        jQuery('.payment-overlay').css('display', 'block');
                        stripeSourceHandler(result.source);
                      }
                    }); 
                }
                else {
                    jQuery('.payment-overlay').css('display', 'block');
                    form.submit();
                }
            }
            else {
                jQuery('#edit-submit').removeAttr('disabled');
                var errorElement = document.getElementById('address-errors');
                errorElement.textContent = 'All fields are required.';
                jQuery('#address-errors').slideDown(200);
            }
        }  
        else {
            jQuery('#edit-submit').removeAttr('disabled');
            var errorElement = document.getElementById('agree-errors');
            errorElement.textContent = 'You must agree to the terms.';
            jQuery('#agree-errors').slideDown(200);
        }        

    });
    
    form
    .querySelector('select[name=country]')
    .addEventListener('change', event => {
      event.preventDefault();
      const country = event.target.value;
      const zipLabel = form.querySelector('input#edit-zip');
      // Only show the state input for the United States.
      form.querySelector('select[name=country]').parentElement.parentElement.classList.toggle('with-state', country === 'US');
      // Update the ZIP label to make it more relevant for each country.
      form.querySelector('input#edit-zip').placeholder =
        country === 'US'
          ? 'ZIP'
          : country === 'UK' ? 'Postcode' : 'Postal Code';
//      event.target.parentElement.className = `field ${country}`;
      showRelevantPaymentMethods(country);
    });    
    
    function showRelevantPaymentMethods(country) {
//        hasAlipay = ['CN', 'HK', 'SG', 'JP'];
        hasAlipay = ['__'];
        if (hasAlipay.indexOf(country) !== -1) {
            jQuery('#payment-methods').addClass('visible');
        }
        else {
            jQuery('#payment-methods').removeClass('visible');
            jQuery('#edit-payment-method').val('card');
            jQuery('#payment-methods li').removeClass('active');            
            jQuery('#payment-methods li:first-child').addClass('active');            
        }
        
        jQuery('.form-item-country .field-prefix img ').attr('src', '/files/flags/' + country + '.png');
    }
    
    function stripeSourceHandler(source) {
      // Insert the source ID into the form so it gets submitted to the server
      var form = document.getElementById('payment-form');
      var hiddenInput = document.createElement('input');
      hiddenInput.setAttribute('type', 'hidden');
      hiddenInput.setAttribute('name', 'stripeSource');
      hiddenInput.setAttribute('value', source.id);
      form.appendChild(hiddenInput);

      // Submit the form
      form.submit();
    }    
    
    jQuery('#payment-methods li label').click(function () {
        type = jQuery(this).data('type');
        jQuery('#edit-payment-method').val(type);
        
        jQuery('#payment-methods li').removeClass('active');
        jQuery(this).parent().addClass('active');
        
        jQuery('.payment-element-wrapper').removeClass('visible');
        jQuery('.' + type +'-element-wrapper').addClass('visible');
        
    });
    
    jQuery('#edit-fs .form-text').keyup(function () {
        var errorElement = document.getElementById('address-errors');
        errorElement.textContent = '';
        jQuery('#address-errors').slideUp(200);        
    });
    
    showRelevantPaymentMethods(jQuery('#edit-country').val());
    
});