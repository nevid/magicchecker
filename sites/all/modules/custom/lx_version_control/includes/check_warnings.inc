<?php


function lx_version_control_page() {
    return 'Warnings admin page';
}

function lx_version_control_settings() {
    $form['lx_vc_index'] = array('#type' => 'textfield',
        '#title' => 'Last index.php version',
        '#default_value' => variable_get('lx_vc_index', '2.2'),
        '#required' => true
        );
    
    $form['lx_vc_proxy'] = array('#type' => 'textfield',
        '#title' => 'Last index.php version',
        '#default_value' => variable_get('lx_vc_proxy', '1.0'),
        '#required' => true
        );    
    
    $form['lx_vc_sig'] = array('#type' => 'textfield',
        '#title' => 'Last Sig version',
        '#default_value' => variable_get('lx_vc_sig', '.'),
        '#required' => true
        );
    
    return system_settings_form($form);
}

function lx_version_check_campaigns_warnings() {    
    
        lx_version_check_campaigns_warnings_from_clickhouse();
    }
    
function lx_version_check_campaigns_warnings_from_clickhouse() {
    
    
    require_once getcwd() .'/sites/all/modules/custom/lx_domains_cmp/includes/user_clickhouse_log.inc';
        
    $cids = array();

    $event_date = date('Y-m-d');
    $date = date('Y-m-d H:i:00', time() - 11*60);
    
    $index_last_version = variable_get('lx_vc_index', '2.2');
    $sig_last_version   = variable_get('lx_vc_sig', '.');
    
    $date = date('Y-m-d H:i:00', time() - 11*60);
    
    
    // выбираем кампании с устаревшим index.php
    $query = "SELECT DISTINCT uid, cid FROM clicklog WHERE id IN (SELECT id FROM clicklog PREWHERE event_date = {event_date} AND click_date> {click_date} WHERE server_vars not like {adapi})";
    $params = ['event_date' => "'".$event_date."'",
               'click_date' => "'".$date."'",
               'adapi' => "'adapi = " .$index_last_version ."%'"];
    
    $result = ch_user_query($query, $params);
    
    if (isset($result['result']) && sizeof($result['result'])) {
        // Parse the result set
        foreach ($result['result'] as $row) {
            $cids[$row['cid']] = array($row['uid'], 10, 0, 0, 0, 0);
        }
        
    }  
    
    // выбираем кампании с рабочим, но старым прокси (тут нужно будет сразу сделать под новую версию)
//    $result = $DBH->query('SELECT DISTINCT uid, cid FROM `day_stat_' .date('Y_m_d') .'` WHERE pof_sig<>"" AND pof_sig NOT LIKE "%' .$sig_last_version .'%" AND pof_sig NOT LIKE "%-1%" AND click_date > "' .$date .'"');    
//    
//    $query = "SELECT DISTINCT uid, cid FROM clicklog WHERE id IN (SELECT id FROM clicklog PREWHERE event_date = {event_date} AND click_date> {click_date} WHERE pof_sig<>{pof_sig1}) AND pof_sig NOT LIKE "%' .$sig_last_version .'%" AND pof_sig NOT LIKE "%-1%"";
//    $params = ['event_date' => "'".$event_date."'",
//               'click_date' => "'".$date."'",
//               'pof_sig1' => "'N_'"];
//    
//    $result = ch_user_query($query, $params);    
//    
//    
//    if ($result !== false) {
//        // Parse the result set
//        foreach ($result as $row) {
//            if (!isset($cids[$row['cid']])) {
//                 $cids[$row['cid']] = array($row['uid'], 0, 10, 0, 0, 0);
//            }
//            else {
//                $cids[$row['cid']][2] = 10;
//            }
//        }
//    }    
    
    // выбираем кампании с cdn в режиме proxy
    $query = "SELECT DISTINCT uid, cid FROM clicklog WHERE id IN (SELECT id FROM clicklog PREWHERE event_date = {event_date} AND click_date> {click_date} WHERE proxy={proxy}) ORDER BY id DESC";
    $params = ['event_date' => "'".$event_date."'",
               'click_date' => "'".$date."'",
               'proxy' => "'cloudflare.com'"];
    
    $result = ch_user_query($query, $params);    
    
    
    if (isset($result['result']) && sizeof($result['result'])) {
        // Parse the result set
        foreach ($result['result'] as $row) {
            if (!isset($cids[$row['cid']])) {
                 $cids[$row['cid']] = array($row['uid'], 0, 0,10,0, 0);
            }
            else {
                $cids[$row['cid']][3] = 10;
            }
        } 
    }       
    
    // у кого выключен прокси
    $proxy_is_off = [];    
    
    // выбираем кампании с выключенным прокси
    $query = "SELECT DISTINCT uid, cid FROM clicklog WHERE id IN (SELECT id FROM clicklog PREWHERE event_date = {event_date} AND click_date> {click_date} WHERE pof_sig={pof_sig}) ORDER BY id DESC";
    $params = ['event_date' => "'".$event_date."'",
               'click_date' => "'".$date."'",
               'pof_sig' => "'N_'"];
    
    $result = ch_user_query($query, $params);    
    
    if (isset($result['result']) && sizeof($result['result'])) {
        foreach ($result['result'] as $row) {
            $proxy_is_off[$row['cid']] = $row['uid'];
        }         
    }
    
    
    
    // выбираем кампании с включенным прокси
    // 1. Определяем, у кого есть данные от прокси фильтра
    $proxy_is_on_but_super_disabled = [];    
    $query = "SELECT DISTINCT uid, cid FROM clicklog WHERE id IN (SELECT id FROM clicklog PREWHERE event_date = {event_date} AND click_date> {click_date} WHERE pof_sig<>{pof_sig}) ORDER BY id DESC";
    $params = ['event_date' => "'".$event_date."'",
               'click_date' => "'".$date."'",
               'pof_sig' => "'N_'",
        ];
    
    $result = ch_user_query($query, $params);    
    
    if (isset($result['result']) && sizeof($result['result'])) {
        // Parse the result set
        foreach ($result['result'] as $row) {
            
            if (isset($proxy_is_off[$row['cid']])) {
                unset($proxy_is_off[$row['cid']]);
            }
            
            $proxy_is_on_but_super_disabled[$row['cid']] = $row['uid'];
        } 
    }      
    
    // 2. Находим кампании с выключенным супер фильтром...
    $res = db_select('domain_compaigns', 'c')
                ->fields('c', ['cid'])
                ->condition('data', '%' .db_like('magic_filter_super') .'%', 'LIKE')
                ->execute()->fetchAll();
    
    foreach ($res as $r) {
        if (isset($proxy_is_on_but_super_disabled[$r->cid])) {
            unset($proxy_is_on_but_super_disabled[$r->cid]);
        }
    }
    
    $res = db_select('domain_compaigns', 'c')
                ->fields('c', ['cid'])
                ->condition('data', '%' .db_like('magic_filter_super') .'%', 'NOT LIKE')
                ->execute()->fetchAll();
    
    foreach ($res as $r) {
        if (isset($proxy_is_off[$r->cid])) {
            unset($proxy_is_off[$r->cid]);
        }
    }    
    
    // добавляем кампании с выключенным супер фильтром
    if (sizeof($proxy_is_off)) {
        // Parse the result set
        foreach ($proxy_is_off as $cid => $uid) {
            
            if (!isset($cids[$cid])) {
                 $cids[$cid] = array($uid, 0, 0,0,10, 0);
            }
            else {
                $cids[$cid][4] = 10;
            }
        } 
    }       
    
    // добавляем кампании с включенным прокси, но отключенным суперфильтром
    if (sizeof($proxy_is_on_but_super_disabled)) {
        // Parse the result set
        foreach ($proxy_is_on_but_super_disabled as $cid => $uid) {
            
            if (!isset($cids[$cid])) {
                 $cids[$cid] = array($uid, 0, 0,0,0, 10);
            }
            else {
                $cids[$cid][5] = 10;
            }
        } 
    }       
     
     
//    print_r($cids); die();
     
    db_query('TRUNCATE {users_index_vc}');

    if (sizeof($cids)) {
        foreach ($cids as $cid => $r) {
            $r = [':cid' => $cid, 
                  ':uid' => $r[0], 
                  ':index_last' => $r[1], 
                  ':proxy_last' => $r[2], 
                  ':is_cdn' => $r[3], 
                 ':no_super_data' => $r[4],
                 ':super_not_enabled' => $r[5] 
                    ];
             try {
                db_query('INSERT INTO {users_index_vc} (cid, uid, index_last, proxy_last, is_cdn, no_super_data, super_not_enabled) VALUES (:cid, :uid, :index_last, :proxy_last, :is_cdn, :no_super_data, :super_not_enabled)', $r);   
             }
             catch (Exception $e) {
                 print_r($r);
                 print $e->getMessage();
                 die();
             }
     
        }
    }
    
    print 'ok';
}