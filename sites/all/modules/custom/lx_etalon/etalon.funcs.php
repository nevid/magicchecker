<?php

// при изменении этой функции нужно ОБЯЗАТЕЛЬНО обновить такую же в server_utility.php боевых серверов

function makeStampFromArray($headers) {
    $allowedHeaders =  array('Accept' => 'A',
                            'Accept-Charset' => 'T',
                            'Accept-Encoding' => 'A',
                            'Accept-Language' => 'T',
                            'Connection' => 'T',
                            'Host' => 'T',
                            'Upgrade-Insecure-Requests' => 'A',
                            'User-Agent' =>	'T',
                            'X-Requested-With' => 'T',
                            'Magic-Header' => 'T'
                            );
    
    
    $stamp = array();

    foreach ($headers as $k => $h) {
        if (isset($allowedHeaders[$k])) {
            if ($allowedHeaders[$k] == 'T') {
                $stamp[] = $k;
            }
            else {
                $stamp[] = $k .'=' .$h;
            }
        }
    }
    
    return implode('|', $stamp);
}