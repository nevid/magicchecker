<?php

define('LX_ETALON_NUM_WHERE', 12);

function lx_etalon_db_query_constructor() {
    require_once(dirname(__FILE__).'/config.php');
    require_once(dirname(__FILE__).'/etalon.funcs.php');
    
    $output = '';
    $rows_count = 0;
    $page = 0;
    $last_cond_num = 0;
    
    try {  
        if (isset($_GET['table']) && $_GET['table'] == 'ausers_hosts') {
            $db = new PDO("mysql:host=" .LX_LOG_HOST .";dbname=" .LX_LOG_DB, LX_LOG_USER, LX_LOG_PASS);  
            $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
        }
        else {
            $db = new PDO("mysql:host=" .LX_ETALON_HOST .";dbname=" .LX_ETALON_DB, LX_ETALON_USER, LX_ETALON_PASS);  
            $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
        }

        $table_fields = array();
        
        $selected_table = '';
        
        if (isset($_GET['table'])) {
            $selected_table = $_GET['table'];
        }
        else {
            $selected_table = 'parsed';
        }
        
        $sth = $db->query('SHOW COLUMNS FROM '.$selected_table);
        $sth->execute();

        while ($var = $sth->fetch(PDO::FETCH_OBJ)) {
            $table_fields[$var->Field] = $var->Field;
        }
        
        $filters = drupal_get_form('lx_etalon_db_query_constructor_form', $table_fields);
        $output = drupal_render($filters);
        
        if (isset($_GET['select_type'])) {
            
            $get_params = $_GET;
            $get_params['select_type'] = 'SELECT ';
            
            $query = $_GET['select_type'];
            if ($query == 'SELECT ' && !sizeof($_GET['fields'])) {
                $query .= ' * ';
            }
            else {
                if (strpos($query, '(') !== false) {
                    $query .= ' , ';
                }                
                
                $query .= ' ' .implode(', ', $_GET['fields']);
            }
            
            $query .= ' FROM ' .$selected_table .' ';
            
            $where = array();
            
            for ($i = 1; $i <= LX_ETALON_NUM_WHERE; $i ++) {
                if (isset($_GET['condition_field_' .$i]) && ($_GET['condition_op_' .$i] != '') && ($_GET['condition_val_' .$i] != '' )) {
                    if (strpos($_GET['condition_val_' .$i], '||') === false) {
                        $where[] = $_GET['condition_field_' .$i] .' ' .$_GET['condition_op_' .$i] .' '  .'"' .$_GET['condition_val_' .$i] .'"';
                    }
                    else {
                        $or = explode('||', $_GET['condition_val_' .$i]);
                        $or_conds = array();
                        foreach ($or as $or_1) {
                            $or_conds[] = '(' .$_GET['condition_field_' .$i] .' ' .$_GET['condition_op_' .$i] .' ' .'"' .trim($or_1) .'"' .')';
                        }
                        
                        $where[] = '(' .implode(' OR ', $or_conds) .')';
                    }
                    $last_cond_num ++;
                }
                
            }  
            
            
            
            // доп условия из списка
//            for ($i = 100; $i <= 120; $i ++) {
//                if (isset($_GET['condition_field_' .$i]) && $_GET['condition_op_' .$i] && $_GET['condition_val_' .$i]) {
//                    if (strpos($_GET['condition_val_' .$i] === false)) {
//                        $where[] = $_GET['condition_field_' .$i] .' ' .$_GET['condition_op_' .$i] .' '  .'"' .$_GET['condition_val_' .$i] .'"';
//                    }
//                    else {
//                        $or = explode('||', $_GET['condition_val_' .$i]);
//                        $or_conds = array();
//                        foreach ($or as $or_1) {
//                            $or_conds[] = '(' .$_GET['condition_field_' .$i] .' ' .$_GET['condition_op_' .$i] .' ' .'"' .trim($or_1) .'"' .')';
//                        }
//                        
//                        $where[] = '(' .implode(' OR ', $or_conds) .')';
//                    }
//                }
//            }             
            
            
            
            if (sizeof($where)) {
                $query .= ' WHERE ';
                $query .= implode(' AND ', $where) .' ';
            }            
            
            
            
            if (!isset($_GET['order_by_field'])) {
                if ($_GET['select_type'] == 'SELECT count(*) as cnt ') {
                    $query .= 'GROUP BY ' .implode(', ', $_GET['fields']) .' ';
                    $query .= 'ORDER BY cnt ' .$_GET['sort_order'] .' ';
                }
                else if ($_GET['select_type'] == 'SELECT SUM(cnt) as cnt ') {
                    $query .= 'GROUP BY ' .implode(', ', $_GET['fields']) .' ';
                    $query .= 'ORDER BY cnt ' .$_GET['sort_order'] .' ';                    
                }
                else {
                    if (sizeof($_GET['fields'])) {
                        $query .= 'ORDER BY ';
                        foreach ($_GET['fields'] as $f) {
                            $query .= $f .' ' .$_GET['sort_order'] .', ';
                        }

                        $query = rtrim(trim($query, ' '), ',');
                    }
                }
            }
            else {
                $query .= 'ORDER BY ' .$_GET['order_by_field'] .' ' .$_GET['sort_order'];
            }
            
            
            if (!isset($_GET['tocsv']) && (strpos($query, 'count(*)') === false) && (strpos($query, 'SUM(') === false)) {
                $count_query = explode('FROM', $query);
                $count_query = 'SELECT count(*) FROM' .$count_query[1];
                
                $sth = $db->query($count_query);
                $sth->execute();
                $rows_count = $sth->fetchColumn();
                
                $page = isset($_GET['page']) ? $_GET['page'] : 0;
                $offset = $page*(int)$_GET['limit'];

                $query .= ' LIMIT ' .$_GET['limit'] .' OFFSET ' .$offset;
            }    
             
//            $query = str_replace('SELECT ,', 'SELECT ', $query);
//            print $query; die();
            
            $sth = $db->query($query);
            $sth->execute();
            
            $rows = array();
            $headers = array();

            $csv = '';
            if (isset($_GET['tocsv'])) {
                header("Content-type: application/x-download");
                header("Content-Disposition: attachment; filename=result.csv");
                $csv = fopen('php://output', 'w');
            }    
            
//            $_data = new DbRowIterator($sth);
//            $data = new LastPeriodIterator($_data);
            
            while ($r = $sth->fetch(PDO::FETCH_OBJ)) {
//            foreach ($data as $r) {
                $r = (array)$r;
                
                if (empty($headers)) {
                    $headers = array_keys($r);
                    fputcsv($csv, $headers);
                }
                
                if (isset($_GET['tocsv'])) {
                    fputcsv($csv, $r);
                }
                else {

                    $all_url = '';

                    if (!isset($_GET['noaddlinks'])) {
                        $cond_ops = array();
                        $last_cond_num ++;
                        foreach ($headers as $h) {
                            if ($h == 'cnt') continue;
                             $header_found = false;
                            for ($i = 1; $i <= LX_ETALON_NUM_WHERE; $i ++) {
                                if (isset($get_params['condition_field_' .$i]) && $get_params['condition_op_' .$i] && $get_params['condition_val_' .$i]) {
                                    if ($get_params['condition_field_' .$i] == $h) {
                                        $get_params['condition_val_' .$i] = $r[$h];
                                        $header_found = true;
                                    }
                                        
                                }   
                            }   
                            
                            if (!$header_found) {
                                $get_params['condition_field_' .$last_cond_num] = $h;
                                $get_params['condition_op_' .$last_cond_num] = '=';
                                $get_params['condition_val_' .$last_cond_num] = $r[$h];
                                $last_cond_num ++;
                            }
                        }    
                        
                        foreach ($get_params as $k => $v) {
                            if (($k != 'fields')) {
                                $all_url .= $k .'=' .urlencode($v) .'&';
                            }
                        }
                    }    

                    foreach ($r as $kk => $rr) {

                        $field_get_params = '';

                        if (!isset($_GET['noaddlinks'])) {
                            if ($kk != 'cnt') {
                                $rr = '<a target="_blank" href="/admin/content/etalon-db-query?' .$all_url .'noaddlinks=1' .'">' .$rr .'</a>';
                            }
                        }    

                        $r[$kk] = '<nobr>' .$rr .'</nobr>';
                    }
                    
                    if (isset($_GET['noaddlinks'])) {
                        $r['etalon_link'] = '<a href="/admin/content/etalon-db-query/' .trim(strip_tags($r['id'])) .'/make-etalon">Make Etalon</a>';
                    }

                    $rows[] = $r;
                }    
            }
            
            if (isset($_GET['tocsv'])) {
                fclose($csv);
                $sth = null;
                $db = null;  
                die();                
            }
            
            $output .= '<div class="restbl" style="clear: both; padding-top: 20px">'; 
            if (isset($_GET['show_query']) && $_GET['show_query']) {
                $output .= '<p>' .$query .'</p>';
            }    
            
            $output .= '<p>Found rows: ' .($rows_count ? $rows_count : sizeof($rows)) .'</p>';
            
            if (sizeof($rows)) {
                $url = explode('&', $_SERVER['REQUEST_URI']);
                foreach ($url as $k => $v) {
                    if (strpos('order_by_field', $v) || strpos('sort_order', $v)) {
                        unset($url[$k]);
                    }
                }
                $url = implode('&', $url);

                if ($_GET['select_type'] == 'SELECT ') {
                    foreach ($headers as $k => $h) {
                        $class_arr = '';
                        $hurl = '';
                        
                        if (isset($_GET['order_by_field']) && ($_GET['order_by_field'] == $h)) {
                            $hurl = $url .'&order_by_field=' .$h .'&sort_order=' .($_GET['sort_order'] == 'ASC' ? 'DESC' : 'ASC');
                            $class_arr = 'col-sortable arr-' .($_GET['sort_order'] == 'ASC' ? 'ASC' : 'DESC');
                        }
                        else {
                            $hurl = $url .'&order_by_field=' .$h .'&sort_order=ASC';
                        }
                        
                        $headers[$k] = '<a class="' .$class_arr .'" href="' .$hurl .'">' .$h .'</a>';
                    }
                }    
                
                if (isset($_GET['noaddlinks'])) {
                    $headers[] = '';
                }
                
                
                $output .= theme('table', array('header' => $headers, 'rows' => $rows)) .($rows_count ? __get_pager($page, $rows_count, $_GET['limit']) : '');
            }
            else {
                $output .= '<p>Nothing found...</p>';
            }
            
            $output .= '</div>';
            
        }
        else {
            $output .= '<div style="clear: both; padding-top: 20px">Nothing selected...</div>';
        }
        
        
        $sth = null;
        $db = null;
    }  
    catch(PDOException $e) {              
        drupal_set_message($e->getMessage(), 'error');
    }   
    
    return $output;
}

function lx_etalon_db_query_constructor_form($form, $form_state, $table_fields) {
    require_once(dirname(__FILE__).'/config.php');
    
    $form['mu'] = array('#type' => 'markup',
        '#markup' => '<style type="text/css" media="all">@import url("/sites/all/modules/custom/lx_etalon/css/style.css");</style>'
        );
    
    $selected_table = '';
    if (isset($_GET['table'])) {
        $selected_table = $_GET['table'];
    }
    else {
        $selected_table = 'parsed';
    }    
    $tables = array('parsed', 'grouped_langs', 'aggregated_data', 'ausers_hosts');
    
//    $db = new PDO("mysql:host=" .LX_ETALON_HOST .";dbname=" .LX_ETALON_DB, LX_ETALON_USER, LX_ETALON_PASS);  
//    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
    
//    $sth = $db->query('SHOW TABLES LIKE "parsed_day_%"');
//    $sth->execute();
//    
//    while ($r = $sth->fetchColumn()) {
//        $tables[] = $r;
//    }
    
//    $sth = null;
//    $db = null;
    
    $form['table'] = array('#type' => 'select',
        '#title' => 'Таблица',
        '#options' => drupal_map_assoc($tables),
        '#default_value' => $selected_table,
        '#suffix' => '<script>jQuery(document).ready(function () {jQuery("#edit-table").change(function () { location.href="/admin/content/etalon-db-query?table=" + jQuery(this).val();}); });</script>'
        );
    
    $operations = array('=' => '=',
                        '<>' => '<>',
                        'LIKE' => 'LIKE',
                        'NOT LIKE' => 'NOT LIKE'
        );
    
    $selects = array('SELECT count(*) as cnt ' => 'Показать количество',
                            'SELECT SUM(cnt) as cnt ' => 'Показать количество',
                            'SELECT ' => 'Показать поля',        
            );
    
    if ($selected_table == 'grouped_langs') {
        unset($selects['SELECT count(*) as cnt ']);
    }
    else {
        unset($selects['SELECT SUM(cnt) as cnt ']);
    }
    
    
    $form['select_type'] = array('#type' => 'select',
        '#title' => 'Вид запроса',
        '#options' => $selects,
        '#default_value' => isset($_GET['select_type']) ? $_GET['select_type'] : 'SELECT count(*) '
        );
    
    $form['sort_order'] = array('#type' => 'select',
        '#title' => 'Порядок сортировки',
        '#options' => array('' => 'Выберите', 'ASC' => 'ASC', 'DESC' => 'DESC'),
        '#default_value' => isset($_GET['sort_order']) ? $_GET['sort_order'] : '',
        );    
    
    $form['limit'] = array('#type' => 'select',
        '#title' => 'Показывать по',
        '#options' => array(100 => 100, 300 => 300, 500 => 500),
        '#default_value' => isset($_GET['limit']) ? $_GET['limit'] : '',
        );    
    
    $form['show_query'] = array('#type' => 'checkbox',
        '#title' => 'Показывать сформированный запрос',
        '#default_value' => isset($_GET['show_query']) ? $_GET['show_query'] : 0,
        );    
    
    $form['submit'] = array('#type' => 'submit',
        '#value' => 'Show',
        '#suffix' => isset($_GET['select_type']) ? ' <a style="margin-left: 20px" class="button" href="' .$_SERVER['REQUEST_URI'] .'&tocsv=1">To CSV</a>' : ''
        );    
    
    $form['fields'] = array('#type' => 'checkboxes',
        '#title' => 'Показать поля',
        '#options' => $table_fields,
        '#default_value' => isset($_GET['fields']) ? $_GET['fields'] : array()
        );
        
    
    $form['conds'] = array('#type' => 'markup',
        '#markup' => '<strong>Условия</strong><br />'
        );
    
    for ($i = 1; $i <= LX_ETALON_NUM_WHERE; $i ++) {
        $form['fs_'.$i] = array('#type' => 'fieldset',
            );
                
                
        $form['fs_'.$i]['condition_field_' .$i] = array('#type' => 'select',
        '#options' => array('' => 'Выберите поле') + $table_fields,
        '#default_value' => isset($_GET['condition_field_' .$i]) ? $_GET['condition_field_' .$i] : '',
        '#prefix' => '<div class="cond-inline">',
        '#suffix' => '</div>'
        );
        
        $form['fs_'.$i]['condition_op_' .$i] = array('#type' => 'select',
        '#options' => array('' => 'Выберите операцию') + $operations,
        '#default_value' => isset($_GET['condition_op_' .$i]) ? $_GET['condition_op_' .$i] : '',
        '#prefix' => '<div class="cond-inline">',
        '#suffix' => '</div>'            
        );
        
        $form['fs_'.$i]['condition_val_' .$i] = array('#type' => 'textfield',
            '#default_value' => isset($_GET['condition_val_' .$i]) ? $_GET['condition_val_' .$i] : '',
            '#attributes' => array('placeholder' => 'Введите значение. Для OR используйте ||'),
            '#prefix' => '<div class="cond-inline clear-right">',
            '#suffix' => '</div>',
            '#maxlength' => 1000
            );
        
    }    
    
    $form['#method'] = 'get';
    
    return $form;
}


// формирует список страниц под таблицей
function __get_pager2($page, $cnt, $limit) {
    $output = '';

    if ($cnt > $limit) {

        $output = '<div class="item-list"><ul class="pager">';

        $url = explode('?', $_SERVER['REQUEST_URI']);
        if (isset($url[1])) {
            $params = explode('&', $url[1]);
            if (isset($_GET['page'])) {
                foreach ($params as $k => $v) {
                    if ($v == 'page=' . $_GET['page']) {
                        unset($params[$k]);
                    }
                }
            }
            $page_url = $url[0] . '?' . implode('&', $params) . '&page=';
        } else {
            $page_url = $url[0] . '?page=';
        }



        if ($page != 0) {
            $output .= '<li><a href="' . $page_url . ($page - 1) . '"><<<</a></li>';
        }

        if (($page + 1) != ceil($cnt / $limit)) {
            $output .= '<li><a href="' . $page_url . ($page + 1) . '">>>></a></li>';
        }

        $output .= '</ul></div>';
    }


    return $output;
}

function lx_etalon_db_query_etalons() {
    require_once(dirname(__FILE__).'/config.php');
    
    $rows = array();
    $output = '';
    $rows_count = 0;
    $page = 0;
    $last_cond_num = 0;
    $limit = 100;
    
    try {  
        $db = new PDO("mysql:host=" .LX_ETALON_HOST .";dbname=" .LX_ETALON_DB, LX_ETALON_USER, LX_ETALON_PASS);  
        $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );     

        $where = array();
        $query = 'SELECT * FROM etalon_stamps';
        if (isset($_GET['version']) || isset($_GET['browser']) || isset($_GET['os']) || isset($_GET['status'])) {
            if (isset($_GET['version']) && ($_GET['version'] != '')) {
                $where[] = 'version="' .(int)$_GET['version'] .'"';
            }
            if (isset($_GET['browser']) && ($_GET['browser'] != '')) {
                $where[] = 'browser="' .check_plain($_GET['browser']) .'"';
            }
            
            if (isset($_GET['os']) && ($_GET['os'] != '')) {
                $where[] = 'os="' .check_plain($_GET['os']) .'"';
            }
            
            if (isset($_GET['browserv_from']) && ($_GET['browserv_from'] != '')) {
                $where[] = 'browserv_from="' .check_plain($_GET['browserv_from']) .'"';
            }
            
            if (isset($_GET['status']) && ($_GET['status'] != '')) {
                $where[] = 'status=' .(int)($_GET['status']);
            }
            
            if (sizeof($where)) {
                $query .= ' WHERE ' .implode(' AND ', $where);
            }
        }        
        
        $count_query = explode('FROM', $query);
        $count_query = 'SELECT count(*) FROM' .$count_query[1];

        $sth = $db->query($count_query);
        $sth->execute();
        $rows_count = $sth->fetchColumn();
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 100;

        $page = isset($_GET['page']) ? $_GET['page'] : 0;
        $offset = $page*$limit;

        if (isset($_GET['order_by_field'])) {
            $query .= ' ORDER BY ' .$_GET['order_by_field'] .' ' .$_GET['sort_order'];

        }
        else {
            $query .= ' ORDER BY id DESC';
        }    
        
        
        $query .= ' LIMIT ' .$limit .' OFFSET ' .$offset;       
        
        $sth = $db->query($query);
        
        
        $statuses = array(1 => 'Exact', 2 => 'Prob', 10 => 'Gen');
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            if (strpos($row['light_v'], '}') !== false) {
                $light = unserialize($row['light_v']);
                
                $light_v = array();
                foreach ($light as $l) {
                    $light_v[] = $l['stamp'];
                    $row['light_v'] = implode('<br />||<br />', $light_v);
                }
            }
            
//            $cur_sts = $row['status'];
//            unset($row['browserv_to']);
            $row['status'] = $statuses[$row['status']];
//            $row['delete'] = '<div class="operations"><span>Operations</span>'
//                    . '       <ul>'
//                    .'          <li><a href="/admin/content/etalon-db-query/etalons/' .$row['id'] .'/status/' .($cur_sts == 1 ? 2 : 1) .'?destination=admin/content/etalon-db-query/etalons">Set ' .($cur_sts == 1 ? 'Prob' : 'Exact') .'</a></li>'
//                    . '         <li><a class="active waves-effect waves-button waves-classic" href="/admin/content/etalon-db-query/etalons/' .$row['id'] .'/delete?destination=admin/content/etalon-db-query/etalons">Delete</a></li>'
//                    . '      </ul>';
            $rows[] = $row;
        }
        
        $sth = null;
        $db = null;       
        
        $headers = array('id' => 'id', 'os' => 'os', 'osv' => 'osv','browser' => 'browser', 'browserv_from' => 'brv_from', 'browserv_to' => 'brv_to', 'stamp' => 'stamp');
        foreach ($headers as $k => $h) {
            $class_arr = '';
            $hurl = '';

            if (isset($_GET['order_by_field']) && ($_GET['order_by_field'] == $k)) {
                $hurl = $url .'&order_by_field=' .$k .'&sort_order=' .($_GET['sort_order'] == 'ASC' ? 'DESC' : 'ASC');
                $class_arr = 'col-sortable arr-' .($_GET['sort_order'] == 'ASC' ? 'ASC' : 'DESC');
            }
            else {
                $hurl = $url .'&order_by_field=' .$k .'&sort_order=ASC';
            }
            
            $hurl .= isset($_GET['version']) ? '&version=' .urlencode($_GET['version']) : '';
            $hurl .= isset($_GET['os']) ? '&os=' .urlencode($_GET['os']) : '';
            $hurl .= isset($_GET['browser']) ? '&browser=' .urlencode($_GET['browser']) : '';
            $hurl .= isset($_GET['browserv_from']) ? '&browserv_from=' .  check_plain($_GET['browserv_from']) : '';
            $hurl .= isset($_GET['status']) ? '&status=' .check_plain($_GET['status']) : '';

            $headers[$k] = '<a class="' .$class_arr .'" href="/admin/content/etalon-db-query/etalons?' .$hurl .'">' .$h .'</a>';
        }   
        
        $headers[] = 'Rules';
        $headers[] = 'Light';
        $headers[] = 'Status';
        $headers[] = 'Version';
        $headers[] = 'Mark';
        $headers[] = 'Row';
//        $headers[] = '';
        
        $form = drupal_get_form('lx_etalon_db_query_etalons_form');
        return '<div class="restbl" style="position: relative">'.drupal_render($form) .'<p style="position: absolute; z-index: 100; top: 30px; right: 15px;">Total: ' .$rows_count .'</p>' .theme('table', array('header' => $headers, 'rows' => $rows))  .($rows_count ? __get_pager2($page, $rows_count, $limit) : '') .'</div>';
    }  
    catch(PDOException $e) {              
        drupal_set_message($e->getMessage(), 'error');
    }          
    
    return 'Not found';
}


function lx_etalon_db_query_etalons_form() {
    $browser_list = array();
    $os_list = array();
    $version_list = array();
    
    $form['mu'] = array('#type' => 'markup',
        '#markup' => '<style type="text/css" media="all">@import url("/sites/all/modules/custom/lx_etalon/css/style.css");</style>'
        );    
    
    try {  
        $db = new PDO("mysql:host=" .LX_ETALON_HOST .";dbname=" .LX_ETALON_DB, LX_ETALON_USER, LX_ETALON_PASS);  
        $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );     

        $sth = $db->query('SELECT DISTINCT(browser) as br FROM etalon_stamps ORDER BY browser ASC');
        $sth->execute();
        while ($b = $sth->fetch(PDO::FETCH_ASSOC)) {
            $browser_list[$b['br']] = $b['br'];
        }

        $sth = $db->query('SELECT DISTINCT(os) as oss FROM etalon_stamps ORDER BY os ASC');
        $sth->execute();
        while ($b = $sth->fetch(PDO::FETCH_ASSOC)) {
            $os_list[$b['oss']] = $b['oss'];
        }
        
        if (isset($_GET['browser']) || isset($_GET['os'])|| isset($_GET['status'])) {
            $where = array();
            $query = 'SELECT DISTINCT(browserv_from) as bv FROM etalon_stamps ';
            
            if (isset($_GET['version']) && ($_GET['version'] != '')) {
                $where[] = 'version=' .(int)$_GET['version'];
            }
            
            if (isset($_GET['browser']) && ($_GET['browser'] != '')) {
                $where[] = 'browser="' .check_plain($_GET['browser']) .'"';
            }
            
            if (isset($_GET['os']) && ($_GET['os'] != '')) {
                $where[] = 'os="' .check_plain($_GET['os']) .'"';
            }
            
            if (isset($_GET['status']) && ($_GET['status'] != '')) {
                $where[] = 'status=' .(int)$_GET['status'];
            }
            
            if (sizeof($where)) {
                $query .= ' WHERE ' .implode(' AND ', $where);
                
                $query .= ' ORDER BY browserv_from ASC';
            }
            else {
                $query = '';
            }
            
            
            if ($query) {
                $sth = $db->query($query);
                $sth->execute();
                while ($b = $sth->fetch(PDO::FETCH_ASSOC)) {
                    $version_list[$b['bv']] = $b['bv'];
                }
            }    
        }
        
        $sth = null;
        $db = null;       
    }  
    catch(PDOException $e) {              
        drupal_set_message($query .'; ' .$e->getMessage(), 'error');
    }       
    
    $form['fs'] = array('#type' => 'fieldset', '#attributes' => array('class' => array('pad22')));
    
    $form['fs']['version'] = array('#type' => 'select',
        '#title' => 'Version',
        '#options' => array('' => 'All', 1 => 1, 2 => 2),
        '#default_value' => isset($_GET['version']) ? $_GET['version'] : ''
        );
    
    $form['fs']['browser'] = array('#type' => 'select',
        '#title' => 'Browser',
        '#options' => array('' => 'All') + $browser_list,
        '#default_value' => isset($_GET['browser']) ? $_GET['browser'] : ''
        );
    
    $form['fs']['os'] = array('#type' => 'select',
        '#title' => 'Os',
        '#options' => array('' => 'All') + $os_list,
        '#default_value' => isset($_GET['os']) ? $_GET['os'] : ''
        );

    if (!sizeof($version_list)) {
//        $form['fs']['browserv_from'] = array('#type' => 'textfield',
//            '#title' => 'Browser version'
//            );
    }
    else {
        $form['fs']['browserv_from'] = array('#type' => 'select',
            '#title' => 'Browser version',
            '#options' => array('' => 'All') + $version_list
            );        
    }
    
    $form['fs']['status'] = array('#type' => 'select',
        '#title' => 'Status',
        '#options' => array('' => 'All', 1 => 'Exact', 2 => 'Probably', 10 => 'Generated')
        );       
    
    if (isset($_GET['browser'])) {
        $form['fs']['browser']['#default_value'] = check_plain($_GET['browser']);
    }
    
    if (isset($_GET['os'])) {
        $form['fs']['os']['#default_value'] = check_plain($_GET['os']);
    }
    
    if (isset($_GET['browserv_from'])) {
        $form['fs']['browserv_from']['#default_value'] = check_plain($_GET['browserv_from']);
    }
    
    
    if (isset($_GET['status'])) {
        $form['fs']['status']['#default_value'] = check_plain($_GET['status']);
    }    
    
    $form['#action'] = '/admin/content/etalon-db-query/etalons';
    $form['#method'] = 'get';
    
    $form['fs']['submit'] = array('#type' => 'submit',
        '#value' => 'Search'
        );
    
    return $form;
}


function lx_etalon_etalon_delete_form($form, &$form_state, $id) {
    
    drupal_set_title('Are you really want to delete etalon <b>' .$id .'</b>?', PASS_THROUGH);
    
    $form['id'] = array('#type' => 'value',
        '#default_value' => $id
        );
    
    
    $form['submit'] = array('#type' => 'submit',
        '#value' => t('Delete')
        );
    
    return $form;    
    
}

function lx_etalon_etalon_delete_form_submit($form, &$form_state) {
    require_once(dirname(__FILE__).'/config.php');
    $db = new PDO("mysql:host=" .LX_ETALON_HOST .";dbname=" .LX_ETALON_DB, LX_ETALON_USER, LX_ETALON_PASS);  
    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );   
    
    $sth = $db->query('DELETE FROM etalon_stamps WHERE id=' .$form_state['values']['id']);
    $sth->execute();

    $sth = null;
    $db = null;     
    
    drupal_set_message('Etalon <b>' .$form_state['values']['id'] .'</b> was magically deleted!');
}

function lx_etalon_set_etalon_status($id, $status) {
    require_once(dirname(__FILE__).'/config.php');
    $db = new PDO("mysql:host=" .LX_ETALON_HOST .";dbname=" .LX_ETALON_DB, LX_ETALON_USER, LX_ETALON_PASS);  
    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );   
    
    $sth = $db->query('UPDATE etalon_stamps SET status=' .(int)$status .' WHERE id=' .(int)$id);
    $sth->execute();

    $sth = null;
    $db = null;     
    
    drupal_set_message('Etalon ' .$id .': status changed!');
    if (isset($_GET['destination'])) {
        drupal_goto($_GET['destination']);
    }
}

// формирует эталон из строки таблицы parsed
function lx_etalon_make_etalon($parsed_id) {
    require_once(dirname(__FILE__).'/config.php');
    $db = new PDO("mysql:host=" .LX_ETALON_HOST .";dbname=" .LX_ETALON_DB, LX_ETALON_USER, LX_ETALON_PASS);  
    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );   
    
    $sth = $db->query('SELECT id, os, browser, browserv, http_headers_asis FROM etalon_stamps WHERE id=' .(int)$parsed_id);
    $sth->execute();
    
    $obj = $sth->fetch(PDO::FETCH_ASSOC);
    
    if (!empty($obj)) {
//        __
    }

    $sth = null;
    $db = null;     
    
    drupal_set_message('Etalon ' .$id .': status changed!');
    if (isset($_GET['destination'])) {
        drupal_goto($_GET['destination']);
    }    
}