<?php

//list of all available batch operations
function lx_ce_batch_operations_get_list() {
    return array(
        1 => array(
            'name' => t('Active'),
            'iclass' => 'fa-play',
            'aclass' => 'is-active',
            'id' => 1
        ),
        2 => array(
            'name' => t('Stop'),
            'iclass' => 'fa-pause',
            'aclass' => 'is-under',
            'id' => 2
        ),
        3 => array(
            'name' => t('Delete'),
            'iclass' => 'fa-trash-o',
            'aclass' => 'edelete',
            'id' => 3
        )
    );
}

function lx_ce_batch_operations_run($user, $batch_id) {
    if (isset($_POST['eids'])) {
        $eids = $_POST['eids'];

        switch ($batch_id) {
            case 1:
                lx_ce_batch_operations_active_process($user, $eids);
                break;

            case 2:
                lx_ce_batch_operations_stop_process($user, $eids);
                break;

            case 3:
                lx_ce_batch_operations_delete_process($user, $eids);
                break;
        }
    }
}

function lx_ce_batch_operations_active_process($user, $eids) {
    $result = 0;
    $events = lx_campaigns_events_get_events($eids);
    if (count($eids) == 1) {
        $event = $events;
        if ($user->uid == $event->uid && $event->event_status != 1) {
            lx_campaigns_events_set_event_status($event->eid, 1);
        }
        $result = 1;
    } else if (count($eids) > 1) {
        foreach($events as $event) {
            if ($user->uid == $event->uid && $event->event_status != 1) {
                lx_campaigns_events_set_event_status($event->eid, 1);

            }
        }
        $result = 1;
    }

    drupal_json_output(array('result' => $result, 'type' => 1, 'status' => 'fa-play'));
}

function lx_ce_batch_operations_stop_process($user, $eids) {
    $result = 0;
    $events = lx_campaigns_events_get_events($eids);
    if (count($eids) == 1) {
        $event = $events;
        if ($user->uid == $event->uid && $event->event_status != 0) {
            lx_campaigns_events_set_event_status($event->eid, 0);
        }
        $result = 1;
    } else if (count($eids) > 1) {
        foreach ($events as $event) {
            if ($user->uid == $event->uid && $event->event_status != 0) {
                lx_campaigns_events_set_event_status($event->eid, 0);
            }
        }
        $result = 1;
    }

    drupal_json_output(array('result' => $result, 'type' => 2, 'status' => 'fa-pause'));
}

function lx_ce_batch_operations_delete_process($user, $eids) {
    foreach($eids as $eid) {
        lx_campaigns_events_delete_rule($eid);
    }
    drupal_json_output(array('result' => 1, 'type' => 3));
}
