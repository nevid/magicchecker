<div class="actions-panel">
    <ul class="action-links">
        <li><a class="waves-effect waves-button waves-float waves-classic" href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i>ADD RULE</a></li>
    </ul>
    <?= $batch_block ?>
    <form action="#" method="get" id="lx-domains-cmp-filter" accept-charset="UTF-8">
        <div>
            <fieldset class="pad12 container-inline form-wrapper form-wrapper" style="position: relative; top: 1px; padding-top: 10px !important" id="edit-fs">
                <div class="fieldset-wrapper">
                    <div class="form-item form-type-textfield form-item-name">
                        <label for="edit-name"><i class="fa fa-filter" aria-hidden="true"></i><span class="rem-ttl">Filters:&nbsp;&nbsp;&nbsp;</span> </label>
                        <input placeholder="Event Name" type="text" id="edit-name" name="name" value="" size="60" maxlength="128" class="form-text">
                    </div>
                    <input class="fa-check form-submit" type="submit" id="edit-submit" name="op" value="Apply filter">
                    <a class="reset-btn reset-afilter" href="#">Reset</a>
                </div>
            </fieldset>
        </div>
    </form>
</div>
