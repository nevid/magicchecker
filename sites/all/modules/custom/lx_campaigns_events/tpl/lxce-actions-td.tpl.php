<a href="/user/<?= $uid ?>/rules/edit/<?= $eid ?>" data-eid = "<?= $eid ?>" class="waves-effect waves-button waves-classic edit-event-btn" oldtitle="Edit">
    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
</a>
<a href="/user/<?= $uid ?>/rules/delete" data-eid = "<?= $eid ?>" class="waves-effect waves-button waves-classic delete-event-btn" oldtitle="Delete">
    <i class="fa fa-trash-o" aria-hidden="true"></i>
</a>