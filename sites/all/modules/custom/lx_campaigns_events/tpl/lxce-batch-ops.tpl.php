<div class="batch-ops-btn">
    <ul class="batch-ops-list">
        <li>
            <div class="li-1"><span class="selected-cmps"><span class="txt">Selected: <span class="seled">1</span> | </span></span><span class="edit-btn"><span class="txt">Edit</span> <i class="fa fa-sort-desc" aria-hidden="true"></i></span></div>
            <ul class="ops-list" style="width: 186px;">
                <?php foreach ($bo_list as $operation): ?>
                    <?php if ($operation['id'] == 3): ?>
                        <li class="no-hover"><a><hr></a></li>
                    <?php endif;?>
                    <li><a class="control-btn <?=$operation['aclass']?>" data-id="<?=$operation['id']?>"><i class="fa <?=$operation['iclass']?>" aria-hidden="true"></i> <?=$operation['name']?></a></li>
                <?php endforeach;?>
            </ul>
        </li>
    </ul>
    <a href="#" title="Clear selection" class="close-select"></a>
</div>

