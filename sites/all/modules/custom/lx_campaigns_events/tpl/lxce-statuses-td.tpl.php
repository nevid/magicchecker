<?php $is_active = ($status == 1 ? ' active' : ''); ?>
<?php $is_not_active = ($status == 0 ? ' active' : ''); ?>
<div class="status-control">
    <a title="Active" class="status-control-btn is-active<?=$is_active?>" href="user/<?=$uid?>/rules/status-save/<?=$eid?>/1">
        <i class="fa fa-play" aria-hidden="true"></i>
    </a>
    <a title="Pause" class="status-control-btn is-under<?=$is_not_active?>" href="user/<?=$uid?>/rules/status-save/<?=$eid?>/0">
        <i class="fa fa-pause" aria-hidden="true"></i>
    </a>
</div>