<div class = "overlay-over-blured-confirm" style="display: none">
    <div class="overlay-over-blured-content-confirm" id = "overlay-over-blured-content-confirm">
        <div class="conf-text confirm-inputs">
            Are you sure you want to delete the selected rules?
        </div>
        <div class="conf-buttons">
            <a class = "cancel-popup-confirm" href = "#">Cancel</a>
            <a href = "" class = "save-confirm">Confirm</a>
        </div>
    </div>
</div>