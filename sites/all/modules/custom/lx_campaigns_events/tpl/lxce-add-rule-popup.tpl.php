<div class="overlay-over-blured-rule" style="display: none">
  <div class="overlay-over-blured-content-rule" id = "overlay-over-blured-content-rule">
    <div class="conf-text rule-inputs">
      <form class = "edit-rule-save-form">
        <span class="close"></span>
        <input style = "display:none;" id="rule-id" name="rule-id" value="" class="form-text" type="text">
        <h1>Event</h1>
        <div class="form-item form-item-name">
          <label for="edit-rule-name" class="rule-id-text">Rule name</label>
          <input placeholder="Rule name" id="edit-rule-name" name="edit-rule-name" value="" size="15" maxlength="128" class="form-text" type="text">
        </div>

        <div class="form-item form-item-campaigns">
          <label for = "edit-apply-to">Apply to</label>
          <select id="edit-apply-to" name="applyto" class="form-select">
            <option value="0" selected="selected">All Active Campaigns</option>
            <option value="1">Selected Campaigns</option>
          </select>
        </div>

        <div class="form-item form-item-campaigns-results">
            <div class="count"><span>X</span> campaigns</div>
            <div class="edit">
                <a href="" class="waves-effect waves-button waves-classic edit-apply-to-btn" oldtitle="Edit">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>
            </div>
        </div>

        <div class = "selected-campaigns" style = "display:none;">
          <select id = "selected-campaigns-select" multiple="multiple" name="selected-campaigns-select[]">
            <?php foreach($options['cids'] as $cid => $cmp_name): ?>
            <option value="<?= $cid ?>"><?= $cmp_name ?></option>
            <?php endforeach;?>
          </select>
        </div>

        <div class="form-item form-item-conditions">
          <label for = "edit-condition">Conditions</label>
          <div class="form-item-data">
            <select id="edit-condition" name="condition" class="form-select">
              <?php foreach($options['conditions'] as $val => $text): ?>
              <?php $selected = ($val == 0) ? ' selected="selected"' : ''; ?>
              <option value="<?= $val ?>"<?= $selected ?>><?= $text ?></option>
              <?php endforeach;?>
            </select>
            <select id="edit-rate" name="rate" class="form-select">
              <?php foreach ($options['rate'] as $rate): ?>
              <?php $selected = ($rate == 10) ? ' selected="selected"' : ''; ?>
              <option value="<?= $rate ?>"<?= $selected ?>><?= $rate ?>%</option>
              <?php endforeach;?>
            </select>
            <span class = "last-word">in the last</span>
            <select id="edit-time" name="time" class="form-select">
              <?php foreach ($options['time'] as $time): ?>
              <?php $selected = ($rate == 5) ? ' selected="selected"' : ''; ?>
              <option value="<?= $time ?>"<?= $selected ?>><?= $time ?> min</option>
              <?php endforeach;?>
            </select>
          </div>
        </div>

        <h2>Actions</h2>
        <div class="form-item form-item-hook">
            <label for = "edit-condition">Call URL</label>
            <input placeholder="Call URL" placeholder="http://example.com/mywebhook.php" id="edit-webhook" name="webhook" value="" size="50" maxlength="256" class="form-text" type="text">
        </div>

        <div class="form-item form-item-status">
          <input type="checkbox" id="edit-status" name="status" value="1" class="form-checkbox">
          <label for = "edit-status">Set campaign’s status to Under Review</label>
        </div>
        <div class="form-item form-item-status">
          <input type="checkbox" id="edit-send-mail" name="send-mail" value="1" class="form-checkbox">
          <label for = "edit-send-mail">Send email</label>
        </div>
        <div class="form-item form-item-pause">
          <input type="checkbox" id="edit-pause-rule" name="pause-rule" value="1" class="form-checkbox">
          <label for = "edit-pause-rule">Pause rule after it triggered</label>
        </div>

      </form>
    </div>
    <div class="conf-buttons">
        <a class = "cancel-popup-rule" href = "#">Cancel</a>
        <a href = "" class = "save-rule">Save rule</a>
		<a href = "#" class = "apply-sel-camp-changes">Done</a>
    </div>
  </div>
</div>