$ = jQuery;
$(document).ready(function () {
    //Add multiselect plugin
    $('select#edit-condition').multi();
    $('select').multi({
        'enable_search': true
    });
	
	$('.edit-rule-save-form span.close').on('click', function(e) {
		if ($('.overlay-over-blured-rule').hasClass('selected-campaigns-window')) {
			$('.overlay-over-blured-rule').removeClass('selected-campaigns-window');
		} else {
			closeAddRulePopup();
		}
		e.preventDefault();
	});
	
	$('.form-item-campaigns-results .edit-apply-to-btn').on('click', function(e) {
		$('#edit-apply-to').val(1);
		$('#edit-apply-to').change();
		$('.overlay-over-blured-rule').addClass('selected-campaigns-window');
		e.preventDefault();
	});
	
	$('a.apply-sel-camp-changes').on('click', function(e) {
		$('.overlay-over-blured-rule').removeClass('selected-campaigns-window');
		e.preventDefault();
	});

    //Table sorting. Add stupid table plugin
    $("#events-table").stupidtable();

    //Click to status row icon
    $('.status-control .status-control-btn').on('click', addStatusClickEvent);
	$('.status-control-btn').removeClass('waves-effect');
	$('.status-control-btn').removeClass('waves-button');
	$('.status-control-btn').removeClass('waves-classic');

    //Click to Add rule button
    $('.action-links .waves-button').on('click', function (e) {
        addRulePopup();
        $('form.edit-rule-save-form h1').text('Add Rule');
        e.preventDefault();
    });

    $('#edit-apply-to').on('change', function () {
		if ($(window).width() < 580) {
			if ($(this).val() == 1) {
				$('.form-item-campaigns-results').show();
				$('.overlay-over-blured-rule').addClass('selected-campaigns-window');
			} else {
				$('.form-item-campaigns-results').hide();
				$('.overlay-over-blured-rule').removeClass('selected-campaigns-window');
			}	
		} else {
			if ($(this).val() == 1) {
				$('.selected-campaigns').show();
				$('.overlay-over-blured-rule').addClass('selected-campaigns-window');
			} else {
				$('.selected-campaigns').hide();
			}
		}
		
    });

    $('.save-rule').on('click', function (e) {
        var event_vars = [];
        if (!$('.overlay-over-blured-rule').hasClass('clicked')) {
            $('.overlay-over-blured-rule').addClass('clicked');
            if (ruleFormValidation()) {
                event_vars = $("form.edit-rule-save-form").serializeArray();
                $.ajax({
                    method: "POST",
                    url: window.location.pathname + "/save",
                    data: {
                        form_arr: event_vars
                    }
                }).done(function (msg) {
                    closeAddRulePopup();
                    $('.overlay-over-blured-rule').removeClass('clicked');
                    if (msg.result == 2) {
                        placeEventFirstInTable('edit-event-' + msg.eid, msg.name, msg.countCmps, event_vars);
                    } else {
                        addEventInTable(event_vars, msg.result);
                    }

                });
            } else {
                $('.overlay-over-blured-rule').removeClass('clicked');
            }
        }
        e.preventDefault();
    });

    $('.delete-event-btn').on('click', addDeleteEventClickEvent);
    $('.edit-event-btn').on('click', addEditEventClickEvent);

    //Search form
    var searchByNameField = $('#edit-name');
    searchByNameField.on('change keydown paste input', searchByName);
    $('.reset-afilter').on('click', function(e){
        searchByNameField.val('');
        $(this).hide();
        e.preventDefault();
    });

    //Batch operations

    //check/uncheck checkboxes
    $('#events-table .form-checkbox').on('click', checkboxSelectEventProcess);

    //click on batch operation
    $('.ops-list a.control-btn').on('click', batchProcess);

    //mobile table version
    tableChangeOnMobile();
    $( window ).resize(function() {
        tableChangeOnMobile();
    });
});

function batchProcess(e) {
    var batchId = $(this).attr('data-id');
    var eids = [];

    $('#events-table .form-checkbox:checked').each(function( index ) {
        if ($(this).attr('id') != 'edit-check-all-events') {
            eids.push($(this).attr('id').split('edit-event-')[1]);
        }
    });

    if ($(this).hasClass('edelete')) {
        confirmPopup('batch', batchId, eids);
    } else {
        batchAjax(batchId, eids);
    }

    e.preventDefault();
}

function batchAjax(batchId, eids) {
    $.ajax({
        method: "POST",
        url: window.location.pathname + "/batch/" + batchId,
        data: {
            eids: eids
        }
    }).done(function (msg) {
        if (msg.result == 1) {
            switch (batchId) {

                //status change
                case '1':
                case '2':
                    $.each(eids, function( index, eid ) {
                        var tr = $('input#edit-event-'+eid).parent().parent();
                        var statusBtn = tr.find('.' + msg.status);
                        tr.find('.status-control-btn.active').removeClass('active');
                        statusBtn.parent().addClass('active');
                    });
                    break;

                //delete
                case '3':
                    $.each(eids, function( index, eid ) {
                        var tr = $('input#edit-event-'+eid).parent().parent();
                        tr.remove();
                        rearrangeEvenOddClasses();
                    });
                    break;
            }

            //Снимаем все чекбоксы
            $('#events-table .form-checkbox').each(function( index ) {
                $(this).prop('checked', false);
            });
            $('.actions-panel').removeClass('edit-mode');
        }
    });
}

function checkboxSelectEventProcess(e) {
    var chck = $(this);
    var tbl = $('#events-table');

    /* check/uncheck all checkbox */
    if (chck.attr('id') == 'edit-check-all-events') {
        checkUncheckAllCheckboxes(chck, tbl);
    }

    var checkboxes_count = getCountCheckboxesChecked(tbl);
    if (checkboxes_count > 0) {
        $('.selected-cmps .txt .seled').text(checkboxes_count);
        $('.actions-panel').addClass('edit-mode');
    } else {
        $('.actions-panel').removeClass('edit-mode');
    }

    $('.batch-ops-btn .close-select').on('click', function(e) {
        $('.actions-panel').removeClass('edit-mode');
        tbl.find('.form-checkbox').each(function( index ) {
            $(this).prop('checked', false);
        });
        e.preventDefault();
    });

}

function getCountCheckboxesChecked(tbl) {
    var checked = 0;
    tbl.find('.form-checkbox').each(function( index ) {
        if ($(this).prop('checked') == true && $(this).attr('id') != 'edit-check-all-events') {
            checked += 1;
        }
    });
    return checked;
}

function checkUncheckAllCheckboxes(chck, tbl) {
    if (chck.prop('checked') == true) {
        tbl.find('.form-checkbox').each(function( index ) {
            $(this).prop('checked', true)
        });
    } else {
        tbl.find('.form-checkbox').each(function( index ) {
            $(this).prop('checked', false)
        });
    }
}

function searchByName(e) {
    var mask = $('#edit-name').val();
    if ($(this).val() != '') {
        $('.reset-afilter').show();

        $('#events-table tbody tr').each(function( index ) {
            var name = $(this).find(':nth-child(2)').text();
            if (name.match("^" + mask + "")) {
                $(this).removeClass('afilter-hidden');
            } else {
                $(this).addClass('afilter-hidden');
            }
        });
    } else {
        $('.reset-afilter').hide();
        $('#events-table tbody tr').each(function( index ) {
            $(this).removeClass('afilter-hidden');
        });
    }
}

function addEventInTable(event_vars, eid) {
    var ename = '';
    var ecampaigns = 'All';
    var ecampaignsCount = 0;
    var condition = null;
	var description = '';
	var rate = null;
	var time = null;
    var uid = window.location.pathname.split('/')[2];
    
	for (var i in event_vars) {
        if (event_vars[i].name == 'edit-rule-name') {
            ename = event_vars[i].value;

        }
        if (event_vars[i].name == 'applyto') {
            if (event_vars[i].value == 1) {
                ecampaigns = 'campaigns';
            }
        }
        if (event_vars[i].name == 'condition') {
            condition = event_vars[i].value;
			description = $('#edit-condition option[value="' + condition + '"]').text();
        }
		if (event_vars[i].name == 'rate') {
            rate = event_vars[i].value + '%';
        }
		if (event_vars[i].name == 'time') {
            time = event_vars[i].value + ' min';
        }
        if (event_vars[i].name == 'selected-campaigns-select[]') {
            ecampaignsCount = ecampaignsCount + 1;
        }
    }
    if (ecampaignsCount > 0) {
        ecampaigns = ecampaignsCount + ' ' + ecampaigns;
    }
	description += ' ' + rate + ' in the last ' + time;

    var tr = '' +
        '<tr class="odd new">' +
        '   <td class=""><input type="checkbox" id="edit-event-"' + eid + ' name="event_' + eid + '" value="1" class="form-checkbox"></td>' +
        '   <td class="">' +
        '       <div class = "name">' + ename + '</div>' +
        '       <div class = "appender">' +
        '           <div class="mob-description">Description: <b>' + description + '</b></div>' +
		'			<div class="mob-applied-to">Applied to: <b>' + ecampaigns + '</b></div>' +
		'			<div class = "last-checked">' + 'Last checked: <b>not checked yet</b>' + '</div>' +
        '           <div class = "last-triggered">' + 'Last triggered: <b>not checked yet</b>' + '</div>' +
        '       </div>' +
        '   </td>' +
		'   <td class="td-description-td">' + description + '</td>' + 
        '   <td class="td-status">' +
        '      <div class="status-control">' +
        '         <a title="Active" class="status-control-btn is-active active" href="user/9/rules/status-save/' + eid + '/1">' +
        '         <i class="fa fa-play" aria-hidden="true"></i>' +
        '         </a>' +
        '         <a title="Pause" class="status-control-btn is-under" href="user/9/rules/status-save/' + eid + '/0">' +
        '         <i class="fa fa-pause" aria-hidden="true"></i>' +
        '         </a>' +
        '      </div>' +
        '   </td>' +
        '   <td class="td-applied-to">' + ecampaigns + '</td>' +
        '   <td class="last-checked-td">not checked yet</td>' +
        '   <td class="last-triggered-td">not checked yet</td>' +
        '   <td class="td-actions">' +
        '       <a href="/user/' + uid + '/rules/edit/' + eid + '" data-eid = "' + eid + '" class="waves-effect waves-button waves-classic edit-event-btn" oldtitle="Edit">' +
        '         <i class="fa fa-pencil-square-o" aria-hidden="true"></i>' +
        '       </a>' +
        '       <a href="/user/' + uid + '/rules/delete" data-eid = "' + eid + '" class="waves-effect waves-button waves-classic delete-event-btn" oldtitle="Delete">' +
        '         <i class="fa fa-trash-o" aria-hidden="true"></i>' +
        '       </a>' +
        '   </td>' +
        '</tr>';

    var inserted = $(tr).insertBefore("#events-table tbody tr:first");

    //add events for new tr
    inserted.find('.status-control .status-control-btn').on('click', addStatusClickEvent);
    inserted.find('.delete-event-btn').on('click', addDeleteEventClickEvent);
    inserted.find('.edit-event-btn').on('click', addEditEventClickEvent);
    inserted.find('.form-checkbox').on('click', checkboxSelectEventProcess);
    fadeOutNewClass($('tr.new'));
    rearrangeEvenOddClasses(); 
}

function placeEventFirstInTable(id, name, countCmps, event_vars) {
    var tr = $('#' + id).parent().parent();
    var countCampaigns = 'All';

    //changed values
	var ename = '';
    var ecampaigns = 'All';
    var ecampaignsCount = 0;
    var condition = null;
	var description = '';
	var rate = null;
	var time = null;
	for (var i in event_vars) {
        if (event_vars[i].name == 'edit-rule-name') {
            ename = event_vars[i].value;

        }
        if (event_vars[i].name == 'applyto') {
            if (event_vars[i].value == 1) {
                ecampaigns = 'campaigns';
            }
        }
        if (event_vars[i].name == 'condition') {
            condition = event_vars[i].value;
			description = $('#edit-condition option[value="' + condition + '"]').text();
        }
		if (event_vars[i].name == 'rate') {
            rate = event_vars[i].value + '%';
        }
		if (event_vars[i].name == 'time') {
            time = event_vars[i].value + ' min';
        }
        if (event_vars[i].name == 'selected-campaigns-select[]') {
            ecampaignsCount = ecampaignsCount + 1;
        }
    }
	if (ecampaignsCount > 0) {
        ecampaigns = ecampaignsCount + ' ' + ecampaigns;
    }
	description += ' ' + rate + ' in the last ' + time;
	
	tr.find('.name').text(ename);
	tr.find('.mob-description').html('Description: <b>' + description + '</b>');
	tr.find('.mob-applied-to').html('Applied to: <b>' + ecampaigns + '</b>');
	tr.find('.td-description-td').text(description);
	tr.find('.td-applied-to').text(ecampaigns);

    //move tr
    tr.insertBefore("#events-table tbody tr:first");
    tr.addClass("new");
    fadeOutNewClass(tr);
    rearrangeEvenOddClasses();
}

function addStatusClickEvent(e) {
    if (!$(this).hasClass('active')) {
        $.ajax({
            url: '/' + $(this).attr('href')
        }).done(function (msg) {
            //console.log(msg);
        });
    }
    e.preventDefault();
}

function addDeleteEventClickEvent(e) {
    var eid = $(this).attr('data-eid');
    var href = $(this).attr('href');
    confirmPopup(href, eid, null);
    e.preventDefault();
}

function addEditEventClickEvent(e) {
    var eid = $(this).attr('data-eid');
    $.ajax({
        method: "POST",
        url: window.location.pathname + "/edit/" + eid
    }).success(function (data) {
        addRulePopup();
        $('form.edit-rule-save-form h1').text('Edit Rule');
        $('#rule-id').val(eid);
        $('#edit-rule-name').val(data.event.name);
        $('#edit-apply-to').val(data.event['apply_to']);
        $('#edit-apply-to').trigger( "change" );

        $('#selected-campaigns-select option:selected').each(function (index, value) {
          $(this).prop('selected', false);
        });

        if (data.event['apply_to'] == 0) {
            $(".multi-wrapper .selected-wrapper a").each(function (index, value) {
                $(this).click();
            });
        }

        if (data.event['apply_to'] == 1 && data.event['campaigns'].length > 0) {
            //console.log(data.event['campaigns']);
            $.each(data.event['campaigns'], function (index, value) {
                $(".multi-wrapper .non-selected-wrapper input[id='" + value + "']").click();
                /*var cmpName = $("#selected-campaigns-select option[value='" + value + "']").text();
                $(".multi-wrapper .non-selected-wrapper a:contains('" + cmpName + "')").each(function (index, value) {
                    if ($(this).text() == cmpName) {
                        $(this).click();
                    }
                });*/
            });
        }
        $("#edit-condition").val(data.event['condition']);
        $("#edit-rate").val(data.event['rate_filter']);
        $("#edit-time").val(data.event['time_filter']);
        $("#edit-webhook").val(data.event['webhook']);
        if (data.event['set_status'] > 0) {
            $("#edit-status").prop('checked', true);
        } else {
            $("#edit-status").prop('checked', false);
        }
        if (data.event['send_mail'] == 1) {
            $("#edit-send-mail").prop('checked', true);
        } else {
            $("#edit-send-mail").prop('checked', false);
        }
        if (data.event['pause_rule'] == 1) {
            $("#edit-pause-rule").prop('checked', true);
        } else {
            $("#edit-pause-rule").prop('checked', false);
        }

        $('#selected-campaigns-select option:selected').each(function (index, value) {
            //console.log($(this).text());
        });
		
		//Класс добавляется из-за подставления в форму дефолтных значений при редактировании, поэтому убираем его
		$('.overlay-over-blured-rule').removeClass('selected-campaigns-window');
    });
    e.preventDefault();
}

function rearrangeEvenOddClasses() {
    var newClass = '';
    $('#events-table tbody tr').each(function (index) {
        if ($(this).hasClass('even')) {
            $(this).removeClass('even');
        }
        if ($(this).hasClass('odd')) {
            $(this).removeClass('odd');
        }
        newClass = (index % 2) ? 'odd' : 'even';
        $(this).addClass(newClass);
    });
}

function ruleFormValidation() {
    var is_validated = true;
    $('.ce-error').remove();
    $('.ce-form-error').removeClass('ce-form-error');

    //Event rule name
    var name = $('.overlay-over-blured-rule #edit-rule-name').val();

    if (name == '' || name.replace(' ', '') == '') {
        $("<span class = 'ce-error rule-name-error'>Required</span>").insertAfter(".overlay-over-blured-rule #edit-rule-name");
        $("#edit-rule-name").addClass('ce-form-error');
        is_validated = false;
    }

    //Selected campaigns
    if ($('#edit-apply-to').val() == 1 && $('.selected-campaigns select option:selected').length == 0) {
        $("<span class = 'ce-error selected-campaigns-error'>Select at list one company</span>").insertAfter(".overlay-over-blured-rule #edit-apply-to");
        $("#edit-apply-to").addClass('ce-form-error');
        is_validated = false;
    }

    //Event webhook
    var webhook = $('.overlay-over-blured-rule  #edit-webhook').val();
    if (webhook != '') {
        if (!validateUrl(webhook)) {
            $("<span class = 'ce-error edit-webhook-error'>Must be valid URL</span>").insertAfter("#edit-webhook");
            $("#edit-webhook").addClass('ce-form-error');
            is_validated = false;
        }
    }

    return is_validated;
}


function validateUrl(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    if (str.split('http://').length == 1 && str.split('https://').length == 1) {
        return false;
    }
    return pattern.test(str);
}

function addRulePopup() {
    $('#page #content').addClass('bg-blured');

    $('body').append($('.overlay-over-blured-rule'));
    $('.overlay-over-blured-rule').show();
	if ($(window).width() < 768){
		$('body').addClass('show-popup');	
	}
    //Дефолтные значения
    $('#rule-id').val('');
    $('#edit-rule-name').val('');

    $('#selected-campaigns-select option:selected').each(function (index, value) {
        $(this).prop('selected', false);
    });

    $('#edit-apply-to').val(0);
    $('#edit-apply-to').trigger( "change" );


    $("#edit-condition").val(0);
    $("#edit-rate").val(10);
    $("#edit-time").val(5);
    $("#edit-webhook").val('');
    $("#edit-status").prop('checked', false);
    $("#edit-send-mail").prop('checked', false);
    $("#edit-pause-rule").prop('checked', false);

    $('a.cancel-popup-rule').on('click', function (e) {
        closeAddRulePopup();
        e.preventDefault();
    });

}

function confirmPopup(type, batchId, eids) {
    $('#page #content').addClass('bg-blured');

    $('body').append($('.overlay-over-blured-confirm'));
    $('.overlay-over-blured-confirm').show();

    $('a.cancel-popup-confirm').on('click', function (e) {
        closeConfirmPopup();
        e.preventDefault();
    });
    var saveBtn = $('.overlay-over-blured-content-confirm .save-confirm');
    saveBtn.off();
    if (type == 'batch'){
        saveBtn.on('click', function (e) {
            batchAjax(batchId, eids);
            closeConfirmPopup();
            e.preventDefault();
        });
    } else {
        saveBtn.on('click', function (e) {
            var eid = batchId;
            var href = type;
            var tr = $('a[data-eid="' + eid + '"]').parent().parent();

            tr.remove();
            rearrangeEvenOddClasses();
            $.ajax({
                method: "POST",
                url: href,
                data: {
                    eid: eid
                }
            }).done(function (msg) {
                //console.log(msg);
                closeConfirmPopup();
            });

            e.preventDefault();
        });
    }
}

function closeConfirmPopup() {
    $('#page #content').removeClass('bg-blured');
    $('.overlay-over-blured-confirm').hide();
}

function closeAddRulePopup() {
    $('#page #content').removeClass('bg-blured');
    $('.overlay-over-blured-rule').hide();
}

function fadeOutNewClass(tr) {
    setInterval(function() {
        tr.removeClass("new", 2000);
    }, 3000);
}

function tableChangeOnMobile() {
    /*if ($(window).width() < 768) {
        $("#events-table tr").each(function( index ) {
            $(this).find('td:eq(4)').hide();
            $(this).find('th:eq(4)').hide();
            $(this).find('td:eq(5)').hide();
            $(this).find('th:eq(5)').hide();
            //$('#events-table .appender').show();
        });
    } else {
        $('#events-table td:hidden').show();
        $('#events-table th:hidden').show();
        //$('#events-table .appender').hide();
    }*/
}