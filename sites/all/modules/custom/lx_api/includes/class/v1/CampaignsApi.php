<?php
require_once 'Api.php';

class lxCampaignsApi extends lxApi
{
    public  $apiName = 'campaigns';
    
    private $filterNames = ['vpn_proxy' => 'VPN or Proxy IP',
                            'bots' => 'Bots',
                            'magic_filter_light' => 'Magic Filter Light',
                            'magic_filter_normal' => 'Magic Filter Normal',
                            'magic_filter_super' => 'Magic Filter Super',
                            'countries' => 'Visitor\'s Location',
                            'device_os' => 'Devices/OSs',
                            'browsers_languages' => 'Browsers/Languages',
                            'browsers' => 'Browsers',
                            'languages' => 'Browsers\' Languages',
                            'isp_organizations' => 'ISP/Organizations',
                            'ip_list' => 'IP List',
                            'referer' => 'Referer',
                            'url_substrings' => 'URL Substrings',
                            'user_agent' => 'User Agent',
                            'time_of_day' => 'Time of Day'
        ];
    
    // загружаем кампанию из базы
    private function loadCampaign($campaignID) {
        $campaign = (array)db_select('domain_compaigns', 'c')
                    ->fields('c')
                    ->condition('uid', $this->userID)
                    ->condition('cid', $campaignID)
                    ->execute()
                    ->fetchObject();
        
        if (!empty($campaign)) {
            $campaign['data'] = unserialize($campaign['data']);
        }
        
        return !empty($campaign) ? $campaign : null;
    }
    
    // возвращаем заготовку для кампании
    private function emptyCampaign() {
        
        $campaign = ['cid' => '',
                     'uid' => $this->userID,
                     'created' => time(),
                     'name' => '',
                     'status' => 7,
                     'set_active_after_x_imps' => 0,
                     'set_under_review_after' => 0,
                     'show_from' => '',
                     'show_to' => '',
                     'data' => [],
            ];
        
        return $campaign;
    }
    
    // сохраняем кампанию
    private function saveCampaign($campaign) {
        if ($campaign['cid']) {
            $id = $campaign['cid'];
            unset($campaign['cid']);
            db_update('domain_compaigns')->fields($campaign)->condition('cid', $id)->execute(); 
        }
        else {
            $campaign['cid'] = md5(uniqid(rand(), 1));
            $campaign['data'] = serialize($campaign['data']);
            
            db_insert('domain_compaigns')->fields($campaign)->execute();             
        }
        
        return $campaign['cid'];
    }


    private function getCampaignsList($uid, $getData = []) {
                
        date_default_timezone_set('UTC');
        $account = user_load($uid);    

        $statuses = __lx_campaign_statuses(false);
        $sorts = array(1 => 'updated', 2 => 'created', 3 => 'cnt_sort', 4 => 'cnt_block_sort', 5 => 'name', 6 => 'name');
        $sortOrder = 'DESC';


        $sort_by = 3;
        if (isset($getData['sortby']) && $getData['sortby']) {
            $sort_by = (int) $getData['sortby'];
            if (!in_array($sort_by, array(1, 2, 3, 4, 5, 6))) {
                $sort_by = 1;
            }
        }

        if (in_array($sort_by, array(3,4)) && !isset($getData['page'])) {
            // 1. берем статистику за сегодня из базы статистики
            try {
                $db = new PDO("mysql:host=" . LX_MYSQLI_STAT_HOST . ";dbname=" . LX_MYSQLI_STAT_DB, LX_MYSQLI_STAT_USER, LX_MYSQLI_STAT_PASS);
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                // выбираем из дневной
                $sth = $db->prepare('SELECT * FROM day_cmp_stat_' . date('Y_m_d') .' WHERE uid=:uid ORDER BY total_clicks DESC');

                $sth->bindValue(':uid', $account->uid, PDO::PARAM_INT);
                $sth->execute();
                $res = $sth->fetchAll(PDO::FETCH_ASSOC);

                $cids = array();
                if (sizeof($res)) {
                    foreach ($res as $r) { 
                        $total_blocked = 0;
                        foreach ($r as $kk => $rr) {
                            if (!in_array($kk, array('id', 'uid', 'cid', 'total_clicks'))) {
                                $total_blocked += (int)$rr;
                            }
                        }
                        $cids[$r['cid']] = array('total' => $r['total_clicks'], 'blocked' => $total_blocked);
                    }
                }
                $sth = null;
                $db = null;

                if (sizeof($cids)) {

                    db_update('domain_compaigns')
                            ->fields(array('cnt_sort' => 0, 'cnt_block_sort' => 0))
                            ->condition('uid', $account->uid)
                            ->execute();


                    foreach ($cids as $cid => $clicks) {
                        db_update('domain_compaigns')
                            ->fields(array('cnt_sort' => $clicks['total'], 'cnt_block_sort' => $clicks['blocked']))
                            ->condition('cid', $cid)
                            ->execute();
                    }             

                }
            } catch (PDOException $e) {
//                print 'error';
    //            $err_file = LX_AD_LOGS_FOLDER . '/errors_' . date('Y-m-d_H') . '.log';
    //            file_put_contents($err_file, date('d.m.Y H:s') . "\t Get campaign stat form stat db: " . $e->getMessage() . "\n", FILE_APPEND);
            }
        }

        if ($sort_by == 5) {
            $sortOrder = 'ASC';
        }

        $list_is_filtered = 0;

        $res = db_select('domain_compaigns', 'd')
                    ->fields('d')
                    ->condition('d.uid', $account->uid);      

        if (isset($getData['name']) && trim($getData['name'])) {
            $res->condition('d.name', '%'.db_like($getData['name']) .'%', 'LIKE');
            $list_is_filtered = 1;
        }

        if (isset($getData['status']) && is_numeric($getData['status']) && $getData['status'] > 0) {
            $res->condition('d.status', $getData['status']);
            $list_is_filtered = 1;
        }

        if (sizeof($tags)) {
            $tags_cids = array();
            $res->join('cmp_tags', 't', 't.cid=d.cid');
            $res->condition('t.tag_id', $tags, 'IN');
            $list_is_filtered = 1;
        }

        $res->orderBy($sorts[$sort_by], $sortOrder);

        $per_page = 30;  
        
        if (isset($getData['per_page']) && is_numeric($getData['per_page'])) {
          $per_page = $getData['per_page'];
        } 
        
        if ($per_page > 100) {
            $per_page = 100;
        }          

        $res = $res->extend('PagerDefault')
                    ->limit($per_page);

        $data = $res->execute();

        // считаем общее количество кампаний юзера

        $res = db_select('domain_compaigns', 'd');
        $res->addExpression('COUNT(d.cid)');
        $res->condition('d.uid', $account->uid);      

        if (isset($getData['name']) && trim($getData['name'])) {
            $res->condition('d.name', '%'.db_like($getData['name']) .'%', 'LIKE');
        }

        if (isset($getData['status']) && is_numeric($getData['status']) && $getData['status'] > 0) {
            $res->condition('d.status', $getData['status']);
        }

        if (sizeof($tags)) {
            $tags_cids = array();
            $res->join('cmp_tags', 't', 't.cid=d.cid');
            $res->condition('t.tag_id', $tags, 'IN');
        }

        $cidsTotal = $res->execute()->fetchField();  

        // /считаем общее количество кампаний юзера


        // ставим кампанию, которая была создана или отредактирована первой в списке
//        if (isset($_SESSION['cmp_updated_nid'])) {
//            $_res = db_select('domain_compaigns', 'd')
//                    ->fields('d')
//                    ->condition('d.cid', $_SESSION['cmp_updated_nid'])
//                    ->execute()->fetchObject();
//
//            if (!empty($_res) && !empty($data)) {
//                $__data = array();
//                $__data[] = $_res;
//                foreach ($data as $k => $r) {
//                    if ($r->cid != $_res->cid) {
//                        $__data[] = $r;
//                    }
//                }
//                $data = $__data;
//            } 
//        }


        $rows = array();

        if (!empty($data)) {

            $warnings = [];
            $warn_data = [];
            foreach ($data as $r) {
                $warn_data[] = $r;
            }


            $warnings = lx_version_control_check($warn_data);
            $data = $warn_data;


            require_once(drupal_get_path('module', 'lx_domains_cmp') .'/includes/cmp_tags.inc');
            foreach ($data as $r) {

                $_cmp_tags_array = campaign_tags_get_campaign_tags($r->cid);

                $cmp_warning = '';
                if (isset($warnings[$r->cid])) {
                    $cmp_warning = $warnings[$r->cid];
                }

                $rows[$r->cid] = ['name' => $r->name,
                                    'status' => $statuses[$r->status],
                                    'today_clicks' => ['total' => 0,
                                                       'safe_page' => 0],
                                    'safe_page_statistics' => [],
                                    'warning' => $cmp_warning,
                                    'tags' => sizeof($_cmp_tags_array) ? array_values($_cmp_tags_array) : []
                    ];    
            }

            if (sizeof($rows)) {

                $campaigns_stat = __get_campaigns_cnt_stat(array_keys($rows), true);

                $block_reasons = __get_reasons_translate();
                $block_reasons[103] = 'Show Money Page to All';

                if (sizeof($campaigns_stat)) {
                    foreach ($campaigns_stat as $cid => $r) {

                        $r['today'][4] += $r['today'][1];
                        unset($r['today'][1]);

                        $short_stat = array();
                        $rows[$cid]['today_clicks']['total'] = $r['today']['all'];


                        foreach ($r['today'] as $kk => $vv) {
                            if (is_numeric($kk)) {
                                $rows[$cid]['today_clicks']['safe_page'] += $vv;

                                if ($vv > 0) {
                                    $rows[$cid]['safe_page_statistics'][$block_reasons[$kk]] = ['clicks' => mcfn($vv, 0, false), 'percent' => mcfn($vv/$r['today']['all']*100, 2, false) .'%'];                                    
                                }
                            }
                        }

                        $rows[$cid]['today_clicks']['safe_page_percent'] = $rows[$cid]['today_clicks']['total'] ? mcfn($rows[$cid]['today_clicks']['safe_page']/$rows[$cid]['today_clicks']['total']*100, 2,false) : 0;
                        $rows[$cid]['today_clicks']['total'] = mcfn($rows[$cid]['today_clicks']['total'], 0, false);
                    }
                }
            }
        }


        return $rows;
    }
    
    // проверяем параметры при добавлении кампании
    private function validateParams($requestMethod, &$params) {
        $valid = true;
        $validationResponse = ['ErrorText' => 'Bad Request', 'ErrorCode' => 400, 'UserErrorText' => '', 'validData' => true];
        
        print $requestMethod ."\n";
        
        if ($requestMethod == 'POST') {
            if (isset($params['campaign_id'])) {
                $valid = false;
                $validationResponse['UserErrorText'] = 'You can\'t specify Campaign ID while creating new campaign. If you want to update existing campaign use PUT or PATCH request instead.';
            }
            else {
                if (!isset($params['landers']) || !isset($params['landers']['safe_page']) || !isset($params['landers']['money_page'])) {
                    $valid = false;
                    $validationResponse['UserErrorText'] = 'Safe page URL and Money page URL are required';
                }                
            }
        }
        else if (!isset($params['campaign_id']) || !preg_match('/^[a-z0-9]{32}$/', $params['campaign_id'])) {
            $valid = false;
            $validationResponse['UserErrorText'] = 'Campaign ID is required.';
        }
        
        if ($valid && isset($params['status'])) {
            if (!isset($params['status']['status'])) {
                $valid = false;
                $validationResponse['UserErrorText'] = 'Campaign Status is required';
            }
        }
        
        
        if ($valid && isset($params['landers'])) {
            if (isset($params['landers']['safe_page']) && !valid_url(trim($params['landers']['safe_page']), true)) {
                $valid = false;
                $validationResponse['UserErrorText'] = 'Check Safe page format.';
            }
            else if (isset($params['landers']['money_page']) &&!valid_url(trim($params['landers']['money_page']), true)) {
                $valid = false;
                $validationResponse['UserErrorText'] = 'Check Money page format.';
            }
        
            if ($valid && isset($params['landers']['safe_page_type'])) {
//                $params['landers']['safe_page_type']  = (int)$params['landers']['safe_page_type'];
                if (!in_array($params['landers']['safe_page_type'], [1,2])) {
                    $valid = false;
                    $validationResponse['UserErrorText'] = 'Wrong field falue: safe_page_type.';
                }
            }    
            
            if ($valid && isset($params['landers']['money_page_type'])) {
//                $params['landers']['money_page_type']  = (int)$params['landers']['money_page_type'];
                if (!in_array($params['landers']['money_page_type'], [1,2])) {
                    $valid = false;
                    $validationResponse['UserErrorText'] = 'Wrong field falue: money_page_type.';
                }
            }    
            
            if ($valid && isset($params['landers']['safe_page_append_url_query'])) {
//                $params['landers']['safe_page_append_url_query'] = (int)$params['landers']['safe_page_append_url_query'];
                if (!in_array($params['landers']['safe_page_append_url_query'], [0,1])) {
                    $valid = false;
                    $validationResponse['UserErrorText'] = 'Wrong field falue: safe_page_append_url_query.';
                }    
            }    
            
            if ($valid && isset($params['landers']['money_page_append_url_query'])) {
//                $params['landers']['money_page_append_url_query'] = (int)$params['landers']['money_page_append_url_query'];
                if (!in_array($params['landers']['money_page_append_url_query'], [0,1])) {
                    $valid = false;
                    $validationResponse['UserErrorText'] = 'Wrong field falue: money_page_append_url_query.';
                }    
            }   
            
        // проверка на то, что мани и сейф должны быть в одной папке... подумать, нужно ли это проверять!!!
//        else {
//            if ($params['landers']['safe_page'] == 2 && $params['landers']['money_page'] == 2) { // 1 - redirect, 2 - show content
//                $su = explode('/', trim($params['landers']['safe_page']));
//                $mu = explode('/', trim($params['landers']['money_page']));
//
//                unset($su[sizeof($su)-1]);
//                unset($mu[sizeof($mu)-1]);
//
//                $su = implode('/', $su);
//                $mu = implode('/', $mu);
//
//                if ($su != $mu) {
//                    $valid = false;
//                    $validationResponse['UserErrorText'] = 'Safe Page and Money Page must be in the same folder.';
//                }
//            }             
//        }             
       
        }
        
        if (isset($params['filters'])) {
            if ($valid && isset($params['filters']['global_filters'])) {
                if (sizeof($params['filters']['global_filters'])) {
                    foreach ($params['filters']['global_filters'] as $filter_name => $filter_data) {
                        
                        if (!isset($this->filterNames[$filter_name])) {
                            $valid = false;
                            $validationResponse['UserErrorText'] = "Wrong filter name : ". check_plain($filter_name) ."."; 
                            break;                            
                        }
                        
                        if (!isset($filter_data['status'])) {
                            $valid = false;
                            $validationResponse['UserErrorText'] = $this->filterNames[$filter_name] .": status field is required.";
                            break;
                        }
                        else if (!in_array($filter_data['status'], [0,1])){
                            $valid = false;
                            $validationResponse['UserErrorText'] = "Wrong field falue: " .$this->filterNames[$filter_name] ."."; 
                            break;
                        }
                    }
                }
            }
            
            $three_states_filters = ['countries', 'isp_organizations', 'ip_list', 'referer', 'time_of_day'];
            foreach ($three_states_filters as $t_filter) {

                if ($valid && isset($params['filters'][$t_filter])) {
                    if (sizeof($params['filters'][$t_filter])) {
                        if (!isset($params['filters'][$t_filter]['status'])) {
                            $valid = false;
                            $validationResponse['UserErrorText'] = $this->filterNames[$t_filter] .": status field is required.";
                        }
                        else if (!in_array($params['filters'][$t_filter]['status'], [0, 1, 100])){
                            $valid = false;
                            $validationResponse['UserErrorText'] = "Wrong Status field falue: " .$this->filterNames[$t_filter] .".";
                        }
                    }
                }    
            }    
            
            // countries
            if ($valid && isset($params['filters']['countries']) && $params['filters']['countries']['status']) {
                if (($params['filters']['countries']['status'] != 0) && isset($params['filters']['countries']['list']) && sizeof($params['filters']['countries']['list'])) {
                    
                    foreach ($params['filters']['countries']['list'] as $k => $v) {
                        if (!preg_match('/^[A-Z]{2}$/', $v)) {
                            $valid = false;
                            $validationResponse['UserErrorText'] = $this->filterNames['countries'] .": incorrect field value: " .check_plain($v) .".";
                            break;
                        }
                    }

                    if ($valid) {
                        require_once(drupal_get_path('module', 'lx_domains_cmp') .'/includes/countries.inc');
                        $countries = ___get_countries_list();
                        
                        foreach ($params['filters']['countries']['list'] as $k => $v) {
                            if (!isset($countries[$v])) {
                                $valid = false;
                                $validationResponse['UserErrorText'] = $this->filterNames['countries'] .": incorrect field value: " .check_plain($v) .".";
                                break;
                            }
                        }
                    }
                }
                else {
                    $valid = false;
                    $validationResponse['UserErrorText'] = $this->filterNames['countries'] .": list of countries is required.";
                }
            }
            
            // isp_organizations
            if ($valid && isset($params['filters']['isp_organizations']) && $params['filters']['isp_organizations']['status']) {
                if (($params['filters']['isp_organizations']['status'] != 0) && isset($params['filters']['isp_organizations']['list']) && sizeof($params['filters']['isp_organizations']['list'])) {
                    
                    foreach ($params['filters']['isp_organizations']['list'] as $k => $v) {   
                        if (!trim($v)) {
                            unset($params['filters']['isp_organizations']['list'][$k]);
                        } 
                        else {
                            $params['filters']['isp_organizations']['list'][$k] = str_replace('&amp;','&',trim(check_plain($v)));
                        }
                    }
                }
                else {
                    $valid = false;
                    $validationResponse['UserErrorText'] = $this->filterNames['isp_organizations'] .": list of ISPs is required.";
                }
            }            
            
            // Devices/OSes
            if ($valid && isset($params['filters']['device_os']) && sizeof($params['filters']['device_os'])) {
                $devices_list = __get_devices_list();
                
                foreach ($params['filters']['device_os'] as $device => $oses) {
                    if (!isset($devices_list[$device])) {
                        $valid = false;
                        $validationResponse['UserErrorText'] = $this->filterNames['device_os'] .": not valid device name: " .check_plain($device) .".";
                        
                        break;
                    }
                    
                    foreach ($oses as $oses_v) {
                        if (!in_array($oses_v, $devices_list[$device]['os'])) {
                            $valid = false;
                            $validationResponse['UserErrorText'] = $this->filterNames['device_os'] .": not valid OS name: " .check_plain($oses_v) .".";

                            break(2);                            
                        }
                    }
                }
            }
            
            // Browsers/Languages
            if ($valid && isset($params['filters']['browsers_languages']) && sizeof($params['filters']['browsers_languages'])) {
                
                // browsers
                if (isset($params['filters']['browsers_languages']['browsers']) && sizeof($params['filters']['browsers_languages']['browsers'])) {
                    $allowed_browsers = array_keys(__get_browsers_families());
                    
                    foreach ($params['filters']['browsers_languages']['browsers'] as $browser) {
                        if (!in_array($browser, $allowed_browsers)) {
                            $valid = false;
                            $validationResponse['UserErrorText'] = "$this->filterNames['browsers']: not valid Browser name: " .check_plain($browser) .".";

                            break;
                        }
                    }
                }
                
                // languages
                if ($valid && isset($params['filters']['browsers_languages']['languages']) && sizeof($params['filters']['browsers_languages']['languages'])) {
                    if (!isset($params['filters']['browsers_languages']['languages']['status'])) {
                        $valid = false;
                        $validationResponse['UserErrorText'] = "$this->filterNames['languages']: status field is required.";
                    }
                    else if (!in_array($params['filters']['browsers_languages']['languages']['status'], [0, 1, 100])){
                        $valid = false;
                        $validationResponse['UserErrorText'] = "Wrong field falue: $this->filterNames['languages'].";
                    }
                    
                    
                    if ($valid) {
                        if (isset($params['filters']['browsers_languages']['languages']['list']) && sizeof($params['filters']['browsers_languages']['languages']['list'])) {
                            require_once(drupal_get_path('module', 'lx_domains_cmp') .'/includes/countries.inc');
                            $allowed_langs = ___get_langs_list();
                            

                        
                            foreach ($params['filters']['browsers_languages']['languages']['list'] as $k => $v) {
                                if (!isset($allowed_langs[$v])) {
                                    $valid = false;
                                    $validationResponse['UserErrorText'] = "$this->filterNames['languages']: incorrect field value: " .check_plain($v) .".";
                                    break;
                                }
                            }                            
                        }
                        else {
                            $valid = false;
                            $validationResponse['UserErrorText'] = "$this->filterNames['languages']: languages list is required.";                            
                        }
                    }
                }
                
            }
            
            
            // ip list
            if ($valid && isset($params['filters']['ip_list']) && sizeof($params['filters']['ip_list'])) {
                
                foreach ($params['filters']['ip_list'] as $k_ip => $ip) {
                    $right_ip_found = false;
                    if (strpos($ip, '-')) {
                        $ips = explode('-', $ip);
                        $ips[0] = trim($ips[0]);
                        $ips[1] = trim($ips[1]);

                        if (_valid_ip_v4($ips[0]) && _valid_ip_v4($ips[1])) {
                            $right_ip_found = true;
                        } else if (_valid_ip_v6($ips[0]) && _valid_ip_v6($ips[1])) {
                            $right_ip_found = true;
                        }
                    } else {
                        if (_valid_ip_v4($ip)) {
                            $right_ip_found = true;
                        } else if (_valid_ip_v6($ip)) {
                            $right_ip_found = true;
                        }
                    }
                    if (!$right_ip_found) {
                        $valid = false;
                        $validationResponse['UserErrorText'] = "$this->filterNames['ip_list']: wrong IP format: " .check_plain($params['filters']['ip_list'][$k_ip]) .".";     
                        
                        break;
                    }
                }                 
            }
            
            
            // referer
            if ($valid && isset($params['filters']['referer'])) {
                if (sizeof($params['filters']['referer']['list']) && ($params['filters']['referer']['status'] != 0)) {
                    $valid = false;
                    $validationResponse['UserErrorText'] = "$this->filterNames['referer']: Referers List can\'t be empty.";     
                    
                }
            }
            
            // url_substrings
            if ($valid && isset($params['filters']['url_substrings']) && ($params['filters']['url_substrings']['status'] == 1)) {
                if (empty($params['filters']['url_substrings']['required_list']) && empty($params['filters']['url_substrings']['disallowed_list'])) {
                    $valid = false;
                    $validationResponse['UserErrorText'] = "$this->filterNames['url_substrings']: Required or Disallowed lists are required.";                         
                }
            }
            
            // user_agent
            if ($valid && isset($params['filters']['user_agent'])) {
                if (empty($params['filters']['user_agent']['required_list']) && empty($params['filters']['user_agent']['disallowed_list'])) {
                    $valid = false;
                    $validationResponse['UserErrorText'] = "$this->filterNames['user_agent']: Required or Disallowed lists are required.";                         
                }                
            }
            
            // time_of_day
            if ($valid && isset($params['filters']['time_of_day']) && ($params['filters']['time_of_day']['status'] > 0)) {
                if (empty($params['filters']['time_of_day']['selected_time']) || empty($params['filters']['time_of_day']['timezone'])) {
                    $valid = false;
                    $validationResponse['UserErrorText'] = "$this->filterNames['time_of_day']: Time and Timezone are required.";     
                }
                else {
                    
                    
                    if (!preg_match('/^[0-9]{2}\:[0-9]{2} \- [0-9]{2}\:[0-9]{2}$/', $params['filters']['time_of_day']['selected_time'])) {
                        $valid = false;
                        $validationResponse['UserErrorText'] = "$this->filterNames['time_of_day']: Wrong Time value."; 
                    }
                    
                    if ($valid) {
                        $list = DateTimeZone::listAbbreviations();
                        $idents = DateTimeZone::listIdentifiers();

                        $data = array();
                        foreach ($list as $abbr => $info) {
                            foreach ($info as $zone) {
                                if (!empty($zone['timezone_id'])
                                        AND ! in_array($zone['timezone_id'], $added)
                                        AND
                                        in_array($zone['timezone_id'], $idents)) {
                                    $z = new DateTimeZone($zone['timezone_id']);
                                    $c = new DateTime(null, $z);
                                    $zone['time'] = $c->format('H:i a');
                                    $data[] = $zone;
    //                                $offset[] = $z->getOffset($c);
    //                                $added[] = $zone['timezone_id'];
                                }
                            }
                        }

                        $options = array();
                        foreach ($data as $key => $row) {
                            $options[$row['timezone_id']] = 1;
                        }    
                        $options['servertime'] = 1;

                        if (!isset($options[$params['filters']['time_of_day']['timezone']])) {
                            $valid = false;
                            $validationResponse['UserErrorText'] = "$this->filterNames['time_of_day']: Wrong Timezone value.";                             
                        }
                    }    
                }
            }
            
        }
        
             
        
        $validationResponse['validData'] = $valid;
        
        
        print_r($validationResponse); die();
        
        
        return $validationResponse;
    }
    
    // обрабатываем входные параметры и в зависимости от метода: создаем или обновляем
    private function processParamsToCampaign($requestMethod, $params) {
        global $user;

        $campaign = null;
        
        if (isset($params['campaign_id'])) {
            $campaign = $this->loadCampaign($params['campaign_id']);
            if (empty($campaign)) {
                return $this->response('Campaign not found', '404');
            }
        }
        else {
            $campaign = $this->emptyCampaign();
        }
        
        
        if (isset($params['landers'])) {
            if (isset($params['landers']['safe_page'])) {
                $campaign['data']['safe_page'] = str_replace('&amp;','&',_mc_check_url(trim($params['landers']['safe_page'])));
            }  
            
            if (isset($params['landers']['money_page'])) {
                $campaign['data']['money_page'] = str_replace('&amp;','&',_mc_check_url(trim($params['landers']['money_page'])));
            }  
            
            if (isset($params['landers']['safe_page_type'])) {
                $campaign['data']['safe_page_show_type']  = $params['landers']['safe_page_type'];
            }    
            
            if (isset($params['landers']['money_page_type'])) {
                $campaign['data']['money_page_show_type']  = $params['landers']['money_page_type'];
            }    
            
            if (isset($params['landers']['safe_page_append_url_query'])) {
                $campaign['data']['safe_page_send_params']  =  ($params['landers']['safe_page_append_url_query'] == 1) ? 10 : 0;
            }    
            
            if (isset($params['landers']['money_page_append_url_query'])) {
                $campaign['data']['money_page_send_params']  =  ($params['landers']['money_page_append_url_query'] == 1) ? 10 : 0;
            }
        }    
        
        


        

//        if ($data['money_page_show_type'] == 2) {
//            if (isset($_POST['param_protect']) && ($_POST['param_protect'] == 10)) {
//                $data['param_protect'] = 10;
//                $data['param_protect_name'] = check_plain($_POST['param_protect_name']);
//                $data['param_protect_value'] = check_plain($_POST['param_protect_value']);
//                $data['param_protect_hide_after'] = (int)$_POST['param_protect_hide_after'];
//                if (!$data['param_protect_hide_after']) {
//                    $data['param_protect_hide_after'] = 600;
//                }
//            }
//        }    

        if ($this->method == 'POST') {
            $data['hosting'] = 1;
            $data['bots']    = 1;
            $data['magic_filter_light'] = 10;
            
        }
        

        




        if (isset($_POST['magic_filter_light']) && ($_POST['magic_filter_light'] == 10)) {
            $data['magic_filter_light'] = 10;
        }
        else {
            require_once(drupal_get_path('module', 'lx_intercom') .'/includes/users.inc');
            lx_intercom_mfl_disabled(arg(1));
        }


        if (isset($_POST['magic_filter_normal']) && ($_POST['magic_filter_normal'] == 10)) {
            $data['magic_filter_normal'] = 10;
    //        $data['magic_filter_normal_type'] = is_numeric($_POST['magic_filter_normal_type']) ? $_POST['magic_filter_normal_type'] : 1;}
        }

        if (isset($_POST['pass_fb_prefetch']) && ($_POST['pass_fb_prefetch'] == 10)) {
            $data['pass_fb_prefetch'] = 10;
        }


        if (isset($_POST['magic_filter_super']) && ($_POST['magic_filter_super'] == 10)) {
            $data['magic_filter_super'] = 10;
        }
    
//
//        // js фильтры
//
//        if ($is_admin) {
//            if (isset($_POST['js_timezone']) && ($_POST['js_timezone'] == 10)) {
//                $data['js_timezone'] = 10;
//                $data['js_timezone_delta'] = 120;//isset($_POST['js_timezone_delta']) ? (int)$_POST['js_timezone_delta'] : 0;
//            }    
//
//            if (isset($_POST['js_magic']) && ($_POST['js_magic'] == 10)) {
//                $data['js_magic'] = 10;
//            }    
//
//            if (isset($_POST['js_luminati']) && ($_POST['js_luminati'] == 10)) {
//                $data['js_luminati'] = 10;
//            }    
//
//
//            $data['show_first'] = 1;
//
//
//            if (isset($_POST['hide_script']) && ($_POST['hide_script'] == 10)) {
//                $data['hide_script'] = 10;
//            }   
//        }    

        // /js фильтры


        $data['lang_status'] = (int) $form_state['values']['lang_status'];
        $langs = explode(',', trim(check_plain($form_state['values']['langs'])));
        foreach ($langs as $k => $v) {
            if (!preg_match('/[a-z]{2}/', $v)) {
                unset($langs[$k]);
            }
        }

        if (sizeof($langs)) {
            $data['langs'] = $langs;
        } else {
            $data['lang_status'] = 0;
        }


        $data['location_status'] = (int) $form_state['values']['location_status'];
        $countries = explode(',', trim(check_plain($form_state['values']['location_list'])));
        foreach ($countries as $k => $v) {
            if (!preg_match('/[A-Z]{2}/', $v)) {
                unset($countries[$k]);
            }
        }

        if (sizeof($countries)) {
            $data['countries'] = $countries;
        } else {
            $data['location_status'] = 0;
        }    

        $form_state['values']['name'] = check_plain($form_state['values']['name']);

        $set_active_after_x_imps = 0;
        $set_under_review_after = 0;
        $cmp_status = 7;
        $show_from = '';
        $show_to = '';


        $cmp_status = (int) $form_state['values']['cmp_status'];

        $set_active_after_x_imps = (int) $form_state['values']['set_active_after_x_imps'];
        $set_under_review_after = (int) $form_state['values']['set_under_review_after'];


        $devices = __get_devices_list();
        
        $data['devices_work'] = array();

        foreach ($devices as $device_type => $device_info) {
            if (isset($form_state['values']['check_device_' . $device_type]) && ($form_state['values']['check_device_' . $device_type] == 10)) {
                $data['check_device_' . $device_type] = array();
                if (!in_array($device_type, array('smarttv'))) {
                    if (isset($form_state['values']['check_device_' . $device_type . '_os']) && sizeof($form_state['values']['check_device_' . $device_type . '_os'])) {
                        foreach ($form_state['values']['check_device_' . $device_type . '_os'] as $v) {
                            if ($v && in_array($v, $devices[$device_type]['os'])) {
                                $data['check_device_' . $device_type][] = $v;
                            }
                        }
                    }
                }

                $data['devices_work'][$device_type] = $data['check_device_' . $device_type];
            }
        }

        $browsers = [];
        if (isset($form_state['values']['browsers']) && sizeof($form_state['values']['browsers'])) {
            $families = __get_browsers_families();
            foreach ($form_state['values']['browsers'] as $browsers_k => $browsers_v) {
                if ($browsers_v) {
                    $browsers = array_merge($browsers, $families[$browsers_k]);
                }            
            }

            if (sizeof($browsers)) {
                $data['browsers'] = $browsers;
            }    
        }

        $data['isp_list_type'] = (int) $form_state['values']['isp_list_type'];
        $data['isp_list'] = explode("\n", $form_state['values']['isp_list']);
        if (sizeof($data['isp_list'])) {
            foreach ($data['isp_list'] as $k => $v) {
                if (!trim($v)) {
                    unset($data['isp_list'][$k]);
                } else {
                    $data['isp_list'][$k] = str_replace('&amp;','&',trim(check_plain($data['isp_list'][$k])));
                }
            }
        }

        $data['ip_list_type'] = (int) $form_state['values']['ip_list_type'];
        $data['ip_list'] = explode("\n", $form_state['values']['ip_list']);
        if (sizeof($data['ip_list'])) {
            foreach ($data['ip_list'] as $k => $v) {
                if (!trim($v)) {
                    unset($data['ip_list'][$k]);
                } else {
                    $data['ip_list'][$k] = trim(check_plain($data['ip_list'][$k]));
                }
            }
        }

        // обрабатываем IP и сохраняем их в long одиночные или диапазоны)
        if ($data['ip_list_type'] && sizeof($data['ip_list'])) {
            $data['ip_list_work'] = array();
            foreach ($data['ip_list'] as $k_ip => $ip) {
                $right_ip_found = false;
                if (strpos($ip, '-')) {
                    $ips = explode('-', $ip);
                    $ips[0] = trim($ips[0]);
                    $ips[1] = trim($ips[1]);

                    if (_valid_ip_v4($ips[0]) && _valid_ip_v4($ips[1])) {
                        $data['ip_list_work'][] = array(ip2long($ips[0]), ip2long($ips[1]));
                        $right_ip_found = true;
                    } else if (_valid_ip_v6($ips[0]) && _valid_ip_v6($ips[1])) {
                        $data['ip_list_work'][] = array(ip2long_v6($ips[0]), ip2long_v6($ips[1]));
                        $right_ip_found = true;
                    }
                } else {
                    if (_valid_ip_v4($ip)) {
                        $data['ip_list_work'][] = array(ip2long($ip), ip2long($ip));
                        $right_ip_found = true;
                    } else if (_valid_ip_v6($ip)) {
                        $data['ip_list_work'][] = array(ip2long_v6($ip), ip2long_v6($ip));
                        $right_ip_found = true;
                    } 
    //                    else {
    //                        if (preg_match('/^([0-9]{1,3}\.){1,3}$/', $ip)) {
    //                            $octets = explode('.', $ip);
    //                            $ip_from = rtrim($ip, '.');
    //                            $ip_to = $ip_from;
    //                            for ($i_ip = sizeof($octets); $i_ip <= 4; $i_ip ++) {
    //                                $ip_from .= '.0';
    //                                $ip_to .= '.255';
    //                            }
    //
    //                            $data['ip_list_work'][] = array(ip2long($ip_from), ip2long($ip_to));
    //                            $right_ip_found = true;
    //                        }
    //                    }
                }
                if (!$right_ip_found) {
                    drupal_set_message('Wrong IP format: ' . check_plain($data['ip_list'][$k_ip]) . '. String has been deleted.', 'error');
                    unset($data['ip_list'][$k_ip]);
                }
            }
        }

        $data['ref_list_type'] = (int) $form_state['values']['ref_list_type'];
        $data['ref_list'] = explode("\n", $form_state['values']['ref_list']);
        if (sizeof($data['ref_list'])) {
            foreach ($data['ref_list'] as $k => $v) {
                if (!trim($v)) {
                    unset($data['ref_list'][$k]);
                } else {
                    $data['ref_list'][$k] = str_replace('&amp;','&',trim(check_plain($data['ref_list'][$k])));
                }
            }
        }

        $data['clk_blank'] = (int) $form_state['values']['clk_blank'];

        $urlsbs_list = explode("\n", str_replace('&amp;','&',trim(check_plain($form_state['values']['urlsbs_list_required']))));

        if (sizeof($urlsbs_list)) {
            foreach ($urlsbs_list as $_k => $_v) {
                $_v = trim($_v);
                if (!$_v) {
                    unset($urlsbs_list[$_k]);
                } else {
                    $urlsbs_list[$_k] = $_v;
                }
            }
        }
        $data['urlsbs_list_required'] = $urlsbs_list;

        $data['ref_list_type'] = (int) $form_state['values']['ref_list_type'];
        $data['urlsbs_list_type'] = (int) $form_state['values']['urlsbs_list_type'];


        $urlsbs_list = explode("\n", str_replace('&amp;','&',trim(check_plain($form_state['values']['urlsbs_list_disallowed']))));
        if (sizeof($urlsbs_list)) {
            foreach ($urlsbs_list as $_k => $_v) {
                $_v = trim($_v);
                if (!$_v) {
                    unset($urlsbs_list[$_k]);
                } else {
                    $urlsbs_list[$_k] = $_v;
                }
            }
        }

        $data['urlsbs_list_disallowed'] = $urlsbs_list;

        $data['ua_list_type'] = (int) $form_state['values']['ua_list_type'];

        //---

        $ua_list = explode("\n", str_replace('&amp;','&',trim(check_plain($form_state['values']['ua_list_required']))));

        if (sizeof($ua_list)) {
            foreach ($ua_list as $_k => $_v) {
                $_v = trim($_v);
                if (!$_v) {
                    unset($ua_list[$_k]);
                } else {
                    $ua_list[$_k] = $_v;
                }
            }
        }

        $data['ua_list_required'] = $ua_list;



        $ua_list = explode("\n", str_replace('&amp;','&',trim(check_plain($form_state['values']['ua_list_disallowed']))));
        if (sizeof($ua_list)) {
            foreach ($ua_list as $_k => $_v) {
                $_v = trim($_v);
                if (!$_v) {
                    unset($ua_list[$_k]);
                } else {
                    $ua_list[$_k] = $_v;
                }
            }
        }

        $data['ua_list_disallowed'] = $ua_list;

        //---

        if ((int) $form_state['values']['time_type']) {
            $data['time_type'] = (int) $form_state['values']['time_type'];
            $data['selected_time'] = preg_match('/[0-9]{2}\:[0-9]{2} \- [0-9]{2}\:[0-9]{2}/', $form_state['values']['selected_time']) ? $form_state['values']['selected_time'] : '00:00 - 24:00';
            $data['cmp_timezone'] = check_plain($form_state['values']['cmp_timezone']);
            $time = explode(' - ', $data['selected_time']);
            if ($data['cmp_timezone'] && ($data['cmp_timezone'] != 'servertime')) {
                $cur_offset = 0;
                foreach ($form_state['timezones_info'] as $k => $timezone) {
                    if ($timezone['timezone_id'] == $data['cmp_timezone']) {
                        $from_day = strtotime(date('j F Y') . ' ' . $time[0]) - ($timezone['offset']);
                        $to_day = strtotime(date('j F Y') . ' ' . $time[1]) - ($timezone['offset']);

                        $show_from = date('H:i', $from_day);
                        $show_to = date('H:i', $to_day);
                    }
                }
            } else {
                $show_from = $time[0];
                $show_to = $time[1];
            }
        }
        else {
            $data['time_type'] = 0;
        }


        if (isset($form_state['values']['cid'])) {

            db_update('domain_compaigns')
                    ->fields(array('name' => $form_state['values']['name'],
                        'status' => $cmp_status,
                        'set_active_after_x_imps' => $set_active_after_x_imps,
                        'set_under_review_after' => $set_under_review_after,
                        'show_from' => $show_from,
                        'show_to' => $show_to,
                        'data' => serialize($data)))
                    ->condition('cid', $form_state['values']['cid'])->execute();

            $cmp_id = $form_state['values']['cid'];

            drupal_set_message('Campaign <b>' . $form_state['values']['name'] . '</b> was magically saved!');

            lx_magic_log2('Campaign ' .$form_state['values']['cid'] .' updated.', 'Campaign ' . $form_state['values']['name'] . ' updated.', '', 3);
        } 
        else {
            $better_token = md5(uniqid(rand(), 1));

            $cmp_create_time = time();
            db_insert('domain_compaigns')
                    ->fields(array('cid' => $better_token,
                        'uid' => arg(1),
                        'created' => $cmp_create_time,
                        'name' => $form_state['values']['name'],
                        'status' => $cmp_status,
                        'set_active_after_x_imps' => $set_active_after_x_imps,
                        'set_under_review_after' => $set_under_review_after,
                        'show_from' => $show_from,
                        'show_to' => $show_to,
                        'data' => serialize($data)
                    ))->execute();

            $cmp_id = $better_token;

            drupal_set_message('Campaign <b>' . $form_state['values']['name'] . '</b> was magically added!');
            lx_magic_log2('Campaign ' .$cmp_id .' created.', 'Campaign ' . $form_state['values']['name'] . ' created.', '', 3);

            //  обновляем статистику Intercom по кампаниям
            require_once(drupal_get_path('module', 'lx_intercom') .'/includes/users.inc');
            lx_intercom_inc_user_campaign(arg(1), date('Y-m-d H:i:s', $cmp_create_time));
        }

        // new functionality!
        require_once drupal_get_path('module', 'lx_domains_cmp') .'/includes/cmp_queue.inc';
        lx_domains_cmp_add_to_aud_queue($cmp_id);

        // сохраняем тэги кампании
        $cmp_tags = explode(',', trim(check_plain($form_state['values']['tag_list'])));
        foreach ($cmp_tags as $k => $v) {
            if (!trim($v)) {
                unset($cmp_tags[$k]);
            }
            else {
                $cmp_tags[$k] = truncate_utf8($cmp_tags[$k], 32);
            }
        }

        require_once($_SERVER['DOCUMENT_ROOT'] .'/' .drupal_get_path('module', 'lx_domains_cmp') .'/includes/cmp_tags.inc');
        campaign_tags_set($user->uid, $cmp_id, $cmp_tags);        
    }
    
    
    private function mapFieldsToUser($cmp) {
        
        date_default_timezone_set('UTC');
        
        $statuses = __lx_campaign_statuses();
        
        $mapped = [
                'campaign_id' => $cmp->cid,
                'name'        => $cmp->name,
                'created'     => date('Y-m-d H:i:s'),
                'updated'     => $cmp->updated,
                'status'      => [
                    'status'                      => $statuses[$cmp->status],
                    'set_active_after_imps'       => $cmp->set_active_after_x_imps,
                    'set_under_review_after_mins' => $cmp->set_under_review_after,
                ],
                'landers' => [
                        'safe_page' => $cmp->data['safe_page'],
                        'safe_page_type' => $cmp->data['safe_page_show_type'], //== 1 ? 'Redirect' (1) : 'Show content' (2),
                    // [safe_page_send_params] => 10
                    
                        'money_page' => $cmp->data['money_page'],
                        'money_page_type' => $cmp->data['money_page_show_type'], // == 1 ? 'Redirect' (1) : 'Show content' (2),
                    
                    // [money_page_send_params] => 10
                ],
            
                'filters' => [
                    'global_filters' => [
                        'vpn_proxy'           => ['status' => $cmp->data['hosting'] ? 1 : 0],
                        'bots'                => ['status' => $cmp->data['bots'] ? 1 : 0],
                        'magic_filter_light'  => ['status' => $cmp->data['magic_filter_light']  == 10 ? 1 : 0],
                        'magic_filter_normal' => ['status' => $cmp->data['magic_filter_normal']   == 10 ? 1 : 0],
                        'magic_filter_super'  => ['status' => $cmp->data['magic_filter_super']  == 10 ? 1 : 0],
                    ],
                    
                    'countries' => [
                        'status' => $cmp->data['location_status'],
                        'list'   => $cmp->data['countries'],
                    ],
                    
                    'device_os' => [
                        'desktops' => (isset($cmp->data['check_device_desktops']) && sizeof($cmp->data['check_device_desktops'])) ? $cmp->data['check_device_desktops'] : [],
                        'smartphones' => (isset($cmp->data['check_device_smartphones']) && sizeof($cmp->data['check_device_smartphones'])) ? $cmp->data['check_device_smartphones'] : [],
                        'gameconsoles' => (isset($cmp->data['check_device_gameconsoles']) && sizeof($cmp->data['check_device_gameconsoles'])) ? $cmp->data['check_device_gameconsoles'] : [],
                        'smarttv' => (isset($cmp->data['check_device_smarttv']) && sizeof($cmp->data['check_device_smarttv'])) ? $cmp->data['check_device_smarttv'] : [],
                    ],
                    
                    'browsers_languages' => [
                        'browsers'  => (isset($cmp->data['browsers']) && sizeof($cmp->data['browsers'])) ? array_keys(__get_browser_families_by_browsers($cmp->data['browsers'])) : [],
                        'languages' => [
                            'status'         => $cmp->data['lang_status'],
                            'list'           => $cmp->data['langs'],
                        ]
                    ],
                    
                    'isp_organizations' => [
                        'status'            => $cmp->data['isp_list_type'],
                        'list'              => (isset($cmp->data['isp_list']) && sizeof($cmp->data['isp_list'])) ? $cmp->data['isp_list'] : [],
                    ],
                    
                    'ip_list' => [
                        'status'            => $cmp->data['ip_list_type'],
                        'list'              => (isset($cmp->data['ip_list']) && sizeof($cmp->data['ip_list'])) ? $cmp->data['ip_list'] : [],
                    ],
                    
                    'referer' => [
                        'status'            => $cmp->data['ref_list_type'],
                        'list'              => (isset($cmp->data['ref_list']) && sizeof($cmp->data['ref_list'])) ? $cmp->data['ref_list'] : [],
                        'block_blank'       => isset($cmp->data['clk_blank']) ? $cmp->data['clk_blank'] : 0
                    ],

                    'url_substrings' => [
                        'status' => $cmp->data['urlsbs_list_type'],
                        'required_list'     => (isset($cmp->data['urlsbs_list_required']) && sizeof($cmp->data['urlsbs_list_required'])) ? $cmp->data['urlsbs_list_required'] : [],
                        'disallowed_list'   => (isset($cmp->data['urlsbs_list_disallowed']) && sizeof($cmp->data['urlsbs_list_disallowed'])) ? $cmp->data['urlsbs_list_disallowed'] : [],
                    ],                    

                    'user_agent' => [
                        'status' => $cmp->data['ua_list_type'],
                        'required_list'     => (isset($cmp->data['ua_list_required']) && sizeof($cmp->data['ua_list_required'])) ? $cmp->data['ua_list_required'] : [],
                        'disallowed_list'   => (isset($cmp->data['ua_list_disallowed']) && sizeof($cmp->data['ua_list_disallowed'])) ? $cmp->data['ua_list_disallowed'] : [],
                    ],                    

                    'time_of_day' => [
                        'status' => $cmp->data['time_type'],
                        'selected_time'     => isset($cmp->data['selected_time']) ? $cmp->data['selected_time'] : '',
                        'timezone'          => isset($cmp->data['cmp_timezone'])  ? $cmp->data['cmp_timezone'] : '',
                    ],                    
                ]            
            ];
        
        if ($cmp->data['safe_page_show_type'] == 1) {
            $mapped['landers']['safe_page_append_url_query'] = (isset($cmp->data['safe_page_send_params']) && ($cmp->data['safe_page_send_params'] == 10)) ?  1 : 0;
        }
        if ($cmp->data['money_page_send_params'] == 1) {
            $mapped['landers']['money_page_append_url_query'] = (isset($cmp->data['money_page_send_params']) && ($cmp->data['money_page_send_params'] == 10)) ?  1 : 0;
        }
        
        return $mapped;
    }

    /**
     * Метод GET
     * Вывод списка всех записей
     * http://ДОМЕН/users
     * @return string
     */
    public function indexAction() {
        
        $campaigns = $this->getCampaignsList(arg(1));
        
        if(sizeof($campaigns)){
            return $this->response($campaigns, 200);
        }
        
        return $this->response('Data not found', 404);
    }

    /**
     * Метод GET
     * Просмотр отдельной записи (по id)
     * http://ДОМЕН/users/1
     * @return string
     */
    public function viewAction() {
        
        $cmp = db_select('domain_compaigns', 'd')
                ->fields('d')
                ->condition('uid', $this->userID)
                ->condition('cid', $this->cmpID)
                ->execute()->fetchObject();
        
        $cmp->data = unserialize($cmp->data);
        
//        print_r($cmp); die();
        
        $cmp = $this->mapFieldsToUser($cmp);

        if($cmp){
            
            return $this->response($cmp, 200);
        }
        return $this->response('Data not found', 404);
    }

    /**
     * Метод POST
     * Создание новой записи
     * http://ДОМЕН/users + параметры запроса name, email
     * @return string
     */
    public function createAction() {
//        $name = $this->requestParams['name'] ? $this->requestParams['name'] : '';
//        $email = $this->requestParams['email'] ? $this->requestParams['email'] : '';
//        $postData = file_get_contents('php://input');
//        
//        print "\n\n ---- \n\n $postData \n\n ----\n\n";
//        
//        $postData = parse_str($postData);
//        
//        print_r($postData); die();
        
        // проверяем введенные параметры
        $validate_result = $this->validateParams($this->method, $this->requestParams);
        
        
        
        if ($validate_result['validData']) {       
            $id = $this->processParamsToCampaign($this->method, $this->requestParams);
            return $this->response('Created /campaigns/' .$id, 201);
        }
        else {
            return $this->response($validate_result['ErrorText'], $validate_result['ErrorCode']);
        }

        
        if($name && $email){
//            $db = (new Db())->getConnect();
//            $user = new Users($db, [
//                'name' => $name,
//                'email' => $email
//            ]);
//            if($user = $user->saveNew()){
//                return $this->response('Data saved.', 200);
//            }
        }
        return $this->response("Saving error", 500);
    }

    /**
     * Метод PUT
     * Обновление отдельной записи (по ее id)
     * http://ДОМЕН/users/1 + параметры запроса name, email
     * @return string
     */
    public function updateAction() {
        
        // проверяем введенные параметры
        $validate_result = $this->validateParams($this->method, $this->requestParams);        
        
        
        print 'updateAction'; die();
//        $parse_url = parse_url($this->requestUri[0]);
//        $userId = $parse_url['path'] ? $parse_url['path'] : null;
//
//        $db = (new Db())->getConnect();
//
//        if(!$userId || !Users::getById($db, $userId)){
//            return $this->response("User with id=$userId not found", 404);
//        }
//
//        $name = $this->requestParams['name'] ? $this->requestParams['name'] : '';
//        $email = $this->requestParams['email'] ? $this->requestParams['email'] : '';
//
//        if($name && $email){
//            if($user = Users::update($db, $userId, $name, $email)){
//                return $this->response('Data updated.', 200);
//            }
//        }
//        return $this->response("Update error", 400);
    }

    /**
     * Метод DELETE
     * Удаление отдельной записи (по ее id)
     * http://ДОМЕН/users/1
     * @return string
     */
    public function deleteAction() {
        
        // проверяем введенные параметры
        $validate_result = $this->validateParams($this->method, $this->requestParams);        
        
        print 'deleteAction'; die();
//        $parse_url = parse_url($this->requestUri[0]);
//        $userId = $parse_url['path'] ? $parse_url['path'] : null;
//
//        $db = (new Db())->getConnect();
//
//        if(!$userId || !Users::getById($db, $userId)){
//            return $this->response("User with id=$userId not found", 404);
//        }
//        if(Users::deleteById($db, $userId)){
//            return $this->response('Data deleted.', 200);
//        }
//        return $this->response("Delete error", 500);
    }
    
    /**
     * Метод PUT
     * Обновление отдельной записи (по ее id)
     * http://ДОМЕН/users/1 + параметры запроса name, email
     * @return string
     */
    public function updateFieldAction() {       
        
        // проверяем введенные параметры
        $validate_result = $this->validateParams($this->method, $this->requestParams);
        
        
        
        
        print 'updateFieldAction'; die();
//        $parse_url = parse_url($this->requestUri[0]);
//        $userId = $parse_url['path'] ? $parse_url['path'] :  null;
//
//        $db = (new Db())->getConnect();
//
//        if(!$userId || !Users::getById($db, $userId)){
//            return $this->response("User with id=$userId not found", 404);
//        }
//
//        $name = $this->requestParams['name'] ? $this->requestParams['name'] : '';
//        $email = $this->requestParams['email'] ? $this->requestParams['email'] : '';
//
//        if($name && $email){
//            if($user = Users::update($db, $userId, $name, $email)){
//                return $this->response('Data updated.', 200);
//            }
//        }
//        return $this->response("Update error", 400);
    }    

}