<?php

abstract class lxApi {
    public $apiName = ''; //api name (campaigns, users...)
    public $apiVersion = ''; //api version 
    public $userID = 0; //user ID
    public $cmpID = ''; //cmp_id

    protected $method = ''; //GET|POST|PUT|DELETE|PATCH

    public $requestUri = [];
    public $requestParams = [];

    protected $action = ''; //Название метода для выполнения


    public function __construct() {        
        
        //  пример запроса с авторизацией
        // curl -u username:password -i -H 'Accept:application/json' http://clients.magicchecker.com/user/1/api/v1/campaigns - получить список кампаний
        // curl -u username:password -i -H 'Accept:application/json' http://clients.magicchecker.com/user/1/api/v1/campaigns/03a8962d93e2e9ec7e84beb42a0d4af7 - получить кампанию по id
        // curl -u username:password -i -H 'Accept:application/json' http://clients.magicchecker.com/user/1/api/v1/campaigns -X PUT - создать кампанию
        // curl -u username:password -i -H 'Accept:application/json' http://clients.magicchecker.com/user/1/api/v1/campaigns/03a8962d93e2e9ec7e84beb42a0d4af7 -X PATCH - изменить поля
        // curl -u username:password -i -H 'Accept:application/json' http://clients.magicchecker.com/user/1/api/v1/campaigns/03a8962d93e2e9ec7e84beb42a0d4af7 -X DELETE - удалить кампанию
        
        
//        print_r($_SERVER); die();
        
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");

        //Массив GET параметров разделенных слешем
        $api_url = explode('/', trim($_SERVER['REQUEST_URI'],'/'));
        
        $this->userID = (int)$api_url[1];
        $this->apiVersion = (int)$api_url[3];
        
        if ($api_url[5] && preg_match('/^[a-z0-9]{32}$/', $api_url[5])) {
            $this->cmpID = $api_url[5];
//            unset($api_url[5]);
        }
        
        if (!$this->validUser()) {
            return $this->response("User not found or not active.", 404);
        }
        
        unset($api_url[0]);
        unset($api_url[1]);
        
        $api_url = array_values($api_url);
        $this->requestUri = $api_url;        
        
        //Определение метода запроса
        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PATCH') {
                $this->method = 'PATCH';
            }
        }
        
        if (!in_array($this->method, ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])) {
            return $this->response("Bad Request", 400);
        }
        
        // Открываем на чтение поток ввода
        if (in_array($this->method, ['POST', 'PUT', 'PATCH', 'DELETE'])) {
            $f = fopen('php://input', 'r');
            $data = stream_get_contents($f);
            fclose($f);
            
            if ($data) {    
                $this->requestParams = json_decode($data, true);
            }
            else {
                return $this->response("Bad Request", 400);
            }
        }    
        else {
            $this->requestParams = $_REQUEST;
        }
            
    }

    private function validUser() {
        $validUser = false;
        
        
        if (is_numeric($this->userID)) {

//            if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
                $userExist = (int)db_select('users', 'u')
                                ->fields('u', ['uid'])
                                ->condition('uid', $this->userID)
                                ->condition('status', 1)
                                ->execute()->fetchField();

                if ($userExist && ($userExist > 0)) {
                    $is_active = db_select('field_data_field_active_status', 's')
                                    ->fields('s', ['field_active_status_value'])
                                    ->condition('entity_id', $this->userID)
                                    ->execute()->fetchField();

                    if (in_array($is_active, [100, 200])) {
                        $validUser = true;
                    }
                }
//            }    
        }
        
        return $validUser;
    }

    public function run() {
        
        //Первые 2 элемента массива URI должны быть "api" и название таблицы
        if(array_shift($this->requestUri) !== 'api' || array_shift($this->requestUri) !== 'v1' || array_shift($this->requestUri) !== $this->apiName){ // как-то обработать версию API
            return $this->response("API not found", 404);
        }
        
        
        //Определение действия для обработки
        $this->action = $this->getAction();

        
        //Если метод(действие) определен в дочернем классе API
        if (method_exists($this, $this->action)) {
            return $this->{$this->action}();
        } else {
            return $this->response("Invalid Method", 405);
        }
    }

    protected function response($data, $status = 500) {
        header("HTTP/1.1 " . $status . " " . $this->requestStatus($status));
        print json_encode($data);
        die();
    }

    private function requestStatus($code) {
        $status = array(
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status[$code])?$status[$code]:$status[500];
    }

    protected function getAction()
    {
        $method = $this->method;
        switch ($method) {
            case 'GET':
                if($this->cmpID){
                    return 'viewAction';
                } else {
                    return 'indexAction';
                }
                break;
            case 'POST':
                return 'createAction';
                break;
            case 'PUT':
                return 'updateAction';
                break;
            case 'DELETE':
                return 'deleteAction';
            case 'PATCH':
                return 'updateFieldAction';
                break;
            default:
                return null;
        }
    }

    abstract protected function indexAction();
    abstract protected function viewAction();
    abstract protected function createAction();
    abstract protected function updateAction();
    abstract protected function deleteAction();
    abstract protected function updateFieldAction();
}