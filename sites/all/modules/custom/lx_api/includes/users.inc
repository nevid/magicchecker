<?php

function lx_api_error_responce($error_code, $header_code, $message) {
    
    if ($code == 1) {
        
        $responce = ["error" => ["message" => $message,
                                 "code" => $error_code,
//                                 "fbtrace_id" => "FOXX2AhLh80"
                    ]];
        
        header($header_code);

    }
    
    print json_encode($responce);
    die();
    
}

function lx_api_entry_point() {
    
    $api_version = arg(3);
    
    if ($api_version && in_array($api_version, ['v1'])) {
        if (arg(4) == 'campaigns') { 
            require_once(drupal_get_path('module', 'lx_api')) .'/includes/class/' .$api_version .'/CampaignsApi.php';

            try {
                $api = new lxCampaignsApi();
                print $api->run();

            } catch (Exception $e) {
                // ??
            }        
        }    
        else {
            lx_api_error_responce(1, "HTTP/1.0 404 Not Found", 'API not found.');
        }
    }
    else {
        lx_api_error_responce(1, "HTTP/1.0 404 Not Found", 'API not found.');
    }
}
