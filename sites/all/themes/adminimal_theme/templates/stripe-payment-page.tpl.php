<?php
global $payment_tarif;

$price = 0;
if ($payment_tarif->type == 'tarif') {
    $price = $payment_tarif->field_total_for_period['und'][0]['value'];
}
else {
    $price = $payment_tarif->field_invoice_total['und'][0]['value'];
}

$ttl = '';
if ($payment_tarif->type == 'tarif') {
    if ($payment_tarif->title == 'Month') {
        $ttl = 'Monthly';
    }
    else if ($payment_tarif->title == '6 Month') {
        $ttl = '6 Months';
    }
    else {
        $ttl = $payment_tarif->title;
    }
    
    $ttl .= ' Subscription'; 
}
else {
    $ttl = $payment_tarif->title;
}

?>
<script type="text/javascript"> //<![CDATA[ 
var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
//]]>
</script>
<div id="main" class="checkout">
    <header>
      <a class="shop" href="/user/<?php print arg(1) ?>/payments">Return to Payments page</a>
    </header>
    <div id="checkout">
        <?php if ($messages): ?>
                        <div id="console" class="clearfix"><?php print $messages; ?></div>
                <?php endif; ?>  
                        
        <?php print render($page['content']); ?>                        
    </div>
  </div>
  <div id="summary">
    <header>
      <?php if (arg(3) != 'update-source') : ?>
        <h1>Order Summary</h1>
      <?php endif; ?>  
    </header>
    <div id="order-total">
      <?php if (arg(3) != 'update-source') { ?>  
      <div class="line-item subtotal">
        <div class="label">
            <strong><?php print $ttl ?></strong><?php if ($payment_tarif->type == 'tarif') : ?><br />
            <ul>
                <li>Unlimited access</li>
                <li>Spy services & bots protection</li>
                <li>Competitor’s detecting</li>
                <li>24 hours support</li>
            </ul>
            <?php endif; ?>
      </div>
          <p class="price" data-subtotal>$<?php print number_format($price) ?></p>
      </div>
      <div class="line-item total">
        <p class="label">Total</p>
        <p class="price" data-total>$<?php print number_format($price) ?></p>
      </div>
      <?php } else { ?>  
        <div class="line-item">
          <p class="label" style="text-align: center">You card won't be charged now.<br />Just update your payment information.</p>
        </div>        
      <?php } ?>  
        
        
      <div class="line-item demo">
        <div id="demo">
          <p class="note">We do not store your payment information on our site.<br />
All payments powered by <a target="_blank" href="https://stripe.com">Stripe</a>.</p>
        </div>
      </div>      
        <div class="line-item" style="margin-top: 20px">
            <a target="_blank" href="https://stripe.com" style="display: inline-block;padding-top: 14px;margin-left: 47px;"><img height="40px" src="/files/powered_by_stripe.png" /></a>&nbsp;&nbsp;&nbsp;
            <span><script language="JavaScript" type="text/javascript">TrustLogo("https://magicchecker.com/files/comodo.png", "CL1", "none");</script><a  href="https://www.instantssl.com/" id="comodoTL"></a></span>
        </div>  
    </div>
  </div>
 