<?php
    if (in_array(arg(3), ['stripe-subscription', 'update-source'])) {
        require_once '/home/user173magic/data/www/magicchecker.com/sites/all/themes/adminimal_theme/templates/stripe-payment-page.tpl.php';
    }
    else {
?>
<div id="page">

	<div id="content" class="clearfix">

<div id="topper" class="clearfix">
    <div class="user-small-menu"><ul><li class="user-mail"><i class="fa fa-user" aria-hidden="true"></i><?php print lx_user_name_from_mail($user->mail) ?></li><li class="ulink" title="Settings"><a href="/user/<?php print $user->uid ?>/edit"><i class="fa fa-cogs" aria-hidden="true"></i></a></li><li class="ulink"><a href="/user/logout" title="Log out"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li></ul></div>
    <div class="small-logo"><a href="/"><img src="/files/logo-small.png" /></a></div>
</div>    
        
<div id="branding" class="clearfix">
	<?php print render($title_prefix); ?>

	<?php if ($title): ?>
		<h1 class="page-title"><?php print $title; ?></h1>
	<?php endif; ?>

	<?php print render($title_suffix); ?>
        
    <?php print render($page['branding']); ?>        

</div>

<div id="navigation">

  <?php if ($primary_local_tasks): ?>
    <?php print render($primary_local_tasks); ?>
  <?php endif; ?>

  <?php if ($secondary_local_tasks): ?>
    <div class="tabs-secondary clearfix"><ul class="tabs secondary"><?php print render($secondary_local_tasks); ?></ul></div>
  <?php endif; ?>

</div>        

        
		<div class="element-invisible"><a id="main-content"></a></div>
        <div class="mobile-header"></div>

	<?php if ($messages): ?>
		<div id="console" class="clearfix"><?php print $messages; ?></div>
	<?php endif; ?>

	<?php if ($page['help']): ?>
		<div id="help">
			<?php print render($page['help']); ?>
		</div>
	<?php endif; ?>
        
    <?php print $breadcrumb; ?>    

	<?php if (isset($page['content_before'])): ?>
		<div id="content-before">
			<?php print render($page['content_before']); ?>
		</div>
	<?php endif; ?>

	<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>

  <div id="content-wrapper">

    <?php if (isset($page['sidebar_left'])): ?>
      <div id="sidebar-left">
        <?php print render($page['sidebar_left']); ?>
      </div>
    <?php endif; ?>

    <div id="main-content">
	    <?php print render($page['content']); ?>
	  </div>

    <?php if (isset($page['sidebar_right'])): ?>
      <div id="sidebar-right">
        <?php print render($page['sidebar_right']); ?>
      </div>
    <?php endif; ?>
	
	</div>

	<?php if (isset($page['content_after'])): ?>
		<div id="content-after">
			<?php print render($page['content_after']); ?>
		</div>
	<?php endif; ?>

	</div>

	<div id="footer">
		<span>&copy; 2016 - <?php print date('Y'); ?> MagicChecker.com</span>
	</div>

</div>
<?php if (user_is_logged_in() && (arg(0) != 'admin')) : ?>
<?php
$user_hash_hmac = hash_hmac(
  'sha256', // hash function
  $user->mail, // user's id
  'PNVrp-nKSqVMX90_GArjpocgqXaJE1A_dPh6lF8-' // secret key (keep safe!)
);
?>      
<script>
  window.intercomSettings = {
    app_id: "e9514oon",
    email: "<?php print $user->mail ?>",
    created_at: <?php print $user->created ?>,
    user_hash: "<?php print $user_hash_hmac ?>"
  };
  </script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/e9514oon';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

<?php  endif; }