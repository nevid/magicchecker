<div class="new-look blue-header">
    <table <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
       <?php if (!empty($title) || !empty($caption)) : ?>
         <caption><?php print $caption . $title; ?></caption>
      <?php endif; ?>
      <?php if (!empty($header)) : ?>
        <thead>
          <tr>
            <?php foreach ($header as $field => $label): ?>
              <?php if ($field == 'nid') continue; ?>
              <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?> scope="col">
                <?php print $label; ?>
              </th>
            <?php endforeach; ?>
          </tr>
        </thead>
      <?php endif; ?>
      <tbody>
        <?php foreach ($rows as $row_count => $row): ?>
          <tr <?php if ($row_classes[$row_count]) { print 'class="' . implode(' ', $row_classes[$row_count]) .'"';  } ?>>
            <?php foreach ($row as $field => $content): ?>
              <?php if ($field == 'nid') continue; ?>
              <?php if ($field == 'field_payment_system_order_numbe') { 
              
                  if (!is_numeric(trim(strip_tags($content)))) {
                      print '<td style="width: 30px;text-align: center;font-size: 14px;padding-left: 5px !important;"><a target="_blank" href="/user/' .arg(1) .'/payments/invoice/' .trim(strip_tags($row['nid'])) .'"><i class="fa fa-file" aria-hidden="true" style="margin-left: 8px"></i></a></td>';
                  }
                  else {
                      print '<td style="width: 30px;">&nbsp;</td>';
                  }
              }
              else {?>
                <td <?php if ($field_classes[$field][$row_count]) { print 'class="'. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
                  <?php print $content; ?>
                </td>
              <?php } ?>  
            <?php endforeach; ?>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>    
</div>