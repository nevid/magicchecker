<div id="page">

    <?php
        print '<div class="front-logo"><a href="https://magicchecker.com"><img src="/files/logo-front.png" /></a></div>';
    ?>

	<?php if ($messages): ?>
		<div id="console" class="clearfix"><?php print $messages; ?></div>
	<?php endif; ?>

<?php
    $block = block_load('user', 'login');
    $block = _block_render_blocks(array($block));
    $block_build = _block_get_renderable_array($block);
    echo drupal_render($block_build);
    $block = block_load('block', '4');
    $block = _block_render_blocks(array($block));
    $block_build = _block_get_renderable_array($block);
    echo drupal_render($block_build);
?>


</div>
