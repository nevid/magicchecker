<div id="page">
    
    <?php
        print '<div class="front-logo"><a href="https://magicchecker.com"><img src="/files/logo-front.png" /></a></div>';
    ?>    
    
	<?php if ($messages): ?>
		<div id="console" class="clearfix"><?php print $messages; ?></div>
	<?php endif; ?>
        
    <div class="block-user req-pass">   
        <h2>Request new password</h2>
        <?php print render($page['content']); ?>
    </div>
</div>
