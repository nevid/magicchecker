#!/usr/bin/env php
<?php

    // Define settings.
    $cmd = 'index.php';
    define('DRUPAL_ROOT', getcwd());
    $_SERVER['HTTP_HOST']       = 'default';
    $_SERVER['PHP_SELF']        = '/index.php';
    $_SERVER['REMOTE_ADDR']     = '127.0.0.1';
    $_SERVER['SERVER_SOFTWARE'] = NULL;
    $_SERVER['REQUEST_METHOD']  = 'GET';
    $_SERVER['QUERY_STRING']    = '';
    $_SERVER['PHP_SELF']        = $_SERVER['REQUEST_URI'] = '/';
    $_SERVER['HTTP_USER_AGENT'] = 'console';
    $modules_to_enable          = array('user');

    // Bootstrap Drupal.

    include_once './includes/bootstrap.inc';
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    global $user;

    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    ini_set('log_errors', 'On');

 

    include_once './sites/all/modules/custom/lx_domains_cmp/includes/cmp_queue.inc';
  
    for ($i = 0; $i < 3; $i ++) {
        lx_domains_cmp_aud_queue_status(0, true);
        sleep(15);
    }