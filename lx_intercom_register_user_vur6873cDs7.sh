#!/usr/bin/env php
<?php

    // Define settings.
    $cmd = 'index.php';
    define('DRUPAL_ROOT', getcwd());
    $_SERVER['HTTP_HOST']       = 'default';
    $_SERVER['PHP_SELF']        = '/index.php';
    $_SERVER['REMOTE_ADDR']     = '127.0.0.1';
    $_SERVER['SERVER_SOFTWARE'] = NULL;
    $_SERVER['REQUEST_METHOD']  = 'GET';
    $_SERVER['QUERY_STRING']    = '';
    $_SERVER['PHP_SELF']        = $_SERVER['REQUEST_URI'] = '/';
    $_SERVER['HTTP_USER_AGENT'] = 'console';
    $modules_to_enable          = array('user');

    // Bootstrap Drupal.

    include_once './includes/bootstrap.inc';
    
    
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    global $user;

    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    ini_set('log_errors', 'On');

 
    include_once './sites/all/modules/custom/lx_domains_cmp/lx_domains_cmp.module';
    include_once './sites/all/modules/custom/lx_intercom/includes/users.inc';
  
    $res = db_select('users_registration', 'r')
                ->fields('r')
                ->orderBy('id', 'ASC')
                ->range(0, 10)
                ->execute()->fetchAll();
    
    if (sizeof($res)) {
        
        // сразу удаляем, чтобы новый процесс не оборабатывал то же самое при большом количестве регистраций.
        $to_delete = [];
        foreach ($res as $r) {
            $to_delete[] = $r->id;
        }    
        
        db_delete('users_registration')
            ->condition('id', $to_delete, 'IN')
            ->execute();

        
        // регим юзера в интеркме, отправляем письма нам и юзеру ссылку на подтверждение мыла
        foreach ($res as $r) {
            _lx_intercom_create_user($r);
        }
        
    }