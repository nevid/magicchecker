<?php

  // Define settings.
 $cmd = 'index.php';
  define('DRUPAL_ROOT', getcwd());
  $_SERVER['HTTP_HOST']       = 'default';
  $_SERVER['PHP_SELF']        = '/index.php';
  $_SERVER['REMOTE_ADDR']     = '127.0.0.1';
  $_SERVER['SERVER_SOFTWARE'] = NULL;
  $_SERVER['REQUEST_METHOD']  = 'GET';
  $_SERVER['QUERY_STRING']    = '';
  $_SERVER['PHP_SELF']        = $_SERVER['REQUEST_URI'] = '/';
  $_SERVER['HTTP_USER_AGENT'] = 'console';
  $modules_to_enable          = array('user');

  // Bootstrap Drupal.

  include_once './includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  global $user;

  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);
  ini_set('log_errors', 'On');

//  define('LX_LOG_EXPORT_PORTION_SIZE', 10000); //rows

  include_once './sites/all/modules/custom/lx_domains_cmp/lx_log_export/lx_log_export.module';
  include_once './sites/all/modules/custom/lx_domains_cmp/lx_domains_cmp.module';
  include_once './sites/all/modules/custom/lx_domains_cmp/includes/user_clickhouse_log.inc';


  //Достаём текущие заявки
  $requests = lx_log_export_get_requests();
//  print_R('Сгенерированы отчёты:<br/>');
  foreach ($requests as $request) {
    $data = array();
    $request['query']['pubs_id'] = trim($request['query']['pubs_id']);
    //Делаем выборку данных
    if ($request['rep_type'] == 1) {
      $header = lx_log_export_header_type1();
      $data = lx_log_export_get_request_data($request);
      array_unshift($data, $header);
    } else if($request['rep_type'] == 2) {
      $reasons = __get_reasons_translate() + array(103 => 'Money Page to All');
      $header = lx_log_export_header_type2($reasons, $request['query']['pubs_id']);
      $data = lx_log_export_get_data_for_rep_type2($request, $reasons);
      //Создаём хеадер
      array_unshift($data, $header);
    }

    $is_empty = true;
    if (count($data) > 1) {
      $is_empty = false;
      
        //log_айдикампании_2018-10-09_12-30.csv
        $filename = lx_log_export_generate_file_name($request['cid'], $request['rep_type']);
        //Сохраняем в csv
        lx_log_export_save_array_to_csv ($data, $filename);
        //сохраняем название файла в базе
        lx_log_export_save_filename_and_status_in_db($request['lid'], $request['filename']);      
    }

    //Отправляем на почту
    lx_log_export_send_email(drupal_realpath('public://')  . '/lx_log_export/' . $filename . '.gz', $filename . '.gz', $is_empty, $request['uid']);

    //Обновляем статус в БД
    lx_log_export_save_filename_and_status_in_db($request['lid'], '', 3);

//    print_R('<a href="/files/lx_log_export/' . $filename . '.gz">' . $filename . '.gz</a><br/>');
  }


  function lx_log_export_get_data_for_rep_type2($request, $reasons) {
    $data = array();
    $log_records = lx_log_export_get_request_data($request);

    foreach ($log_records as $key => $row) {
      //Найден по ключу
      if (preg_match("/" . $request['query']['pubs_id'] . "=/i", $row[9])) {
        $parts = explode($request['query']['pubs_id'].'=', $row[9]);
        $parts1 = explode('&', $parts[1]);

        $pubs_value = $parts1[0];
        if (!isset($data[$pubs_value])) {
          $pubs_row = array(
              $request['query']['pubs_id'] => $pubs_value,
            'total_clicks' => 0,
            'total_blocks' => 0,
          );
        } else {
          $pubs_row =  $data[$pubs_value];
        }

        $pubs_row['total_clicks'] += 1;
        if ($row[4] && $row[4] != 'Money Page to All'){
          $pubs_row['total_blocks'] += 1;
        }

        lx_log_export_set_block_reason($pubs_row, $reasons, $row[4]);
        $data[$pubs_value] = $pubs_row;
      }
    }

    //расчёт процентов
    //Удаляем лишнее
    unset($reasons[0]);
    unset($reasons[1]);
    unset($reasons[101]);
    unset($reasons[102]);
    unset($reasons[103]);

    foreach ($data as $key => $row) {
      $data[$key]['total_blocks'] = lx_log_export_get_percent($data[$key]['total_blocks'], $data[$key]['total_clicks']);
      foreach ($reasons as $k => $reason) {
        $data[$key][$reason] =  lx_log_export_get_percent($data[$key][$reason], $data[$key]['total_clicks']);
      }
    }
    uasort($data,'lx_log_export_sort');

    return $data;
  }

  function lx_log_export_header_type2($reasons, $pubs_id) {
    $header = array($pubs_id, 'Total Clicks', 'Safe Page Clicks');
    foreach ($reasons as $rid => $reason) {
      if (!in_array($rid, array(0, 1, 101, 102, 103))) {
        $header[] = $reason;
      }
    }
    return $header;
  }

  function lx_log_export_header_type1() {
    return array('Date', 'User IP', 'Country', 'ISP/Org', 'Blocked', 'OS', 'Browser','User-Agent', 'Referer', 'URL');
  }

  function lx_log_export_sort($a, $b) {
    if($a['total_clicks'] == $b['total_clicks']) {
        return 0;
    }
    return ($a['total_clicks'] > $b['total_clicks']) ? -1 : 1;
  }

  function lx_log_export_get_percent($val, $total) {
    return $val . '(' . round($val * 100/$total) . '%)';
  }

  function lx_log_export_get_requests($status = 1) {
    $db = new PDO("mysql:host=" . LX_MYSQLI_HOST . ";dbname=" . LX_MYSQLI_DB, LX_MYSQLI_USER, LX_MYSQLI_PASS);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $db_results = $db->query("SELECT * FROM `" . LX_MYSQLI_DB_PREFIX . "log_export_queue` WHERE `status` = 1");
    $requests = array();
    foreach($db_results as $db_result) {
      $req = $db_result;
      $db->query("UPDATE `" . LX_MYSQLI_DB_PREFIX . "log_export_queue` SET `status` = 2 WHERE lid=" .$req['lid']);
      $req['query'] = unserialize($req['query']);
      $req['user'] = user_load($req['uid']);
      $requests[] = $req;
      lx_log_export_get_request_data($req);
    }

    return $requests;
  }

  function lx_log_export_get_request_data($request) {
    $date_from = null;
    $date_to = null;

    $ch_cur_show_date = '2018-09-27';
    $ch_cur_date_range = [];

    $period_type = isset($request['query']['period_type']) ? (int)$request['query']['period_type'] : 8;
    if (!$period_type) {
      $period_type = 8;
    }

    if ($period_type == 100 && isset($request['query']['dates']) && preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4} \- [0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $request['query']['dates'])) {
      $dates = explode(' - ', $request['query']['dates']);
      $date_from = implode('-',array_reverse(explode('.', $dates[0])));
      $date_to = implode('-',array_reverse(explode('.', $dates[1])));
    } else {
      if ($period_type == 1) {
        $date_from =  date("Y-m-d", strtotime("this Monday"));
        //воскресенье прошлой недели
        $date_to = date('Y-m-d');
      } else if ($period_type == 6) {
        //понедельник прошлой недели
        $date_from =  date("Y-m-d", strtotime("last Monday"));
        //воскресенье прошлой недели
        $date_to =  date("Y-m-d", strtotime("last Sunday"));
      } else if ($period_type == 2) {
        $date_from = date('Y-m-d', time() - 86400*6);
        $date_to = date('Y-m-d');
      } else if ($period_type == 3) {
        $date_from = date('Y-m-01');
        $date_to = date('Y-m-d');
      } else if ($period_type == 4) {
        $date_from = date('Y-m-d', time() - 86400*30);
        $date_to = date('Y-m-d');
      } else if ($period_type == 10) {
        $date_from = date('Y-m-d', $request['user']->created);
        $date_to = date('Y-m-d');
      } else if ($period_type == 5) {
        $date_from =  date("Y-m-d", strtotime("first day of last month"));
        //воскресенье прошлой недели
        $date_to =  date("Y-m-d", strtotime("last day of last month"));
      } else if ($period_type == 7) {
        $date_from =  date("Y-m-d", time() - 86400);
        //воскресенье прошлой недели
        $date_to =  date("Y-m-d", time() - 86400);
      } else if ($period_type == 8) {
        $date_from =  date("Y-m-d", time());
        //воскресенье прошлой недели
        $date_to =  date("Y-m-d", time());
      }
    }

    if ($date_from && $date_to) {
      if ($date_from != $date_to) {
        $date_from_ts = strtotime($date_from);
        $date_to_ts = strtotime($date_to);
        while($date_from_ts <= $date_to_ts) {
          $ch_cur_date_range[] = date('Y-m-d', $date_from_ts);
          $date_from_ts += 86400;
          // ограничиваем поиск семью днями
          if (sizeof($ch_cur_date_range) == 7) {
            break;
          }
        }
      } else {
        $ch_cur_date_range[] = $date_from;
      }
    }

    if (!in_array($ch_cur_show_date, $ch_cur_date_range)) {
      $ch_cur_show_date = $ch_cur_date_range[sizeof($ch_cur_date_range) -1];
    }

    $extra = array('country' => 'country',
      'isp' => 'isp_org',
      'os' => 'ua_os',
      'browser' => 'ua_browser',
      'ua' => 'user_agent',
      'referer' => 'user_referer',
      'url' => 'user_page_url'
    );

    $cmp = db_select('domain_compaigns', 'c')
      ->fields('c')
      ->condition('uid', $request['user']->uid)
      ->condition('cid', $request['cid'])
      ->execute()->fetchObject();

    $is_admin = user_access('administer users');

    try {
      $db = new PDO("mysql:host=" . LX_MYSQLI_STAT_HOST . ";dbname=" . LX_MYSQLI_STAT_DB, LX_MYSQLI_STAT_USER, LX_MYSQLI_STAT_PASS);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $reasons = __get_reasons_translate() + array(103 => 'Money Page to All');
      if (!$is_admin) {
        $reasons[1] = $reasons[4];
      }

      $where_params = array();
      $where = array();

      if (isset($request['query']['blockedby']) && is_numeric($request['query']['blockedby']) && ($request['query']['blockedby'] > -1)) {
        if ($request['query']['blockedby'] == 1000) {
          $where[] = 'block_reason>{block_reason} AND block_reason<{block_reason2}';
          $where_params['block_reason'] = 0;
          $where_params['block_reason2'] = 103;
        } else {
          if (!$is_admin && ($request['query']['blockedby'] == 4)) {
            $where[] = '((block_reason={block_reason1}) OR (block_reason={block_reason2}) )';
            $where_params['block_reason1'] = 1;
            $where_params['block_reason2'] = 4;
          } else {
            $where[] = 'block_reason={block_reason}';
            $where_params['block_reason'] = (int)$request['query']['blockedby'];
          }
        }
      }

      if (isset($request['query']['userip']) && trim($request['query']['userip'])) {
        if (is_valid_ip(trim($request['query']['userip'])) != 'unknown') {
          $where[] = 'real_ip={real_ip}';
          $where_params['real_ip'] = "'".trim($request['query']['userip']) ."'";
        } else {
          //drupal_set_message('Wrong IP format',' error');
          //drupal_goto('user/' .$acc->uid .'/campaigns/' .$cid .'/stastistics/log/' .arg(6));
        }
      }

      if (isset($request['query']['selfield']) && $request['query']['selfield'] && in_array($request['query']['selfield'], array_keys($extra)) && isset($request['query']['selfieldval']) && trim($request['query']['selfieldval'])) {
        if ( !in_array($request['query']['selfield'], ['referer', 'url'])) {
          $where[] =  $extra[$request['query']['selfield']] ."='{" .$extra[$request['query']['selfield']] ."}'";
          $where_params[$extra[$request['query']['selfield']]] = check_plain($request['query']['selfieldval']);
        } else {
          $where[] =  "positionCaseInsensitiveUTF8(". $extra[$request['query']['selfield']].", '{" .$extra[$request['query']['selfield']] ."}') > 0" ;
          $where_params[$extra[$request['query']['selfield']]] = str_replace('&amp;','&',$request['query']['selfieldval']);
        }
      }

      $sort_order = 'DESC';

      if (isset($request['query']['last'])) {
        $where[] = 'id<{id}';
        $where_params['id'] = (int)$request['query']['last'];
      } else if (isset($request['query']['first'])) {
        $where[] = 'id>{id}';
        $where_params['id'] = (int)$request['query']['first'];
        $sort_order = 'ASC';
      }

      if ($cmp->reset_stat) {
        $where[] = 'click_date>{click_date}';
        $where_params['click_date'] = "'".$cmp->reset_stat ."'";
      }

      $_query = 'SELECT id, click_date, real_ip, country, isp_org, block_reason, ua_os, ua_browser, user_agent, user_referer, user_page_url'
         . '       FROM clicklog 
                   WHERE id IN (
                     SELECT id FROM clicklog 
                     PREWHERE event_date = {event_date} AND cid={cid} ' .(sizeof($where) ? ' 
                     WHERE ' .implode(' AND ', $where) : '') .' 
                     ORDER BY id ' .$sort_order .' 
                     LIMIT {limit}
                   ) 
                     ORDER BY id DESC';
      $where_params['event_date'] = "'" .$ch_cur_show_date ."'";
      $where_params['cid'] = "'".$request['cid']."'";
      $where_params['limit'] = LX_LOG_EXPORT_MAX_ROWS_SIZE;

      $data = ch_user_query($_query, $where_params);

      $rows = array();
      $headers = array();
      $objects = array();

      if (sizeof($data)) {
        $objects = $data['result'];
        $cnt = sizeof($objects);

        if ($cnt < LX_LOG_EXPORT_MAX_ROWS_SIZE && (sizeof($ch_cur_date_range) > 1)) {
          $cur_date_key = array_search($ch_cur_show_date, $ch_cur_date_range);
          $cur_date_key --;
          while (isset($ch_cur_date_range[$cur_date_key]) && $cnt < LX_LOG_EXPORT_MAX_ROWS_SIZE) {
            $ch_cur_show_date = $ch_cur_date_range[$cur_date_key];
            $where_params['event_date'] = "'" .$ch_cur_show_date ."'";
            $where_params['limit'] = LX_LOG_EXPORT_MAX_ROWS_SIZE - $cnt;
            $data = ch_user_query($_query, $where_params);
            if (sizeof($data)) {
              $objects += $data['result'];
              $cnt += sizeof($data['result']);
            }

            $cur_date_key --;
          }
        }

        foreach ($objects as $r) {
          if (!$first) {
            $first = $r['id'];
          }

          foreach ($r as $kk => $rr) {
            if ($rr == 'N_') {
              $r[$kk] = '';
            }
          }

          $last = $r['id'];
          $rows[] = array($r['click_date'],
            $r['real_ip'],
            $r['country'],
            $r['isp_org'],
            $reasons[$r['block_reason']],
            $r['ua_os'],
            $r['ua_browser'],
            htmlspecialchars($r['user_agent'], ENT_QUOTES),
            htmlspecialchars($r['user_referer'], ENT_QUOTES),
            htmlspecialchars($r['user_page_url'], ENT_QUOTES),
          );
        }
        unset($objects);
      }

      $db = null;

      return $rows;

    } catch (PDOException $e) {
      lx_magic_log2('Clickhouse user log error', 'Clickhouse user log error', $e->getMessage(), 200, true);
      print 'error';
    }
  }

